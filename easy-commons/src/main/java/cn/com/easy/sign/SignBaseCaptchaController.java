package cn.com.easy.sign;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;

import cn.com.easy.dto.MessageDTO;
import cn.com.easy.exception.BusinessException;
import cn.com.easy.utils.ResponseOutputUtils;

/**
 * 签名验证码基础控制器
 * 
 * @author nibili 2017年8月31日
 * 
 */
public abstract class SignBaseCaptchaController {

	/**
	 * 获取验证码
	 * 
	 * @param request
	 * @param response
	 * @param signBaseParamDto
	 *            只需要 signCaptchaId 即可
	 * @author nibili 2017年8月31日
	 */
	@RequestMapping("/sign/captcha.json")
	public void jdbc(HttpServletRequest request, HttpServletResponse response, SignBaseParamDto signBaseParamDto) {
		try {
			String captcha = SignCaptchaUtil.getCaptcha(request, signBaseParamDto);
			ResponseOutputUtils.renderJson(response, MessageDTO.newInstance("", true, captcha));
		} catch (BusinessException ex) {
			ResponseOutputUtils.renderJson(response, MessageDTO.newInstance("", false, ex.getLocalizedMessage()));
		} catch (Exception e) {
			ResponseOutputUtils.renderJson(response, MessageDTO.newInstance("", false, "获取验证码异常，稍后再试"));
		}
	}

}
