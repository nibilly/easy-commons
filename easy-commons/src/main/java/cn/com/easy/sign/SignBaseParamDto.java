package cn.com.easy.sign;

import org.apache.commons.lang3.StringUtils;

import cn.com.easy.exception.BusinessException;

/**
 * 签名基础参数
 * 
 * @author nibili 2017年8月31日
 * 
 */
public class SignBaseParamDto {

	/** 与 验证码对应的 id，用于查找在session中的验证码，用于防并发，造成的验证码失效的问题，客户端可以设置一个随机数， */
	private String signCaptchaId;
	/** 验证码 */
	private String captcha;

	/** 用户id */
	private String appId;
	/** 时间戳 格式：2014-07-24 03:07:50 */
	private String timeStamp;
	/** 签名串 */
	private String sign;

	/**
	 * 验证字段
	 * 
	 * @throws BusinessException
	 * @author nibili 2017年9月26日
	 */
	public void validate() throws BusinessException {
		if (StringUtils.isBlank(this.appId) == true) {
			throw new BusinessException("appId不能为空");
		}
		if (StringUtils.isBlank(this.timeStamp) == true) {
			throw new BusinessException("时间戳不能为空");
		}
		if (StringUtils.isBlank(this.sign) == true) {
			throw new BusinessException("签名不能为空不能为空");
		}
	}

	/**
	 * get 与验证码对应的id，用于查找在session中的验证码，用于防并发，造成的验证码失效的问题，客户端可以设置一个随机数，
	 * 
	 * @return
	 * @author nibili 2017年8月31日
	 */
	public String getSignCaptchaId() {
		return signCaptchaId;
	}

	/**
	 * set 与验证码对应的id，用于查找在session中的验证码，用于防并发，造成的验证码失效的问题，客户端可以设置一个随机数，
	 * 
	 * @param signCaptchaId
	 * @author nibili 2017年8月31日
	 */
	public void setSignCaptchaId(String signCaptchaId) {
		this.signCaptchaId = signCaptchaId;
	}

	/**
	 * get 验证码
	 * 
	 * @return
	 * @author nibili 2017年8月31日
	 */
	public String getCaptcha() {
		return captcha;
	}

	/**
	 * set 验证码
	 * 
	 * @param captcha
	 * @author nibili 2017年8月31日
	 */
	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	/**
	 * get 用户id
	 * 
	 * @return
	 * @author nibili 2017年8月31日
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * set 用户id
	 * 
	 * @param appId
	 * @author nibili 2017年8月31日
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * get 时间戳格式：2014-07-2403:07:50
	 * 
	 * @return
	 * @author nibili 2017年8月31日
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * set 时间戳格式：2014-07-2403:07:50
	 * 
	 * @param timeStamp
	 * @author nibili 2017年8月31日
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * get 签名串
	 * 
	 * @return
	 * @author nibili 2017年8月31日
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * set 签名串
	 * 
	 * @param sign
	 * @author nibili 2017年8月31日
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}

}
