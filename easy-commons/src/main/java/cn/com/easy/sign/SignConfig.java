package cn.com.easy.sign;

/**
 * 签名参数配置
 * 
 * @author nibili 2017年8月31日
 * 
 */
public class SignConfig {

	/** 私钥用于生成签名 */
	private String privateKey = "";
	/** 公钥用于验签 */
	private String publicKey = "";

	/**
	 * get 私钥用于生成签名
	 * 
	 * @return
	 * @author nibili 2017年8月30日
	 */
	public String getPrivateKey() {
		return privateKey;
	}

	/**
	 * set 私钥用于生成签名
	 * 
	 * @param privateKey
	 * @author nibili 2017年8月30日
	 */
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	/**
	 * get 公钥用于验签
	 * 
	 * @return
	 * @author nibili 2017年8月30日
	 */
	public String getPublicKey() {
		return publicKey;
	}

	/**
	 * set 公钥用于验签
	 * 
	 * @param publicKey
	 * @author nibili 2017年8月30日
	 */
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

}