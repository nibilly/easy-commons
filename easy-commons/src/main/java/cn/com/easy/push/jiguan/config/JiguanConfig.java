package cn.com.easy.push.jiguan.config;


/**
 * 极光配置
 * 
 * @author nibili 2017年6月6日
 * 
 */
public class JiguanConfig {

	/** 极光secret */
	private String secret;
	/** 极光key */
	private String appKey;

	/**
	 * get 极光secret
	 * 
	 * @return
	 * @author nibili 2017年6月6日
	 */
	public String getSecret() {
		return secret;
	}

	/**
	 * set 极光secret
	 * 
	 * @param 极光secret
	 * @author nibili 2017年6月6日
	 */
	public void setSecret(String secret) {
		this.secret = secret;
	}

	/**
	 * get 极光key
	 * 
	 * @return
	 * @author nibili 2017年6月6日
	 */
	public String getAppKey() {
		return appKey;
	}

	/**
	 * set 极光key
	 * 
	 * @param appKey
	 * @author nibili 2017年6月6日
	 */
	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

}
