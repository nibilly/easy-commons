package cn.com.easy.push.jiguan.dto;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import cn.com.easy.push.jiguan.enums.PlatformEnum;

/**
 * 极光消息dto<br/>
 * 包括发送目标的别名 和 目标的标签
 * 
 * @author nibili 2017年6月6日
 * 
 */
public class JiguanMessageDto {

	/**
	 * 是通知还是消息，默认是通知<br/>
	 * <br/>
	 * 通知会在 手机的通知栏（状态栏）上会显示的一条通知信息。 <br/>
	 * 消息所以不会被SDK展示到通知栏上。其内容完全由开发者自己定义。
	 * 详参：http://docs.jiguang.cn/jpush/guideline/intro/#_2
	 * */
	private boolean isNotification = true;

	/** 通知的标题 */
	private String title;
	/** 通知的文本内容 */
	private String alert;
	/** 额外参数 */
	private Map<String, String> extraParamMap = Maps.newHashMap();

	/**
	 * 要发送的平台，默认发送到所有平台<br>
	 * 可选 Platform.android,Platform.ios
	 * */
	private PlatformEnum platform = PlatformEnum.ALL;
	/**
	 * 别名<br/>
	 * null 此次调用不设置此值。（注：不是指的字符串"null"）<br/>
	 * "" （空字符串）表示取消之前的设置。<br/>
	 * 每次调用设置有效的别名，覆盖之前的设置。<br/>
	 * 有效的别名组成：字母（区分大小写）、数字、下划线、汉字、特殊字符(v2.1.6支持)@!#$&*+=.|。<br/>
	 * 限制：alias 命名长度限制为 40 字节。（判断长度需采用UTF-8编码）<br/>
	 * */
	private List<String> alias = Lists.newArrayList();
	/**
	 * 标签<br/>
	 * null 此次调用不设置此值。（注：不是指的字符串"null"）<br/>
	 * 空数组或列表表示取消之前的设置。<br/>
	 * 每次调用至少设置一个 tag，覆盖之前的设置，不是新增。<br/>
	 * 有效的标签组成：字母（区分大小写）、数字、下划线、汉字、特殊字符(v2.1.6支持)@!#$&*+=.|。<br/>
	 * 限制：每个 tag 命名长度限制为 40 字节，最多支持设置 1000 个 tag，但总长度不得超过7K字节。（判断长度需采用UTF-8编码）<br/>
	 * 
	 * */
	private List<String> tags = Lists.newArrayList();

	/** 此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义 */
	private boolean iosApns = true;
	/** 此字段是给开发者自己给推送编号，方便推送者分辨推送记录 */
	private int sendno = 1;
	/** (秒)此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天； */
	private long timeToLive = 86400;

	/**
	 * get 此字段是给开发者自己给推送编号，方便推送者分辨推送记录
	 * 
	 * @return
	 * @author nibili 2017年10月11日
	 */
	public int getSendno() {
		return sendno;
	}

	/**
	 * set 此字段是给开发者自己给推送编号，方便推送者分辨推送记录
	 * 
	 * @param sendno
	 * @author nibili 2017年10月11日
	 */
	public void setSendno(int sendno) {
		this.sendno = sendno;
	}

	/**
	 * get (毫秒)此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天；
	 * 
	 * @return
	 * @author nibili 2017年10月11日
	 */
	public long getTimeToLive() {
		return timeToLive;
	}

	/**
	 * set (毫秒)此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天；
	 * 
	 * @param timeToLive
	 * @author nibili 2017年10月11日
	 */
	public void setTimeToLive(long timeToLive) {
		this.timeToLive = timeToLive;
	}

	/**
	 * get 此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
	 * 
	 * @return
	 * @author nibili 2017年10月11日
	 */
	public boolean getIosApns() {
		return iosApns;
	}

	/**
	 * set 此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
	 * 
	 * @param iosApns
	 * @author nibili 2017年10月11日
	 */
	public void setIosApns(boolean iosApns) {
		this.iosApns = iosApns;
	}

	/**
	 * get 额外参当我
	 * 
	 * @return
	 * @author nibili 2017年6月7日
	 */
	public Map<String, String> getExtraParamMap() {
		return extraParamMap;
	}

	/**
	 * set 额外参当我
	 * 
	 * @param extraParamMap
	 * @author nibili 2017年6月7日
	 */
	public void setExtraParamMap(Map<String, String> extraParamMap) {
		this.extraParamMap = extraParamMap;
	}

	/**
	 * get 是通知还是消息，默认是通知<br>
	 * <br>
	 * 通知会在手机的通知栏（状态栏）上会显示的一条通知信息。<br>
	 * 消息所以不会被SDK展示到通知栏上。其内容完全由开发者自己定义。详参：http:docs.jiguang.
	 * cnjpushguidelineintro#_2
	 * 
	 * @return
	 * @author nibili 2017年6月6日
	 */
	public boolean getIsNotification() {
		return isNotification;
	}

	/**
	 * set 是通知还是消息，默认是通知<br>
	 * <br>
	 * 通知会在手机的通知栏（状态栏）上会显示的一条通知信息。<br>
	 * 消息所以不会被SDK展示到通知栏上。其内容完全由开发者自己定义。详参：http:docs.jiguang.
	 * cnjpushguidelineintro#_2
	 * 
	 * @param isNotification
	 * @author nibili 2017年6月6日
	 */
	public void setIsNotification(boolean isNotification) {
		this.isNotification = isNotification;
	}

	/**
	 * get 要发送的平台，默认发送到所有平台<br>
	 * 可选Platform.android,Platform.ios
	 * 
	 * @return
	 * @author nibili 2017年6月6日
	 */
	public PlatformEnum getPlatform() {
		return platform;
	}

	/**
	 * set 要发送的平台，默认发送到所有平台<br>
	 * 可选Platform.android,Platform.ios
	 * 
	 * @param platform
	 * @author nibili 2017年6月6日
	 */
	public void setPlatform(PlatformEnum platform) {
		this.platform = platform;
	}

	/**
	 * get 通知的标题
	 * 
	 * @return
	 * @author nibili 2017年6月6日
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * set 通知的标题
	 * 
	 * @param title
	 * @author nibili 2017年6月6日
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * get 通知的文本内容
	 * 
	 * @return
	 * @author nibili 2017年6月6日
	 */
	public String getAlert() {
		return alert;
	}

	/**
	 * set 通知的文本内容
	 * 
	 * @param alert
	 * @author nibili 2017年6月6日
	 */
	public void setAlert(String alert) {
		this.alert = alert;
	}

	/**
	 * get 别名<br/>
	 * null 此次调用不设置此值。（注：不是指的字符串"null"）<br/>
	 * "" （空字符串）表示取消之前的设置。<br/>
	 * 每次调用设置有效的别名，覆盖之前的设置。<br/>
	 * 有效的别名组成：字母（区分大小写）、数字、下划线、汉字、特殊字符(v2.1.6支持)@!#$&*+=.|。<br/>
	 * 限制：alias 命名长度限制为 40 字节。（判断长度需采用UTF-8编码）<br/>
	 * 
	 * 
	 * @return
	 * @author nibili 2017年6月6日
	 */
	public List<String> getAlias() {
		return alias;
	}

	/**
	 * set 别名<br/>
	 * null 此次调用不设置此值。（注：不是指的字符串"null"）<br/>
	 * "" （空字符串）表示取消之前的设置。<br/>
	 * 每次调用设置有效的别名，覆盖之前的设置。<br/>
	 * 有效的别名组成：字母（区分大小写）、数字、下划线、汉字、特殊字符(v2.1.6支持)@!#$&*+=.|。<br/>
	 * 限制：alias 命名长度限制为 40 字节。（判断长度需采用UTF-8编码）<br/>
	 * 
	 * 
	 * @param alias
	 * @author nibili 2017年6月6日
	 */
	public void setAlias(List<String> alias) {
		this.alias = alias;
	}

	/**
	 * get 标签<br/>
	 * null 此次调用不设置此值。（注：不是指的字符串"null"）<br/>
	 * 空数组或列表表示取消之前的设置。<br/>
	 * 每次调用至少设置一个 tag，覆盖之前的设置，不是新增。<br/>
	 * 有效的标签组成：字母（区分大小写）、数字、下划线、汉字、特殊字符(v2.1.6支持)@!#$&*+=.|。<br/>
	 * 限制：每个 tag 命名长度限制为 40 字节，最多支持设置 1000 个 tag，但总长度不得超过7K字节。（判断长度需采用UTF-8编码）<br/>
	 * 
	 * @return
	 * @author nibili 2017年6月6日
	 */
	public List<String> getTags() {
		return tags;
	}

	/**
	 * set 标签<br/>
	 * null 此次调用不设置此值。（注：不是指的字符串"null"）<br/>
	 * 空数组或列表表示取消之前的设置。<br/>
	 * 每次调用至少设置一个 tag，覆盖之前的设置，不是新增。<br/>
	 * 有效的标签组成：字母（区分大小写）、数字、下划线、汉字、特殊字符(v2.1.6支持)@!#$&*+=.|。<br/>
	 * 限制：每个 tag 命名长度限制为 40 字节，最多支持设置 1000 个 tag，但总长度不得超过7K字节。（判断长度需采用UTF-8编码）<br/>
	 * 
	 * @param tags
	 * @author nibili 2017年6月6日
	 */
	public void setTags(List<String> tags) {
		this.tags = tags;
	}

}
