package cn.com.easy.push.jiguan.enums;

import cn.jpush.api.push.model.Platform;

/**
 * 平台类型
 * 
 * @author nibili 2017年6月6日
 * 
 */
public enum PlatformEnum {
	ALL(1, Platform.all()), IOS(2, Platform.ios()), ANDROID(3, Platform.android());

	/** 序号 */
	private int index;
	/** 平台 */
	private Platform platform;

	/** 构造方法 */
	private PlatformEnum(int index, Platform platform) {
		this.platform = platform;
		this.index = index;
	}

	/**
	 * 根据序号查找玫举类型
	 * 
	 * @param index
	 * @return
	 * @author nibili 2017年6月7日
	 */
	public static PlatformEnum findByIndex(int index) {
		for (PlatformEnum c : PlatformEnum.values()) {
			if (c.getIndex() == index) {
				return c;
			}
		}
		return null;
	}

	/**
	 * get 序号
	 * 
	 * @return
	 * @author nibili 2017年6月6日
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * set 序号
	 * 
	 * @param index
	 * @author nibili 2017年6月6日
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * get 平台
	 * 
	 * @return
	 * @author nibili 2017年6月6日
	 */
	public Platform getPlatform() {
		return platform;
	}

	/**
	 * set 平台
	 * 
	 * @param platform
	 * @author nibili 2017年6月6日
	 */
	public void setPlatform(Platform platform) {
		this.platform = platform;
	}

}
