package cn.com.easy.push.jiguan.dto;

import java.util.Set;

/**
 * 别名和标签的dto
 * 
 * @author nibili 2017年6月6日
 * 
 */
public class JiguanAliasAndTagsDto {
	/**
	 * 别名<br/>
	 * null 此次调用不设置此值。（注：不是指的字符串"null"）<br/>
	 * "" （空字符串）表示取消之前的设置。<br/>
	 * 每次调用设置有效的别名，覆盖之前的设置。<br/>
	 * 有效的别名组成：字母（区分大小写）、数字、下划线、汉字、特殊字符(v2.1.6支持)@!#$&*+=.|。<br/>
	 * 限制：alias 命名长度限制为 40 字节。（判断长度需采用UTF-8编码）<br/>
	 * */
	private String alias;
	/**
	 * 标签<br/>
	 * null 此次调用不设置此值。（注：不是指的字符串"null"）<br/>
	 * 空数组或列表表示取消之前的设置。<br/>
	 * 每次调用至少设置一个 tag，覆盖之前的设置，不是新增。<br/>
	 * 有效的标签组成：字母（区分大小写）、数字、下划线、汉字、特殊字符(v2.1.6支持)@!#$&*+=.|。<br/>
	 * 限制：每个 tag 命名长度限制为 40 字节，最多支持设置 1000 个 tag，但总长度不得超过7K字节。（判断长度需采用UTF-8编码）<br/>
	 * 
	 * */
	private Set<String> tags;

	/**
	 * get 别名<br/>
	 * null 此次调用不设置此值。（注：不是指的字符串"null"）<br/>
	 * "" （空字符串）表示取消之前的设置。<br/>
	 * 每次调用设置有效的别名，覆盖之前的设置。<br/>
	 * 有效的别名组成：字母（区分大小写）、数字、下划线、汉字、特殊字符(v2.1.6支持)@!#$&*+=.|。<br/>
	 * 限制：alias 命名长度限制为 40 字节。（判断长度需采用UTF-8编码）<br/>
	 * 
	 * 
	 * @return
	 * @author nibili 2017年6月6日
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * set 别名<br/>
	 * null 此次调用不设置此值。（注：不是指的字符串"null"）<br/>
	 * "" （空字符串）表示取消之前的设置。<br/>
	 * 每次调用设置有效的别名，覆盖之前的设置。<br/>
	 * 有效的别名组成：字母（区分大小写）、数字、下划线、汉字、特殊字符(v2.1.6支持)@!#$&*+=.|。<br/>
	 * 限制：alias 命名长度限制为 40 字节。（判断长度需采用UTF-8编码）<br/>
	 * 
	 * 
	 * @param alias
	 * @author nibili 2017年6月6日
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * get 标签<br/>
	 * null 此次调用不设置此值。（注：不是指的字符串"null"）<br/>
	 * 空数组或列表表示取消之前的设置。<br/>
	 * 每次调用至少设置一个 tag，覆盖之前的设置，不是新增。<br/>
	 * 有效的标签组成：字母（区分大小写）、数字、下划线、汉字、特殊字符(v2.1.6支持)@!#$&*+=.|。<br/>
	 * 限制：每个 tag 命名长度限制为 40 字节，最多支持设置 1000 个 tag，但总长度不得超过7K字节。（判断长度需采用UTF-8编码）<br/>
	 * 
	 * @return
	 * @author nibili 2017年6月6日
	 */
	public Set<String> getTags() {
		return tags;
	}

	/**
	 * set 标签<br/>
	 * null 此次调用不设置此值。（注：不是指的字符串"null"）<br/>
	 * 空数组或列表表示取消之前的设置。<br/>
	 * 每次调用至少设置一个 tag，覆盖之前的设置，不是新增。<br/>
	 * 有效的标签组成：字母（区分大小写）、数字、下划线、汉字、特殊字符(v2.1.6支持)@!#$&*+=.|。<br/>
	 * 限制：每个 tag 命名长度限制为 40 字节，最多支持设置 1000 个 tag，但总长度不得超过7K字节。（判断长度需采用UTF-8编码）<br/>
	 * 
	 * @param tags
	 * @author nibili 2017年6月6日
	 */
	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
}
