package cn.com.easy.push.jiguan;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.com.easy.dto.MessageDTO;
import cn.com.easy.exception.BusinessException;
import cn.com.easy.push.jiguan.dto.JiguanAliasAndTagsDto;
import cn.com.easy.utils.ResponseOutputUtils;

/**
 * 极光基础控制器
 * 
 * @author nibili 2017年6月6日
 * 
 */
public abstract class JiguanAliasAndTagsBaseController {

	private Logger logger = LoggerFactory.getLogger(JiguanAliasAndTagsBaseController.class);

	/**
	 * 获取极光别名和标签dto
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @author nibili 2017年6月6日
	 */
	public abstract JiguanAliasAndTagsDto getJiguanAliasAndTagsDto(HttpServletRequest request, HttpServletResponse response) throws BusinessException;

	/**
	 * 获取别名和标签
	 * 
	 * @param request
	 * @param response
	 * @author nibili 2017年6月6日
	 */
	@RequestMapping("/alias_and_tags.json")
	public void getAliasAndTags(HttpServletRequest request, HttpServletResponse response) {
		try {
			JiguanAliasAndTagsDto jiguanAliasAndTagsDto = this.getJiguanAliasAndTagsDto(request, response);
			ResponseOutputUtils.renderJson(response, MessageDTO.newInstance("", true, jiguanAliasAndTagsDto));
		} catch (BusinessException ex) {
			ResponseOutputUtils.renderJson(response, MessageDTO.newInstance("", false, ex.getMessage()));
			logger.error("获取别名和标签异常(自定义异常)", ex);
		} catch (Exception ex) {
			ResponseOutputUtils.renderJson(response, MessageDTO.newInstance("", false, "获取别名和标签异常，请稍后再试"));
			logger.error("获取别名和标签异常", ex);
		}
	}
}
