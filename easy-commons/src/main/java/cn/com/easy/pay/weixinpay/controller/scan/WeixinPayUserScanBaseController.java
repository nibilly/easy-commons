package cn.com.easy.pay.weixinpay.controller.scan;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.com.easy.exception.BusinessException;
import cn.com.easy.pay.weixinpay.config.WeixinPayConfig;
import cn.com.easy.pay.weixinpay.param.UnifiedOrderReturnParam;
import cn.com.easy.pay.weixinpay.param.UnifiedOrderUserParam;
import cn.com.easy.pay.weixinpay.utils.WeixinPayUnifiedOrderUtils;
import cn.com.easy.utils.QRCodeImgUtils;

/**
 * 微信用户扫码支付基础控制器
 * 
 * @author nibili 2017年5月11日
 * 
 */
public abstract class WeixinPayUserScanBaseController {

	private Logger logger = LoggerFactory.getLogger(WeixinPayUserScanBaseController.class);

	/**
	 * 统一下单成功回调
	 * 
	 * @param request
	 * @param unifiedOrderUserParam
	 * @param unifiedOrderReturnParam
	 * @throws BusinessException
	 * @author nibili 2017年5月8日
	 */
	public abstract void successCallback(HttpServletRequest request, UnifiedOrderUserParam unifiedOrderUserParam, UnifiedOrderReturnParam unifiedOrderReturnParam)
			throws BusinessException;

	/**
	 * 统一下单失败的跳转 页面
	 * 
	 * @param request
	 * @param unifiedOrderUserParam
	 * @param unifiedOrderReturnParam
	 * @param errorMessage
	 *            其他错误消息
	 * @return
	 * @throws BusinessException
	 * @author nibili 2017年5月11日
	 */
	public abstract String errorPage(HttpServletRequest request, UnifiedOrderUserParam unifiedOrderUserParam, UnifiedOrderReturnParam unifiedOrderReturnParam, String errorMessage)
			throws BusinessException;

	/**
	 * 获取统一下单参数
	 * 
	 * @param request
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public abstract UnifiedOrderUserParam getUnifiedOrderUserParam(HttpServletRequest request) throws BusinessException;

	/**
	 * 获取微信支付配置
	 * 
	 * @param httpRequest
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public abstract WeixinPayConfig getWeixinPayConfig(HttpServletRequest httpRequest) throws BusinessException;

	/**
	 * 统一下单并生成支付二维码
	 * 
	 * @param request
	 * @param response
	 * @author nibili 2017年5月7日
	 * @throws BusinessException
	 * @throws Exception
	 */
	@RequestMapping("/unifiedorder/qrcode")
	public String unifiedorderAndQrCode(HttpServletRequest request, HttpServletResponse response) throws BusinessException {
		try {
			// 微信支付配置
			WeixinPayConfig weixinPayConfig = this.getWeixinPayConfig(request);
			// 用户定义的下单参数
			UnifiedOrderUserParam unifiedOrderUserParam = this.getUnifiedOrderUserParam(request);
			// 统一下单 请求
			UnifiedOrderReturnParam unifiedOrderReturnParam = WeixinPayUnifiedOrderUtils.doUnifiedOrder(unifiedOrderUserParam, weixinPayConfig);
			// 判断下单是否成功,SUCCESS/FAIL
			if (StringUtils.equals(unifiedOrderReturnParam.getReturn_code(), "SUCCESS") == true) {
				// 成功
				successCallback(request, unifiedOrderUserParam, unifiedOrderReturnParam);
				// 向客户端发送支付二维码
				String code_url = unifiedOrderReturnParam.getCode_url();
				QRCodeImgUtils.createImg(code_url, 230, 230, response);
				return "";
			} else {
				// 错误
				return errorPage(request, unifiedOrderUserParam, unifiedOrderReturnParam, "");
			}
		} catch (BusinessException ex) {
			logger.error("微信统一下单发生异常(自定义异常):" + ex.getMessage(), ex);
			// 错误
			return errorPage(request, null, null, ex.getMessage());
		} catch (Exception ex) {
			logger.error("微信统一下单发生异常:" + ex.getMessage(), ex);
			// 错误
			return errorPage(request, null, null, ex.getMessage());
		}

	}

}
