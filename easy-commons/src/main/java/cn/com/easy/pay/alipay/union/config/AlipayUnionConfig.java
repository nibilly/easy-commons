package cn.com.easy.pay.alipay.union.config;

/**
 * app支付配置
 * 
 * @author nibili 2017年4月6日
 * 
 */
public class AlipayUnionConfig {

	/** 编码 */
	public static String CHARSET = "UTF-8";

	/** 返回格式 */
	public final static String FORMAT = "json";
	/** 请求网关地址 */
	public final static String URL = "https://openapi.alipay.com/gateway.do";

	/** RSA2 */
	private String signtype = "RSA2";
	/** 商户appid */
	private String app_id = "2016120703958591";
	/** 私钥 pkcs8格式的 */
	private String rsa_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCH+muJXxWH7bdGQGdwcuLuiS1JtYvcVsMA3WvVkvwUawhdh2/kAqjSKmJQOnhYL2rkp1DYrkVdtWnfjb2OPQZ8TANY1pgeHXU8Umc6N1QXwEXDOdayhAFraV+CWlH/y1iLOJidfYHgPacsUkL4eS/qvNvEREx8P8lZKjmseuMRq0nMbp/mLpkq+K3hy0CZICCwJ5NJAfwf1zA97sfNFFNLueoRglxo64FFThCVqAicCRdYu/gZwTpTir5QhKyv/VU3vcOlInzbP6D5IbueoY9XdNVLVQ7ddZTk41PtRFzHT/SSQwsEbb224nxj1zZMOWoHN9QX3pkXha5RwgfrdHIhAgMBAAECggEAB8Y+jb0c+weRWffmshwvc1nud+9kTrW7Mnp2ZuCffv9bnBVZRV0Nj+6SbmnZZZlYcDhMvAbCuofbxMdS1iYRD9w8uMCUM+yVPlyrwD1XloAMvQu6hbxcZGrC5i4J0CktJ9CVkTOS0GJWA4e0goZTQgWIPyF0igoPlgR/BaiFAQaHkjiECQqmhXfHDgfDQYUgDZao4flJ8SKthFWOnXPlehq8gTUH2ll3j9E90CyEMEsVWeOtE7tfIXMh7xmGdX/ZZcMHP0YTF4EsGZshs+pci64DmuRIf1GkS6hCwEhMYlgZoZDYoirfDqpicwrkekzMJzoVcmWLlwICddTw7OCIvQKBgQDTaecV/6Wb1S2DEaI9TdB3RZ6a/v1JzlMzF8a/58i3HszRmJNrKq9L5S5M7BEPDakWAtfw1OiXQMxE8iXac9eOgl3BasisORh+q/plV52htuuw6yRozpthidCGs9YR8NJVQ01Da0PEVzW+JIE95VCDJUsdwE99e3Zcc+0MHTuAAwKBgQCkp80j/s+G7RrczaUQmPdrd+CYDKdxltHDJLdw1ilRS5Z9qP64cS5YZ6ciU9LvqZ3cPSgUJ1585NBqmGmCbxgedaujyYfFLyZo0vqzvN2ACXBj68czyJvIAKV1qui/Nqdvx5U6YI82AaJrNTNuuKvtpQi/Dei/EJM3XmKhXEymCwKBgQCai3wmFK/XbsrKWYWkEtIlnnsHCwfVgEJJw0PC3YlYc+d+AG8jlBx4+pwSFy+RNYc5U/LPVpSAnRm1/N23B5+3yY0H6yuu2j4Ru15b3uSYZsN8nSgMhZwIfVo2XhhLHSRkt8IvWqY7TRU4Xi8sNAnbFNQId6CkNjCb/RJ4HJZ2SQKBgHW0IphlsfKREo/fTe00fEJN2ra27QftOZwXZi76WWTs9PJ1WUl+VxQJXNfUNkju5pMnALyPfCMp6rcYpuwAAKzdj0oxaaYS05FBqMfN7kV1+erkBG3HLV1m4ODhwSnXpw/oC4xEtHt15aZfzJmM2BK01DDHf6b98U82YKla8swLAoGBAIJfcct9Dq2/yeyKIZt/FhWRnOHTCC6JHNWYrbcAdo7tY9E0q4+FQd/vxMPE3zCZob7nh7YuNlTMJCpmSqQPZYrcusT8W6biwqU0NCPakh5K5oecUA+Ll0wPTgZkdkQbvFP3DZr/KHleCCyC5aQvGnKuA8ogf4UPizP+4YsECV85";
	/** 支付宝公钥 */
	private String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApjogvhntO5LS8T1eolKnhs6HoVdA1PYcqdgBdtmK4Zjl+wf1DgciFH5cQv/hTukpn7s4fAWfod5+UFOsBrKODYlPGemQWC73quo7y/zZBIWdkIZQkiZfiD+iSvF9GbxaxTXV2HFYdkv7SViGVipQpwYhi2H8Rm0FonP5fa4qW/D0AlaAEVikAIDLmYZw7tE6hP2Bw2lqNKyRA7y28x+PA0XjyMOvUOxSD2raeGfue3FWOYLdsij1MHCxuT5TFybX49Wfazig3idH5WsshfOvuArgl14+2wZgLWEQwXpCau9Ubvr00UlzRhe2E/3tbGBxHfFKLtVU6pQoLqsl14r1SQIDAQAB";
	/** 服务器异步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 */
	private String notify_url = "http://商户网关地址/alipay.trade.wap.pay-JAVA-UTF-8/notify_url.jsp";
	/** 商户可以自定义同步跳转地址 */
	private String return_url = "http://商户网关地址/alipay.trade.wap.pay-JAVA-UTF-8/return_url.jsp";

	/** wap支付时需要：销售产品码 必填 */
	private String product_code = "QUICK_WAP_PAY";

	/**
	 * get wap支付时需要：销售产品码必填
	 * 
	 * @return
	 * @author nibili 2017年5月16日
	 */
	public String getProduct_code() {
		return product_code;
	}

	/**
	 * set wap支付时需要：销售产品码必填
	 * 
	 * @param product_code
	 * @author nibili 2017年5月16日
	 */
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	/**
	 * get 商户可以自定义同步跳转地址
	 * 
	 * @return
	 * @author nibili 2017年5月16日
	 */
	public String getReturn_url() {
		return return_url;
	}

	/**
	 * set 商户可以自定义同步跳转地址
	 * 
	 * @param return_url
	 * @author nibili 2017年5月16日
	 */
	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}

	/**
	 * get 商户appid
	 * 
	 * @return
	 * @author nibili 2017年4月7日
	 */
	public String getApp_id() {
		return app_id;
	}

	/**
	 * set 商户appid
	 * 
	 * @param app_id
	 * @author nibili 2017年4月7日
	 */
	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}

	/**
	 * get 私钥pkcs8格式的
	 * 
	 * @return
	 * @author nibili 2017年4月7日
	 */
	public String getRsa_private_key() {
		return rsa_private_key;
	}

	/**
	 * set 私钥pkcs8格式的
	 * 
	 * @param rsa_private_key
	 * @author nibili 2017年4月7日
	 */
	public void setRsa_private_key(String rsa_private_key) {
		this.rsa_private_key = rsa_private_key;
	}

	/**
	 * get 支付宝公钥
	 * 
	 * @return
	 * @author nibili 2017年4月7日
	 */
	public String getAlipay_public_key() {
		return alipay_public_key;
	}

	/**
	 * set 支付宝公钥
	 * 
	 * @param alipay_public_key
	 * @author nibili 2017年4月7日
	 */
	public void setAlipay_public_key(String alipay_public_key) {
		this.alipay_public_key = alipay_public_key;
	}

	/**
	 * get notify_url
	 * 
	 * @return
	 * @author nibili 2017年4月7日
	 */
	public String getNotify_url() {
		return notify_url;
	}

	/**
	 * set notify_url
	 * 
	 * @param notify_url
	 * @author nibili 2017年4月7日
	 */
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	/**
	 * get signtype
	 * 
	 * @return
	 * @author nibili 2017年4月7日
	 */
	public String getSigntype() {
		return signtype;
	}

	/**
	 * set signtype
	 * 
	 * @param signtype
	 * @author nibili 2017年4月7日
	 */
	public void setSigntype(String signtype) {
		this.signtype = signtype;
	}

}
