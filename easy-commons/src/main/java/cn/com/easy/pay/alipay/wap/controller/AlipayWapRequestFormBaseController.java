package cn.com.easy.pay.alipay.wap.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.com.easy.exception.BusinessException;
import cn.com.easy.pay.alipay.union.config.AlipayUnionConfig;
import cn.com.easy.utils.ResponseOutputUtils;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayTradeWapPayRequest;

/**
 * 生成支付宝wap请求页面的控制器，会把组装好的form的html返回给客户端
 * 
 * @author nibili 2017年4月7日
 * 
 */
public abstract class AlipayWapRequestFormBaseController {

	private Logger logger = LoggerFactory.getLogger(AlipayWapRequestFormBaseController.class);

	/**
	 * 业务参数
	 * 
	 * @return
	 * @author nibili 2017年4月6日
	 */
	public abstract AlipayPayParam getAlipayParam(HttpServletRequest httpRequest) throws BusinessException;

	/**
	 * 系统参数
	 * 
	 * @return
	 * @author nibili 2017年4月6日
	 */
	public abstract AlipayUnionConfig getAlipayConfig(HttpServletRequest httpRequest) throws BusinessException;

	/**
	 * 功能：支付宝手机网站支付接口(alipay.trade.wap.pay)接口调试入口页面 版本：2.0 修改日期：2016-11-01 说明：
	 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
	 * 请确保项目文件有可写权限，不然打印不了日志。
	 * 
	 * @param httpRequest
	 * @param httpResponse
	 * @throws IOException
	 * @author nibili 2017年4月7日
	 * @throws AlipayApiException
	 */
	@RequestMapping("/request/form.html")
	public void requestForm(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
		try {
			//
			AlipayUnionConfig alipayConfig = getAlipayConfig(httpRequest);
			//
			AlipayPayParam alipayParam = getAlipayParam(httpRequest);
			// SDK 公共请求类，包含公共请求参数，以及封装了签名与验签，开发者无需关注签名与验签
			// 调用RSA签名方式
			AlipayClient client = new DefaultAlipayClient(AlipayUnionConfig.URL, alipayConfig.getApp_id(), alipayConfig.getRsa_private_key(), AlipayUnionConfig.FORMAT, AlipayUnionConfig.CHARSET,
					alipayConfig.getAlipay_public_key(), alipayConfig.getSigntype());
			AlipayTradeWapPayRequest alipay_request = new AlipayTradeWapPayRequest();
			// 封装请求支付信息
			AlipayTradeWapPayModel model = new AlipayTradeWapPayModel();
			model.setOutTradeNo(alipayParam.getOut_trade_no());
			model.setSubject(alipayParam.getSubject());
			model.setTotalAmount(alipayParam.getTotal_amount());
			model.setBody(alipayParam.getBody());
			model.setTimeoutExpress(alipayParam.getTimeout_express());
			model.setProductCode(alipayConfig.getProduct_code());
			alipay_request.setBizModel(model);
			// 设置异步通知地址
			alipay_request.setNotifyUrl(alipayConfig.getNotify_url());
			// 设置同步地址
			alipay_request.setReturnUrl(alipayConfig.getReturn_url());
			// form表单生产
			String form = "";
			// 调用SDK生成表单
			form = client.pageExecute(alipay_request).getBody();
			httpResponse.setContentType("text/html;charset=" + AlipayUnionConfig.CHARSET);
			httpResponse.getWriter().write(form);// 直接将完整的表单html输出到页面
			httpResponse.getWriter().flush();
			httpResponse.getWriter().close();
		} catch (BusinessException ex) {
			ResponseOutputUtils.renderJson(httpResponse, ex.getMessage());
			logger.error("", ex);
		} catch (Exception e) {
			logger.error("", e);
		}
	}

	/**
	 * 支付要动态变化的参数
	 * 
	 * @author nibili 2017年4月6日
	 * 
	 */
	public static class AlipayPayParam {

		/** 商户订单号，商户网站订单系统中唯一订单号，必填 */
		private String out_trade_no = "";
		/** 订单名称，必填 */
		private String subject = "";
		/** 付款金额，必填 */
		private String total_amount = "";
		/** 商品描述，可空 */
		private String body = "";
		/** 超时时间 可空 */
		private String timeout_express = "";

		/**
		 * get 商户订单号，商户网站订单系统中唯一订单号，必填
		 * 
		 * @return
		 * @author nibili 2017年4月7日
		 */
		public String getOut_trade_no() {
			return out_trade_no;
		}

		/**
		 * set 商户订单号，商户网站订单系统中唯一订单号，必填
		 * 
		 * @param out_trade_no
		 * @author nibili 2017年4月7日
		 */
		public void setOut_trade_no(String out_trade_no) {
			this.out_trade_no = out_trade_no;
		}

		/**
		 * get 订单名称，必填
		 * 
		 * @return
		 * @author nibili 2017年4月7日
		 */
		public String getSubject() {
			return subject;
		}

		/**
		 * set 订单名称，必填
		 * 
		 * @param subject
		 * @author nibili 2017年4月7日
		 */
		public void setSubject(String subject) {
			this.subject = subject;
		}

		/**
		 * get 付款金额，必填
		 * 
		 * @return
		 * @author nibili 2017年4月7日
		 */
		public String getTotal_amount() {
			return total_amount;
		}

		/**
		 * set 付款金额，必填
		 * 
		 * @param total_amount
		 * @author nibili 2017年4月7日
		 */
		public void setTotal_amount(String total_amount) {
			this.total_amount = total_amount;
		}

		/**
		 * get 商品描述，可空
		 * 
		 * @return
		 * @author nibili 2017年4月7日
		 */
		public String getBody() {
			return body;
		}

		/**
		 * set 商品描述，可空
		 * 
		 * @param body
		 * @author nibili 2017年4月7日
		 */
		public void setBody(String body) {
			this.body = body;
		}

		/**
		 * get 超时时间可空
		 * 
		 * @return
		 * @author nibili 2017年4月7日
		 */
		public String getTimeout_express() {
			return timeout_express;
		}

		/**
		 * set 超时时间可空
		 * 
		 * @param timeout_express
		 * @author nibili 2017年4月7日
		 */
		public void setTimeout_express(String timeout_express) {
			this.timeout_express = timeout_express;
		}

	}
}
