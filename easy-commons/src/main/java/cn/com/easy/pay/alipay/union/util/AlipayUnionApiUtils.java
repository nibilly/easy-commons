package cn.com.easy.pay.alipay.union.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.com.easy.pay.alipay.union.config.AlipayUnionConfig;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayDataDataserviceBillDownloadurlQueryModel;
import com.alipay.api.domain.AlipayFundTransOrderQueryModel;
import com.alipay.api.domain.AlipayFundTransToaccountTransferModel;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.domain.AlipayTradeCancelModel;
import com.alipay.api.domain.AlipayTradeCloseModel;
import com.alipay.api.domain.AlipayTradeCreateModel;
import com.alipay.api.domain.AlipayTradeFastpayRefundQueryModel;
import com.alipay.api.domain.AlipayTradeOrderSettleModel;
import com.alipay.api.domain.AlipayTradePayModel;
import com.alipay.api.domain.AlipayTradePrecreateModel;
import com.alipay.api.domain.AlipayTradeQueryModel;
import com.alipay.api.domain.AlipayTradeRefundModel;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayDataDataserviceBillDownloadurlQueryRequest;
import com.alipay.api.request.AlipayFundTransOrderQueryRequest;
import com.alipay.api.request.AlipayFundTransToaccountTransferRequest;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradeCancelRequest;
import com.alipay.api.request.AlipayTradeCloseRequest;
import com.alipay.api.request.AlipayTradeCreateRequest;
import com.alipay.api.request.AlipayTradeFastpayRefundQueryRequest;
import com.alipay.api.request.AlipayTradeOrderSettleRequest;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradePayRequest;
import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayDataDataserviceBillDownloadurlQueryResponse;
import com.alipay.api.response.AlipayFundTransOrderQueryResponse;
import com.alipay.api.response.AlipayFundTransToaccountTransferResponse;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.alipay.api.response.AlipayTradeCancelResponse;
import com.alipay.api.response.AlipayTradeCloseResponse;
import com.alipay.api.response.AlipayTradeCreateResponse;
import com.alipay.api.response.AlipayTradeFastpayRefundQueryResponse;
import com.alipay.api.response.AlipayTradeOrderSettleResponse;
import com.alipay.api.response.AlipayTradePayResponse;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;

/**
 * 支付统一收单 api 工具，封装了常用接口
 * 
 * @author nibili 2017年5月16日
 * 
 */
public class AlipayUnionApiUtils {

	/**
	 * getAlipayClient(alipayUnionConfig)代码较长，封装一个方法，方便调用
	 * 
	 * @author nibili 2017年5月16日
	 */
	private static AlipayClient getAlipayClient(AlipayUnionConfig alipayUnionConfig) {
		AlipayClient alipayClient = new DefaultAlipayClient(AlipayUnionConfig.URL, alipayUnionConfig.getApp_id(), alipayUnionConfig.getRsa_private_key(), AlipayUnionConfig.FORMAT,
				AlipayUnionConfig.CHARSET, alipayUnionConfig.getAlipay_public_key(), alipayUnionConfig.getSigntype());
		return alipayClient;

	}

	/**
	 * App支付
	 * 
	 * @param model
	 * @param notifyUrl
	 * @return
	 * @throws AlipayApiException
	 */
	public static String startAppPayStr(AlipayUnionConfig alipayUnionConfig, AlipayTradeAppPayModel model) throws AlipayApiException {
		AlipayTradeAppPayResponse response = appPay(alipayUnionConfig, model);
		return response.getBody();
	}

	/**
	 * App 支付
	 * https://doc.open.alipay.com/docs/doc.htm?treeId=54&articleId=106370
	 * &docType=1
	 * 
	 * @param model
	 * @param notifyUrl
	 * @return
	 * @throws AlipayApiException
	 */
	public static AlipayTradeAppPayResponse appPay(AlipayUnionConfig alipayUnionConfig, AlipayTradeAppPayModel model) throws AlipayApiException {
		// 实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
		AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
		// SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
		request.setBizModel(model);
		request.setNotifyUrl(alipayUnionConfig.getNotify_url());
		// 这里和普通的接口调用不同，使用的是sdkExecute
		AlipayTradeAppPayResponse response = getAlipayClient(alipayUnionConfig).sdkExecute(request);
		return response;
	}

	/**
	 * Wap支付
	 * https://doc.open.alipay.com/docs/doc.htm?spm=a219a.7629140.0.0.dfHHR3&
	 * treeId=203&articleId=105285&docType=1
	 * 
	 * @throws AlipayApiException
	 * @throws IOException
	 */
	public static void wapPay(HttpServletResponse response, AlipayUnionConfig alipayUnionConfig, AlipayTradeWapPayModel model) throws AlipayApiException, IOException {
		AlipayTradeWapPayRequest alipayRequest = new AlipayTradeWapPayRequest();// 创建API对应的request
		alipayRequest.setReturnUrl(alipayUnionConfig.getReturn_url());
		alipayRequest.setNotifyUrl(alipayUnionConfig.getNotify_url());// 在公共参数中设置回跳和通知地址
		alipayRequest.setBizModel(model);// 填充业务参数
		String form = getAlipayClient(alipayUnionConfig).pageExecute(alipayRequest).getBody(); // 调用SDK生成表单
		HttpServletResponse httpResponse = response;
		httpResponse.setContentType("text/html;charset=" + AlipayUnionConfig.CHARSET);
		httpResponse.getWriter().write(form);// 直接将完整的表单html输出到页面
		httpResponse.getWriter().flush();
	}

	/**
	 * 条形码支付、声波支付
	 * https://doc.open.alipay.com/docs/api.htm?spm=a219a.7629065.0.0.
	 * XVqALk&apiId=850&docType=4
	 * 
	 * @param notifyUrl
	 * @throws AlipayApiException
	 */
	public static String tradePay(AlipayUnionConfig alipayUnionConfig, AlipayTradePayModel model) throws AlipayApiException {
		AlipayTradePayResponse response = tradePayToResponse(alipayUnionConfig, model);
		return response.getBody();
	}

	/**
	 * 条形码支付、声波支付
	 * 
	 * @param alipayUnionConfig
	 * @param model
	 * @return
	 * @throws AlipayApiException
	 * @author nibili 2017年5月16日
	 */
	public static AlipayTradePayResponse tradePayToResponse(AlipayUnionConfig alipayUnionConfig, AlipayTradePayModel model) throws AlipayApiException {
		AlipayTradePayRequest request = new AlipayTradePayRequest();
		request.setBizModel(model);// 填充业务参数
		request.setNotifyUrl(alipayUnionConfig.getNotify_url());
		return getAlipayClient(alipayUnionConfig).execute(request); // 通过getAlipayClient(alipayUnionConfig)调用API，获得对应的response类
	}

	/**
	 * 扫码支付
	 * https://doc.open.alipay.com/docs/doc.htm?spm=a219a.7629140.0.0.i0UVZn
	 * &treeId=193&articleId=105170&docType=1#s4
	 * 
	 * @param notifyUrl
	 * @return
	 * @throws AlipayApiException
	 */
	public static String tradePrecreatePay(AlipayUnionConfig alipayUnionConfig, AlipayTradePrecreateModel model, String notifyUrl) throws AlipayApiException {
		AlipayTradePrecreateResponse response = tradePrecreatePayToResponse(alipayUnionConfig, model);
		return response.getBody();
	}

	/**
	 * 扫码支付
	 * 
	 * @param alipayUnionConfig
	 * @param model
	 * @return
	 * @throws AlipayApiException
	 * @author nibili 2017年5月16日
	 */
	public static AlipayTradePrecreateResponse tradePrecreatePayToResponse(AlipayUnionConfig alipayUnionConfig, AlipayTradePrecreateModel model) throws AlipayApiException {
		AlipayTradePrecreateRequest request = new AlipayTradePrecreateRequest();
		request.setBizModel(model);
		request.setNotifyUrl(alipayUnionConfig.getNotify_url());
		return getAlipayClient(alipayUnionConfig).execute(request);
	}

	/**
	 * 单笔转账到支付宝账户
	 * https://doc.open.alipay.com/docs/doc.htm?spm=a219a.7629140.0.0.54
	 * Ty29&treeId=193&articleId=106236&docType=1
	 * 
	 * @param content
	 * @return
	 * @throws AlipayApiException
	 */
	public static boolean transfer(AlipayUnionConfig alipayUnionConfig, AlipayFundTransToaccountTransferModel model) throws AlipayApiException {
		AlipayFundTransToaccountTransferResponse response = transferToResponse(alipayUnionConfig, model);
		String result = response.getBody();
		// System.out.println("transfer result>" + result);
		if (response.isSuccess()) {
			return true;
		} else {
			// 调用查询接口查询数据
			JSONObject jsonObject = JSONObject.parseObject(result);
			String out_biz_no = jsonObject.getJSONObject("alipay_fund_trans_toaccount_transfer_response").getString("out_biz_no");
			AlipayFundTransOrderQueryModel queryModel = new AlipayFundTransOrderQueryModel();
			model.setOutBizNo(out_biz_no);
			boolean isSuccess = transferQuery(alipayUnionConfig, queryModel);
			if (isSuccess) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 单笔转账到支付宝账户
	 * 
	 * @param alipayUnionConfig
	 * @param model
	 * @return
	 * @throws AlipayApiException
	 * @author nibili 2017年5月16日
	 */
	public static AlipayFundTransToaccountTransferResponse transferToResponse(AlipayUnionConfig alipayUnionConfig, AlipayFundTransToaccountTransferModel model)
			throws AlipayApiException {
		AlipayFundTransToaccountTransferRequest request = new AlipayFundTransToaccountTransferRequest();
		request.setBizModel(model);
		return getAlipayClient(alipayUnionConfig).execute(request);
	}

	/**
	 * 转账查询接口
	 * 
	 * @param content
	 * @return
	 * @throws AlipayApiException
	 */
	public static boolean transferQuery(AlipayUnionConfig alipayUnionConfig, AlipayFundTransOrderQueryModel model) throws AlipayApiException {
		AlipayFundTransOrderQueryResponse response = transferQueryToResponse(alipayUnionConfig, model);
		// log.info("transferQuery result>" + response.getBody());
		// System.out.println("transferQuery result>" + response.getBody());
		if (response.isSuccess()) {
			return true;
		}
		return false;
	}

	public static AlipayFundTransOrderQueryResponse transferQueryToResponse(AlipayUnionConfig alipayUnionConfig, AlipayFundTransOrderQueryModel model) throws AlipayApiException {
		AlipayFundTransOrderQueryRequest request = new AlipayFundTransOrderQueryRequest();
		request.setBizModel(model);
		return getAlipayClient(alipayUnionConfig).execute(request);
	}

	/**
	 * 交易查询接口
	 * https://doc.open.alipay.com/docs/api.htm?spm=a219a.7395905.0.0.8H2JzG
	 * &docType=4&apiId=757
	 * 
	 * @param bizContent
	 * @return
	 * @throws AlipayApiException
	 */
	public static boolean isTradeQuery(AlipayUnionConfig alipayUnionConfig, AlipayTradeQueryModel model) throws AlipayApiException {
		AlipayTradeQueryResponse response = tradeQuery(alipayUnionConfig, model);
		if (response.isSuccess()) {
			return true;
		}
		return false;
	}

	/**
	 * 交易查询接口
	 * 
	 * @param alipayUnionConfig
	 * @param model
	 * @return
	 * @throws AlipayApiException
	 * @author nibili 2017年5月16日
	 */
	public static AlipayTradeQueryResponse tradeQuery(AlipayUnionConfig alipayUnionConfig, AlipayTradeQueryModel model) throws AlipayApiException {
		AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
		request.setBizModel(model);
		return getAlipayClient(alipayUnionConfig).execute(request);
	}

	/**
	 * 交易撤销接口
	 * https://doc.open.alipay.com/docs/api.htm?spm=a219a.7395905.0.0.XInh6e
	 * &docType=4&apiId=866
	 * 
	 * @param bizContent
	 * @return
	 * @throws AlipayApiException
	 */
	public static boolean isTradeCancel(AlipayUnionConfig alipayUnionConfig, AlipayTradeCancelModel model) throws AlipayApiException {
		AlipayTradeCancelResponse response = tradeCancel(alipayUnionConfig, model);
		if (response.isSuccess()) {
			return true;
		}
		return false;
	}

	public static AlipayTradeCancelResponse tradeCancel(AlipayUnionConfig alipayUnionConfig, AlipayTradeCancelModel model) throws AlipayApiException {
		AlipayTradeCancelRequest request = new AlipayTradeCancelRequest();
		request.setBizModel(model);
		AlipayTradeCancelResponse response = getAlipayClient(alipayUnionConfig).execute(request);
		return response;
	}

	/**
	 * 关闭订单
	 * https://doc.open.alipay.com/docs/api.htm?spm=a219a.7629065.0.0.21yRUe
	 * &apiId=1058&docType=4
	 * 
	 * @param model
	 * @return
	 * @throws AlipayApiException
	 */
	public static boolean isTradeClose(AlipayUnionConfig alipayUnionConfig, AlipayTradeCloseModel model) throws AlipayApiException {
		AlipayTradeCloseResponse response = tradeClose(alipayUnionConfig, model);
		if (response.isSuccess()) {
			return true;
		}
		return false;
	}

	public static AlipayTradeCloseResponse tradeClose(AlipayUnionConfig alipayUnionConfig, AlipayTradeCloseModel model) throws AlipayApiException {
		AlipayTradeCloseRequest request = new AlipayTradeCloseRequest();
		request.setBizModel(model);
		return getAlipayClient(alipayUnionConfig).execute(request);

	}

	/**
	 * 统一收单交易创建接口
	 * https://doc.open.alipay.com/docs/api.htm?spm=a219a.7629065.0.0.21
	 * yRUe&apiId=1046&docType=4
	 * 
	 * @param model
	 * @param notifyUrl
	 * @return
	 * @throws AlipayApiException
	 */
	public static AlipayTradeCreateResponse tradeCreate(AlipayUnionConfig alipayUnionConfig, AlipayTradeCreateModel model, String notifyUrl) throws AlipayApiException {
		AlipayTradeCreateRequest request = new AlipayTradeCreateRequest();
		request.setBizModel(model);
		request.setNotifyUrl(alipayUnionConfig.getNotify_url());
		return getAlipayClient(alipayUnionConfig).execute(request);
	}

	/**
	 * 退款 https://doc.open.alipay.com/docs/api.htm?spm=a219a.7395905.0.0.SAyEeI&
	 * docType=4&apiId=759
	 * 
	 * @param content
	 * @return
	 * @throws AlipayApiException
	 */
	public static String tradeRefund(AlipayUnionConfig alipayUnionConfig, AlipayTradeRefundModel model) throws AlipayApiException {
		AlipayTradeRefundResponse response = tradeRefundToResponse(alipayUnionConfig, model);
		return response.getBody();
	}

	public static AlipayTradeRefundResponse tradeRefundToResponse(AlipayUnionConfig alipayUnionConfig, AlipayTradeRefundModel model) throws AlipayApiException {
		AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
		request.setBizModel(model);
		return getAlipayClient(alipayUnionConfig).execute(request);
	}

	/**
	 * 退款查询
	 * https://doc.open.alipay.com/docs/api.htm?spm=a219a.7629065.0.0.KQeTSa
	 * &apiId=1049&docType=4
	 * 
	 * @param model
	 * @return
	 * @throws AlipayApiException
	 */
	public static String tradeRefundQuery(AlipayUnionConfig alipayUnionConfig, AlipayTradeFastpayRefundQueryModel model) throws AlipayApiException {
		AlipayTradeFastpayRefundQueryResponse response = tradeRefundQueryToResponse(alipayUnionConfig, model);
		return response.getBody();
	}

	public static AlipayTradeFastpayRefundQueryResponse tradeRefundQueryToResponse(AlipayUnionConfig alipayUnionConfig, AlipayTradeFastpayRefundQueryModel model)
			throws AlipayApiException {
		AlipayTradeFastpayRefundQueryRequest request = new AlipayTradeFastpayRefundQueryRequest();
		request.setBizModel(model);
		return getAlipayClient(alipayUnionConfig).execute(request);
	}

	/**
	 * 查询对账单下载地址
	 * 
	 * @param bizContent
	 * @return
	 * @throws AlipayApiException
	 */
	public static String billDownloadurlQuery(AlipayUnionConfig alipayUnionConfig, AlipayDataDataserviceBillDownloadurlQueryModel model) throws AlipayApiException {
		AlipayDataDataserviceBillDownloadurlQueryResponse response = billDownloadurlQueryToResponse(alipayUnionConfig, model);
		return response.getBillDownloadUrl();
	}

	public static AlipayDataDataserviceBillDownloadurlQueryResponse billDownloadurlQueryToResponse(AlipayUnionConfig alipayUnionConfig,
			AlipayDataDataserviceBillDownloadurlQueryModel model) throws AlipayApiException {
		AlipayDataDataserviceBillDownloadurlQueryRequest request = new AlipayDataDataserviceBillDownloadurlQueryRequest();
		request.setBizModel(model);
		return getAlipayClient(alipayUnionConfig).execute(request);
	}

	/**
	 * 交易结算接口
	 * https://doc.open.alipay.com/docs/api.htm?spm=a219a.7395905.0.0.nl0RS3
	 * &docType=4&apiId=1147
	 * 
	 * @param bizContent
	 * @return
	 * @throws AlipayApiException
	 */
	public static boolean isTradeOrderSettle(AlipayUnionConfig alipayUnionConfig, AlipayTradeOrderSettleModel model) throws AlipayApiException {
		AlipayTradeOrderSettleResponse response = tradeOrderSettle(alipayUnionConfig, model);
		if (response.isSuccess()) {
			return true;
		}
		return false;
	}

	public static AlipayTradeOrderSettleResponse tradeOrderSettle(AlipayUnionConfig alipayUnionConfig, AlipayTradeOrderSettleModel model) throws AlipayApiException {
		AlipayTradeOrderSettleRequest request = new AlipayTradeOrderSettleRequest();
		request.setBizModel(model);
		return getAlipayClient(alipayUnionConfig).execute(request);
	}

	/**
	 * 电脑网站支付(PC支付)
	 * 
	 * @param model
	 * @param notifyUrl
	 * @param returnUrl
	 * @return
	 * @throws AlipayApiException
	 * @throws IOException
	 */
	public static void tradePage(HttpServletResponse httpResponse, AlipayUnionConfig alipayUnionConfig, AlipayTradePayModel model, String notifyUrl, String returnUrl)
			throws AlipayApiException, IOException {
		AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
		request.setBizModel(model);
		request.setNotifyUrl(notifyUrl);
		request.setReturnUrl(returnUrl);
		String form = getAlipayClient(alipayUnionConfig).pageExecute(request).getBody();// 调用SDK生成表单
		httpResponse.setContentType("text/html;charset=" + AlipayUnionConfig.CHARSET);
		httpResponse.getWriter().write(form);// 直接将完整的表单html输出到页面
		httpResponse.getWriter().flush();
		httpResponse.getWriter().close();
	}

	/**
	 * 将异步通知的参数转化为Map
	 * 
	 * @param request
	 * @return
	 */
	public static Map<String, String> toMap(HttpServletRequest request) {
		System.out.println(">>>>" + request.getQueryString());
		Map<String, String> params = new HashMap<String, String>();
		@SuppressWarnings("unchecked")
		Map<String, String[]> requestParams = request.getParameterMap();
		for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
			}
			// 乱码解决，这段代码在出现乱码时使用。
			// valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
			params.put(name, valueStr);
		}
		return params;
	}

}
