package cn.com.easy.pay.weixinpay.param;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 统一下单返回参数<br/>
 * 详参： https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_1
 * 
 * @author nibili 2017年5月7日
 * 
 */
@XmlRootElement(name = "xml")
public class UnifiedOrderReturnParam {
	/**
	 * 返回状态码 是 String(16) SUCCESS SUCCESS/FAIL
	 * 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
	 */
	private String return_code;

	/** 返回信息 否 String(128) 签名失败 返回信息，如非空，为错误原因 签名失败 参数格式校验错误 */
	private String return_msg;
	/* 以下字段在return_code为SUCCESS的时候有返回 */
	/** 应用APPID 是 String(32) wx8888888888888888 调用接口提交的应用ID */
	private String appid;
	/** 商户号 是 String(32) 1900000109 调用接口提交的商户号 */
	private String mch_id;
	/** 设备号 否 String(32) 013467007045764 调用接口提交的终端设备号， */
	private String device_info;
	/** 随机字符串 是 String(32) 5K8264ILTKCH16CQ2502SI8ZNMTM67VS 微信返回的随机字符串 */
	private String nonce_str;
	/** 签名 是 String(32) C380BEC2BFD727A4B6845133519F3AD6 微信返回的签名，详见签名算法 */
	private String sign;
	/** 业务结果 是 String(16) SUCCESS SUCCESS/FAIL */
	private String result_code;
	/** 错误代码 否 String(32) SYSTEMERROR 详细参见第6节错误列表 */
	private String err_code;
	/** 错误代码描述 否 String(128) 系统错误 错误返回的信息描述 */
	private String err_code_des;
	/* 以下字段在return_code 和result_code都为SUCCESS的时候有返回 */
	/** 交易类型 是 String(16) JSAPI 调用接口提交的交易类型，取值如下：JSAPI，NATIVE，APP，详细说明见参数规定 */
	private String trade_type;
	/**
	 * 预支付交易会话标识 是 String(64) wx201410272009395522657a690389285100
	 * 微信生成的预支付回话标识，用于后续接口调用中使用，该值有效期为2小时
	 */
	private String prepay_id;

	/**
	 * 二维码链接 否 String(64) URl：weixin：//wxpay/s/An4baqw
	 * trade_type为NATIVE时有返回，用于生成二维码，展示给用户进行扫码支付
	 */
	private String code_url;

	/**
	 * get
	 * 二维码链接否String(64)URl：weixin：wxpaysAn4baqwtrade_type为NATIVE时有返回，用于生成二维码，
	 * 展示给用户进行扫码支付
	 * 
	 * @return
	 * @author nibili 2017年5月10日
	 */
	public String getCode_url() {
		return code_url;
	}

	/**
	 * set
	 * 二维码链接否String(64)URl：weixin：wxpaysAn4baqwtrade_type为NATIVE时有返回，用于生成二维码，
	 * 展示给用户进行扫码支付
	 * 
	 * @param code_url
	 * @author nibili 2017年5月10日
	 */
	public void setCode_url(String code_url) {
		this.code_url = code_url;
	}

	/**
	 * get 返回状态码是String(16)SUCCESSSUCCESSFAIL此字段是通信标识，非交易标识，
	 * 交易是否成功需要查看result_code来判断
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getReturn_code() {
		return return_code;
	}

	/**
	 * set 返回状态码是String(16)SUCCESSSUCCESSFAIL此字段是通信标识，非交易标识，
	 * 交易是否成功需要查看result_code来判断
	 * 
	 * @param return_code
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setReturn_code(String return_code) {
		this.return_code = return_code;
	}

	/**
	 * get 返回信息否String(128)签名失败返回信息，如非空，为错误原因签名失败参数格式校验错误
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getReturn_msg() {
		return return_msg;
	}

	/**
	 * set 返回信息否String(128)签名失败返回信息，如非空，为错误原因签名失败参数格式校验错误
	 * 
	 * @param return_msg
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setReturn_msg(String return_msg) {
		this.return_msg = return_msg;
	}

	/**
	 * get 以下字段在return_code为SUCCESS的时候有返回应用APPID是Strin
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getAppid() {
		return appid;
	}

	/**
	 * set 以下字段在return_code为SUCCESS的时候有返回应用APPID是Strin
	 * 
	 * @param appid
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setAppid(String appid) {
		this.appid = appid;
	}

	/**
	 * get 商户号是String(32)1900000109调用接口提交的商户号
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getMch_id() {
		return mch_id;
	}

	/**
	 * set 商户号是String(32)1900000109调用接口提交的商户号
	 * 
	 * @param mch_id
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

	/**
	 * get 设备号否String(32)013467007045764调用接口提交的终端设备号，
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getDevice_info() {
		return device_info;
	}

	/**
	 * set 设备号否String(32)013467007045764调用接口提交的终端设备号，
	 * 
	 * @param device_info
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}

	/**
	 * get 随机字符串是String(32)5K8264ILTKCH16CQ2502SI8ZNMTM67VS微信返回的随机字符串
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getNonce_str() {
		return nonce_str;
	}

	/**
	 * set 随机字符串是String(32)5K8264ILTKCH16CQ2502SI8ZNMTM67VS微信返回的随机字符串
	 * 
	 * @param nonce_str
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	/**
	 * get 签名是String(32)C380BEC2BFD727A4B6845133519F3AD6微信返回的签名，详见签名算法
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * set 签名是String(32)C380BEC2BFD727A4B6845133519F3AD6微信返回的签名，详见签名算法
	 * 
	 * @param sign
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setSign(String sign) {
		this.sign = sign;
	}

	/**
	 * get 业务结果是String(16)SUCCESSSUCCESSFAIL
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getResult_code() {
		return result_code;
	}

	/**
	 * set 业务结果是String(16)SUCCESSSUCCESSFAIL
	 * 
	 * @param result_code
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}

	/**
	 * get 错误代码否String(32)SYSTEMERROR详细参见第6节错误列表
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getErr_code() {
		return err_code;
	}

	/**
	 * set 错误代码否String(32)SYSTEMERROR详细参见第6节错误列表
	 * 
	 * @param err_code
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setErr_code(String err_code) {
		this.err_code = err_code;
	}

	/**
	 * get 错误代码描述否String(128)系统错误错误返回的信息描述
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getErr_code_des() {
		return err_code_des;
	}

	/**
	 * set 错误代码描述否String(128)系统错误错误返回的信息描述
	 * 
	 * @param err_code_des
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setErr_code_des(String err_code_des) {
		this.err_code_des = err_code_des;
	}

	/**
	 * get 以下字段在return_code和result_code都为SUCCESS的时候有返回交易类型是String(16)
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getTrade_type() {
		return trade_type;
	}

	/**
	 * set 以下字段在return_code和result_code都为SUCCESS的时候有返回交易类型是String(16)
	 * 
	 * @param trade_type
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}

	/**
	 * get 预支付交易会话标识是String(64)wx201410272009395522657a690389285100微信生成的预支付回话标识，
	 * 用于后续接口调用中使用，该值有效期为2小时
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getPrepay_id() {
		return prepay_id;
	}

	/**
	 * set 预支付交易会话标识是String(64)wx201410272009395522657a690389285100微信生成的预支付回话标识，
	 * 用于后续接口调用中使用，该值有效期为2小时
	 * 
	 * @param prepay_id
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setPrepay_id(String prepay_id) {
		this.prepay_id = prepay_id;
	}

}
