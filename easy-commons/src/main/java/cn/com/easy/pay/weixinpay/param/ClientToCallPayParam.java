package cn.com.easy.pay.weixinpay.param;

/**
 * 返回给客户端用于发起支付的参数
 * 
 * @author nibili 2017年5月24日
 * 
 */
public class ClientToCallPayParam {

	/** 应用id String(32) 是 wx8888888888888888 微信开放平台审核通过的应用APPID */
	private String appid;
	/** 商户号 String(32) 是 1900000109 微信支付分配的商户号 */
	private String partnerid;
	/** 预支付交易会话ID String(32) 是 WX1217752501201407033233368018 微信返回的支付交易会话ID */
	private String prepayid;
	/** 扩展字段 String(128) 是 Sign=WXPay 暂填写固定值Sign=WXPay */
	private String packageValue = "WXPay";
	/**
	 * 随机字符串 String(32) 是 5K8264ILTKCH16CQ2502SI8ZNMTM67VS
	 * 随机字符串，不长于32位。推荐随机数生成算法
	 */
	private String noncestr;
	/** 时间戳 String(10) 是 1412000000 时间戳，请见接口规则-参数规定 */
	private String timestamp;
	/** 签名 String(32) 是 C380BEC2BFD727A4B6845133519F3AD6 签名，详见签名生成算法 */
	private String sign;

	/** 错误代码 否 String(32) SYSTEMERROR 详细参见第6节错误列表 */
	private String err_code;
	/** 错误代码描述 否 String(128) 系统错误 错误返回的信息描述 */
	private String err_code_des;

	/**
	 * get 错误代码否String(32)SYSTEMERROR详细参见第6节错误列表
	 * 
	 * @return
	 * @author nibili 2017年5月24日
	 */
	public String getErr_code() {
		return err_code;
	}

	/**
	 * set 错误代码否String(32)SYSTEMERROR详细参见第6节错误列表
	 * 
	 * @param err_code
	 * @author nibili 2017年5月24日
	 */
	public void setErr_code(String err_code) {
		this.err_code = err_code;
	}

	/**
	 * get 错误代码描述否String(128)系统错误错误返回的信息描述
	 * 
	 * @return
	 * @author nibili 2017年5月24日
	 */
	public String getErr_code_des() {
		return err_code_des;
	}

	/**
	 * set 错误代码描述否String(128)系统错误错误返回的信息描述
	 * 
	 * @param err_code_des
	 * @author nibili 2017年5月24日
	 */
	public void setErr_code_des(String err_code_des) {
		this.err_code_des = err_code_des;
	}

	/**
	 * get 应用idString(32)是wx8888888888888888微信开放平台审核通过的应用APPID
	 * 
	 * @return
	 * @author nibili 2017年5月24日
	 */
	public String getAppid() {
		return appid;
	}

	/**
	 * set 应用idString(32)是wx8888888888888888微信开放平台审核通过的应用APPID
	 * 
	 * @param appid
	 * @author nibili 2017年5月24日
	 */
	public void setAppid(String appid) {
		this.appid = appid;
	}

	/**
	 * get 商户号String(32)是1900000109微信支付分配的商户号
	 * 
	 * @return
	 * @author nibili 2017年5月24日
	 */
	public String getPartnerid() {
		return partnerid;
	}

	/**
	 * set 商户号String(32)是1900000109微信支付分配的商户号
	 * 
	 * @param partnerid
	 * @author nibili 2017年5月24日
	 */
	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}

	/**
	 * get 预支付交易会话IDString(32)是WX1217752501201407033233368018微信返回的支付交易会话ID
	 * 
	 * @return
	 * @author nibili 2017年5月24日
	 */
	public String getPrepayid() {
		return prepayid;
	}

	/**
	 * set 预支付交易会话IDString(32)是WX1217752501201407033233368018微信返回的支付交易会话ID
	 * 
	 * @param prepayid
	 * @author nibili 2017年5月24日
	 */
	public void setPrepayid(String prepayid) {
		this.prepayid = prepayid;
	}

	/**
	 * get 扩展字段String(128)是Sign=WXPay暂填写固定值Sign=WXPay
	 * 
	 * @return
	 * @author nibili 2017年5月24日
	 */
	public String getPackageValue() {
		return packageValue;
	}

	/**
	 * set 扩展字段String(128)是Sign=WXPay暂填写固定值Sign=WXPay
	 * 
	 * @param packageValue
	 * @author nibili 2017年5月24日
	 */
	public void setPackageValue(String packageValue) {
		this.packageValue = packageValue;
	}

	/**
	 * get
	 * 随机字符串String(32)是5K8264ILTKCH16CQ2502SI8ZNMTM67VS随机字符串，不长于32位。推荐随机数生成算法
	 * 
	 * @return
	 * @author nibili 2017年5月24日
	 */
	public String getNoncestr() {
		return noncestr;
	}

	/**
	 * set
	 * 随机字符串String(32)是5K8264ILTKCH16CQ2502SI8ZNMTM67VS随机字符串，不长于32位。推荐随机数生成算法
	 * 
	 * @param noncestr
	 * @author nibili 2017年5月24日
	 */
	public void setNoncestr(String noncestr) {
		this.noncestr = noncestr;
	}

	/**
	 * get 时间戳String(10)是1412000000时间戳，请见接口规则-参数规定
	 * 
	 * @return
	 * @author nibili 2017年5月24日
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * set 时间戳String(10)是1412000000时间戳，请见接口规则-参数规定
	 * 
	 * @param timestamp
	 * @author nibili 2017年5月24日
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * get 签名String(32)是C380BEC2BFD727A4B6845133519F3AD6签名，详见签名生成算法
	 * 
	 * @return
	 * @author nibili 2017年5月24日
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * set 签名String(32)是C380BEC2BFD727A4B6845133519F3AD6签名，详见签名生成算法
	 * 
	 * @param sign
	 * @author nibili 2017年5月24日
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}

}
