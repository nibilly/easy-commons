package cn.com.easy.pay.alipay.www.sign;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import cn.com.easy.pay.alipay.www.util.Base64Util;

public class RSA {

	public static final String SIGN_ALGORITHMS = "SHA1WithRSA";

	public static void main(String[] args) {
		String private_key = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAN2I1tvgZnBBgtgu8xpLaB33BFU0RhiB/IKIlu1ETsOpQzvSfxEWOdV4OA1/YUBtqHe8zaolaJU52usIPVAqh6wfCPIrlC0L77j+4MbbjRuG9LSEZEr1DmjPYZf9qkmucYiTjYvYV851JncgSGCq8Xt7fgg/BOc54Km3P3436JQlAgMBAAECgYBFZ32I3UC6oouPlwXqkv1HhgABgaMEQpUNgCVoqCHVAs5hi4zrmX789fD5qZKaUMCa4re8OoQNXFbNk4NB/lEKckaBA9hjvlMCGTuv5zaHAmyw/0mp9JbQBLMt1C9PCgIsa3HcXHu5lMocxo4OqUnxhl89MmhTfBeyr7bl6ROZgQJBAPgi+b/nDDn0/+MGEIqG8Wf2srNQvSfMVWPOJk9zkHjCUuacXh5v2jC+rD/uJcvXYnef+zv1XbRoxleh+va9/L0CQQDkjg1wGPWlW7i08X15fa7sBn4mucQvf8L7BZKa2UU82d75t/ntrG3zqg3/Y2Q5g8tO95dj2I2bEtwmMPfPFk+JAkAmwmfapv+M6g0ybBcXXF7HC4z8X4c8eTl5XpEzZezSQCBs703wWS5u4MnNLqjuv1UdNHss8XXSoW8h7yZhdmnNAkAtUMT1Tt2VlaP/rEnWhsbh4T+t6fMt/8TnBGnW7fE+aWw+Uu3o4+FBCJyR4GEEzk7HL2wCzfL1pj2kDN11X9URAkEAyEVyzlAhxB8kXtSMmnIe9iGVFryTirVmvRVX2fV53hcQtn2kMa9ZkZgfwss28Ht6d0rTal6Z/Z9DEIAxoirU8w==";
		String input_charset = "utf-8";
		String content = "_input_charset=utf-8&notify_url=http://localhost:90/alipay/webNotify&out_trade_no=149095525137133899&partner=2088521367961200&payment_type=1&return_url=http://localhost:90/user/buyer/order/alipayReturnPage&seller_id=2088521367961200&service=create_direct_pay_by_user&subject=旧货交易商品(僦牛)&total_fee=0.10";
		sign(content, private_key, input_charset);
	}

	/**
	 * RSA签名
	 * 
	 * @param content
	 *            待签名数据
	 * @param privateKey
	 *            商户私钥
	 * @param input_charset
	 *            编码格式
	 * @return 签名值
	 */
	public static String sign(String content, String privateKey, String input_charset) {
		try {
			PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64Util.decode(privateKey));
			KeyFactory keyf = KeyFactory.getInstance("RSA");
			PrivateKey priKey = keyf.generatePrivate(priPKCS8);

			java.security.Signature signature = java.security.Signature.getInstance(SIGN_ALGORITHMS);

			signature.initSign(priKey);
			signature.update(content.getBytes(input_charset));

			byte[] signed = signature.sign();

			return Base64Util.encode(signed);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * RSA验签名检查
	 * 
	 * @param content
	 *            待签名数据
	 * @param sign
	 *            签名值
	 * @param ali_public_key
	 *            支付宝公钥
	 * @param input_charset
	 *            编码格式
	 * @return 布尔值
	 */
	public static boolean verify(String content, String sign, String ali_public_key, String input_charset) {
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			byte[] encodedKey = Base64Util.decode(ali_public_key);
			PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));

			java.security.Signature signature = java.security.Signature.getInstance(SIGN_ALGORITHMS);

			signature.initVerify(pubKey);
			signature.update(content.getBytes(input_charset));

			boolean bverify = signature.verify(Base64Util.decode(sign));
			return bverify;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * 解密
	 * 
	 * @param content
	 *            密文
	 * @param private_key
	 *            商户私钥
	 * @param input_charset
	 *            编码格式
	 * @return 解密后的字符串
	 */
	public static String decrypt(String content, String private_key, String input_charset) throws Exception {
		PrivateKey prikey = getPrivateKey(private_key);

		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, prikey);

		InputStream ins = new ByteArrayInputStream(Base64Util.decode(content));
		ByteArrayOutputStream writer = new ByteArrayOutputStream();
		// rsa解密的字节大小最多是128，将需要解密的内容，按128位拆开解密
		byte[] buf = new byte[128];
		int bufl;

		while ((bufl = ins.read(buf)) != -1) {
			byte[] block = null;

			if (buf.length == bufl) {
				block = buf;
			} else {
				block = new byte[bufl];
				for (int i = 0; i < bufl; i++) {
					block[i] = buf[i];
				}
			}

			writer.write(cipher.doFinal(block));
		}

		return new String(writer.toByteArray(), input_charset);
	}

	/**
	 * 得到私钥
	 * 
	 * @param key
	 *            密钥字符串（经过base64编码）
	 * @throws Exception
	 */
	public static PrivateKey getPrivateKey(String key) throws Exception {

		byte[] keyBytes;

		keyBytes = Base64Util.decode(key);

		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);

		KeyFactory keyFactory = KeyFactory.getInstance("RSA");

		PrivateKey privateKey = keyFactory.generatePrivate(keySpec);

		return privateKey;
	}
}
