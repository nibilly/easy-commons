package cn.com.easy.pay.alipay.www.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.com.easy.exception.BusinessException;
import cn.com.easy.pay.alipay.www.config.AlipayConfig;
import cn.com.easy.pay.alipay.www.util.AlipaySubmit;
import cn.com.easy.utils.ResponseOutputUtils;

/**
 * 生成支付宝请求页面的控制器，会把组装好的form的html返回给客户端
 * 
 * @author nibili 2017年4月6日
 * 
 */
public abstract class AlipayRequestFormBaseController {

	private Logger logger = LoggerFactory.getLogger(AlipayRequestFormBaseController.class);

	/**
	 * 业务参数
	 * 
	 * @return
	 * @author nibili 2017年4月6日
	 */
	public abstract AlipayPayParam getAlipayParam(HttpServletRequest httpRequest) throws BusinessException;

	/**
	 * 系统参数
	 * 
	 * @return
	 * @author nibili 2017年4月6日
	 */
	public abstract AlipayConfig getAlipayConfig(HttpServletRequest httpRequest) throws BusinessException;

	/**
	 * 生成支付宝请求页面的控制器，会把组装好的form的html返回给客户端
	 * 
	 * @param httpRequest
	 * @param httpResponse
	 * @throws IOException
	 * @author nibili 2017年4月7日
	 * @throws BusinessException
	 */
	@RequestMapping("/request/form.html")
	public void requestForm(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws Exception {
		try {
			AlipayConfig alipayConfig = getAlipayConfig(httpRequest);
			AlipayPayParam alipayParam = getAlipayParam(httpRequest);
			// 把请求参数打包成数组
			Map<String, String> sParaTemp = new HashMap<String, String>();
			sParaTemp.put("service", alipayConfig.getService());
			sParaTemp.put("partner", alipayConfig.getPartner());
			sParaTemp.put("seller_id", alipayConfig.getSeller_id());
			sParaTemp.put("_input_charset", alipayConfig.input_charset);
			sParaTemp.put("payment_type", alipayConfig.payment_type);
			sParaTemp.put("notify_url", alipayConfig.notify_url);
			sParaTemp.put("return_url", alipayConfig.return_url);
			sParaTemp.put("anti_phishing_key", alipayConfig.anti_phishing_key);
			sParaTemp.put("exter_invoke_ip", alipayConfig.exter_invoke_ip);
			sParaTemp.put("out_trade_no", alipayParam.getOut_trade_no());
			sParaTemp.put("subject", alipayParam.getSubject());
			sParaTemp.put("total_fee", alipayParam.getTotal_fee());
			sParaTemp.put("body", alipayParam.getBody());
			// 其他业务参数根据在线开发文档，添加参数.文档地址:https://doc.open.alipay.com/doc2/detail.htm?spm=a219a.7629140.0.0.O9yorI&treeId=62&articleId=103740&docType=1
			// 如sParaTemp.put("参数名","参数值");
			// 建立请求
			String sHtmlText = AlipaySubmit.buildRequest(sParaTemp, "get", "确认", alipayConfig);
			httpResponse.setContentType("text/html;charset=" + alipayConfig.getInput_charset());
			httpResponse.getWriter().write(sHtmlText);// 直接将完整的表单html输出到页面
			httpResponse.getWriter().flush();
			httpResponse.getWriter().close();
		} catch (BusinessException ex) {
			ResponseOutputUtils.renderJson(httpResponse, ex.getMessage());
			logger.error("", ex);
		}
	}

	/**
	 * 支付要动态变化的参数
	 * 
	 * @author nibili 2017年3月31日
	 * 
	 */
	public static class AlipayPayParam {

		/** 商户订单号，商户网站订单系统中唯一订单号，必填 */
		private String out_trade_no = "";

		/** 订单名称，必填 */
		private String subject = "";

		/** 付款金额，必填 */
		private String total_fee = "";

		/** 商品描述，可空 */
		private String body = "";

		/**
		 * get 商户订单号，商户网站订单系统中唯一订单号，必填
		 * 
		 * @return
		 * @author nibili 2017年4月6日
		 */
		public String getOut_trade_no() {
			return out_trade_no;
		}

		/**
		 * set 商户订单号，商户网站订单系统中唯一订单号，必填
		 * 
		 * @param out_trade_no
		 * @author nibili 2017年4月6日
		 */
		public void setOut_trade_no(String out_trade_no) {
			this.out_trade_no = out_trade_no;
		}

		/**
		 * get 订单名称，必填
		 * 
		 * @return
		 * @author nibili 2017年4月6日
		 */
		public String getSubject() {
			return subject;
		}

		/**
		 * set 订单名称，必填
		 * 
		 * @param subject
		 * @author nibili 2017年4月6日
		 */
		public void setSubject(String subject) {
			this.subject = subject;
		}

		/**
		 * get 付款金额，必填
		 * 
		 * @return
		 * @author nibili 2017年4月6日
		 */
		public String getTotal_fee() {
			return total_fee;
		}

		/**
		 * set 付款金额，必填
		 * 
		 * @param total_fee
		 * @author nibili 2017年4月6日
		 */
		public void setTotal_fee(String total_fee) {
			this.total_fee = total_fee;
		}

		/**
		 * get 商品描述，可空
		 * 
		 * @return
		 * @author nibili 2017年4月6日
		 */
		public String getBody() {
			return body;
		}

		/**
		 * set 商品描述，可空
		 * 
		 * @param body
		 * @author nibili 2017年4月6日
		 */
		public void setBody(String body) {
			this.body = body;
		}

	}
}
