package cn.com.easy.pay.weixinpay.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.com.easy.dto.MessageDTO;
import cn.com.easy.exception.BusinessException;
import cn.com.easy.pay.weixinpay.config.WeixinPayConfig;
import cn.com.easy.utils.ResponseOutputUtils;

import com.tencent.business.RefundQueryBusiness;
import com.tencent.common.Configure;
import com.tencent.protocol.refund_query_protocol.RefundQueryReqData;
import com.tencent.protocol.refund_query_protocol.RefundQueryResData;

/**
 * 微信支付 退款查询
 * 
 * @author nibili 2017年5月12日
 * 
 */
public abstract class WeixinPayRefundQueryBaseController {

	private Logger logger = LoggerFactory.getLogger(WeixinPayRefundQueryBaseController.class);

	/** 刷卡支付sdk,退款查询服务 */
	private RefundQueryBusiness refundQueryBusiness;

	/**
	 * 获取微信支付配置<br/>
	 * 
	 * @param httpRequest
	 * @return
	 * @throws BusinessException
	 * @author nibili 2017年5月12日
	 */
	public abstract WeixinPayConfig getWeixinPayConfig(HttpServletRequest httpRequest) throws BusinessException;

	/**
	 * 获取退款参数
	 * 
	 * @param httpRequest
	 * @return
	 * @throws BusinessException
	 * @author nibili 2017年5月12日
	 */
	public abstract RefundQueryReqData getRefundQueryReqData(Configure configure, HttpServletRequest httpRequest) throws BusinessException;

	/**
	 * 
	 * @param request
	 * @param response
	 * @param authCode
	 * @author nibili 2017年5月12日
	 */
	@RequestMapping("/refund")
	public void refund(final HttpServletRequest request, final HttpServletResponse response, String authCode) {
		try {
			WeixinPayConfig weixinPayConfig = getWeixinPayConfig(request);
			Configure configure = new Configure();
			configure.setKey(weixinPayConfig.getAppkey());
			configure.setAppID(weixinPayConfig.getAppid());
			configure.setMchID(weixinPayConfig.getMch_id());
			configure.setSubMchID(weixinPayConfig.getSubMchID());
			configure.setCertLocalPath(weixinPayConfig.getCertLocalPath());
			configure.setCertPassword(weixinPayConfig.getCertPassword());
			refundQueryBusiness = new RefundQueryBusiness(configure);
			//
			RefundQueryReqData refundQueryReqData = getRefundQueryReqData(configure, request);
			//
			refundQueryBusiness.run(refundQueryReqData, new RefundQueryBusiness.ResultListener() {

				@Override
				public void onFailByReturnCodeError(RefundQueryResData refundQueryResData) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onFailByReturnCodeFail(RefundQueryResData refundQueryResData) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onFailBySignInvalid(RefundQueryResData refundQueryResData) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onRefundQueryFail(RefundQueryResData refundQueryResData) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onRefundQuerySuccess(RefundQueryResData refundQueryResData) {
					// TODO Auto-generated method stub

				}
			});
		} catch (BusinessException ex) {
			logger.error("微信退款发生异常(自定义异常):" + ex.getMessage(), ex);
			// 错误
			ResponseOutputUtils.renderJson(response, MessageDTO.newInstance("", false, ex.getMessage()));
		} catch (Exception ex) {
			logger.error("微信退款发生异常:" + ex.getMessage(), ex);
			// 错误
			ResponseOutputUtils.renderJson(response, MessageDTO.newInstance("", false, ex.getMessage()));
		}

	}

	/**
	 * API返回ReturnCode不合法，支付请求逻辑错误，请仔细检测传过去的每一个参数是否合法，或是看API能否被正常访问 <br/>
	 * 例：请检查每一个参数是否合法
	 * 
	 * @param refundResData
	 * @author nibili 2017年5月12日
	 */
	public abstract void onFailByReturnCodeError(HttpServletRequest request, HttpServletResponse response, RefundQueryReqData refundQueryReqData);

	/**
	 * API返回ReturnCode为FAIL，支付API系统返回失败，请检测Post给API的数据是否规范合法 <br/>
	 * 例 ： 请检测Post给API的数据是否规范合法
	 * 
	 * @param request
	 * @param response
	 * @param scanPayResData
	 * @author nibili 2017年5月12日
	 */
	public abstract void onFailByReturnCodeFail(HttpServletRequest request, HttpServletResponse response, RefundQueryReqData refundQueryReqData);

	/**
	 * 支付请求API返回的数据签名验证失败，有可能数据被篡改了<br/>
	 * 例：返回签名校验失败，此条数据疑似被篡改
	 * 
	 * @param request
	 * @param response
	 * @param scanPayResData
	 * @author nibili 2017年5月12日
	 */
	public abstract void onFailBySignInvalid(HttpServletRequest request, HttpServletResponse response, RefundQueryResData refundQueryResData);

	/**
	 * 退款查询失败
	 * 
	 * @param request
	 * @param response
	 * @param refundQueryResData
	 * @author nibili 2017年5月12日
	 */
	public abstract void onRefundQueryFail(HttpServletRequest request, HttpServletResponse response, RefundQueryResData refundQueryResData);

	/**
	 * 退款成功后，信息会在这里返回
	 * 
	 * @param request
	 * @param response
	 * @param refundQueryResData
	 * @author nibili 2017年5月12日
	 */
	public abstract void onRefundQuerySuccess(HttpServletRequest request, HttpServletResponse response, RefundQueryResData refundQueryResData);
}
