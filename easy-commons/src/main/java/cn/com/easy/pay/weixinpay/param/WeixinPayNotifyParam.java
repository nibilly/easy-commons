package cn.com.easy.pay.weixinpay.param;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 微信支付异步回调返回参数
 * 
 * @author nibili 2017年5月8日
 * 
 */
@XmlRootElement(name = "xml")
public class WeixinPayNotifyParam {

	/**
	 * 返回状态码 是 String(16) SUCCESS SUCCESS/FAIL
	 * 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
	 */
	private String return_code;
	/** 返回信息 否 String(128) 签名失败 返回信息，如非空，为错误原因 签名失败 参数格式校验错误 */
	private String return_msg;
	/** 应用ID 是 String(32) wx8888888888888888 微信开放平台审核通过的应用APPID */
	private String appid;
	/** 商户号 是 String(32) 1900000109 微信支付分配的商户号 */
	private String mch_id;
	/** 设备号 否 String(32) 013467007045764 微信支付分配的终端设备号， */
	private String device_info;
	/** 随机字符串 是 String(32) 5K8264ILTKCH16CQ2502SI8ZNMTM67VS 随机字符串，不长于32位 */
	private String nonce_str;
	/** 签名 是 String(32) C380BEC2BFD727A4B6845133519F3AD6 签名，详见签名算法 */
	private String sign;
	/** 业务结果 是 String(16) SUCCESS SUCCESS/FAIL */
	private String result_code;
	/** 错误代码 否 String(32) SYSTEMERROR 错误返回的信息描述 */
	private String err_code;
	/** 错误代码描述 否 String(128) 系统错误 错误返回的信息描述 */
	private String err_code_des;
	/** 用户标识 是 String(128) wxd930ea5d5a258f4f 用户在商户appid下的唯一标识 */
	private String openid;
	/** 是否关注公众账号 否 String(1) Y 用户是否关注公众账号，Y-关注，N-未关注，仅在公众账号类型支付有效 */
	private String is_subscribe;
	/** 交易类型 是 String(16) APP APP */
	private String trade_type;
	/** 付款银行 是 String(16) CMC 银行类型，采用字符串类型的银行标识，银行类型见银行列表 */
	private String bank_type;
	/** 总金额 是 Int 100 订单总金额，单位为分 */
	private String total_fee;
	/** 货币种类 否 String(8) CNY 货币类型，符合ISO4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型 */
	private String fee_type;
	/** 现金支付金额 是 Int 100 现金支付金额订单现金支付金额，详见支付金额 */
	private int cash_fee;
	/** 现金支付货币类型 否 String(16) CNY 货币类型，符合ISO4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型 */
	private String cash_fee_type;
	/** 代金券金额 否 Int 10 代金券或立减优惠金额<=订单总金额，订单总金额-代金券或立减优惠金额=现金支付金额，详见支付金额 */
	private int coupon_fee;
	/** 代金券使用数量 否 Int 1 代金券或立减优惠使用数量 */
	private int coupon_count;
	/** 微信支付订单号 是 String(32) 1217752501201407033233368018 微信支付订单号 */
	private String transaction_id;
	/**
	 * 商户订单号 是 String(32) 1212321211201407033568112322
	 * 商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@ ，且在同一个商户号下唯一。
	 */
	private String out_trade_no;
	/** 商家数据包 否 String(128) 123456 商家数据包，原样返回 */
	private String attach;
	/**
	 * 支付完成时间 是 String(14) 20141030133525
	 * 支付完成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则
	 */
	private String time_end;

	/**
	 * get 返回状态码是String(16)SUCCESSSUCCESSFAIL此字段是通信标识，非交易标识，
	 * 交易是否成功需要查看result_code来判断
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getReturn_code() {
		return return_code;
	}

	/**
	 * set 返回状态码是String(16)SUCCESSSUCCESSFAIL此字段是通信标识，非交易标识，
	 * 交易是否成功需要查看result_code来判断
	 * 
	 * @param return_code
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setReturn_code(String return_code) {
		this.return_code = return_code;
	}

	/**
	 * get 返回信息否String(128)签名失败返回信息，如非空，为错误原因签名失败参数格式校验错误
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getReturn_msg() {
		return return_msg;
	}

	/**
	 * set 返回信息否String(128)签名失败返回信息，如非空，为错误原因签名失败参数格式校验错误
	 * 
	 * @param return_msg
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setReturn_msg(String return_msg) {
		this.return_msg = return_msg;
	}

	/**
	 * get 应用ID是String(32)wx8888888888888888微信开放平台审核通过的应用APPID
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getAppid() {
		return appid;
	}

	/**
	 * set 应用ID是String(32)wx8888888888888888微信开放平台审核通过的应用APPID
	 * 
	 * @param appid
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setAppid(String appid) {
		this.appid = appid;
	}

	/**
	 * get 商户号是String(32)1900000109微信支付分配的商户号
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getMch_id() {
		return mch_id;
	}

	/**
	 * set 商户号是String(32)1900000109微信支付分配的商户号
	 * 
	 * @param mch_id
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

	/**
	 * get 设备号否String(32)013467007045764微信支付分配的终端设备号，
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getDevice_info() {
		return device_info;
	}

	/**
	 * set 设备号否String(32)013467007045764微信支付分配的终端设备号，
	 * 
	 * @param device_info
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}

	/**
	 * get 随机字符串是String(32)5K8264ILTKCH16CQ2502SI8ZNMTM67VS随机字符串，不长于32位
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getNonce_str() {
		return nonce_str;
	}

	/**
	 * set 随机字符串是String(32)5K8264ILTKCH16CQ2502SI8ZNMTM67VS随机字符串，不长于32位
	 * 
	 * @param nonce_str
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	/**
	 * get 签名是String(32)C380BEC2BFD727A4B6845133519F3AD6签名，详见签名算法
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * set 签名是String(32)C380BEC2BFD727A4B6845133519F3AD6签名，详见签名算法
	 * 
	 * @param sign
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setSign(String sign) {
		this.sign = sign;
	}

	/**
	 * get 业务结果是String(16)SUCCESSSUCCESSFAIL
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getResult_code() {
		return result_code;
	}

	/**
	 * set 业务结果是String(16)SUCCESSSUCCESSFAIL
	 * 
	 * @param result_code
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}

	/**
	 * get 错误代码否String(32)SYSTEMERROR错误返回的信息描述
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getErr_code() {
		return err_code;
	}

	/**
	 * set 错误代码否String(32)SYSTEMERROR错误返回的信息描述
	 * 
	 * @param err_code
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setErr_code(String err_code) {
		this.err_code = err_code;
	}

	/**
	 * get 错误代码描述否String(128)系统错误错误返回的信息描述
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getErr_code_des() {
		return err_code_des;
	}

	/**
	 * set 错误代码描述否String(128)系统错误错误返回的信息描述
	 * 
	 * @param err_code_des
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setErr_code_des(String err_code_des) {
		this.err_code_des = err_code_des;
	}

	/**
	 * get 用户标识是String(128)wxd930ea5d5a258f4f用户在商户appid下的唯一标识
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getOpenid() {
		return openid;
	}

	/**
	 * set 用户标识是String(128)wxd930ea5d5a258f4f用户在商户appid下的唯一标识
	 * 
	 * @param openid
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setOpenid(String openid) {
		this.openid = openid;
	}

	/**
	 * get 是否关注公众账号否String(1)Y用户是否关注公众账号，Y-关注，N-未关注，仅在公众账号类型支付有效
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getIs_subscribe() {
		return is_subscribe;
	}

	/**
	 * set 是否关注公众账号否String(1)Y用户是否关注公众账号，Y-关注，N-未关注，仅在公众账号类型支付有效
	 * 
	 * @param is_subscribe
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setIs_subscribe(String is_subscribe) {
		this.is_subscribe = is_subscribe;
	}

	/**
	 * get 交易类型是String(16)APPAPP
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getTrade_type() {
		return trade_type;
	}

	/**
	 * set 交易类型是String(16)APPAPP
	 * 
	 * @param trade_type
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}

	/**
	 * get 付款银行是String(16)CMC银行类型，采用字符串类型的银行标识，银行类型见银行列表
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getBank_type() {
		return bank_type;
	}

	/**
	 * set 付款银行是String(16)CMC银行类型，采用字符串类型的银行标识，银行类型见银行列表
	 * 
	 * @param bank_type
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setBank_type(String bank_type) {
		this.bank_type = bank_type;
	}

	/**
	 * get 总金额是Int100订单总金额，单位为分
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getTotal_fee() {
		return total_fee;
	}

	/**
	 * set 总金额是Int100订单总金额，单位为分
	 * 
	 * @param total_fee
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}

	/**
	 * get 货币种类否String(8)CNY货币类型，符合ISO4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getFee_type() {
		return fee_type;
	}

	/**
	 * set 货币种类否String(8)CNY货币类型，符合ISO4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	 * 
	 * @param fee_type
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setFee_type(String fee_type) {
		this.fee_type = fee_type;
	}

	/**
	 * get 现金支付金额是Int100现金支付金额订单现金支付金额，详见支付金额
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public int getCash_fee() {
		return cash_fee;
	}

	/**
	 * set 现金支付金额是Int100现金支付金额订单现金支付金额，详见支付金额
	 * 
	 * @param cash_fee
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setCash_fee(int cash_fee) {
		this.cash_fee = cash_fee;
	}

	/**
	 * get 现金支付货币类型否String(16)CNY货币类型，符合ISO4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getCash_fee_type() {
		return cash_fee_type;
	}

	/**
	 * set 现金支付货币类型否String(16)CNY货币类型，符合ISO4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	 * 
	 * @param cash_fee_type
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setCash_fee_type(String cash_fee_type) {
		this.cash_fee_type = cash_fee_type;
	}

	/**
	 * get 代金券金额否Int10代金券或立减优惠金额<=订单总金额，订单总金额-代金券或立减优惠金额=现金支付金额，详见支付金额
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public int getCoupon_fee() {
		return coupon_fee;
	}

	/**
	 * set 代金券金额否Int10代金券或立减优惠金额<=订单总金额，订单总金额-代金券或立减优惠金额=现金支付金额，详见支付金额
	 * 
	 * @param coupon_fee
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setCoupon_fee(int coupon_fee) {
		this.coupon_fee = coupon_fee;
	}

	/**
	 * get 代金券使用数量否Int1代金券或立减优惠使用数量
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public int getCoupon_count() {
		return coupon_count;
	}

	/**
	 * set 代金券使用数量否Int1代金券或立减优惠使用数量
	 * 
	 * @param coupon_count
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setCoupon_count(int coupon_count) {
		this.coupon_count = coupon_count;
	}

	/**
	 * get 微信支付订单号是String(32)1217752501201407033233368018微信支付订单号
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getTransaction_id() {
		return transaction_id;
	}

	/**
	 * set 微信支付订单号是String(32)1217752501201407033233368018微信支付订单号
	 * 
	 * @param transaction_id
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	/**
	 * get 商户订单号是String(32)1212321211201407033568112322商户系统内部订单号，要求32个字符内，只能是数字、
	 * 大小写字母_-|@，且在同一个商户号下唯一。
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getOut_trade_no() {
		return out_trade_no;
	}

	/**
	 * set 商户订单号是String(32)1212321211201407033568112322商户系统内部订单号，要求32个字符内，只能是数字、
	 * 大小写字母_-|@，且在同一个商户号下唯一。
	 * 
	 * @param out_trade_no
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	/**
	 * get 商家数据包否String(128)123456商家数据包，原样返回
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getAttach() {
		return attach;
	}

	/**
	 * set 商家数据包否String(128)123456商家数据包，原样返回
	 * 
	 * @param attach
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setAttach(String attach) {
		this.attach = attach;
	}

	/**
	 * get 支付完成时间是String(14)20141030133525支付完成时间，格式为yyyyMMddHHmmss，
	 * 如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getTime_end() {
		return time_end;
	}

	/**
	 * set 支付完成时间是String(14)20141030133525支付完成时间，格式为yyyyMMddHHmmss，
	 * 如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则
	 * 
	 * @param time_end
	 * @author nibili 2017年5月8日
	 */
	@XmlElement
	public void setTime_end(String time_end) {
		this.time_end = time_end;
	}

}
