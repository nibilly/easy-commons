package cn.com.easy.pay.alipay.www.config;

import cn.com.easy.pay.alipay.www.util.UtilDate;

/**
 * 类名：AlipayConfig 功能：基础配置类 详细：设置帐户有关信息及返回路径 版本：3.4 修改日期：2016-03-08 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 * 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {

	/** 即时到账支付服务，用于设置变量 service */
	public final static String SERVICE_PAY = "create_direct_pay_by_user";
	/** 有密退款服务，用于设置变量 service */
	public final static String SERVICE_REFUND_BY_PWD = "refund_fastpay_by_platform_pwd";
	/** 无密退款服务，用于设置变量 service */
	public final static String SERVICE_REFUND_NO_PWD = "refund_fastpay_by_platform_nopwd";

	/** ↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ */

	/**
	 * 合作身份者ID，签约账号，以2088开头由16位纯数字组成的字符串，查看地址：https://b.alipay.com/order/
	 * pidAndKey.htm
	 */
	public String partner = "2088521367961200";

	/** 收款支付宝账号，以2088开头由16位纯数字组成的字符串，一般情况下收款账号就是签约账号 */
	public String seller_id = partner;

	/**
	 * 商户的私钥,需要PKCS8格式，RSA公私钥生成：https://doc.open.alipay.com/doc2/detail.htm?spm=
	 * a219a.7629140.0.0.nBDxfy&treeId=58&articleId=103242&docType=1
	 */
	public String private_key = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAN2I1tvgZnBBgtgu8xpLaB33BFU0RhiB/IKIlu1ETsOpQzvSfxEWOdV4OA1/YUBtqHe8zaolaJU52usIPVAqh6wfCPIrlC0L77j+4MbbjRuG9LSEZEr1DmjPYZf9qkmucYiTjYvYV851JncgSGCq8Xt7fgg/BOc54Km3P3436JQlAgMBAAECgYBFZ32I3UC6oouPlwXqkv1HhgABgaMEQpUNgCVoqCHVAs5hi4zrmX789fD5qZKaUMCa4re8OoQNXFbNk4NB/lEKckaBA9hjvlMCGTuv5zaHAmyw/0mp9JbQBLMt1C9PCgIsa3HcXHu5lMocxo4OqUnxhl89MmhTfBeyr7bl6ROZgQJBAPgi+b/nDDn0/+MGEIqG8Wf2srNQvSfMVWPOJk9zkHjCUuacXh5v2jC+rD/uJcvXYnef+zv1XbRoxleh+va9/L0CQQDkjg1wGPWlW7i08X15fa7sBn4mucQvf8L7BZKa2UU82d75t/ntrG3zqg3/Y2Q5g8tO95dj2I2bEtwmMPfPFk+JAkAmwmfapv+M6g0ybBcXXF7HC4z8X4c8eTl5XpEzZezSQCBs703wWS5u4MnNLqjuv1UdNHss8XXSoW8h7yZhdmnNAkAtUMT1Tt2VlaP/rEnWhsbh4T+t6fMt/8TnBGnW7fE+aWw+Uu3o4+FBCJyR4GEEzk7HL2wCzfL1pj2kDN11X9URAkEAyEVyzlAhxB8kXtSMmnIe9iGVFryTirVmvRVX2fV53hcQtn2kMa9ZkZgfwss28Ht6d0rTal6Z/Z9DEIAxoirU8w==";

	/** 支付宝的公钥,查看地址：https://b.alipay.com/order/pidAndKey.htm */
	public String alipay_public_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";

	/** 服务器异步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 */
	public String notify_url = "";

	/** 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 */
	public String return_url = "";

	/** 签名方式 */
	public String sign_type = "RSA";

	/** 调试用，创建TXT日志文件夹路径，见AlipayCore.java类中的logResult(String sWord)打印方法。 */
	public String log_path = "/home/data/alipay/log";

	/** 字符编码格式 目前支持 gbk 或 utf-8 */
	public String input_charset = "utf-8";

	/** 支付类型 ，无需修改 */
	public String payment_type = "1";

	/** 调用的接口名，无需修改 */
	public String service = "create_direct_pay_by_user";

	// 退款日期 时间格式 yyyy-MM-dd HH:mm:ss
	public String refund_date = UtilDate.getDateFormatter();

	// 调用的接口名，无需修改
	// public static String service = "refund_fastpay_by_platform_pwd";

	/** ↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ */

	/** ↓↓↓↓↓↓↓↓↓↓ 请在这里配置防钓鱼信息，如果没开通防钓鱼功能，为空即可 ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ */

	/** 防钓鱼时间戳 若要使用请调用类文件submit中的query_timestamp函数 */
	public String anti_phishing_key = "";

	/** 客户端的IP地址 非局域网的外网IP地址，如：221.0.0.1 */
	public String exter_invoke_ip = "";

	/**
	 * get
	 * ↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓合作身份者ID，签约账号，以2088开头由16位纯数字组成的字符串
	 * ，查看地址：htt
	 * 
	 * @return
	 * @author nibili 2017年3月31日
	 */
	public String getPartner() {
		return partner;
	}

	/**
	 * set
	 * ↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓合作身份者ID，签约账号，以2088开头由16位纯数字组成的字符串
	 * ，查看地址：htt
	 * 
	 * @param partner
	 * @author nibili 2017年3月31日
	 */
	public void setPartner(String partner) {
		this.partner = partner;
	}

	/**
	 * get 收款支付宝账号，以2088开头由16位纯数字组成的字符串，一般情况下收款账号就是签约账号
	 * 
	 * @return
	 * @author nibili 2017年3月31日
	 */
	public String getSeller_id() {
		return seller_id;
	}

	/**
	 * set 收款支付宝账号，以2088开头由16位纯数字组成的字符串，一般情况下收款账号就是签约账号
	 * 
	 * @param seller_id
	 * @author nibili 2017年3月31日
	 */
	public void setSeller_id(String seller_id) {
		this.seller_id = seller_id;
	}

	/**
	 * get 商户的私钥需要PKCS8格式，RSA公私钥生成：https:doc.open.alipay.comdoc2detail.htm?spm=
	 * a219a.7629140.0.0.nBDxfy&treeId=58&articleId=103242&docType=1
	 * 
	 * @return
	 * @author nibili 2017年3月31日
	 */
	public String getPrivate_key() {
		return private_key;
	}

	/**
	 * set 商户的私钥需要PKCS8格式，RSA公私钥生成：https:doc.open.alipay.comdoc2detail.htm?spm=
	 * a219a.7629140.0.0.nBDxfy&treeId=58&articleId=103242&docType=1
	 * 
	 * @param private_key
	 * @author nibili 2017年3月31日
	 */
	public void setPrivate_key(String private_key) {
		this.private_key = private_key;
	}

	/**
	 * get 支付宝的公钥查看地址：https:b.alipay.comorderpidAndKey.htm
	 * 
	 * @return
	 * @author nibili 2017年3月31日
	 */
	public String getAlipay_public_key() {
		return alipay_public_key;
	}

	/**
	 * set 支付宝的公钥查看地址：https:b.alipay.comorderpidAndKey.htm
	 * 
	 * @param alipay_public_key
	 * @author nibili 2017年3月31日
	 */
	public void setAlipay_public_key(String alipay_public_key) {
		this.alipay_public_key = alipay_public_key;
	}

	/**
	 * get 服务器异步通知页面路径需http:格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	 * 
	 * @return
	 * @author nibili 2017年3月31日
	 */
	public String getNotify_url() {
		return notify_url;
	}

	/**
	 * set 服务器异步通知页面路径需http:格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	 * 
	 * @param notify_url
	 * @author nibili 2017年3月31日
	 */
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	/**
	 * get 页面跳转同步通知页面路径需http:格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	 * 
	 * @return
	 * @author nibili 2017年3月31日
	 */
	public String getReturn_url() {
		return return_url;
	}

	/**
	 * set 页面跳转同步通知页面路径需http:格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	 * 
	 * @param return_url
	 * @author nibili 2017年3月31日
	 */
	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}

	/**
	 * get 签名方式
	 * 
	 * @return
	 * @author nibili 2017年3月31日
	 */
	public String getSign_type() {
		return sign_type;
	}

	/**
	 * set 签名方式
	 * 
	 * @param sign_type
	 * @author nibili 2017年3月31日
	 */
	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}

	/**
	 * get 调试用，创建TXT日志文件夹路径，见AlipayCore.java类中的logResult(StringsWord)打印方法。
	 * 
	 * @return
	 * @author nibili 2017年3月31日
	 */
	public String getLog_path() {
		return log_path;
	}

	/**
	 * set 调试用，创建TXT日志文件夹路径，见AlipayCore.java类中的logResult(StringsWord)打印方法。
	 * 
	 * @param log_path
	 * @author nibili 2017年3月31日
	 */
	public void setLog_path(String log_path) {
		this.log_path = log_path;
	}

	/**
	 * get 字符编码格式目前支持gbk或utf-8
	 * 
	 * @return
	 * @author nibili 2017年3月31日
	 */
	public String getInput_charset() {
		return input_charset;
	}

	/**
	 * set 字符编码格式目前支持gbk或utf-8
	 * 
	 * @param input_charset
	 * @author nibili 2017年3月31日
	 */
	public void setInput_charset(String input_charset) {
		this.input_charset = input_charset;
	}

	/**
	 * get 支付类型，无需修改
	 * 
	 * @return
	 * @author nibili 2017年3月31日
	 */
	public String getPayment_type() {
		return payment_type;
	}

	/**
	 * set 支付类型，无需修改
	 * 
	 * @param payment_type
	 * @author nibili 2017年3月31日
	 */
	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}

	/**
	 * get 调用的接口名，无需修改
	 * 
	 * @return
	 * @author nibili 2017年3月31日
	 */
	public String getService() {
		return service;
	}

	/**
	 * set 调用的接口名，无需修改
	 * 
	 * @param service
	 * @author nibili 2017年3月31日
	 */
	public void setService(String service) {
		this.service = service;
	}

	/**
	 * get ↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
	 * 
	 * @return
	 * @author nibili 2017年3月31日
	 */
	public String getAnti_phishing_key() {
		return anti_phishing_key;
	}

	/**
	 * set ↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
	 * 
	 * @param anti_phishing_key
	 * @author nibili 2017年3月31日
	 */
	public void setAnti_phishing_key(String anti_phishing_key) {
		this.anti_phishing_key = anti_phishing_key;
	}

	/**
	 * get 客户端的IP地址非局域网的外网IP地址，如：221.0.0.1
	 * 
	 * @return
	 * @author nibili 2017年3月31日
	 */
	public String getExter_invoke_ip() {
		return exter_invoke_ip;
	}

	/**
	 * set 客户端的IP地址非局域网的外网IP地址，如：221.0.0.1
	 * 
	 * @param exter_invoke_ip
	 * @author nibili 2017年3月31日
	 */
	public void setExter_invoke_ip(String exter_invoke_ip) {
		this.exter_invoke_ip = exter_invoke_ip;
	}

	/**
	 * get refund_date
	 * 
	 * @return
	 * @author nibili 2017年4月1日
	 */
	public String getRefund_date() {
		return refund_date;
	}

	/**
	 * set refund_date
	 * 
	 * @param refund_date
	 * @author nibili 2017年4月1日
	 */
	public void setRefund_date(String refund_date) {
		this.refund_date = refund_date;
	}

	/** ↑↑↑↑↑↑↑↑↑↑请在这里配置防钓鱼信息，如果没开通防钓鱼功能，为空即可 ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ */

}
