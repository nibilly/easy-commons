package cn.com.easy.pay.weixinpay.config;

import org.apache.commons.lang3.StringUtils;

import cn.com.easy.exception.BusinessException;

/**
 * 微信支付配置
 * 
 * @author nibili 2017年5月7日
 * 
 */
public class WeixinPayConfig {

	/** 应用ID 是 String(32) wxd678efh567hg6787 微信开放平台审核通过的应用APPID */
	private String appid;
	/** 商户号 是 String(32) 1230000109 微信支付分配的商户号 */
	private String mch_id;
	/** 签名类型 否 String(32) HMAC-SHA256 签名类型，目前支持HMAC-SHA256和MD5，默认为MD5 */
	private String sign_type = "MD5";
	/** appkey */
	private String appkey;
	/**
	 * 通知地址 是 String(256) http://www.weixin.qq.com/wxpay/pay.php
	 * 接收微信支付异步通知回调地址，通知url必须为直接可访问的url，不能携带参数。 <br/>
	 * 可参考：alipayConfig.setNotify_url(WebUtils.getHttpDomainBase(httpRequest) +
	 * "/alipay/app/notify");
	 */
	private String notify_url;
	/** 交易类型 是 String(16) APP 支付类型 */
	private String trade_type;
	/** 指定支付方式 否 String(32) no_credit no_credit--指定不能使用信用卡支付 */
	private String limit_pay;

	/** 受理模式下给子商户分配的子商户号 */
	private String subMchID = "";
	/** HTTPS证书的本地路径 */
	private String certLocalPath = "";
	/** HTTPS证书密码，默认密码等于商户号MCHID */
	private String certPassword = "";

	/**
	 * 必填参数
	 * 
	 * @param appid
	 * @param mch_id
	 * @param appkey
	 * @param trade_type
	 * @throws BusinessException
	 */
	public WeixinPayConfig(String appid, String mch_id, String appkey, String trade_type) throws BusinessException {
		this.appid = appid;
		this.mch_id = mch_id;
		this.appkey = appkey;
		this.trade_type = trade_type;
		verify();
	}

	/**
	 * 统一下单时notify_url必传
	 * 
	 * @param appid
	 * @param mch_id
	 * @param appkey
	 * @param trade_type
	 * @param notify_url
	 * @throws BusinessException
	 */
	public WeixinPayConfig(String appid, String mch_id, String appkey, String trade_type, String notify_url) throws BusinessException {
		this.appid = appid;
		this.mch_id = mch_id;
		this.appkey = appkey;
		this.trade_type = trade_type;
		this.notify_url = notify_url;
		verify();
		if (StringUtils.isBlank(this.notify_url) == true) {
			throw new BusinessException("notify_url不能为空");
		}
	}

	/**
	 * 验证，抛出BusinessException异常
	 * 
	 * @author nibili 2017年5月8日
	 * @throws BusinessException
	 */
	public void verify() throws BusinessException {
		if (StringUtils.isBlank(appid) == true) {
			throw new BusinessException("应用ID不能为空");
		}
		if (StringUtils.isBlank(mch_id) == true) {
			throw new BusinessException("商户号不能为空");
		}
		if (StringUtils.isBlank(appkey) == true) {
			throw new BusinessException("appkey不能为空");
		}
		if (StringUtils.isBlank(trade_type) == true) {
			throw new BusinessException("交易类型不能为空");
		}
	}

	/**
	 * get 受理模式下给子商户分配的子商户号
	 * 
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public String getSubMchID() {
		return subMchID;
	}

	/**
	 * set 受理模式下给子商户分配的子商户号
	 * 
	 * @param subMchID
	 * @author nibili 2017年5月11日
	 */
	public void setSubMchID(String subMchID) {
		this.subMchID = subMchID;
	}

	/**
	 * get HTTPS证书的本地路径
	 * 
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public String getCertLocalPath() {
		return certLocalPath;
	}

	/**
	 * set HTTPS证书的本地路径
	 * 
	 * @param certLocalPath
	 * @author nibili 2017年5月11日
	 */
	public void setCertLocalPath(String certLocalPath) {
		this.certLocalPath = certLocalPath;
	}

	/**
	 * get HTTPS证书密码，默认密码等于商户号MCHID
	 * 
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public String getCertPassword() {
		return certPassword;
	}

	/**
	 * set HTTPS证书密码，默认密码等于商户号MCHID
	 * 
	 * @param certPassword
	 * @author nibili 2017年5月11日
	 */
	public void setCertPassword(String certPassword) {
		this.certPassword = certPassword;
	}

	/**
	 * get 应用ID是String(32)wxd678efh567hg6787微信开放平台审核通过的应用APPID
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getAppid() {
		return appid;
	}

	/**
	 * set 应用ID是String(32)wxd678efh567hg6787微信开放平台审核通过的应用APPID
	 * 
	 * @param appid
	 * @author nibili 2017年5月7日
	 */
	public void setAppid(String appid) {
		this.appid = appid;
	}

	/**
	 * get 商户号是String(32)1230000109微信支付分配的商户号
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getMch_id() {
		return mch_id;
	}

	/**
	 * set 商户号是String(32)1230000109微信支付分配的商户号
	 * 
	 * @param mch_id
	 * @author nibili 2017年5月7日
	 */
	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

	/**
	 * get 签名类型否String(32)HMAC-SHA256签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getSign_type() {
		return sign_type;
	}

	/**
	 * set 签名类型否String(32)HMAC-SHA256签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
	 * 
	 * @param sign_type
	 * @author nibili 2017年5月7日
	 */
	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}

	/**
	 * get appkey
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getAppkey() {
		return appkey;
	}

	/**
	 * set appkey
	 * 
	 * @param appkey
	 * @author nibili 2017年5月7日
	 */
	public void setAppkey(String appkey) {
		this.appkey = appkey;
	}

	/**
	 * get 通知地址是String(256)http:www.weixin.qq.comwxpaypay.php接收微信支付异步通知回调地址，
	 * 通知url必须为直接可访问的url，不能携带参数。
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getNotify_url() {
		return notify_url;
	}

	/**
	 * set 通知地址是String(256)http:www.weixin.qq.comwxpaypay.php接收微信支付异步通知回调地址，
	 * 通知url必须为直接可访问的url，不能携带参数。
	 * 
	 * @param notify_url
	 * @author nibili 2017年5月8日
	 */
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	/**
	 * get 交易类型是String(16)APP支付类型
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getTrade_type() {
		return trade_type;
	}

	/**
	 * set 交易类型是String(16)APP支付类型
	 * 
	 * @param trade_type
	 * @author nibili 2017年5月8日
	 */
	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}

	/**
	 * get 指定支付方式否String(32)no_creditno_credit--指定不能使用信用卡支付
	 * 
	 * @return
	 * @author nibili 2017年5月8日
	 */
	public String getLimit_pay() {
		return limit_pay;
	}

	/**
	 * set 指定支付方式否String(32)no_creditno_credit--指定不能使用信用卡支付
	 * 
	 * @param limit_pay
	 * @author nibili 2017年5月8日
	 */
	public void setLimit_pay(String limit_pay) {
		this.limit_pay = limit_pay;
	}

}
