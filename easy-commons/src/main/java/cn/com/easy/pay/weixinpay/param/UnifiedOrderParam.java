package cn.com.easy.pay.weixinpay.param;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 统一下单参数<br/>
 * 详参： https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_1
 * 
 * @author nibili 2017年5月7日
 * 
 */
@XmlRootElement(name = "xml")
public class UnifiedOrderParam {

	/** 应用ID 是 String(32) wxd678efh567hg6787 微信开放平台审核通过的应用APPID */
	private String appid;
	/** 商户号 是 String(32) 1230000109 微信支付分配的商户号 */
	private String mch_id;
	/** 设备号 否 String(32) 013467007045764 终端设备号(门店号或收银设备ID)，默认请传"WEB" */
	private String device_info;
	/**
	 * 随机字符串 是 String(32) 5K8264ILTKCH16CQ2502SI8ZNMTM67VS
	 * 随机字符串，不长于32位。推荐随机数生成算法
	 */
	private String nonce_str;
	/** 签名 是 String(32) C380BEC2BFD727A4B6845133519F3AD6 签名，详见签名生成算法 */
	private String sign;
	/** 签名类型 否 String(32) HMAC-SHA256 签名类型，目前支持HMAC-SHA256和MD5，默认为MD5 */
	private String sign_type;
	/**
	 * 商品描述 是 String(128) 腾讯充值中心-QQ会员充值 商品描述交易字段格式根据不同的应用场景按照以下格式：
	 * APP——需传入应用市场上的APP名字-实际商品名称，天天爱消除-游戏充值。
	 */
	private String body;
	/** 商品详情 否 String(8192) { } 商品详细列表，使用Json格式，传输签名前请务必使用CDATA标签将JSON文本串保护起来。 */
	private String detail;
	/** 附加数据 否 String(127) 深圳分店 附加数据，在查询API和支付通知中原样返回，该字段主要用于商户携带订单的自定义数据 */
	private String attach;
	/**
	 * 商户订单号 是 String(32) 20150806125346 商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@
	 * ，且在同一个商户号下唯一。详见商户订单号
	 */
	private String out_trade_no;
	/** 货币类型 否 String(16) CNY 符合ISO 4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型 */
	private String fee_type;
	/** 总金额 是 Int 888 订单总金额，单位为分，详见支付金额 */
	private Integer total_fee;
	/** 终端IP 是 String(16) 123.12.12.123 用户端实际ip */
	private String spbill_create_ip;
	/**
	 * 交易起始时间 否 String(14) 20091225091010
	 * 订单生成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则
	 */
	private String time_start;
	/**
	 * 交易结束时间 否 String(14) 20091227091010
	 * 订单失效时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010。其他详见时间规则
	 * 注意：最短失效时间间隔必须大于5分钟
	 */
	private String time_expire;
	/** 商品标记 否 String(32) WXG 商品标记，代金券或立减优惠功能的参数，说明详见代金券或立减优惠 */
	private String goods_tag;
	/**
	 * 通知地址 是 String(256) http://www.weixin.qq.com/wxpay/pay.php
	 * 接收微信支付异步通知回调地址，通知url必须为直接可访问的url，不能携带参数。
	 */
	private String notify_url;
	/** 交易类型 是 String(16) APP 支付类型 */
	private String trade_type;
	/** 指定支付方式 否 String(32) no_credit no_credit--指定不能使用信用卡支付 */
	private String limit_pay;

	/**
	 * 用户标识 否 String(128) oUpF8uMuAJO_M2pxb1Q9zNjWeS6o
	 * trade_type=JSAPI时（即公众号支付），
	 * 此参数必传，此参数为微信用户在商户对应appid下的唯一标识。openid如何获取，可参考【获取openid
	 * 】。企业号请使用【企业号OAuth2.0接口】获取企业号内成员userid，再调用【企业号userid转openid接口】进行转换
	 */
	private String openid;

	/**
	 * get
	 * 用户标识否String(128)oUpF8uMuAJO_M2pxb1Q9zNjWeS6otrade_type=JSAPI时（即公众号支付），
	 * 此参数必传
	 * ，此参数为微信用户在商户对应appid下的唯一标识。openid如何获取，可参考【获取openid】。企业号请使用【企业号OAuth2.0
	 * 接口】获取企业号内成员userid
	 * ，再调用【企业号userid转用户标识否String(128)oUpF8uMuAJO_M2pxb1Q9zNjWeS6otrade_type
	 * =JSAPI时
	 * （即公众号支付），此参数必传，此参数为微信用户在商户对应appid下的唯一标识。openid如何获取，可参考【获取openid】。企业号请使用
	 * 【企业号OAuth2.0接口】获取企业号内成员userid，再调用【企业号userid转openid接口】进行转换接口】进行转换
	 * 
	 * @return
	 * @author nibili 2017年5月10日
	 */
	public String getOpenid() {
		return openid;
	}

	/**
	 * set
	 * 用户标识否String(128)oUpF8uMuAJO_M2pxb1Q9zNjWeS6otrade_type=JSAPI时（即公众号支付），
	 * 此参数必传
	 * ，此参数为微信用户在商户对应appid下的唯一标识。openid如何获取，可参考【获取openid】。企业号请使用【企业号OAuth2.0
	 * 接口】获取企业号内成员userid，再调用【企业号userid转openid接口】进行转换
	 * 
	 * @param 用户标识否String
	 *            (128)oUpF8uMuAJO_M2pxb1Q9zNjWeS6otrade_type=JSAPI时（即公众号支付），
	 *            此参数必传
	 *            ，此参数为微信用户在商户对应appid下的唯一标识。openid如何获取，可参考【获取openid】。企业号请使用【
	 *            企业号OAuth2.0接口】获取企业号内成员userid，再调用【企业号userid转openid接口】进行转换
	 * @author nibili 2017年5月10日
	 */
	@XmlElement
	public void setOpenid(String openid) {
		this.openid = openid;
	}

	/**
	 * get 应用ID是String(32)wxd678efh567hg6787微信开放平台审核通过的应用APPID
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getAppid() {
		return appid;
	}

	/**
	 * set 应用ID是String(32)wxd678efh567hg6787微信开放平台审核通过的应用APPID
	 * 
	 * @param appid
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setAppid(String appid) {
		this.appid = appid;
	}

	/**
	 * get 商户号是String(32)1230000109微信支付分配的商户号
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getMch_id() {
		return mch_id;
	}

	/**
	 * set 商户号是String(32)1230000109微信支付分配的商户号
	 * 
	 * @param mch_id
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

	/**
	 * get 设备号否String(32)013467007045764终端设备号(门店号或收银设备ID)，默认请传"WEB"
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getDevice_info() {
		return device_info;
	}

	/**
	 * set 设备号否String(32)013467007045764终端设备号(门店号或收银设备ID)，默认请传"WEB"
	 * 
	 * @param device_info
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}

	/**
	 * get
	 * 随机字符串是String(32)5K8264ILTKCH16CQ2502SI8ZNMTM67VS随机字符串，不长于32位。推荐随机数生成算法
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getNonce_str() {
		return nonce_str;
	}

	/**
	 * set
	 * 随机字符串是String(32)5K8264ILTKCH16CQ2502SI8ZNMTM67VS随机字符串，不长于32位。推荐随机数生成算法
	 * 
	 * @param nonce_str
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	/**
	 * get 签名是String(32)C380BEC2BFD727A4B6845133519F3AD6签名，详见签名生成算法
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * set 签名是String(32)C380BEC2BFD727A4B6845133519F3AD6签名，详见签名生成算法
	 * 
	 * @param sign
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setSign(String sign) {
		this.sign = sign;
	}

	/**
	 * get 签名类型否String(32)HMAC-SHA256签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getSign_type() {
		return sign_type;
	}

	/**
	 * set 签名类型否String(32)HMAC-SHA256签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
	 * 
	 * @param sign_type
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}

	/**
	 * get 商品描述是String(128)腾讯充值中心-QQ会员充值商品描述交易字段格式根据不同的应用场景按照以下格式：APP——
	 * 需传入应用市场上的APP名字-实际商品名称，天天爱消除-游戏充值。
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getBody() {
		return body;
	}

	/**
	 * set 商品描述是String(128)腾讯充值中心-QQ会员充值商品描述交易字段格式根据不同的应用场景按照以下格式：APP——
	 * 需传入应用市场上的APP名字-实际商品名称，天天爱消除-游戏充值。
	 * 
	 * @param body
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * get 商品详情否String(8192){}商品详细列表，使用Json格式，传输签名前请务必使用CDATA标签将JSON文本串保护起来。
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getDetail() {
		return detail;
	}

	/**
	 * set 商品详情否String(8192){}商品详细列表，使用Json格式，传输签名前请务必使用CDATA标签将JSON文本串保护起来。
	 * 
	 * @param detail
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setDetail(String detail) {
		this.detail = detail;
	}

	/**
	 * get 附加数据否String(127)深圳分店附加数据，在查询API和支付通知中原样返回，该字段主要用于商户携带订单的自定义数据
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getAttach() {
		return attach;
	}

	/**
	 * set 附加数据否String(127)深圳分店附加数据，在查询API和支付通知中原样返回，该字段主要用于商户携带订单的自定义数据
	 * 
	 * @param attach
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setAttach(String attach) {
		this.attach = attach;
	}

	/**
	 * get 商户订单号是String(32)20150806125346商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|@，
	 * 且在同一个商户号下唯一。详见商户订单号
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getOut_trade_no() {
		return out_trade_no;
	}

	/**
	 * set 商户订单号是String(32)20150806125346商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|@，
	 * 且在同一个商户号下唯一。详见商户订单号
	 * 
	 * @param out_trade_no
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	/**
	 * get 货币类型否String(16)CNY符合ISO4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getFee_type() {
		return fee_type;
	}

	/**
	 * set 货币类型否String(16)CNY符合ISO4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	 * 
	 * @param fee_type
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setFee_type(String fee_type) {
		this.fee_type = fee_type;
	}

	/**
	 * get 总金额是Int888订单总金额，单位为分，详见支付金额
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public Integer getTotal_fee() {
		return total_fee;
	}

	/**
	 * set 总金额是Int888订单总金额，单位为分，详见支付金额
	 * 
	 * @param total_fee
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setTotal_fee(Integer total_fee) {
		this.total_fee = total_fee;
	}

	/**
	 * get 终端IP是String(16)123.12.12.123用户端实际ip
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}

	/**
	 * set 终端IP是String(16)123.12.12.123用户端实际ip
	 * 
	 * @param spbill_create_ip
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setSpbill_create_ip(String spbill_create_ip) {
		this.spbill_create_ip = spbill_create_ip;
	}

	/**
	 * get 交易起始时间否String(14)20091225091010订单生成时间，格式为yyyyMMddHHmmss，
	 * 如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getTime_start() {
		return time_start;
	}

	/**
	 * set 交易起始时间否String(14)20091225091010订单生成时间，格式为yyyyMMddHHmmss，
	 * 如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则
	 * 
	 * @param time_start
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setTime_start(String time_start) {
		this.time_start = time_start;
	}

	/**
	 * get 交易结束时间否String(14)20091227091010订单失效时间，格式为yyyyMMddHHmmss，
	 * 如2009年12月27日9点10分10秒表示为20091227091010。其他详见时间规则注意：最短失效时间间隔必须大于5分钟
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getTime_expire() {
		return time_expire;
	}

	/**
	 * set 交易结束时间否String(14)20091227091010订单失效时间，格式为yyyyMMddHHmmss，
	 * 如2009年12月27日9点10分10秒表示为20091227091010。其他详见时间规则注意：最短失效时间间隔必须大于5分钟
	 * 
	 * @param time_expire
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setTime_expire(String time_expire) {
		this.time_expire = time_expire;
	}

	/**
	 * get 商品标记否String(32)WXG商品标记，代金券或立减优惠功能的参数，说明详见代金券或立减优惠
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getGoods_tag() {
		return goods_tag;
	}

	/**
	 * set 商品标记否String(32)WXG商品标记，代金券或立减优惠功能的参数，说明详见代金券或立减优惠
	 * 
	 * @param goods_tag
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setGoods_tag(String goods_tag) {
		this.goods_tag = goods_tag;
	}

	/**
	 * get 通知地址是String(256)http:www.weixin.qq.comwxpaypay.php接收微信支付异步通知回调地址，
	 * 通知url必须为直接可访问的url，不能携带参数。
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getNotify_url() {
		return notify_url;
	}

	/**
	 * set 通知地址是String(256)http:www.weixin.qq.comwxpaypay.php接收微信支付异步通知回调地址，
	 * 通知url必须为直接可访问的url，不能携带参数。
	 * 
	 * @param notify_url
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	/**
	 * get 交易类型是String(16)APP支付类型
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getTrade_type() {
		return trade_type;
	}

	/**
	 * set 交易类型是String(16)APP支付类型
	 * 
	 * @param trade_type
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}

	/**
	 * get 指定支付方式否String(32)no_creditno_credit--指定不能使用信用卡支付
	 * 
	 * @return
	 * @author nibili 2017年5月7日
	 */
	public String getLimit_pay() {
		return limit_pay;
	}

	/**
	 * set 指定支付方式否String(32)no_creditno_credit--指定不能使用信用卡支付
	 * 
	 * @param limit_pay
	 * @author nibili 2017年5月7日
	 */
	@XmlElement
	public void setLimit_pay(String limit_pay) {
		this.limit_pay = limit_pay;
	}

}
