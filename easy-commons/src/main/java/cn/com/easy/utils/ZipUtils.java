package cn.com.easy.utils;

import java.io.*;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.*;

import org.apache.commons.lang3.StringUtils;

/**
 * 提供了zip的压缩和解压方法
 */
public class ZipUtils {
	/**
	 * 文件夹压缩成ZIP
	 *
	 * @param srcDir           压缩文件夹路径
	 * @param out              压缩文件输出流
	 * @param KeepDirStructure 是否保留原来的目录结构,true:保留目录结构;
	 *                         false:所有文件跑到压缩包根目录下(注意：不保留目录结构可能会出现同名文件,会压缩失败)
	 * @throws RuntimeException 压缩失败会抛出运行时异常
	 */
	public static void toZip(String srcDir, OutputStream out, boolean KeepDirStructure) throws RuntimeException {
		long start = System.currentTimeMillis();
		ZipOutputStream zos = null;
		try {
			zos = new ZipOutputStream(out);
			File sourceFile = new File(srcDir);
			compress(sourceFile, zos, sourceFile.getName(), KeepDirStructure);
			long end = System.currentTimeMillis();
			System.out.println("压缩完成，耗时：" + (end - start) + " ms");
		} catch (Exception e) {
			throw new RuntimeException("zip error from ZipUtils", e);
		} finally {
			if (zos != null) {
				try {
					zos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 文件列表压缩成ZIP
	 *
	 * @param srcFiles 需要压缩的文件列表
	 * @param out      压缩文件输出流
	 * @throws RuntimeException 压缩失败会抛出运行时异常
	 */
	public static void toZip(List<File> srcFiles, OutputStream out) throws RuntimeException {
		long start = System.currentTimeMillis();
		ZipOutputStream zos = null;
		try {
			zos = new ZipOutputStream(out);
			for (File srcFile : srcFiles) {
				byte[] buf = new byte[1024];
				zos.putNextEntry(new ZipEntry(srcFile.getName()));
				int len;
				FileInputStream in = new FileInputStream(srcFile);
				while ((len = in.read(buf)) != -1) {
					zos.write(buf, 0, len);
				}
				zos.closeEntry();
				in.close();
			}
			long end = System.currentTimeMillis();
			System.out.println("压缩完成，耗时：" + (end - start) + " ms");
		} catch (Exception e) {
			throw new RuntimeException("zip error from ZipUtils", e);
		} finally {
			if (zos != null) {
				try {
					zos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 递归压缩方法
	 *
	 * @param sourceFile       源文件
	 * @param zos              zip输出流
	 * @param name             压缩后的名称
	 * @param KeepDirStructure 是否保留原来的目录结构,true:保留目录结构;
	 *                         false:所有文件跑到压缩包根目录下(注意：不保留目录结构可能会出现同名文件,会压缩失败)
	 * @throws Exception
	 */
	private static void compress(File sourceFile, ZipOutputStream zos, String name, boolean KeepDirStructure) throws Exception {
		byte[] buf = new byte[1024];
		if (sourceFile.isFile()) {
			// 向zip输出流中添加一个zip实体，构造器中name为zip实体的文件的名字
			zos.putNextEntry(new ZipEntry(name));
			// copy文件到zip输出流中
			int len;
			FileInputStream in = new FileInputStream(sourceFile);
			while ((len = in.read(buf)) != -1) {
				zos.write(buf, 0, len);
			}
			// Complete the entry
			zos.closeEntry();
			in.close();
		} else {
			File[] listFiles = sourceFile.listFiles();
			if (listFiles == null || listFiles.length == 0) {
				// 需要保留原来的文件结构时,需要对空文件夹进行处理
				if (KeepDirStructure) {
					// 空文件夹的处理
					zos.putNextEntry(new ZipEntry(name + "/"));
					// 没有文件，不需要文件的copy
					zos.closeEntry();
				}
			} else {
				for (File file : listFiles) {
					// 判断是否需要保留原来的文件结构
					if (KeepDirStructure) {
						// 注意：file.getName()前面需要带上父文件夹的名字加一斜杠,
						// 不然最后压缩包中就不能保留原来的文件结构,即：所有文件都跑到压缩包根目录下了
						compress(file, zos, name + "/" + file.getName(), KeepDirStructure);
					} else {
						compress(file, zos, file.getName(), KeepDirStructure);
					}

				}
			}
		}
	}

	/**
	 * 解压缩zip包
	 *
	 * @param zipFilePath        zip文件的全路径
	 * @param destPath           解压后的文件保存的路径
	 * @param includeZipFileName 解压后的文件保存的路径是否包含压缩文件的文件名。true-包含；false-不包含
	 */
	@SuppressWarnings("unchecked")
	public static void unzip(String zipFilePath, String destPath, boolean includeZipFileName) throws Exception {
		if (StringUtils.isEmpty(zipFilePath) || StringUtils.isEmpty(destPath)) {
			throw new Exception();
		}
		File zipFile = new File(zipFilePath);

		// 如果解压后的文件保存路径包含压缩文件的文件名，则追加该文件名到解压路径
		if (includeZipFileName) {
			String fileName = zipFile.getName();
			if (null != fileName && !"".equals(fileName)) {
				fileName = fileName.substring(0, fileName.lastIndexOf("."));
			}
			destPath = destPath + File.separator + fileName + File.separator;
		}

		File destPathFile = new File(destPath);
		// 创建文件夹
		if (!destPathFile.exists() || !destPathFile.isDirectory()) {
			destPathFile.mkdirs();
		}

		// 开始解压
		ZipFile zip = new ZipFile(zipFile);

		Enumeration<ZipEntry> entries = (Enumeration<ZipEntry>) zip.entries();
		// 循环对压缩包里的每一个文件进行解压
		while (entries.hasMoreElements()) {
			ZipEntry entry = entries.nextElement();
			String entryName = entry.getName().replace("/", File.separator);
			String destFilePath = destPath + File.separator + entryName;
			String outPath = destFilePath.substring(0, destFilePath.lastIndexOf(File.separator));
			File pathFile = new File(outPath);
			// 创建文件夹
			if (!pathFile.exists() || !pathFile.isDirectory()) {
				pathFile.mkdirs();
			}

			File destFile = new File(destFilePath);
			// 判断是否为文件夹
			if (destFile.isDirectory()) {
				continue;
			}

			// 删除已存在的文件
			/*
			 * if (destFile.exists()) { //检测文件是否允许删除，如果不允许删除，将会抛出SecurityException
			 *//*
				 * SecurityManager securityManager = new SecurityManager();
				 * securityManager.checkDelete(destFilePath);
				 *//*
					 * //删除已存在的目标文件 destFile.delete(); }
					 */

			// 写入文件
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destFile));
			InputStream inputStream = zip.getInputStream(entry);
			BufferedInputStream bis = new BufferedInputStream(inputStream);
			byte[] buffer = new byte[1024];
			int size = 0;
			while ((size = bis.read(buffer)) != -1) {
				bos.write(buffer, 0, size);
			}
			bos.flush();
			bos.close();
			bis.close();
			inputStream.close();
		}
		zip.close();
	}

	/**
	 * 测试方法 注意：操作时会覆盖重名文件
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// 1、压缩文件夹
			zipDirTest();
//			// 2、压缩文件列表
//			zipFileListTest();
//			// 3、解压测试
			unzipTest();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void unzipTest() throws Exception {
		String zipFilePath = "D:\\work\\project\\Clipboard\\Clipboard\\pakage-bin.zip";
		String unzipFilePath = "E:\\unzip";
		unzip(zipFilePath, unzipFilePath, false);
	}

	@SuppressWarnings("unused")
	private static void zipFileListTest() throws FileNotFoundException {
		String srcFile = "F:\\testzip\\1.txt";
		String destFile = "F:\\testzip\\1.zip";
		FileOutputStream fos = new FileOutputStream(destFile);
		toZip(Arrays.asList(new File(srcFile)), fos);
	}

	private static void zipDirTest() throws FileNotFoundException {
		String srcFileDir = "D:\\work\\project\\Clipboard\\Clipboard\\pakage-bin";
		String destFile = "D:\\work\\project\\Clipboard\\Clipboard\\pakage-bin.zip";
		FileOutputStream fos = new FileOutputStream(destFile);
		toZip(srcFileDir, fos, true);
	}
}