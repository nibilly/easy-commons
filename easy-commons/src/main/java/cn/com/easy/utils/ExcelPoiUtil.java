package cn.com.easy.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFPictureData;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFShape;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFPictureData;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;

/**
 * Excel导入导出工具
 * 
 * @author nibili 2017年3月23日
 * 
 */
@SuppressWarnings("deprecation")
public class ExcelPoiUtil {

	/*-------------------------------------导出-------------------------------------------*/

	/**
	 * @MethodName : listToExcel
	 * @Description : 导出Excel（导出到浏览器，工作表的大小是2003支持的最大值）
	 * @param list     数据源
	 * @param fieldMap 类的英文属性和Excel中的中文列名的对应关系
	 * @param response 使用response可以导出到浏览器
	 * @throws ExcelException
	 */
	public static <T> void listToExcel(List<T> list, List<CellDto> fieldMap, String sheetName, RowDto additionalFieldHeaderRowDto, HttpServletResponse response)
			throws ExcelException {

		listToExcel(list, fieldMap, sheetName, 65535, additionalFieldHeaderRowDto, response);
	}

	/**
	 * 
	 * @param list
	 * @param fieldMap
	 * @param sheetName
	 * @param sheetSize
	 * @param additionalFieldHeaderRowDto
	 * @param isOutputFieldCnNameToExcel
	 * @param response
	 * @throws ExcelException
	 * @auth nibili 2019年2月27日 196888813@qq.com
	 */
	public static <T> void listToExcel(List<T> list, List<CellDto> fieldMap, String sheetName, int sheetSize, RowDto additionalFieldHeaderRowDto,
			boolean isOutputFieldCnNameToExcel, HttpServletResponse response) throws ExcelException {

		// 设置默认文件名为当前时间：年月日时分秒
		String fileName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()).toString();

		// 设置response头信息
		response.reset();
		response.setContentType("application/vnd.ms-excel"); // 改成输出excel文件
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");

		// 创建工作簿并发送到浏览器
		try {

			OutputStream out = response.getOutputStream();
			listToExcel(list, fieldMap, sheetName, sheetSize, additionalFieldHeaderRowDto, "", 0, isOutputFieldCnNameToExcel, out);

		} catch (Exception e) {
			e.printStackTrace();

			// 如果是ExcelException，则直接抛出
			if (e instanceof ExcelException) {
				throw (ExcelException) e;

				// 否则将其它异常包装成ExcelException再抛出
			} else {
				throw new ExcelException("导出Excel失败");
			}
		}
	}

	/**
	 * @MethodName : listToExcel
	 * @Description : 导出Excel（导出到浏览器，可以自定义工作表的大小）
	 * @param list      数据源
	 * @param fieldMap  类的英文属性和Excel中的中文列名的对应关系
	 * @param sheetSize 每个工作表中记录的最大个数
	 * @param response  使用response可以导出到浏览器
	 * @throws ExcelException
	 */
	public static <T> void listToExcel(List<T> list, List<CellDto> fieldMap, String sheetName, int sheetSize, RowDto additionalFieldHeaderRowDto, HttpServletResponse response)
			throws ExcelException {
		listToExcel(list, fieldMap, sheetName, sheetSize, additionalFieldHeaderRowDto, true, response);
	}

	/**
	 * @MethodName : listToExcel
	 * @Description : 导出Excel（可以导出到本地文件系统，也可以导出到浏览器，工作表大小为2003支持的最大值）
	 * @param list                        数据源
	 * @param fieldMap                    类的英文属性和Excel中的中文列名的对应关系
	 * @param additionalFieldHeaderRowDto
	 * @param out                         导出流
	 * @throws ExcelException
	 */
	public static <T> void listToExcel(List<T> list, List<CellDto> fieldMap, String sheetName, RowDto additionalFieldHeaderRowDto, OutputStream out) throws ExcelException {

		listToExcel(list, fieldMap, sheetName, 65535, additionalFieldHeaderRowDto, "", 0, false, out);

	}

	/**
	 * @MethodName : listToExcel
	 * @Description : 导出Excel（可以导出到本地文件系统，也可以导出到浏览器，可自定义工作表大小）
	 * @param list                        数据源
	 * @param fieldMap                    类的英文属性和Excel中的中文列名的对应关系
	 *                                    如果需要的是引用对象的属性，则英文属性使用类似于EL表达式的格式
	 *                                    如：list中存放的都是student，student中又有college属性，而我们需要学院名称，则可以这样写
	 *                                    fieldMap.put("college.collegeName","学院名称")
	 * @param sheetName                   工作表的名称
	 * 
	 * @param sheetSize                   每个工作表中记录的最大个数
	 * 
	 * @param additionalFieldHeaderRowDto 第一行标题栏
	 * @param templatePath                行标题 模板
	 * @param templateHeaderRowTotal      取行标题 行数
	 * @param isOutputFieldCnNameToExcel  是否输入字段中文名到excel中
	 * @param out                         导出流
	 * @throws ExcelException
	 */
	public static <T> void listToExcel(List<T> list, List<CellDto> fieldMap, String sheetName, int sheetSize, RowDto additionalFieldHeaderRowDto, String templatePath,
			int templateHeaderRowTotal, boolean isOutputFieldCnNameToExcel, OutputStream out) throws ExcelException {

		if (list.size() == 0 || list == null) {
			throw new ExcelException("数据源中没有任何数据");
		}
		if (sheetSize > 65535 || sheetSize < 1) {
			sheetSize = 65535;
		}
		try {

			// 创建工作簿
			HSSFWorkbook wb;

			// 如果有模板文件
			if (StringUtils.isNotBlank(templatePath) == true) {
				// excel模板路径
				POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(new File(templatePath)));
				// 读取excel模板
				wb = new HSSFWorkbook(fs);
			} else {
				// 没有
				wb = new HSSFWorkbook();
			}

			// 因为2003的Excel一个工作表最多可以有65536条记录，除去列头剩下65535条
			// 所以如果记录太多，需要放到多个工作表中，其实就是个分页的过程
			// 1.计算一共有多少个工作表
			double sheetNum = Math.ceil(list.size() / new Integer(sheetSize).doubleValue());
			HSSFSheet sheet;
			// 2.创建相应的工作表，并向其中填充数据
			for (int i = 0; i < sheetNum; i++) {

				if (1 == sheetNum) {
					// 如果只有一个工作表的情况
					if (StringUtils.isNotBlank(templatePath) == true) {
						// 有模板文件
						sheet = wb.getSheetAt(i);
						if (sheet == null) {
							sheet = wb.createSheet(sheetName + (i + 1));
						}
					} else {
						sheet = wb.createSheet(sheetName);
					}
					// 添加模板表头
					fillSheet(wb, sheet, list, fieldMap, additionalFieldHeaderRowDto, 0, list.size() - 1, templatePath, templateHeaderRowTotal, isOutputFieldCnNameToExcel);
				} else {
					// 有多个工作表的情况
					if (StringUtils.isNotBlank(templatePath) == true) {
						// 有模板文件
						sheet = wb.getSheetAt(i);
						if (sheet == null) {
							sheet = wb.createSheet(sheetName + (i + 1));
						}
					} else {
						sheet = wb.createSheet(sheetName + (i + 1));
					}
					// 获取开始索引和结束索引
					int firstIndex = i * sheetSize;
					int lastIndex = (i + 1) * sheetSize - 1 > list.size() - 1 ? list.size() - 1 : (i + 1) * sheetSize - 1;
					// 填充工作表
					fillSheet(wb, sheet, list, fieldMap, additionalFieldHeaderRowDto, firstIndex, lastIndex, templatePath, templateHeaderRowTotal, isOutputFieldCnNameToExcel);
				}
			}
			wb.write(out);
		} catch (Exception e) {
			e.printStackTrace();
			// 如果是ExcelException，则直接抛出
			if (e instanceof ExcelException) {
				throw (ExcelException) e;
				// 否则将其它异常包装成ExcelException再抛出
			} else {
				throw new ExcelException("导出Excel失败");
			}
		}
	}

	public static void main(String[] args) throws Exception {

		String excelFilePath = "C:\\Users\\DELL\\Desktop\\dto\\探伤检测_标找找.xls";

		List<List<String>> rows = ExcelUtil.excelToList(excelFilePath, "Sheet1");

//		for (int i = 2, len = rows.size(); i < len; i++) {
		for (int i = 2, len = 100; i < len; i++) {
			try {
				System.out.println("序号：" + i);
				rows.get(i).add("测试");
//				Thread.sleep(50);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// 写入到excel
		ExcelPoiUtil.listToExcel(rows, null, "Sheet", 50, new FileOutputStream(new File("C:\\Users\\DELL\\Desktop\\dto\\探伤检测_标找找_1.xls")));

	}

	public static void listToExcel(List<List<String>> list, List<String> head, String sheetName, int sheetSize, OutputStream out) throws ExcelException {

		if (list.size() == 0 || list == null) {
			throw new ExcelException("数据源中没有任何数据");
		}
		if (sheetSize > 65535 || sheetSize < 1) {
			sheetSize = 65535;
		}
		try {

			// 创建工作簿
			HSSFWorkbook wb = new HSSFWorkbook();

			// 因为2003的Excel一个工作表最多可以有65536条记录，除去列头剩下65535条
			// 所以如果记录太多，需要放到多个工作表中，其实就是个分页的过程
			// 1.计算一共有多少个工作表
			double sheetNum = Math.ceil(list.size() / new Integer(sheetSize).doubleValue());
			HSSFSheet sheet;

			// 2.创建相应的工作表，并向其中填充数据
			for (int i = 0; i < sheetNum; i++) {
//
				if (1 == sheetNum) {
					//
					sheet = wb.createSheet(sheetName);
					// 开始行号
					int startRowNum = 0;
					// 添加抬头
					if (CollectionUtils.isNotEmpty(head)) {
						addOneRow(sheet, startRowNum, head);
						startRowNum++;
					}
					for (int k = 0, len = list.size(); k < len && k < sheetSize; k++) {
						addOneRow(sheet, startRowNum, list.get(k));
						startRowNum++;
					}
					//
				} else {
					sheet = wb.createSheet(sheetName + (i + 1));
					// 开始行号
					int startRowNum = 0;
					// 添加抬头
					if (CollectionUtils.isNotEmpty(head)) {
						addOneRow(sheet, startRowNum, head);
						startRowNum++;
					}
					// 获取开始索引和结束索引
					int firstIndex = i * sheetSize;
					int lastIndex = (i + 1) * sheetSize - 1 > list.size() - 1 ? list.size() - 1 : (i + 1) * sheetSize - 1;
					for (int k = firstIndex, len = list.size(); k < lastIndex && k < len; k++) {
						addOneRow(sheet, startRowNum, list.get(k));
						startRowNum++;
					}
				}
			}
			wb.write(out);
			wb.close();
		} catch (Exception e) {
			e.printStackTrace();
			// 如果是ExcelException，则直接抛出
			if (e instanceof ExcelException) {
				throw (ExcelException) e;
				// 否则将其它异常包装成ExcelException再抛出
			} else {
				throw new ExcelException("导出Excel失败");
			}
		}
	}

	private static void addOneRow(HSSFSheet sheet, int nowRowNum, List<String> colValues) {
		HSSFRow curRow = sheet.createRow(nowRowNum);
		HSSFCell curCell;
		for (int i = 0, len = colValues.size(); i < len; i++) {
			curCell = curRow.createCell(i);
			curCell.setCellValue(colValues.get(i));
		}
	}

	/**
	 * 添加行集合到excel中
	 * 
	 * @param sheet
	 * @param nowColumnRowPositionDto
	 * @param rowDtoList
	 * @return
	 * @throws Exception
	 * @author nibili 2017年4月21日
	 */
	private static ColumnRowPositionDto addRowsToSheet(HSSFWorkbook workbook, org.apache.poi.hssf.usermodel.HSSFCellStyle cellStyle, HSSFSheet sheet,
			ColumnRowPositionDto nowColumnRowPositionDto, List<RowDto> rowDtoList) throws Exception {

		if (CollectionUtils.isNotEmpty(rowDtoList) == true) {
			for (RowDto rowDto : rowDtoList) {
				nowColumnRowPositionDto = addOneRowToSheet(workbook, cellStyle, sheet, nowColumnRowPositionDto, rowDto);
				// 从第一列开始写起
				nowColumnRowPositionDto.setColumnNum(0);
			}
		}
		return nowColumnRowPositionDto;
	}

	/**
	 * 插入行，返回下一行行号
	 * 
	 * @param workbook
	 * @param sheet
	 * @param nowColumnRowPositionDto 开始的表格坐标（即行号和列号）
	 * @param rowDto
	 * @return
	 * @throws Exception
	 * @author nibili 2017年4月21日
	 */
	private static ColumnRowPositionDto addOneRowToSheet(HSSFWorkbook workbook, org.apache.poi.hssf.usermodel.HSSFCellStyle cellStyle, HSSFSheet sheet,
			ColumnRowPositionDto nowColumnRowPositionDto, RowDto rowDto) throws Exception {
		if (rowDto == null || CollectionUtils.isEmpty(rowDto.getCells()) == true) {
			return nowColumnRowPositionDto;
		}
		// 列数据
		List<CellDto> rowCellDtoList = rowDto.getCells();
		// 当前列号
		int nowColumnNum = nowColumnRowPositionDto.getColumnNum();
		// 当前行号
		int nowRowNum = nowColumnRowPositionDto.getRowNum();
		if (CollectionUtils.isNotEmpty(rowCellDtoList) == true) {
			// 最大所占行数，用于确定下一行数据的行号
			int maxRows = 0;
			for (int i = 0, len = rowCellDtoList.size(); i < len; i++) {
				CellDto cellDto = rowCellDtoList.get(i);
				// 当前单元格

				HSSFRow curRow = sheet.getRow(nowRowNum);
				if (curRow == null) {
					curRow = sheet.createRow(nowRowNum);
				}
				HSSFCell curCell = curRow.getCell(nowColumnNum);
				if (curCell == null) {
					curCell = curRow.createCell(nowColumnNum);
				}
				{
					// 单元格字体
					HSSFFont wf_title = workbook.createFont();
					// 定义格式 字体 下划线 斜体 粗体 颜色
					if (cellDto.getIsBold() == true) {
						wf_title.setBold(true); // 粗体
					}
					wf_title.setFontHeightInPoints((short) cellDto.getFontSize()); // 字体大小
					if (StringUtils.isNotBlank(cellDto.getFontName()) == true) {
						wf_title.setFontName(cellDto.getFontName());
					}
					wf_title.setColor(cellDto.getColour());
					cellStyle.setFont(wf_title);
					// 单元格居中等
					cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
					cellStyle.setAlignment(cellDto.getAlignment());
					curCell.setCellStyle(cellStyle);
					// 值
					curCell.setCellValue(cellDto.getCname());
				}
				if (cellDto.getColumns() > 1 || cellDto.getRows() > 1) {
					// 列数 或 行数大于1就要合并
					sheet.addMergedRegion(new CellRangeAddress(nowRowNum, nowRowNum + cellDto.getRows() - 1, nowColumnNum, nowColumnNum + cellDto.getColumns() - 1));// 合并单元格

				}
				// 设置列宽
				if (cellDto.getWidth() != null && cellDto.getWidth() != 0) {
					// 可能占多列，要平均到每一列的宽度
					int perColumnWidth = cellDto.getWidth() / cellDto.getColumns();
					for (int k = 0; k < cellDto.getColumns(); k++) {
						sheet.setColumnWidth(nowColumnNum + k, perColumnWidth);
					}
				}
				// 下一列数据的列号
				nowColumnNum = nowColumnNum + cellDto.getColumns();
				if (cellDto.getRows() > maxRows) {
					maxRows = cellDto.getRows();
				}

			}
			if (rowDto.getRowHeight() != null && rowDto.getRowHeight() != 0) {
				// 设置行高，可能占多行，要平均到每一行的高度
				int perRowHeight = rowDto.getRowHeight() / maxRows;
				for (int k = 0; k < maxRows; k++) {
					HSSFRow curRow = sheet.getRow(k + nowRowNum);
					curRow.setHeight(Short.valueOf(String.valueOf(perRowHeight)));
				}
			}
			// 下一行数据的行号
			nowRowNum = nowRowNum + maxRows;
		}
		ColumnRowPositionDto columnRowPositionDto = new ColumnRowPositionDto();
		columnRowPositionDto.setColumnNum(nowColumnNum);
		columnRowPositionDto.setRowNum(nowRowNum);
		return columnRowPositionDto;
	}

	/**
	 * 将数据填充到excel中
	 * 
	 * @MethodName : fillSheet
	 * @Description : 向工作表中填充数据
	 * @param sheet                 工作表
	 * @param list                  数据源
	 * @param fieldMap              中英文字段对应关系的Map
	 * @param additionalFieldHeader 额外表头
	 * @param firstIndex            开始索引
	 * @param lastIndex             结束索引
	 */
	private static <T> void fillSheet(HSSFWorkbook workbook, HSSFSheet sheet, List<T> list, List<CellDto> fieldMap, RowDto additionalFieldHeaderRowDto, int firstIndex,
			int lastIndex, String templatePath, int templateHeaderRowTotal, boolean isOutputFieldCnNameToExcel) throws Exception {
		ColumnRowPositionDto columnRowPositionDto = new ColumnRowPositionDto(0, 0);
		if (StringUtils.isNotBlank(templatePath) == true) {
			// 有模板，要把模板所占行数 加进来
			columnRowPositionDto.setRowNum(columnRowPositionDto.getRowNum() + templateHeaderRowTotal);
		}
		// 单元格样式
		org.apache.poi.hssf.usermodel.HSSFCellStyle cellStyle = workbook.createCellStyle();
		// 添加表头
		columnRowPositionDto = addOneRowToSheet(workbook, cellStyle, sheet, columnRowPositionDto, additionalFieldHeaderRowDto);

		// 从第一列开始
		columnRowPositionDto.setColumnNum(0);
		// 行集合
		List<RowDto> rowList = Lists.newArrayList();
		if (CollectionUtils.isNotEmpty(fieldMap) == true) {
			// 声明行对象
			RowDto rowDto;
			// 列集合
			List<CellDto> cellList;
			// 列对象
			CellDto cellDto;
			// 定义存放英文字段名和中文字段名的数组
			if (isOutputFieldCnNameToExcel == true) {
				// 填充表头
				rowDto = new RowDto();
				rowDto.setCells(fieldMap);
				rowList.add(rowDto);
			}
			//
			//
			// 添加内容到集合中
			for (int index = firstIndex; index <= lastIndex; index++) {
				//
				rowDto = new RowDto();
				cellList = Lists.newArrayList();
				// 获取单个对象
				T item = list.get(index);
				// 遍历字段集合
				for (CellDto cellDtoTemp : fieldMap) {
					Object objValue = getFieldValueByNameSequence(cellDtoTemp.getEnName(), item);
					// 单元格的值
					String fieldValue = objValue == null ? "" : objValue.toString();
					//
					cellDto = new CellDto();
					BeanUtils.copyProperties(cellDtoTemp, cellDto);
					cellDto.setCname(fieldValue);
					cellList.add(cellDto);
				}
				rowDto.setCells(cellList);
				//
				rowList.add(rowDto);
			}

		}
		// 写入excel
		columnRowPositionDto = addRowsToSheet(workbook, cellStyle, sheet, columnRowPositionDto, rowList);
	}

	/**
	 * 行列坐标点dtok
	 * 
	 * @author nibili 2017年4月21日
	 * 
	 */
	public static class ColumnRowPositionDto {

		/** 行号 */
		private int rowNum;
		/** 列号 */
		private int columnNum;

		public ColumnRowPositionDto() {

		}

		public ColumnRowPositionDto(int columnNum, int rowNum) {
			this.columnNum = columnNum;
			this.rowNum = rowNum;
		}

		/**
		 * get 行号
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public int getRowNum() {
			return rowNum;
		}

		/**
		 * set 行号
		 * 
		 * @param rowNum
		 * @author nibili 2017年4月21日
		 */
		public void setRowNum(int rowNum) {
			this.rowNum = rowNum;
		}

		/**
		 * get 列号
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public int getColumnNum() {
			return columnNum;
		}

		/**
		 * set 列号
		 * 
		 * @param columnNum
		 * @author nibili 2017年4月21日
		 */
		public void setColumnNum(int columnNum) {
			this.columnNum = columnNum;
		}

	}

	/**
	 * 额外字段title
	 * 
	 * @author nibili 2017年4月21日
	 * 
	 */
	public static class CellDto {

		/** 英文名称 */
		private String enName;
		/** 字段名称 */
		private String cname;
		/** 所占列数 */
		private int columns = 1;
		/** 所占行数 */
		private int rows = 1;
		/** 列宽 */
		private Integer width = 0;
		/** 行高 */
		private Integer height = 0;
		/** 字体大小 */
		private int fontSize = 12;
		/** 字体颜色 ，详见：HSSFColor */
		private short colour = HSSFColor.HSSFColorPredefined.BLACK.getIndex();
		/** 字体 */
		private String fontName;
		/** 是否粗体 */
		private boolean isBold = false;
		/** 文本居中，靠左靠右。。。。等排列方式，详见：CellStyle.ALIGN_ */
		private HorizontalAlignment alignment = HorizontalAlignment.CENTER;

		/**
		 * get 英文名称
		 * 
		 * @return
		 * @author nibili 2017年5月10日
		 */
		public String getEnName() {
			return enName;
		}

		/**
		 * set 英文名称
		 * 
		 * @param enName
		 * @author nibili 2017年5月10日
		 */
		public void setEnName(String enName) {
			this.enName = enName;
		}

		/**
		 * get 字段名称
		 * 
		 * @return
		 * @author nibili 2017年5月10日
		 */
		public String getCname() {
			return cname;
		}

		/**
		 * set 字段名称
		 * 
		 * @param cname
		 * @author nibili 2017年5月10日
		 */
		public void setCname(String cname) {
			this.cname = cname;
		}

		/**
		 * get 所占列数
		 * 
		 * @return
		 * @author nibili 2017年5月10日
		 */
		public int getColumns() {
			return columns;
		}

		/**
		 * set 所占列数
		 * 
		 * @param columns
		 * @author nibili 2017年5月10日
		 */
		public void setColumns(int columns) {
			this.columns = columns;
		}

		/**
		 * get 所占行数
		 * 
		 * @return
		 * @author nibili 2017年5月10日
		 */
		public int getRows() {
			return rows;
		}

		/**
		 * set 所占行数
		 * 
		 * @param rows
		 * @author nibili 2017年5月10日
		 */
		public void setRows(int rows) {
			this.rows = rows;
		}

		/**
		 * get 列宽
		 * 
		 * @return
		 * @author nibili 2017年5月10日
		 */
		public Integer getWidth() {
			return width;
		}

		/**
		 * set 列宽
		 * 
		 * @param width
		 * @author nibili 2017年5月10日
		 */
		public void setWidth(Integer width) {
			this.width = width;
		}

		/**
		 * get 行高
		 * 
		 * @return
		 * @author nibili 2017年5月10日
		 */
		public Integer getHeight() {
			return height;
		}

		/**
		 * set 行高
		 * 
		 * @param height
		 * @author nibili 2017年5月10日
		 */
		public void setHeight(Integer height) {
			this.height = height;
		}

		/**
		 * get 字体大小
		 * 
		 * @return
		 * @author nibili 2017年5月10日
		 */
		public int getFontSize() {
			return fontSize;
		}

		/**
		 * set 字体大小
		 * 
		 * @param fontSize
		 * @author nibili 2017年5月10日
		 */
		public void setFontSize(int fontSize) {
			this.fontSize = fontSize;
		}

		/**
		 * get 字体颜色，详见：HSSFColor
		 * 
		 * @return
		 * @author nibili 2017年5月10日
		 */
		public short getColour() {
			return colour;
		}

		/**
		 * set 字体颜色，详见：HSSFColor
		 * 
		 * @param colour
		 * @author nibili 2017年5月10日
		 */
		public void setColour(short colour) {
			this.colour = colour;
		}

		/**
		 * get 字体
		 * 
		 * @return
		 * @author nibili 2017年5月10日
		 */
		public String getFontName() {
			return fontName;
		}

		/**
		 * set 字体
		 * 
		 * @param fontName
		 * @author nibili 2017年5月10日
		 */
		public void setFontName(String fontName) {
			this.fontName = fontName;
		}

		/**
		 * get 是否粗体
		 * 
		 * @return
		 * @author nibili 2017年5月10日
		 */
		public boolean getIsBold() {
			return isBold;
		}

		/**
		 * set 是否粗体
		 * 
		 * @param isBold
		 * @author nibili 2017年5月10日
		 */
		public void setIsBold(boolean isBold) {
			this.isBold = isBold;
		}

		/**
		 * get 文本居中，靠左靠右。。。。等排列方式，详见：CellStyle.ALIGN_
		 * 
		 * @return
		 * @author nibili 2017年5月10日
		 */
		public HorizontalAlignment getAlignment() {
			return alignment;
		}

		/**
		 * set 文本居中，靠左靠右。。。。等排列方式，详见：CellStyle.ALIGN_
		 * 
		 * @param alignment
		 * @author nibili 2017年5月10日
		 */
		public void setAlignment(HorizontalAlignment alignment) {
			this.alignment = alignment;
		}

	}

	/** 行数据 */
	public static class RowDto {
		/** 行高 */
		private Integer rowHeight = null;
		/** 一行中的列数据 */
		private List<CellDto> cells;

		/**
		 * get 一行中的列数据
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public List<CellDto> getCells() {
			return cells;
		}

		/**
		 * set 一行中的列数据
		 * 
		 * @param cells
		 * @author nibili 2017年4月21日
		 */
		public void setCells(List<CellDto> cells) {
			this.cells = cells;
		}

		/**
		 * get 行高
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public Integer getRowHeight() {
			return rowHeight;
		}

		/**
		 * set 行高
		 * 
		 * @param rowHeight
		 * @author nibili 2017年4月21日
		 */
		public void setRowHeight(int rowHeight) {
			this.rowHeight = rowHeight;
		}

	}

	// /*-------------------------------------------------------------------------------导入-----------------------------------------------*/

	/**
	 * 按sheet序号导入
	 * 
	 * @param filePath
	 * @param index
	 * @return
	 * @throws ExcelException
	 * @auth nibili 2020-10-23 17:34:12
	 */
	public static List<List<String>> excelToList(String filePath, Integer index) throws ExcelException {
		return excelToList(filePath, null, index);
	}

	/**
	 * 按sheet名称导入
	 * 
	 * @param filePath
	 * @param sheetName
	 * @return
	 * @throws ExcelException
	 * @auth nibili 2020-10-23 17:34:32
	 */
	public static List<List<String>> excelToList(String filePath, String sheetName) throws ExcelException {
		return excelToList(filePath, sheetName, null);
	}

	public static List<List<String>> excelToList(String filePath, String sheetName, Integer index) throws ExcelException {

		// 定义要返回的list
		List<List<String>> resultList = new ArrayList<List<String>>();
		Workbook wb = null;
		try {
			String extString = filePath.substring(filePath.lastIndexOf("."));
			InputStream is = new FileInputStream(new File(filePath));
			// 根据Excel数据源创建WorkBook
			if (".xls".equals(extString)) {
				wb = new HSSFWorkbook(is);
			} else if (".xlsx".equals(extString)) {
				wb = new XSSFWorkbook(is);
			} else {
				throw new ExcelException("不支持的格式");
			}
			// 获取工作表
			Sheet sheet = null;
			if (index == null) {
				sheet = wb.getSheet(sheetName);
			} else {
				sheet = wb.getSheetAt(index);
			}
			// 获取第一行
			Row row = sheet.getRow(0);
			// 获取最大列数
			int colnum = row.getPhysicalNumberOfCells();
			// 获取工作表的有效行数
			int realRows = 0;
			for (int i = 0, len = sheet.getPhysicalNumberOfRows(); i < len; i++) {
				int nullCols = 0;
				Row rowTemp = sheet.getRow(i);
				if (rowTemp == null) {
					continue;
				}
				for (int j = 0; j < colnum; j++) {
					Cell currentCell = rowTemp.getCell(j);
					if (currentCell == null) {
						nullCols++;
					} else {
						// 不为null但为空
						currentCell.setCellType(CellType.STRING);
						if ("".equals(currentCell.getStringCellValue())) {
							nullCols++;
						}
					}
				}
				if (nullCols == colnum) {
					break;
				} else {
					realRows++;
				}
			}

			// 如果Excel中没有数据则提示错误
			if (realRows <= 1) {
				throw new ExcelException("Excel文件中没有任何数据");
			}

			for (int rowNum = 0; rowNum < realRows; rowNum++) {
				List<String> rowList = Lists.newArrayList();
				Row rowTemp = sheet.getRow(rowNum);
				if (rowTemp == null) {
					continue;
				}
				for (int col = 0, len = colnum; col < len; col++) {
					// 获取当前单元格中的内容
					String content = "-";
					Cell currentCell = rowTemp.getCell(col);
					if (currentCell == null) {

					} else {
						currentCell.setCellType(CellType.STRING);
						content = currentCell.getStringCellValue();
					}
					rowList.add(StringUtils.trim(content));
				}
				resultList.add(rowList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			// 如果是ExcelException，则直接抛出
			if (e instanceof ExcelException) {
				throw (ExcelException) e;

				// 否则将其它异常包装成ExcelException再抛出
			} else {
				e.printStackTrace();
				throw new ExcelException("导入Excel失败");
			}
		} finally {
			if (wb != null) {
				try {
					wb.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return resultList;
	}

	/**
	 * excel文件转成数组
	 * 
	 * @param filePath
	 * @param sheetName
	 * @param entityClass
	 * @param fieldMap
	 * @param uniqueFields
	 * @return
	 * @throws ExcelException
	 * @auth nibili 2019年11月26日 下午2:51:18
	 */
	public static <T> List<T> excelToList(String filePath, String sheetName, Class<T> entityClass, LinkedHashMap<String, String> fieldMap, String[] uniqueFields)
			throws ExcelException {

		// 定义要返回的list
		List<T> resultList = new ArrayList<T>();
		Workbook wb = null;
		try {

			// 根据Excel数据源创建WorkBook
			wb = new HSSFWorkbook(new FileInputStream(new File(filePath)));// 对应一个Excel文件
			// Workbook.getWorkbook(in);
			// 获取工作表
			Sheet sheet = wb.getSheet(sheetName);
			//
			Row row = null;
			// 获取工作表的有效行数
			int realRows = 0;
			for (int i = 0, rows = sheet.getPhysicalNumberOfRows(); i < rows; i++) {

				int nullCols = 0;
				row = sheet.getRow(i);
				if (row == null) {
					continue;
				}
				int cells = row.getPhysicalNumberOfCells();
				for (int j = 0; j < cells; j++) {
					Cell currentCell = row.getCell(j);
					if (currentCell == null || "".equals(currentCell.getStringCellValue())) {
						nullCols++;
					}
				}
				if (nullCols == cells) {
					break;
				} else {
					realRows++;
				}
			}

			// 如果Excel中没有数据则提示错误
			if (realRows <= 1) {
				throw new ExcelException("Excel文件中没有任何数据");
			}

			Row firstRow = sheet.getRow(0);

			String[] excelFieldNames = new String[firstRow.getPhysicalNumberOfCells()];

			// 获取Excel中的列名
			for (int i = 0, len = firstRow.getPhysicalNumberOfCells(); i < len; i++) {
				excelFieldNames[i] = firstRow.getCell(i).getStringCellValue().trim();
			}

			// 判断需要的字段在Excel中是否都存在
			boolean isExist = true;
			List<String> excelFieldList = Arrays.asList(excelFieldNames);
			for (String cnName : fieldMap.keySet()) {
				if (!excelFieldList.contains(cnName)) {
					isExist = false;
					break;
				}
			}

			// 如果有列名不存在，则抛出异常，提示错误
			if (!isExist) {
				throw new ExcelException("Excel中缺少必要的字段，或字段名称有误");
			}

			// 将列名和列号放入Map中,这样通过列名就可以拿到列号
			LinkedHashMap<String, Integer> colMap = new LinkedHashMap<String, Integer>();
			for (int i = 0; i < excelFieldNames.length; i++) {
				// colMap.put(excelFieldNames[i], firstRow[i].getColumn());
				colMap.put(excelFieldNames[i], firstRow.getCell(i).getColumnIndex());
			}

			// 将sheet转换为list
			for (int i = 1; i < realRows; i++) {
				// 新建要转换的对象
				T entity = entityClass.newInstance();

				// 给对象中的字段赋值
				for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
					// 获取中文字段名
					String cnNormalName = entry.getKey();
					// 获取英文字段名
					String enNormalName = entry.getValue();
					// 根据中文字段名获取列号
					int col = colMap.get(cnNormalName);

					// 获取当前单元格中的内容
					String content = sheet.getRow(i).getCell(col).getStringCellValue().trim();

					// 给对象赋值
					setFieldValueByName(enNormalName, content, entity);
				}

				resultList.add(entity);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// 如果是ExcelException，则直接抛出
			if (e instanceof ExcelException) {
				throw (ExcelException) e;

				// 否则将其它异常包装成ExcelException再抛出
			} else {
				e.printStackTrace();
				throw new ExcelException("导入Excel失败");
			}
		} finally {
			if (wb != null) {
				try {
					wb.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return resultList;
	}

	/*
	 * <-------------------------辅助的私有方法------------------------------------------
	 * ----->
	 */
	/**
	 * @MethodName : getFieldValueByName
	 * @Description : 根据字段名获取字段值
	 * @param fieldName 字段名
	 * @param o         对象
	 * @return 字段值
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws ParseException
	 */
	private static Object getFieldValueByName(String fieldName, Object o)
			throws ExcelException, IllegalArgumentException, IllegalAccessException, NoSuchMethodException, SecurityException, InvocationTargetException, ParseException {

		Object fieldValue = null;
		Field field = getFieldByName(fieldName, o.getClass());

		if (field != null) {
			field.setAccessible(true);
			fieldValue = field.get(o);
			//
			Class<?> fieldType = field.getType();
			if (Date.class == fieldType && fieldValue != null) {
				fieldValue = DateUtils.formatDate((Date) fieldValue, "yyyy-MM-dd HH:mm:ss");
			}

			return fieldValue;
		} else {
			// 字段不存在 找get方法
			String fieldGetName = parGetName(fieldName);
			Method fieldGetMet = o.getClass().getMethod(fieldGetName, new Class[] {});
			if (fieldGetMet != null) {
				Object fieldVal = fieldGetMet.invoke(o, new Object[] {});
				return fieldVal;
			}
		}
		throw new ExcelException("类字段不存在");
	}

	/**
	 * 拼接某属性的 get方法
	 * 
	 * @param fieldName
	 * @return String
	 */
	public static String parGetName(String fieldName) {
		if (null == fieldName || "".equals(fieldName)) {
			return null;
		}
		int startIndex = 0;
		if (fieldName.charAt(0) == '_')
			startIndex = 1;
		return "get" + fieldName.substring(startIndex, startIndex + 1).toUpperCase() + fieldName.substring(startIndex + 1);
	}

	/**
	 * @MethodName : getFieldByName
	 * @Description : 根据字段名获取字段
	 * @param fieldName 字段名
	 * @param clazz     包含该字段的类
	 * @return 字段
	 */
	private static Field getFieldByName(String fieldName, Class<?> clazz) {
		// 拿到本类的所有字段
		Field[] selfFields = clazz.getDeclaredFields();

		// 如果本类中存在该字段，则返回
		for (Field field : selfFields) {
			if (field.getName().equals(fieldName)) {
				return field;
			}
		}

		// 否则，查看父类中是否存在此字段，如果有则返回
		Class<?> superClazz = clazz.getSuperclass();
		if (superClazz != null && superClazz != Object.class) {
			return getFieldByName(fieldName, superClazz);
		}

		// 如果本类和父类都没有，则返回空
		return null;
	}

	/**
	 * @MethodName : getFieldValueByNameSequence
	 * @Description : 根据带路径或不带路径的属性名获取属性值
	 *              即接受简单属性名，如userName等，又接受带路径的属性名，如student.department.name等
	 * 
	 * @param fieldNameSequence 带路径的属性名或简单属性名
	 * @param o                 对象
	 * @return 属性值
	 * @throws Exception
	 */
	private static Object getFieldValueByNameSequence(String fieldNameSequence, Object o) throws Exception {

		Object value = null;

		// 将fieldNameSequence进行拆分
		String[] attributes = fieldNameSequence.split("\\.");
		if (attributes.length == 1) {
			value = getFieldValueByName(fieldNameSequence, o);
		} else {
			// 根据属性名获取属性对象
			Object fieldObj = getFieldValueByName(attributes[0], o);
			String subFieldNameSequence = fieldNameSequence.substring(fieldNameSequence.indexOf(".") + 1);
			value = getFieldValueByNameSequence(subFieldNameSequence, fieldObj);
		}
		return value;

	}

	/**
	 * @MethodName : setFieldValueByName
	 * @Description : 根据字段名给对象的字段赋值
	 * @param fieldName  字段名
	 * @param fieldValue 字段值
	 * @param o          对象
	 */
	private static void setFieldValueByName(String fieldName, Object fieldValue, Object o) throws Exception {

		Field field = getFieldByName(fieldName, o.getClass());
		if (field != null) {
			field.setAccessible(true);
			// 获取字段类型
			Class<?> fieldType = field.getType();

			// 根据字段类型给字段赋值
			if (String.class == fieldType) {
				field.set(o, String.valueOf(fieldValue));
			} else if ((Integer.TYPE == fieldType) || (Integer.class == fieldType)) {
				field.set(o, Integer.parseInt(fieldValue.toString()));
			} else if ((Long.TYPE == fieldType) || (Long.class == fieldType)) {
				field.set(o, Long.valueOf(fieldValue.toString()));
			} else if ((Float.TYPE == fieldType) || (Float.class == fieldType)) {
				field.set(o, Float.valueOf(fieldValue.toString()));
			} else if ((Short.TYPE == fieldType) || (Short.class == fieldType)) {
				field.set(o, Short.valueOf(fieldValue.toString()));
			} else if ((Double.TYPE == fieldType) || (Double.class == fieldType)) {
				field.set(o, Double.valueOf(fieldValue.toString()));
			} else if (Character.TYPE == fieldType) {
				if ((fieldValue != null) && (fieldValue.toString().length() > 0)) {
					field.set(o, Character.valueOf(fieldValue.toString().charAt(0)));
				}
			} else if (Date.class == fieldType) {
				field.set(o, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fieldValue.toString()));
			} else {
				field.set(o, fieldValue);
			}
		} else {
			throw new ExcelException(o.getClass().getSimpleName() + "类不存在字段名 " + fieldName);
		}
	}

	/**
	 * 
	 * @author nibili 2017年4月21日
	 * 
	 */
	public static class ExcelException extends Exception {

		/** */
		private static final long serialVersionUID = -7747451731201941253L;

		public ExcelException() {
			// TODO Auto-generated constructor stub
		}

		public ExcelException(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}

		public ExcelException(Throwable cause) {
			super(cause);
			// TODO Auto-generated constructor stub
		}

		public ExcelException(String message, Throwable cause) {
			super(message, cause);
			// TODO Auto-generated constructor stub
		}

	}

	/**
	 * 
	 * 提取表格的图片，并保存到文件夹中
	 * 
	 * @param filePath        excel文件路径；
	 * @param sheetIndex      sheet文件序号，如果
	 *                        sheetIndex不为null,以sheetIndex为准，否则以sheetName为准；
	 * @param sheetName       sheet名
	 * @param picColumIndex   图片所在列的序号，开始序号为1
	 * @param savePicFilePath 保存图片的文件夹跳径
	 * @return
	 * @throws Exception
	 * @auth nibili 2021-8-26 15:14:45
	 */
	public static Table<Integer, Integer, String> getExcelPicAt(String filePath, Integer sheetIndex, String sheetName, Integer picColumIndex, String savePicFilePath)
			throws Exception {
		//
		Table<Integer, Integer, String> table = HashBasedTable.create();
		//
		InputStream inp = new FileInputStream(filePath);
		Workbook wb = WorkbookFactory.create(inp);
		if (wb instanceof HSSFWorkbook) {
			HSSFWorkbook workbook = (HSSFWorkbook) wb;
			List<HSSFPictureData> pictures = workbook.getAllPictures();
			HSSFSheet sheet = (HSSFSheet) workbook.getSheetAt(0);
			for (HSSFShape shape : sheet.getDrawingPatriarch().getChildren()) {
				HSSFClientAnchor anchor = (HSSFClientAnchor) shape.getAnchor();
				if (shape instanceof HSSFPicture) {
					HSSFPicture pic = (HSSFPicture) shape;

					int col = anchor.getCol2();
					if (col + 1 == picColumIndex) {
						int pictureIndex = pic.getPictureIndex() - 1;
						HSSFPictureData picData = pictures.get(pictureIndex);
						String path = savePic(UUID.randomUUID().toString(), picData, savePicFilePath);
						int row = anchor.getRow2();
						table.put(row, col, path);
					}
				}
			}
		} else if (wb instanceof XSSFWorkbook) {
			XSSFWorkbook workbook = (XSSFWorkbook) wb;
			XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(0);
			List<XSSFShape> shapes = sheet.getDrawingPatriarch().getShapes();
			for (XSSFShape shape : shapes) {
				XSSFClientAnchor anchor = (XSSFClientAnchor) shape.getAnchor();
				if (shape instanceof XSSFPicture) {
					XSSFPicture pic = (XSSFPicture) shape;
					int col = anchor.getCol2();
					if (col + 1 == picColumIndex) {
						XSSFPictureData picData = pic.getPictureData();
						String path = savePic(UUID.randomUUID().toString(), picData, savePicFilePath);
						int row = anchor.getRow2();
						table.put(row, col, path);
					}
				}
			}
		}
		return table;
	}

	/**
	 * 保存 excel图片
	 * 
	 * @param i
	 * @param pic
	 * @param savePicFilePath
	 * @throws Exception
	 * @auth nibili 2021-8-26 15:19:32
	 */
	private static String savePic(String i, PictureData pic, String savePicFilePath) throws Exception {
		String ext = pic.suggestFileExtension();
		byte[] data = pic.getData();
		String path = savePicFilePath + "/" + i + "." + ext;
		FileUtils.touch(new File(path));
		FileOutputStream out = new FileOutputStream(path);
		out.write(data);
		out.close();
		return path;
	}

}
