package cn.com.easy.utils.RSA;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import cn.com.easy.utils.HexUtils;

public class AESUtils {

	// 加密
	public static String encrypt(String data, String keyHex, String ivHex) {
		// 偏移量
		byte[] iv = HexUtils.hexStr2Bytes(ivHex);
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			int blockSize = cipher.getBlockSize();
			byte[] dataBytes = data.getBytes();
			int length = dataBytes.length;
			// 计算需填充长度
			if (length % blockSize != 0) {
				length = length + (blockSize - (length % blockSize));
			}
			byte[] plaintext = new byte[length];
			// 填充
			System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

			SecretKeySpec keySpec = new SecretKeySpec(HexUtils.hexStr2Bytes(keyHex), "AES");
			// 设置偏移量参数
			IvParameterSpec ivSpec = new IvParameterSpec(iv);
			cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
			byte[] encryped = cipher.doFinal(plaintext);

			return Base64.getEncoder().encodeToString(encryped);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// 解密
	public static String desEncrypt(String data, String keyHex, String ivHex) {
		byte[] iv = HexUtils.hexStr2Bytes(ivHex);
		try {
			byte[] encryp = Base64.getDecoder().decode(data);
			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			SecretKeySpec keySpec = new SecretKeySpec(HexUtils.hexStr2Bytes(keyHex), "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(iv);
			cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
			byte[] original = cipher.doFinal(encryp);
			return new String(original);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// 测试
	public static void main(String[] args) {
		String data = "abcdefg";
		String key = "b30f7ab3313e5e88e513a866912e96c2";
		String iv = "72ca2c95dad3a26d380f17640175f790";
		String encrypt = encrypt(data, key, iv);
		String desencrypt = desEncrypt(encrypt, key, iv);
		System.out.println("加密后:" + encrypt);
		System.out.println("解密后:" + desencrypt.trim());
	}

}
