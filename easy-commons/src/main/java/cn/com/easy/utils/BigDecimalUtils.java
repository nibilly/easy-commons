package cn.com.easy.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * BigDecimal处理工具类
 * 
 * @author nibili 2017年9月14日
 * 
 */
public class BigDecimalUtils {

	public static void main(String[] args) {
		BigDecimal bd = new BigDecimal(7132.9274234234d);
		System.out.println(BigDecimalUtils.formatBase(bd, 0, "元", false, true, 3, "万", true));
		System.out.println(BigDecimalUtils.formatDefault(bd, "kwh"));
		System.out.println("-------------------------");
		System.out.println(BigDecimalUtils.scale(bd, 2, false));

	}

	/**
	 * 默认格式化保留两位数
	 * 
	 * @param bigDecimal
	 * @return
	 * @auth nibili 2019年9月4日 下午2:35:29
	 */
	public static String formatDefault(BigDecimal bigDecimal) {
		if (bigDecimal == null) {
			return "0.00";
		}
		DecimalFormat df = new DecimalFormat(".##");
		return df.format(bigDecimal);
	}

	/**
	 * 截取小数位
	 * 
	 * @param bigDecimal
	 * @param scale
	 * @param isRounding
	 *            是否四舍五入
	 * @return
	 * @author nibili 2017年9月15日
	 */
	public static BigDecimal scale(BigDecimal bigDecimal, int scale, boolean isRounding) {
		if (bigDecimal == null) {
			return null;
		}
		// 不转成万 的 小数点位数
		StringBuilder decimals = new StringBuilder();
		for (int i = 0; i < scale; i++) {
			if (i == 0) {
				decimals.append(".");
			}
			decimals.append("0");
		}
		DecimalFormat df = new DecimalFormat("##0" + decimals.toString());
		if (isRounding == false) {
			df.setRoundingMode(RoundingMode.FLOOR);
		}
		return new BigDecimal(df.format(bigDecimal));
	}

	/**
	 * 千分号格式化金额输出
	 * 
	 * @param amount
	 *            金额
	 * @return
	 */
	public static String toPerThousandSign(BigDecimal amount) {
		if (amount != null) {
			DecimalFormat df = new DecimalFormat("###,##0.00");
			return df.format(amount);
		} else {
			return "0.00";
		}
	}

	/**
	 * 默认，小于一万保留两位小数位；大于一万会转成万显示（单位：万），保留两位小数位，用千分号分隔;<br/>
	 * 所有小数点等不进行四舍五入
	 * 
	 * @param inputBigDecimal
	 * @param unitString
	 * @return
	 * @author nibili 2017年9月14日
	 */
	public static String formatDefault(BigDecimal inputBigDecimal, String unitString) {
		return BigDecimalUtils.format(inputBigDecimal, 2, unitString, false, true, 2, "万", true);
	}

	/**
	 * 格式化bigDecimal以字符串输出，
	 * 
	 * @param inputBigDecimal
	 * @param decimalLength
	 *            保留小数位长度
	 * @param unitString
	 *            最后输出字符串的单位
	 * @param isRounding是否四舍五入
	 * @param isToChineseTenThousand
	 *            是否超过一千后载取成 XX万
	 * @param toChineseTenThousandDecimalLength
	 *            超过一千后载取成万后，保留小数位数
	 * @param chineseTenThousandUnit
	 *            万的单位:万 或 w
	 * @param isSplitWithThousandSign
	 *            是否使用千分号分隔
	 * @return
	 * @author nibili 2017年9月14日
	 */
	public static String formatBase(BigDecimal inputBigDecimal, int decimalLength, String unitString, boolean isRounding, boolean isToChineseTenThousand,
			int toChineseTenThousandDecimalLength, String chineseTenThousandUnit, boolean isSplitWithThousandSign) {
		return BigDecimalUtils.format(inputBigDecimal, decimalLength, unitString, isRounding, isToChineseTenThousand, toChineseTenThousandDecimalLength, chineseTenThousandUnit,
				isSplitWithThousandSign);
	}

	/**
	 * 格式化bigDecimal以字符串输出，所有小数点等截取会进行四舍五入
	 * 
	 * @param inputBigDecimal
	 * @param decimalLength
	 *            保留小数位长度
	 * @param unitString
	 *            最后输出字符串的单位
	 * @param isRounding
	 *            是否四舍五入
	 * @param isToChineseTenThousand
	 *            是否超过一千后载取成 XX万
	 * @param toChineseTenThousandDecimalLength
	 *            超过一千后载取成万后，保留小数位数
	 * @param chineseTenThousandUnit
	 *            万的单位:万 或 w
	 * @param isSplitWithThousandSign
	 *            是否使用千分号分隔
	 * @return
	 * @author nibili 2017年9月14日
	 */
	private static String format(BigDecimal inputBigDecimal, int decimalLength, String unitString, boolean isRounding, boolean isToChineseTenThousand,
			int toChineseTenThousandDecimalLength, String chineseTenThousandUnit, boolean isSplitWithThousandSign) {

		if (inputBigDecimal != null) {
			// 用于格式化字符串
			String formatString = "###,##0";
			if (isSplitWithThousandSign == false) {
				formatString = "##0";
			}
			// 不转成万 的 小数点位数
			StringBuilder decimals = new StringBuilder();
			for (int i = 0; i < decimalLength; i++) {
				if (i == 0) {
					decimals.append(".");
				}
				decimals.append("0");
			}
			if (isToChineseTenThousand == true && inputBigDecimal.doubleValue() > 9999d) {
				// 转成万
				// 大于等于1万才转
				inputBigDecimal = inputBigDecimal.divide(new BigDecimal(10000));
				// 声明转成万后的小数点位数
				StringBuilder decimalsTenThousand = new StringBuilder();
				for (int i = 0; i < toChineseTenThousandDecimalLength; i++) {
					if (i == 0) {
						decimalsTenThousand.append(".");
					}
					decimalsTenThousand.append("0");
				}

				//
				DecimalFormat df = new DecimalFormat(formatString + decimalsTenThousand.toString());
				if (isRounding == false) {
					df.setRoundingMode(RoundingMode.FLOOR);
				}
				return df.format(inputBigDecimal) + chineseTenThousandUnit + unitString;
			}
			DecimalFormat df = new DecimalFormat(formatString + decimals.toString());
			if (isRounding == false) {
				df.setRoundingMode(RoundingMode.FLOOR);
			}
			return df.format(inputBigDecimal) + unitString;
		} else {
			return "0.00";
		}

	}
}
