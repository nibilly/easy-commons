package cn.com.easy.utils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * long 类型到前端会丢失精度
 * 
 * @author nibili 2018年6月5日
 * 
 */
public class JacksonJsonLongSerializer extends JsonSerializer<Long> {

	@Override
	public void serialize(Long id, JsonGenerator gen, SerializerProvider provider) throws IOException, JsonProcessingException {
		String string = String.valueOf(id);
		gen.writeString(string);
	}

}
