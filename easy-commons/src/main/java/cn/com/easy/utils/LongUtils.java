package cn.com.easy.utils;

/**
 * 数字工具类
 * 
 * @author nibili 2018年7月17日
 * 
 */
public class LongUtils {

	public static void main(String[] args) {
		System.out.println(LongUtils.parse(4321l));
	}

	/**
	 * 将数字转换成万为单位的，并保留一个小数，如果小数位后是0则省略
	 * 
	 * @param num
	 * @return
	 * @auth nibili 2018年7月17日
	 */
	public static String parse(Long num) {
		String numString = String.valueOf(num);
		int length = numString.length();
		if (length > 8) {
			numString = numString.substring(0, length - 8) + "." + numString.charAt(length - 8) + "亿";
			return numString;
		} else if (length >4) {
			numString = numString.substring(0, length - 4) + "." + numString.charAt(length - 4) + "万";
			return numString;
		} else {
			return numString;
		}
	}

}
