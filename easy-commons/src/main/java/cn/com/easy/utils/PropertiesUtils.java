package cn.com.easy.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.DefaultPropertiesPersister;
import org.springframework.util.PropertiesPersister;

/**
 * properties utils
 * 
 * @author nibili 2015-2-5
 * 
 */
public abstract class PropertiesUtils {

	/** The Constant DEFAULT_ENCODING. */
	private static final String DEFAULT_ENCODING = "UTF-8";

	/** The logger. */
	private static Logger logger = LoggerFactory.getLogger(PropertiesUtils.class);

	/** The properties persister. */
	private static PropertiesPersister propertiesPersister = new DefaultPropertiesPersister();

	/** The resource loader. */
	private static ResourceLoader resourceLoader = new DefaultResourceLoader();

	public static void main(String[] args) throws IOException {
		// Properties properties =
		// PropertiesUtils.loadPropertiesByJdk("demo.properties",
		// "quartz.properties");
		// System.out.println(properties.get("db.url"));
		// System.out.println(properties.get("org.quartz.scheduler.instanceName"));
		Properties properties = PropertiesUtils.loadPropertiesByFile("D:/git/easy-commons/easy-commons/src/main/resources/demo.properties",
				"D:/git/easy-commons/easy-commons/src/test/resources/quartz.properties");
		System.out.println(properties.get("db.url"));
		System.out.println(properties.get("org.quartz.scheduler.instanceName"));

	}

	/**
	 * 载入多个properties文件, 相同的属性最后载入的文件将会覆盖之前的载入.
	 * 
	 * @param locations
	 *            the locations
	 * @return the properties
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @see org.springframework.beans.factory.config.PropertyPlaceholderConfigurer
	 */
	public static Properties loadProperties(String... locations) throws IOException {
		Properties props = new Properties();

		for (String location : locations) {

			logger.debug("Loading properties file from classpath:" + location);

			InputStream is = null;
			try {
				Resource resource = resourceLoader.getResource(location);
				is = resource.getInputStream();
				propertiesPersister.load(props, new InputStreamReader(is, DEFAULT_ENCODING));
			} catch (IOException ex) {
				logger.info("Could not load properties from classpath:" + location + ": " + ex.getMessage());
			} finally {
				if (is != null) {
					is.close();
				}
			}
		}
		return props;
	}

	/**
	 * 加载properties文件 <br/>
	 * 载入多个properties文件, 相同的属性最后载入的文件将会覆盖之前的载入.
	 * 
	 * @param locations
	 *            类路径下的文件，例：a.properties,b.properties
	 * @return
	 * @throws IOException
	 * @auth nibili 2018年12月19日 196888813@qq.com
	 */
	public static Properties loadPropertiesByJdk(String... locations) throws IOException {
		Properties properties = new Properties();
		for (String location : locations) {
			// 使用ClassLoader加载properties配置文件生成对应的输入流
			InputStream in = PropertiesUtils.class.getClassLoader().getResourceAsStream(location);
			// 使用properties对象加载输入流
			properties.load(in);
		}
		return properties;
	}

	/**
	 * 路径必须是系统的绝对路径
	 * 
	 * @param filePaths
	 * @return
	 * @throws IOException
	 * @auth nibili 2018年12月19日 196888813@qq.com
	 */
	public static Properties loadPropertiesByFile(String... filePaths) throws IOException {
		Properties properties = new Properties();
		for (String filePath : filePaths) {
			// 使用InPutStream流读取properties文件
			BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
			properties.load(bufferedReader);
		}
		return properties;
	}
}
