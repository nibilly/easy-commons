package cn.com.easy.utils;

import java.text.DecimalFormat;

public class IntegerToWang {
	public static void main(String[] args) {

		System.out.print("输入一个整数：");

		long num = 50050l;

		if (num < 9999) {

			System.out.println("您输入的数字为：" + num);

		} else {

			double n = (double) num / 10000;

			DecimalFormat df = new DecimalFormat("#.0");

			System.out.println("您输入的数字为：" + df.format(n) + "w");

		}

	}
}
