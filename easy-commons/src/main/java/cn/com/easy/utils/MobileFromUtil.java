package cn.com.easy.utils;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * 手机号码归属地查询工具类
 * 
 * @author nibili 2017年7月6日
 * 
 */
public class MobileFromUtil {

	/** 正则表达式,审核要获取手机归属地的手机是否符合格式,可以只输入手机号码前7位 */
	public static final String REGEX_IS_MOBILE = "(?is)(^1[0-9][0-9]\\d{4,8}$)";

	/**
	 * 获得手机号码归属地
	 * 
	 * @param mobileNumber
	 * @return
	 * @throws Exception
	 */
	public static String getMobileFrom(String mobileNumber) throws Exception {
		if (!veriyMobile(mobileNumber)) {
			throw new Exception("不是完整的11位手机号或者正确的手机号前七位");
		}
		String result = null;
		try {
			result = parseMobileFrom(mobileNumber);
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 从www.ip138.com返回的结果网页内容中获取手机号码归属地,结果为：省份 城市
	 * 
	 * @param mobile
	 * @return
	 * @throws IOException
	 */
	private static String parseMobileFrom(String mobile) throws IOException {

		Document document = Jsoup.connect("http://www.ip138.com:8080/search.asp?action=mobile&mobile=" + mobile)
				.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.102 Safari/537.36").get();
		return document.select(".tdc .tdc2").get(1).text();

	}

	/**
	 * 验证手机号
	 * 
	 * @param mobileNumber
	 * @return
	 */
	private static boolean veriyMobile(String mobileNumber) {
		Pattern p = null;
		Matcher m = null;
		p = Pattern.compile(REGEX_IS_MOBILE);
		m = p.matcher(mobileNumber);
		return m.matches();
	}

	/**
	 * 测试
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		System.out.println(getMobileFrom("17005920973"));
	}

}
