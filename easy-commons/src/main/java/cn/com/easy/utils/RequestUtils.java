package cn.com.easy.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import com.github.cage.Cage;
import com.github.cage.GCage;

/**
 * request utils
 * 
 * @author nibili 2015-02-03
 */
public class RequestUtils {

	/** 前端用户 */
	public static String FRONT_USER_TAG = "front_user";

	/** 后端用户 */
	public static String USER_TAG = "user";
	/** 后端用户权限tag */
	public static String USER_PERMISSION_TAG = "permission";
	/** 验证码session key */
	private static final String CAPTCHA_TAG = "CAPTCHA_TAG";
	/** 生成验证码的类 */
	private static final Cage cage = new GCage();

	/**
	 * get 前端用户
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getFrontUser(HttpServletRequest request) {
		return (T) request.getSession().getAttribute(FRONT_USER_TAG);
	}

	/**
	 * set 前端用户
	 * 
	 * @auth nibili 2015-2-3
	 */
	public static void setFrontUser(HttpServletRequest request, Object object) {
		request.getSession().setAttribute(FRONT_USER_TAG, object);
	}

	/**
	 * 发送给客户端验证码
	 * 
	 * @param response
	 * @throws Exception
	 * @auth nibili 2015年8月28日 下午4:05:29
	 */
	public static void sendCaptcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// String token = RandomStringUtils.random(4,
		// "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ");
		String token = RandomStringUtils.random(4, "1234567890");
		request.getSession().setAttribute(CAPTCHA_TAG, token.toLowerCase());
		cage.draw(token, response.getOutputStream());
	}

	/**
	 * 验正验证码
	 * 
	 * @param clientString
	 * @return
	 * @auth nibili 2015年8月28日 下午4:07:46
	 */
	public static boolean validCaptcha(HttpServletRequest request, String clientString) {
		if (StringUtils.isBlank(clientString) == true) {
			return false;
		}
		String inerCaptcha = (String) request.getSession().getAttribute(CAPTCHA_TAG);
		if (StringUtils.equals(inerCaptcha, clientString.toLowerCase()) == false) {
			// 不匹配
			return false;
		} else {
			request.getSession().setAttribute(CAPTCHA_TAG, null);
			return true;
		}
	}

	/**
	 * get current user
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getCurrentUser(HttpServletRequest request) {
		return (T) request.getSession().getAttribute(USER_TAG);
	}

	/**
	 * set current user
	 * 
	 * @auth nibili 2015-2-3
	 */
	public static void setCurrentUser(HttpServletRequest request, Object object) {
		request.getSession().setAttribute(USER_TAG, object);
	}

	/**
	 * 获取权限
	 * 
	 * @param request
	 * @return
	 * @auth nibili 2015年5月12日
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getUserPermission(HttpServletRequest request) {
		return (T) request.getSession().getAttribute(USER_PERMISSION_TAG);
	}

	/**
	 * 设置权限
	 * 
	 * @param request
	 * @param object
	 * @auth nibili 2015年5月12日
	 */
	public static void setUserPermission(HttpServletRequest request, Object object) {
		request.getSession().setAttribute(USER_PERMISSION_TAG, object);
	}

	/** session中微信授权用户实体对应的key */
	public static String USER_WEIXIN_TAG = "user_weixin";

	/**
	 * 获取当前授权的微信用户
	 * 
	 * @param request
	 * @return
	 * @auth nibili 2015年1月21日 下午9:53:25
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getCurrentWeixinUser(HttpServletRequest request) {
		return (T) request.getSession().getAttribute(USER_WEIXIN_TAG);
	}

	/**
	 * 设置当前授权的微信用户
	 * 
	 * @param request
	 * @return
	 * @auth nibili 2015年1月21日 下午9:53:25
	 */
	public static void setCurrentWeixinUser(HttpServletRequest request, Object object) {
		request.getSession().setAttribute(USER_WEIXIN_TAG, object);
	}
}
