package cn.com.easy.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * web常用工具类
 * 
 * @author nibili 2017年4月20日
 * 
 */
public class WebUtils {

	/**
	 * 用于获取网站的访问根路径
	 * 
	 * @param request
	 * @return
	 * @author nibili 2017年4月20日
	 */
	public static String getHttpDomainBase(HttpServletRequest request) {
		//
		String serverPort = "";
		if (request.getServerPort() != 80) {
			serverPort = ":" + request.getServerPort();
		}
		String contextPath = request.getContextPath();
		String path = request.getScheme() + "://" + request.getServerName() + serverPort + contextPath;
		return path;
	}
}
