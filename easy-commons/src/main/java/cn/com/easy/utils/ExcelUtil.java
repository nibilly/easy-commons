package cn.com.easy.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletResponse;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;

import com.google.common.collect.Lists;

/**
 * Excel导入导出工具
 * 
 * @author nibili 2017年3月23日
 * 
 */
public class ExcelUtil {

	/*-------------------------------------导出-------------------------------------------*/

	/**
	 * @MethodName : listToExcel
	 * @Description : 导出Excel（可以导出到本地文件系统，也可以导出到浏览器，可自定义工作表大小）
	 * @param list
	 *            数据源
	 * @param fieldMap
	 *            类的英文属性和Excel中的中文列名的对应关系 如果需要的是引用对象的属性，则英文属性使用类似于EL表达式的格式
	 *            如：list中存放的都是student，student中又有college属性，而我们需要学院名称，则可以这样写
	 *            fieldMap.put("college.collegeName","学院名称")
	 * @param sheetName
	 *            工作表的名称
	 * @param sheetSize
	 *            每个工作表中记录的最大个数
	 * @param additionalFieldHeaderRowDto
	 *            第一行标题栏
	 * @param out
	 *            导出流
	 * @throws ExcelException
	 */
	public static <T> void listToExcel(List<T> list, List<CellDto> fieldMap, String sheetName, int sheetSize, RowDto additionalFieldHeaderRowDto, OutputStream out)
			throws ExcelException {

		if (list.size() == 0 || list == null) {
			throw new ExcelException("数据源中没有任何数据");
		}

		if (sheetSize > 65535 || sheetSize < 1) {
			sheetSize = 65535;
		}

		// 创建工作簿并发送到OutputStream指定的地方
		WritableWorkbook wwb;
		try {
			wwb = Workbook.createWorkbook(out);

			// 因为2003的Excel一个工作表最多可以有65536条记录，除去列头剩下65535条
			// 所以如果记录太多，需要放到多个工作表中，其实就是个分页的过程
			// 1.计算一共有多少个工作表
			double sheetNum = Math.ceil(list.size() / new Integer(sheetSize).doubleValue());

			// 2.创建相应的工作表，并向其中填充数据
			for (int i = 0; i < sheetNum; i++) {
				// 如果只有一个工作表的情况
				if (1 == sheetNum) {
					WritableSheet sheet = wwb.createSheet(sheetName, i);
					fillSheet(sheet, list, fieldMap, additionalFieldHeaderRowDto, 0, list.size() - 1);

					// 有多个工作表的情况
				} else {
					WritableSheet sheet = wwb.createSheet(sheetName + (i + 1), i);

					// 获取开始索引和结束索引
					int firstIndex = i * sheetSize;
					int lastIndex = (i + 1) * sheetSize - 1 > list.size() - 1 ? list.size() - 1 : (i + 1) * sheetSize - 1;
					// 填充工作表
					fillSheet(sheet, list, fieldMap, additionalFieldHeaderRowDto, firstIndex, lastIndex);
				}
			}

			wwb.write();
			wwb.close();

		} catch (Exception e) {
			e.printStackTrace();
			// 如果是ExcelException，则直接抛出
			if (e instanceof ExcelException) {
				throw (ExcelException) e;

				// 否则将其它异常包装成ExcelException再抛出
			} else {
				throw new ExcelException("导出Excel失败");
			}
		}

	}

	/**
	 * @MethodName : listToExcel
	 * @Description : 导出Excel（可以导出到本地文件系统，也可以导出到浏览器，工作表大小为2003支持的最大值）
	 * @param list
	 *            数据源
	 * @param fieldMap
	 *            类的英文属性和Excel中的中文列名的对应关系
	 * @param out
	 *            导出流
	 * @throws ExcelException
	 */
	public static <T> void listToExcel(List<T> list, List<CellDto> fieldMap, String sheetName, RowDto additionalFieldHeaderRowDto, OutputStream out) throws ExcelException {

		listToExcel(list, fieldMap, sheetName, 65535, additionalFieldHeaderRowDto, out);

	}

	/**
	 * @MethodName : listToExcel
	 * @Description : 导出Excel（导出到浏览器，可以自定义工作表的大小）
	 * @param list
	 *            数据源
	 * @param fieldMap
	 *            类的英文属性和Excel中的中文列名的对应关系
	 * @param sheetSize
	 *            每个工作表中记录的最大个数
	 * @param response
	 *            使用response可以导出到浏览器
	 * @throws ExcelException
	 */
	public static <T> void listToExcel(List<T> list, List<CellDto> fieldMap, String sheetName, int sheetSize, RowDto additionalFieldHeaderRowDto, HttpServletResponse response)
			throws ExcelException {

		// 设置默认文件名为当前时间：年月日时分秒
		String fileName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()).toString();

		// 设置response头信息
		response.reset();
		response.setContentType("application/vnd.ms-excel"); // 改成输出excel文件
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");

		// 创建工作簿并发送到浏览器
		try {

			OutputStream out = response.getOutputStream();
			listToExcel(list, fieldMap, sheetName, sheetSize, additionalFieldHeaderRowDto, out);

		} catch (Exception e) {
			e.printStackTrace();

			// 如果是ExcelException，则直接抛出
			if (e instanceof ExcelException) {
				throw (ExcelException) e;

				// 否则将其它异常包装成ExcelException再抛出
			} else {
				throw new ExcelException("导出Excel失败");
			}
		}
	}

	/**
	 * @MethodName : listToExcel
	 * @Description : 导出Excel（导出到浏览器，工作表的大小是2003支持的最大值）
	 * @param list
	 *            数据源
	 * @param fieldMap
	 *            类的英文属性和Excel中的中文列名的对应关系
	 * @param response
	 *            使用response可以导出到浏览器
	 * @throws ExcelException
	 */
	public static <T> void listToExcel(List<T> list, List<CellDto> fieldMap, String sheetName, RowDto additionalFieldHeaderRowDto, HttpServletResponse response)
			throws ExcelException {

		listToExcel(list, fieldMap, sheetName, 65535, additionalFieldHeaderRowDto, response);
	}

	/**
	 * 添加行集合到excel中
	 * 
	 * @param sheet
	 * @param nowColumnRowPositionDto
	 * @param rowDtoList
	 * @return
	 * @throws Exception
	 * @author nibili 2017年4月21日
	 */
	public static ColumnRowPositionDto addRowsToSheet(WritableSheet sheet, ColumnRowPositionDto nowColumnRowPositionDto, List<RowDto> rowDtoList) throws Exception {

		if (CollectionUtils.isNotEmpty(rowDtoList) == true) {
			for (RowDto rowDto : rowDtoList) {
				nowColumnRowPositionDto = addOneRowToSheet(sheet, nowColumnRowPositionDto, rowDto);
				// 从第一列开始写起
				nowColumnRowPositionDto.setColumnNum(0);
			}
		}
		return nowColumnRowPositionDto;
	}

	/**
	 * 插入行，返回下一行行号
	 * 
	 * @param sheet
	 * @param nowColumnRowPositionDto
	 *            开始的表格坐标（即行号和列号）
	 * @param rowDto
	 * @return
	 * @throws Exception
	 * @author nibili 2017年4月21日
	 */
	public static ColumnRowPositionDto addOneRowToSheet(WritableSheet sheet, ColumnRowPositionDto nowColumnRowPositionDto, RowDto rowDto) throws Exception {
		if (rowDto == null || CollectionUtils.isEmpty(rowDto.getCells()) == true) {
			return nowColumnRowPositionDto;
		}
		// 列数据
		List<CellDto> rowCellDtoList = rowDto.getCells();
		// 当前列号
		int nowColumnNum = nowColumnRowPositionDto.getColumnNum();
		// 当前行号
		int nowRowNum = nowColumnRowPositionDto.getRowNum();
		if (CollectionUtils.isNotEmpty(rowCellDtoList) == true) {
			// 最大所占行数，用于确定下一行数据的行号
			int maxRows = 0;
			for (int i = 0, len = rowCellDtoList.size(); i < len; i++) {
				CellDto cellDto = rowCellDtoList.get(i);
				WritableFont wf_title = null;
				// 定义格式 字体 下划线 斜体 粗体 颜色
				if (cellDto.getIsBold() == true) {
					wf_title = new WritableFont(cellDto.getFontName(), cellDto.getFontSize(), WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, cellDto.getColour());
				} else {
					wf_title = new WritableFont(cellDto.getFontName(), cellDto.getFontSize(), WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, cellDto.getColour());
				}
				WritableCellFormat headerFormat = new WritableCellFormat(wf_title);
				headerFormat.setWrap(true);
				headerFormat.setAlignment(cellDto.getAlignment());
				headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
				sheet.addCell(new Label(nowColumnNum, nowRowNum, cellDto.getCname(), headerFormat)); // 普通的带有定义格式的单元格
				if (cellDto.getColumns() > 1 || cellDto.getRows() > 1) {
					// 列数 或 行数大于1就要合并
					sheet.mergeCells(nowColumnNum, nowRowNum, nowColumnNum + cellDto.getColumns() - 1, nowRowNum + cellDto.getRows() - 1); // 合并单元格
				}
				// 设置列宽
				if (cellDto.getWidth() != null) {
					// 可能占多列，要平均到每一列的宽度
					int perColumnWidth = cellDto.getWidth() / cellDto.getColumns();
					for (int k = 0; k < cellDto.getColumns(); k++) {
						sheet.setColumnView(nowColumnNum + k, perColumnWidth);
					}
				}
				// 下一列数据的列号
				nowColumnNum = nowColumnNum + cellDto.getColumns();
				if (cellDto.getRows() > maxRows) {
					maxRows = cellDto.getRows();
				}

			}
			if (rowDto.getRowHeight() != null) {
				// 设置行高，可能占多行，要平均到每一行的高度
				int perRowHeight = rowDto.getRowHeight() / maxRows;
				for (int k = 0; k < maxRows; k++) {
					sheet.setRowView(k + nowRowNum, perRowHeight, false);
				}
			}
			// 下一行数据的行号
			nowRowNum = nowRowNum + maxRows;

		}
		ColumnRowPositionDto columnRowPositionDto = new ColumnRowPositionDto();
		columnRowPositionDto.setColumnNum(nowColumnNum);
		columnRowPositionDto.setRowNum(nowRowNum);
		return columnRowPositionDto;
	}

	/**
	 * 将数据填充到excel中
	 * 
	 * @MethodName : fillSheet
	 * @Description : 向工作表中填充数据
	 * @param sheet
	 *            工作表
	 * @param list
	 *            数据源
	 * @param fieldMap
	 *            中英文字段对应关系的Map
	 * @param additionalFieldHeader
	 *            额外表头
	 * @param firstIndex
	 *            开始索引
	 * @param lastIndex
	 *            结束索引
	 */
	private static <T> void fillSheet(WritableSheet sheet, List<T> list, List<CellDto> fieldMap, RowDto additionalFieldHeaderRowDto, int firstIndex, int lastIndex)
			throws Exception {

		// 添加表头
		ColumnRowPositionDto columnRowPositionDto = addOneRowToSheet(sheet, new ColumnRowPositionDto(0, 0), additionalFieldHeaderRowDto);
		// 从第一列开始
		columnRowPositionDto.setColumnNum(0);
		// 行集合
		List<RowDto> rowList = Lists.newArrayList();
		if (CollectionUtils.isNotEmpty(fieldMap) == true) {
			// 声明行对象
			RowDto rowDto;
			// 列集合
			List<CellDto> cellList;
			// 列对象
			CellDto cellDto;
			// 定义存放英文字段名和中文字段名的数组
			{
				// 填充表头
				rowDto = new RowDto();
				rowDto.setCells(fieldMap);
				rowList.add(rowDto);
			}
			//
			//
			// 添加内容到集合中
			for (int index = firstIndex; index <= lastIndex; index++) {
				//
				rowDto = new RowDto();
				cellList = Lists.newArrayList();
				// 获取单个对象
				T item = list.get(index);
				// 遍历字段集合
				for (CellDto cellDtoTemp : fieldMap) {
					Object objValue = getFieldValueByNameSequence(cellDtoTemp.getEnName(), item);
					// 单元格的值
					String fieldValue = objValue == null ? "" : objValue.toString();
					//
					cellDto = new CellDto();
					BeanUtils.copyProperties(cellDtoTemp, cellDto);
					cellDto.setCname(fieldValue);
					cellList.add(cellDto);
				}
				rowDto.setCells(cellList);
				//
				rowList.add(rowDto);
			}

		}
		// 写入excel
		columnRowPositionDto = addRowsToSheet(sheet, columnRowPositionDto, rowList);
	}

	/**
	 * 行列坐标点dtok
	 * 
	 * @author nibili 2017年4月21日
	 * 
	 */
	public static class ColumnRowPositionDto {

		/** 行号 */
		private int rowNum;
		/** 列号 */
		private int columnNum;

		public ColumnRowPositionDto() {

		}

		public ColumnRowPositionDto(int columnNum, int rowNum) {
			this.columnNum = columnNum;
			this.rowNum = rowNum;
		}

		/**
		 * get 行号
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public int getRowNum() {
			return rowNum;
		}

		/**
		 * set 行号
		 * 
		 * @param rowNum
		 * @author nibili 2017年4月21日
		 */
		public void setRowNum(int rowNum) {
			this.rowNum = rowNum;
		}

		/**
		 * get 列号
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public int getColumnNum() {
			return columnNum;
		}

		/**
		 * set 列号
		 * 
		 * @param columnNum
		 * @author nibili 2017年4月21日
		 */
		public void setColumnNum(int columnNum) {
			this.columnNum = columnNum;
		}

	}

	/**
	 * 额外字段title
	 * 
	 * @author nibili 2017年4月21日
	 * 
	 */
	public static class CellDto {

		/** 英文名称 */
		private String enName;
		/** 字段名称 */
		private String cname;
		/** 所占列数 */
		private int columns = 1;
		/** 所占行数 */
		private int rows = 1;
		/** 列宽 */
		private Integer width = 20;
		/** 行高 */
		private Integer height = 20;
		/** 字体大小 */
		private int fontSize = 12;
		/** 字体颜色 */
		private jxl.format.Colour colour = Colour.BLACK;
		/** 字体 */
		private WritableFont.FontName fontName = WritableFont.ARIAL;
		/** 是否粗体 */
		private boolean isBold = false;
		/** 文本居中，靠左靠右。。。。等排列方式 */
		private Alignment alignment = Alignment.CENTRE;

		/**
		 * get 英文名称
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public String getEnName() {
			return enName;
		}

		/**
		 * set 英文名称
		 * 
		 * @param enName
		 * @author nibili 2017年4月21日
		 */
		public void setEnName(String enName) {
			this.enName = enName;
		}

		/**
		 * get 字段名称
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public String getCname() {
			return cname;
		}

		/**
		 * set 字段名称
		 * 
		 * @param cname
		 * @author nibili 2017年4月21日
		 */
		public void setCname(String cname) {
			this.cname = cname;
		}

		/**
		 * get 所占列数
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public int getColumns() {
			return columns;
		}

		/**
		 * set 所占列数
		 * 
		 * @param columns
		 * @author nibili 2017年4月21日
		 */
		public void setColumns(int columns) {
			this.columns = columns;
		}

		/**
		 * get 所占行数
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public int getRows() {
			return rows;
		}

		/**
		 * set 所占行数
		 * 
		 * @param rows
		 * @author nibili 2017年4月21日
		 */
		public void setRows(int rows) {
			this.rows = rows;
		}

		/**
		 * get fontSize
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public int getFontSize() {
			return fontSize;
		}

		/**
		 * set fontSize
		 * 
		 * @param fontSize
		 * @author nibili 2017年4月21日
		 */
		public void setFontSize(int fontSize) {
			this.fontSize = fontSize;
		}

		/**
		 * get 字体颜色
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public jxl.format.Colour getColour() {
			return colour;
		}

		/**
		 * set 字体颜色
		 * 
		 * @param colour
		 * @author nibili 2017年4月21日
		 */
		public void setColour(jxl.format.Colour colour) {
			this.colour = colour;
		}

		/**
		 * get 字体
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public WritableFont.FontName getFontName() {
			return fontName;
		}

		/**
		 * set 字体
		 * 
		 * @param fontName
		 * @author nibili 2017年4月21日
		 */
		public void setFontName(WritableFont.FontName fontName) {
			this.fontName = fontName;
		}

		/**
		 * get 文本居中，靠左靠右。。。。等排列方式
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public Alignment getAlignment() {
			return alignment;
		}

		/**
		 * set 文本居中，靠左靠右。。。。等排列方式
		 * 
		 * @param alignment
		 * @author nibili 2017年4月21日
		 */
		public void setAlignment(Alignment alignment) {
			this.alignment = alignment;
		}

		/**
		 * get 粗体样式
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public boolean getIsBold() {
			return isBold;
		}

		/**
		 * set 粗体样式
		 * 
		 * @param isBold
		 * @author nibili 2017年4月21日
		 */
		public void setIsBold(boolean isBold) {
			this.isBold = isBold;
		}

		/**
		 * get 列宽
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public Integer getWidth() {
			return width;
		}

		/**
		 * set 列宽
		 * 
		 * @param width
		 * @author nibili 2017年4月21日
		 */
		public void setWidth(Integer width) {
			this.width = width;
		}

		/**
		 * get 行高
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public Integer getHeight() {
			return height;
		}

		/**
		 * set 行高
		 * 
		 * @param height
		 * @author nibili 2017年4月21日
		 */
		public void setHeight(Integer height) {
			this.height = height;
		}

	}

	/** 行数据 */
	public static class RowDto {
		/** 行高 */
		private Integer rowHeight = null;
		/** 一行中的列数据 */
		private List<CellDto> cells;

		/**
		 * get 一行中的列数据
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public List<CellDto> getCells() {
			return cells;
		}

		/**
		 * set 一行中的列数据
		 * 
		 * @param cells
		 * @author nibili 2017年4月21日
		 */
		public void setCells(List<CellDto> cells) {
			this.cells = cells;
		}

		/**
		 * get 行高
		 * 
		 * @return
		 * @author nibili 2017年4月21日
		 */
		public Integer getRowHeight() {
			return rowHeight;
		}

		/**
		 * set 行高
		 * 
		 * @param rowHeight
		 * @author nibili 2017年4月21日
		 */
		public void setRowHeight(int rowHeight) {
			this.rowHeight = rowHeight;
		}

	}

	/*-------------------------------------------------------------------------------导入-----------------------------------------------*/

	/**
	 * 读取excel内容
	 * 
	 * @param filePath
	 * @param sheetName
	 * @return
	 * @throws ExcelException
	 * @throws FileNotFoundException
	 * @auth nibili 2018年11月7日 196888813@qq.com
	 */
	public static List<List<String>> excelToList(String filePath, String sheetName) throws ExcelException, FileNotFoundException {
		return excelToList(new FileInputStream(new File(filePath)), sheetName);
	}

	/**
	 * @MethodName : excelToList
	 * @Description : 将Excel转化为List
	 * @param in
	 *            ：承载着Excel的输入流
	 * @param sheetIndex
	 *            ：要导入的工作表序号
	 * 
	 * @return ：List
	 * @throws ExcelException
	 */
	public static List<List<String>> excelToList(InputStream in, String sheetName) throws ExcelException {

		// 定义要返回的list
		List<List<String>> resultList = new ArrayList<List<String>>();

		try {

			// 根据Excel数据源创建WorkBook
			Workbook wb = Workbook.getWorkbook(in);
			// 获取工作表
			Sheet sheet = wb.getSheet(sheetName);

			// 获取工作表的有效行数
			int realRows = 0;
			for (int i = 0; i < sheet.getRows(); i++) {

				int nullCols = 0;
				for (int j = 0; j < sheet.getColumns(); j++) {
					Cell currentCell = sheet.getCell(j, i);
					if (currentCell == null || "".equals(currentCell.getContents().toString())) {
						nullCols++;
					}
				}

				if (nullCols == sheet.getColumns()) {
					break;
				} else {
					realRows++;
				}
			}

			// 如果Excel中没有数据则提示错误
			if (realRows <= 1) {
				throw new ExcelException("Excel文件中没有任何数据");
			}

			for (int rowNum = 0; rowNum < realRows; rowNum++) {
				List<String> row = Lists.newArrayList();
				Cell[] cells = sheet.getRow(rowNum);
				for (int col = 0, len = cells.length; col < len; col++) {
					// 获取当前单元格中的内容
					String content = sheet.getCell(col, rowNum).getContents().toString().trim();
					row.add(content);
				}
				resultList.add(row);
			}

		} catch (Exception e) {
			e.printStackTrace();
			// 如果是ExcelException，则直接抛出
			if (e instanceof ExcelException) {
				throw (ExcelException) e;

				// 否则将其它异常包装成ExcelException再抛出
			} else {
				e.printStackTrace();
				throw new ExcelException("导入Excel失败");
			}
		}
		return resultList;
	}

	/**
	 * @MethodName : excelToList
	 * @Description : 将Excel转化为List
	 * @param in
	 *            ：承载着Excel的输入流
	 * @param sheetIndex
	 *            ：要导入的工作表序号
	 * @param entityClass
	 *            ：List中对象的类型（Excel中的每一行都要转化为该类型的对象）
	 * @param fieldMap
	 *            ：Excel中的中文列头和类的英文属性的对应关系Map
	 * @param uniqueFields
	 *            ：指定业务主键组合（即复合主键），这些列的组合不能重复
	 * @return ：List
	 * @throws ExcelException
	 */
	public static <T> List<T> excelToList(InputStream in, String sheetName, Class<T> entityClass, LinkedHashMap<String, String> fieldMap, String[] uniqueFields)
			throws ExcelException {

		// 定义要返回的list
		List<T> resultList = new ArrayList<T>();

		try {

			// 根据Excel数据源创建WorkBook
			Workbook wb = Workbook.getWorkbook(in);
			// 获取工作表
			Sheet sheet = wb.getSheet(sheetName);

			// 获取工作表的有效行数
			int realRows = 0;
			for (int i = 0; i < sheet.getRows(); i++) {

				int nullCols = 0;
				for (int j = 0; j < sheet.getColumns(); j++) {
					Cell currentCell = sheet.getCell(j, i);
					if (currentCell == null || "".equals(currentCell.getContents().toString())) {
						nullCols++;
					}
				}

				if (nullCols == sheet.getColumns()) {
					break;
				} else {
					realRows++;
				}
			}

			// 如果Excel中没有数据则提示错误
			if (realRows <= 1) {
				throw new ExcelException("Excel文件中没有任何数据");
			}

			Cell[] firstRow = sheet.getRow(0);

			String[] excelFieldNames = new String[firstRow.length];

			// 获取Excel中的列名
			for (int i = 0; i < firstRow.length; i++) {
				excelFieldNames[i] = firstRow[i].getContents().toString().trim();
			}

			// 判断需要的字段在Excel中是否都存在
			boolean isExist = true;
			List<String> excelFieldList = Arrays.asList(excelFieldNames);
			for (String cnName : fieldMap.keySet()) {
				if (!excelFieldList.contains(cnName)) {
					isExist = false;
					break;
				}
			}

			// 如果有列名不存在，则抛出异常，提示错误
			if (!isExist) {
				throw new ExcelException("Excel中缺少必要的字段，或字段名称有误");
			}

			// 将列名和列号放入Map中,这样通过列名就可以拿到列号
			LinkedHashMap<String, Integer> colMap = new LinkedHashMap<String, Integer>();
			for (int i = 0; i < excelFieldNames.length; i++) {
				colMap.put(excelFieldNames[i], firstRow[i].getColumn());
			}

			// 判断是否有重复行
			// 1.获取uniqueFields指定的列
			Cell[][] uniqueCells = new Cell[uniqueFields.length][];
			for (int i = 0; i < uniqueFields.length; i++) {
				int col = colMap.get(uniqueFields[i]);
				uniqueCells[i] = sheet.getColumn(col);
			}

			// 2.从指定列中寻找重复行
			for (int i = 1; i < realRows; i++) {
				int nullCols = 0;
				for (int j = 0; j < uniqueFields.length; j++) {
					String currentContent = uniqueCells[j][i].getContents();
					Cell sameCell = sheet.findCell(currentContent, uniqueCells[j][i].getColumn(), uniqueCells[j][i].getRow() + 1, uniqueCells[j][i].getColumn(),
							uniqueCells[j][realRows - 1].getRow(), true);
					if (sameCell != null) {
						nullCols++;
					}
				}

				if (nullCols == uniqueFields.length) {
					throw new ExcelException("Excel中有重复行，请检查");
				}
			}

			// 将sheet转换为list
			for (int i = 1; i < realRows; i++) {
				// 新建要转换的对象
				T entity = entityClass.newInstance();

				// 给对象中的字段赋值
				for (Entry<String, String> entry : fieldMap.entrySet()) {
					// 获取中文字段名
					String cnNormalName = entry.getKey();
					// 获取英文字段名
					String enNormalName = entry.getValue();
					// 根据中文字段名获取列号
					int col = colMap.get(cnNormalName);

					// 获取当前单元格中的内容
					String content = sheet.getCell(col, i).getContents().toString().trim();

					// 给对象赋值
					setFieldValueByName(enNormalName, content, entity);
				}

				resultList.add(entity);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// 如果是ExcelException，则直接抛出
			if (e instanceof ExcelException) {
				throw (ExcelException) e;

				// 否则将其它异常包装成ExcelException再抛出
			} else {
				e.printStackTrace();
				throw new ExcelException("导入Excel失败");
			}
		}
		return resultList;
	}

	/*
	 * <-------------------------辅助的私有方法------------------------------------------
	 * ----->
	 */
	/**
	 * @MethodName : getFieldValueByName
	 * @Description : 根据字段名获取字段值
	 * @param fieldName
	 *            字段名
	 * @param o
	 *            对象
	 * @return 字段值
	 */
	private static Object getFieldValueByName(String fieldName, Object o) throws Exception {

		Object value = null;
		Field field = getFieldByName(fieldName, o.getClass());

		if (field != null) {
			field.setAccessible(true);
			value = field.get(o);
			return value;
		} else {
			// 字段不存在 找get方法
			String fieldGetName = parGetName(fieldName);
			Method fieldGetMet = o.getClass().getMethod(fieldGetName, new Class[] {});
			if (fieldGetMet != null) {
				Object fieldVal = fieldGetMet.invoke(o, new Object[] {});
				return fieldVal;
			}
		}
		throw new ExcelException("类字段不存在");
	}

	/**
	 * 拼接某属性的 get方法
	 * 
	 * @param fieldName
	 * @return String
	 */
	public static String parGetName(String fieldName) {
		if (null == fieldName || "".equals(fieldName)) {
			return null;
		}
		int startIndex = 0;
		if (fieldName.charAt(0) == '_')
			startIndex = 1;
		return "get" + fieldName.substring(startIndex, startIndex + 1).toUpperCase() + fieldName.substring(startIndex + 1);
	}

	/**
	 * @MethodName : getFieldByName
	 * @Description : 根据字段名获取字段
	 * @param fieldName
	 *            字段名
	 * @param clazz
	 *            包含该字段的类
	 * @return 字段
	 */
	private static Field getFieldByName(String fieldName, Class<?> clazz) {
		// 拿到本类的所有字段
		Field[] selfFields = clazz.getDeclaredFields();

		// 如果本类中存在该字段，则返回
		for (Field field : selfFields) {
			if (field.getName().equals(fieldName)) {
				return field;
			}
		}

		// 否则，查看父类中是否存在此字段，如果有则返回
		Class<?> superClazz = clazz.getSuperclass();
		if (superClazz != null && superClazz != Object.class) {
			return getFieldByName(fieldName, superClazz);
		}

		// 如果本类和父类都没有，则返回空
		return null;
	}

	/**
	 * @MethodName : getFieldValueByNameSequence
	 * @Description : 根据带路径或不带路径的属性名获取属性值
	 *              即接受简单属性名，如userName等，又接受带路径的属性名，如student.department.name等
	 * 
	 * @param fieldNameSequence
	 *            带路径的属性名或简单属性名
	 * @param o
	 *            对象
	 * @return 属性值
	 * @throws Exception
	 */
	private static Object getFieldValueByNameSequence(String fieldNameSequence, Object o) throws Exception {

		Object value = null;

		// 将fieldNameSequence进行拆分
		String[] attributes = fieldNameSequence.split("\\.");
		if (attributes.length == 1) {
			value = getFieldValueByName(fieldNameSequence, o);
		} else {
			// 根据属性名获取属性对象
			Object fieldObj = getFieldValueByName(attributes[0], o);
			String subFieldNameSequence = fieldNameSequence.substring(fieldNameSequence.indexOf(".") + 1);
			value = getFieldValueByNameSequence(subFieldNameSequence, fieldObj);
		}
		return value;

	}

	/**
	 * @MethodName : setFieldValueByName
	 * @Description : 根据字段名给对象的字段赋值
	 * @param fieldName
	 *            字段名
	 * @param fieldValue
	 *            字段值
	 * @param o
	 *            对象
	 */
	private static void setFieldValueByName(String fieldName, Object fieldValue, Object o) throws Exception {

		Field field = getFieldByName(fieldName, o.getClass());
		if (field != null) {
			field.setAccessible(true);
			// 获取字段类型
			Class<?> fieldType = field.getType();

			// 根据字段类型给字段赋值
			if (String.class == fieldType) {
				field.set(o, String.valueOf(fieldValue));
			} else if ((Integer.TYPE == fieldType) || (Integer.class == fieldType)) {
				field.set(o, Integer.parseInt(fieldValue.toString()));
			} else if ((Long.TYPE == fieldType) || (Long.class == fieldType)) {
				field.set(o, Long.valueOf(fieldValue.toString()));
			} else if ((Float.TYPE == fieldType) || (Float.class == fieldType)) {
				field.set(o, Float.valueOf(fieldValue.toString()));
			} else if ((Short.TYPE == fieldType) || (Short.class == fieldType)) {
				field.set(o, Short.valueOf(fieldValue.toString()));
			} else if ((Double.TYPE == fieldType) || (Double.class == fieldType)) {
				field.set(o, Double.valueOf(fieldValue.toString()));
			} else if (Character.TYPE == fieldType) {
				if ((fieldValue != null) && (fieldValue.toString().length() > 0)) {
					field.set(o, Character.valueOf(fieldValue.toString().charAt(0)));
				}
			} else if (Date.class == fieldType) {
				field.set(o, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fieldValue.toString()));
			} else {
				field.set(o, fieldValue);
			}
		} else {
			throw new ExcelException(o.getClass().getSimpleName() + "类不存在字段名 " + fieldName);
		}
	}

	/**
	 * 
	 * @author nibili 2017年4月21日
	 * 
	 */
	public static class ExcelException extends Exception {

		/** */
		private static final long serialVersionUID = -7747451731201941253L;

		public ExcelException() {
			// TODO Auto-generated constructor stub
		}

		public ExcelException(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}

		public ExcelException(Throwable cause) {
			super(cause);
			// TODO Auto-generated constructor stub
		}

		public ExcelException(String message, Throwable cause) {
			super(message, cause);
			// TODO Auto-generated constructor stub
		}

	}
}
