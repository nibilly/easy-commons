package cn.com.easy.utils.RSA;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;

/**
 * RSA非对称加密辅助类 前端使用jsencrypt进行分段加密不支持modulus形式加密只能支持Encode方式加密，
 * 即前端使用jsencrypt加密时使用encryptByPublicKey()、encryptByPrivateKey()方法 后端进行分段解密
 */
public class RSAUtil {

	/** 指定加密算法为DESede */
	private static String ALGORITHM = "RSA/ECB/PKCS1Padding";// RSA/ECB/PKCS1Padding
	/**
	 * 加密算法RSA
	 */
	public static final String KEY_ALGORITHM = "RSA";
	/**
	 * 签名算法
	 */
	public static final String SIGNATURE_ALGORITHM = "MD5withRSA";

//	/**
//	 * 获取公钥的key
//	 */
//	private static final String PUBLIC_KEY = "RSAPublicKey";
//
//	/**
//	 * 获取私钥的key
//	 */
//	private static final String PRIVATE_KEY = "RSAPrivateKey";

	/* RSA最大加密明文大小 */
	private static final int MAX_ENCRYPT_BLOCK = 117;
	/* RSA最大解密密文大小 */
	private static final int MAX_DECRYPT_BLOCK = 128;

	/**
	 * RSA 位数 如果采用2048 上面最大加密和最大解密则须填写: 245 256 指定key的大小(64的整数倍,最小512位)
	 */
	private static int KEYSIZE = 1024;

	/* 公钥Key字符串 */
	public static String PUBLIC_KEY_ENCODED = "RSAPublicKeyEncoded";
	/* 私钥Key字符串 */
	public static String PRIVATE_KEY_ENCODED = "RSAPrivateEncoded";

	/* 公钥模量key */
	public static String PUBLIC_MODULUS_KEY = "RSAPublicModulusKey";
	/* 公钥指数key */
	public static String PUBLIC_EXPONENT_KEY = "RSAPublicExponentKey";
	/* 私钥模量key */
	public static String PRIVATE_MODULUS_KEY = "RSAPrivateModulusKey";
	/* 私钥指数key */
	public static String PRIVATE_EXPONENT_KEY = "RSAPrivateExponentKey";

	private static Map<String, Object> keyPairMap = new ConcurrentHashMap<>();

	private static KeyFactory keyFactory = null;

	static {
		try {
			keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
			genKeyPair(KEYSIZE);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public RSAUtil() {
		try {
			genKeyPair(KEYSIZE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public RSAUtil(int keySize) {
		try {
			genKeyPair(keySize);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 生成密钥对(公钥和私钥)
	 *
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Object> genKeyPair(int keySize) throws Exception {
		if (keySize <= 0) {
			keySize = KEYSIZE;
		}

		/** RSA算法要求有一个可信任的随机数源 */
		SecureRandom random = new SecureRandom();
		/** 为RSA算法创建一个KeyPairGenerator对象 */
		KeyPairGenerator kpg = KeyPairGenerator.getInstance(KEY_ALGORITHM);
		/** 利用上面的随机数据源初始化这个KeyPairGenerator对象 */
		kpg.initialize(keySize, random);
		/** 生成密匙对 */
		KeyPair kp = kpg.generateKeyPair();
		/** 得到公钥 */
		Key publicKey = kp.getPublic();
		/** 得到私钥 */
		Key privateKey = kp.getPrivate();
		/** 用字符串将生成的密钥写入文件 */

		// keyPairMap.put(PUBLIC_KEY, publicKey);
		// keyPairMap.put(PRIVATE_KEY, privateKey);

		String algorithm = publicKey.getAlgorithm(); // 获取算法
		KeyFactory keyFact = KeyFactory.getInstance(algorithm);

		RSAPublicKeySpec keySpec = (RSAPublicKeySpec) keyFact.getKeySpec(publicKey, RSAPublicKeySpec.class);
		BigInteger publicModulus = keySpec.getModulus();
		BigInteger publicExponent = keySpec.getPublicExponent();

		// 公钥模量及指数
		keyPairMap.put(PUBLIC_MODULUS_KEY, HexUtil.bytes2Hex(publicModulus.toByteArray()));
		keyPairMap.put(PUBLIC_EXPONENT_KEY, HexUtil.bytes2Hex(publicExponent.toByteArray()));
		// 得到公钥字符串

		// keyPairMap.put(PUBLIC_KEY_ENCODED,
		// HexUtil.bytes2Hex(publicKey.getEncoded()));
		keyPairMap.put(PUBLIC_KEY_ENCODED, new String(Base64.encodeBase64(publicKey.getEncoded())));

		RSAPrivateCrtKeySpec privateKeySpec = (RSAPrivateCrtKeySpec) keyFact.getKeySpec(privateKey, RSAPrivateCrtKeySpec.class);
		BigInteger privateModulus = privateKeySpec.getModulus();
		BigInteger privateExponent = privateKeySpec.getPrivateExponent();

		// 私钥模量及指数
		keyPairMap.put(PRIVATE_MODULUS_KEY, HexUtil.bytes2Hex(privateModulus.toByteArray()));
		keyPairMap.put(PRIVATE_EXPONENT_KEY, HexUtil.bytes2Hex(privateExponent.toByteArray()));

		// 得到私钥字符串
		// keyPairMap.put(PRIVATE_KEY_ENCODED,
		// HexUtil.bytes2Hex(privateKey.getEncoded()));
		keyPairMap.put(PRIVATE_KEY_ENCODED, new String(Base64.encodeBase64(privateKey.getEncoded())));
		return keyPairMap;
	}

	/**
	 * 根据给定的16进制系数和专用指数字符串构造一个RSA专用的公钥对象。
	 *
	 * @param hexModulus  系数。
	 * @param hexExponent 专用指数。
	 * @return RSA专用公钥对象。
	 */
	public static RSAPublicKey getRSAPublicKey(String hexModulus, String hexExponent) {
		if (isBlank(hexModulus) || isBlank(hexExponent)) {
			System.out.println("hexModulus and hexExponent cannot be empty. return null(RSAPublicKey).");
			return null;
		}
		byte[] modulus = null;
		byte[] exponent = null;
		try {
			modulus = HexUtil.hex2Bytes(hexModulus);
			// modulus = Base64.decodeBase64(hexModulus);
			exponent = HexUtil.hex2Bytes(hexExponent);
		} catch (Exception ex) {
			System.out.println("hexModulus or hexExponent value is invalid. return null(RSAPublicKey).");
			ex.printStackTrace();
		}
		if (modulus != null && exponent != null) {
			return generateRSAPublicKey(modulus, exponent);
		}
		return null;
	}

	/**
	 * 根据给定的系数和专用指数构造一个RSA专用的公钥对象。
	 *
	 * @param modulus  系数。
	 * @param exponent 专用指数。
	 * @return RSA专用公钥对象。
	 */
	public static RSAPublicKey generateRSAPublicKey(byte[] modulus, byte[] exponent) {
		RSAPublicKeySpec publicKeySpec = new RSAPublicKeySpec(new BigInteger(modulus), new BigInteger(exponent));
		try {
			return (RSAPublicKey) keyFactory.generatePublic(publicKeySpec);
		} catch (InvalidKeySpecException ex) {
			System.out.println("RSAPublicKeySpec is unavailable.");
			ex.printStackTrace();
		} catch (NullPointerException ex) {
			System.out.println("RSAUtil#KEY_FACTORY is null, can not generate KeyFactory instance.");
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * 根据给定的16进制系数和专用指数字符串构造一个RSA专用的私钥对象。
	 *
	 * @param hexModulus  系数。
	 * @param hexExponent 专用指数。
	 * @return RSA专用私钥对象。
	 */
	public static RSAPrivateKey getRSAPrivateKey(String hexModulus, String hexExponent) {
		if (isBlank(hexModulus) || isBlank(hexExponent)) {
			System.out.println("hexModulus and hexExponent cannot be empty. RSAPrivateKey value is null to return.");
			return null;
		}
		byte[] modulus = null;
		byte[] exponent = null;
		try {
			modulus = HexUtil.hex2Bytes(hexModulus);
			// modulus = Base64.decodeBase64(hexModulus);
			exponent = HexUtil.hex2Bytes(hexExponent);
		} catch (Exception ex) {
			System.out.println("hexModulus or hexExponent value is invalid. return null(RSAPrivateKey).");
			ex.printStackTrace();
		}
		if (modulus != null && exponent != null) {
			return generateRSAPrivateKey(modulus, exponent);
		}
		return null;
	}

	/**
	 * 根据给定的系数和专用指数构造一个RSA专用的私钥对象。
	 *
	 * @param modulus  系数。
	 * @param exponent 专用指数。
	 * @return RSA专用私钥对象。
	 */
	public static RSAPrivateKey generateRSAPrivateKey(byte[] modulus, byte[] exponent) {
		RSAPrivateKeySpec privateKeySpec = new RSAPrivateKeySpec(new BigInteger(modulus), new BigInteger(exponent));
		try {
			return (RSAPrivateKey) keyFactory.generatePrivate(privateKeySpec);
		} catch (InvalidKeySpecException ex) {
			System.out.println("RSAPrivateKeySpec is unavailable.");
			ex.printStackTrace();
		} catch (NullPointerException ex) {
			System.out.println("RSAUtil#KEY_FACTORY is null, can not generate KeyFactory instance.");
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * <p>
	 * 用私钥对信息生成数字签名
	 * </p>
	 *
	 * @param data       已加密数据
	 * @param privateKey 私钥(BASE64编码)
	 *
	 * @return
	 * @throws Exception
	 */
	public static String sign(byte[] data, String privateKey) throws Exception {
		byte[] keyBytes = Base64.decodeBase64(privateKey);
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		PrivateKey privateK = keyFactory.generatePrivate(pkcs8KeySpec);
		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
		signature.initSign(privateK);
		signature.update(data);
		return Base64.encodeBase64String(signature.sign());
	}

	/**
	 * <p>
	 * 校验数字签名
	 * </p>
	 *
	 * @param data      已加密数据
	 * @param publicKey 公钥(BASE64编码)
	 * @param sign      数字签名
	 *
	 * @return
	 * @throws Exception
	 *
	 */
	public static boolean verify(byte[] data, String publicKey, String sign) throws Exception {
		byte[] keyBytes = Base64.decodeBase64(publicKey);
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		PublicKey publicK = keyFactory.generatePublic(keySpec);
		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
		signature.initVerify(publicK);
		signature.update(data);
		return signature.verify(Base64.decodeBase64(sign));
	}

	/**
	 * 使用给定的公钥加密给定的字符串。
	 *
	 * @param publicKey 给定的公钥。
	 * @param plaintext 字符串。
	 * @return 给定字符串的密文。
	 */
	public static String encryptString(Key key, String plaintext) {
		if (key == null || plaintext == null) {
			return null;
		}
		byte[] data = plaintext.getBytes();
		try {
			byte[] en_data = encrypt(key, data);
			return new String(Base64.encodeBase64String(en_data));
//			return new String(HexUtil.bytes2Hex(en_data));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * 使用指定的公钥加密数据。
	 *
	 * @param publicKey 给定的公钥。
	 * @param data      要加密的数据。
	 * @return 加密后的数据。
	 */

	public static byte[] encrypt(Key publicKey, byte[] data) throws Exception {
		Cipher ci = Cipher.getInstance(ALGORITHM);
		ci.init(Cipher.ENCRYPT_MODE, publicKey);
		int inputLen = data.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段加密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
				cache = ci.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
			} else {
				cache = ci.doFinal(data, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_ENCRYPT_BLOCK;
		}
		byte[] encryptedData = out.toByteArray();
		out.close();
		return encryptedData;
	}

	/**
	 * 使用给定的公钥/私钥解密给定的字符串。
	 * 
	 * @param key         给定的公钥或私钥
	 * @param encryptText 密文
	 * @return 原文字符串。
	 */
	public static String decryptString(Key key, String encryptText) {
		if (key == null || isBlank(encryptText)) {
			return null;
		}
		try {
			byte[] en_data = Base64.decodeBase64(encryptText);
			// byte[] en_data = HexUtil.hex2Bytes(encryptText);
			byte[] data = decrypt(key, en_data);
			return new String(data);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * 使用指定的公钥解密数据。
	 * 
	 * @param publicKey 指定的公钥
	 * @param data      要解密的数据
	 * @return 原数据
	 * @throws Exception
	 */
	public static byte[] decrypt(Key publicKey, byte[] data) throws Exception {
		Cipher ci = Cipher.getInstance(ALGORITHM);

		ci.init(Cipher.DECRYPT_MODE, publicKey);
		// return ci.doFinal(data);
		int inputLen = data.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段解密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
				cache = ci.doFinal(data, offSet, MAX_DECRYPT_BLOCK);
			} else {
				cache = ci.doFinal(data, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_DECRYPT_BLOCK;
		}
		byte[] decryptedData = out.toByteArray();
		out.close();
		return decryptedData;
	}

	/**
	 * <P>
	 * 私钥解密
	 * </p>
	 *
	 * @param encryptedData 已加密数据
	 * @param privateKey    私钥(BASE64编码)
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptByPrivateKey(byte[] encryptedData, String privateKey) throws Exception {
		byte[] keyBytes = Base64.decodeBase64(privateKey);
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key privateK = keyFactory.generatePrivate(pkcs8KeySpec);
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, privateK);
		int inputLen = encryptedData.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段解密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
				cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_DECRYPT_BLOCK;
		}
		byte[] decryptedData = out.toByteArray();
		out.close();
		return decryptedData;
	}

	/**
	 * <p>
	 * 公钥解密
	 * </p>
	 *
	 * @param encryptedData 已加密数据
	 * @param publicKey     公钥(BASE64编码)
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptByPublicKey(byte[] encryptedData, String publicKey) throws Exception {
		byte[] keyBytes = Base64.decodeBase64(publicKey);
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key publicK = keyFactory.generatePublic(x509KeySpec);
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, publicK);
		int inputLen = encryptedData.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段解密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
				cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_DECRYPT_BLOCK;
		}
		byte[] decryptedData = out.toByteArray();
		out.close();
		return decryptedData;
	}

	/**
	 * <p>
	 * 公钥加密
	 * </p>
	 *
	 * @param data      源数据
	 * @param publicKey 公钥(BASE64编码)
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptByPublicKey(byte[] data, String publicKey) throws Exception {
		byte[] keyBytes = Base64.decodeBase64(publicKey);
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key publicK = keyFactory.generatePublic(x509KeySpec);
		// 对数据加密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, publicK);
		int inputLen = data.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段加密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
				cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(data, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_ENCRYPT_BLOCK;
		}
		byte[] encryptedData = out.toByteArray();
		out.close();
		return encryptedData;
	}

	/**
	 * <p>
	 * 私钥加密
	 * </p>
	 *
	 * @param data       源数据
	 * @param privateKey 私钥(BASE64编码)
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptByPrivateKey(byte[] data, String privateKey) throws Exception {
		byte[] keyBytes = Base64.decodeBase64(privateKey);
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key privateK = keyFactory.generatePrivate(pkcs8KeySpec);
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, privateK);
		int inputLen = data.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段加密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
				cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(data, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_ENCRYPT_BLOCK;
		}
		byte[] encryptedData = out.toByteArray();
		out.close();
		return encryptedData;
	}

	/**
	 * 判断非空字符串
	 * 
	 * @param cs 待判断的CharSequence序列
	 * @return 是否非空
	 */
	private static boolean isBlank(final CharSequence cs) {
		int strLen;
		if (cs == null || (strLen = cs.length()) == 0) {
			return true;
		}
		for (int i = 0; i < strLen; i++) {
			if (Character.isWhitespace(cs.charAt(i)) == false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 返回公钥Key
	 * 
	 * @return
	 */
	public static String getPublicKey() {
		Object obj = keyPairMap.get(PUBLIC_KEY_ENCODED);
		if (obj != null) {
			return obj.toString();
		}
		return null;
	}

	/**
	 * 返回私钥Key
	 * 
	 * @return
	 */
	public static String getPrivateKey() {
		Object obj = keyPairMap.get(PRIVATE_KEY_ENCODED);
		if (obj != null) {
			return obj.toString();
		}
		return null;
	}

	/**
	 * 返回公钥对的模数/系数
	 * 
	 * @return
	 */
	public static String getPublicKeyModulus() {
		Object obj = keyPairMap.get(PUBLIC_MODULUS_KEY);
		if (obj != null) {
			return obj.toString();
		}
		return null;
	}

	/**
	 *
	 * @return
	 */
	public static String getPublicKeyExponent() {
		Object obj = keyPairMap.get(PUBLIC_EXPONENT_KEY);
		if (obj != null) {
			return obj.toString();
		}
		return null;
	}

	/**
	 * 返回私钥对的模数/系数
	 * 
	 * @return
	 */
	public static String getPrivateKeyModulus() {
		Object obj = keyPairMap.get(PRIVATE_MODULUS_KEY);
		if (obj != null) {
			return obj.toString();
		}
		return null;
	}

	/**
	 *
	 * @return
	 */
	public static String getPrivateKeyExponent() {
		Object obj = keyPairMap.get(PRIVATE_EXPONENT_KEY);
		if (obj != null) {
			return obj.toString();
		}
		return null;
	}

	public static void main(String[] args) {
		// new RSAUtil();
		/*
		 * String source =
		 * "123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！123hello你好！";
		 * 
		 * System.out.println("RSAUtil.PUBLIC_MODULUS=="+RSAUtil.getPublicKeyModulus());
		 * System.out.println("RSAUtil.PUBLIC_EXPONENT=="+RSAUtil.getPublicKeyExponent()
		 * );
		 * System.out.println("RSAUtil.PRIVATE_MODULUS=="+RSAUtil.getPrivateKeyModulus()
		 * );
		 * System.out.println("RSAUtil.PRIVATE_EXPONENT=="+RSAUtil.getPrivateKeyExponent
		 * ());
		 * 
		 * //公钥加密，私钥解密 PublicKey publicKey =
		 * RSAUtil.getRSAPublicKey(RSAUtil.getPublicKeyModulus(),
		 * RSAUtil.getPublicKeyExponent()); String encript =
		 * RSAUtil.encryptString(publicKey, source);
		 * System.out.println("加密后数据："+encript); PrivateKey privateKey =
		 * RSAUtil.getRSAPrivateKey(RSAUtil.getPrivateKeyModulus(),
		 * RSAUtil.getPrivateKeyExponent()); String newSource =
		 * RSAUtil.decryptString(privateKey, encript);
		 * System.out.println("解密后数据:"+newSource);
		 */

		// 私钥加密，公钥解密
//		String priKeyStr = RSAUtil.encryptString(privateKey, source);
//		System.out.println("加密后数据："+priKeyStr);
//		String oldSource = RSAUtil.decryptString(publicKey, priKeyStr);
//		System.out.println("解密后数据:"+oldSource);

		try {
			test();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	static void test() throws Exception {
		String publicKey = RSAUtil.getPublicKey();
		String privateKey = RSAUtil.getPrivateKey();

		System.out.println("RSAUtil.publicKey==" + publicKey);
		System.out.println("RSAUtil.privateKey==" + privateKey);

		System.err.println("公钥加密——私钥解密");
		String source = "12345678";
		// System.out.println("\r加密前文字：\r\n" + source);
		byte[] data = source.getBytes();
		byte[] str = new String(Base64.encodeBase64(RSAUtil.encryptByPublicKey(data, publicKey))).getBytes();
		// System.out.println("加密后文字：\r\n" + new
		// String(Base64.encodeBase64(encodedData)));
		// byte[] data =
		// "e2el90IAtp1O9sCPcyeIy1FRv2yjs0wxNYFTZUPw2GaIu16U9QFH5TLft+IL157Kj5ILr/fYRig2VFxUhna6Pr4FL67sD1wqB5SYoO5wgbUBaKf+xUolg2U+KvejyCLX5xbADshbqK6Pv4foo5apmTlz9UB0cF4y4meeqEDODtE=".getBytes();
		byte[] encodedData = Base64.decodeBase64(str);
		byte[] decodedData = RSAUtil.decryptByPrivateKey(encodedData, privateKey);
		String target = new String(decodedData);
		System.out.println("解密后文字: \r\n" + target);
	}

	static void testSign() throws Exception {
		String publicKey = RSAUtil.getPublicKey();
		String privateKey = RSAUtil.getPrivateKey();
		System.err.println("私钥加密——公钥解密");
		String source = "这是一行测试RSA数字签名的无意义文字";
		System.out.println("原文字：\r\n" + source);
		byte[] data = source.getBytes();
		byte[] encodedData = RSAUtil.encryptByPrivateKey(data, privateKey);
		System.out.println("加密后：\r\n" + Base64.encodeBase64String(encodedData));
		byte[] decodedData = RSAUtil.decryptByPublicKey(encodedData, publicKey);
		String target = new String(decodedData);
		System.out.println("解密后: \r\n" + target);
		System.err.println("私钥签名——公钥验证签名");
		String sign = RSAUtil.sign(encodedData, privateKey);
		System.err.println("签名:\r" + sign);
		boolean status = RSAUtil.verify(encodedData, publicKey, sign);
		System.err.println("验证结果:\r" + status);
	}

	static void testHttpSign() throws Exception {
		String publicKey = RSAUtil.getPublicKey();
		String privateKey = RSAUtil.getPrivateKey();
		String param = "id=1&name=张三";
		byte[] encodedData = RSAUtil.encryptByPrivateKey(param.getBytes(), privateKey);
		System.out.println("加密后：" + encodedData);

		byte[] decodedData = RSAUtil.decryptByPublicKey(encodedData, publicKey);
		System.out.println("解密后：" + new String(decodedData));

		String sign = RSAUtil.sign(encodedData, privateKey);
		System.err.println("签名：" + sign);

		boolean status = RSAUtil.verify(encodedData, publicKey, sign);
		System.err.println("签名验证结果：" + status);
	}
}