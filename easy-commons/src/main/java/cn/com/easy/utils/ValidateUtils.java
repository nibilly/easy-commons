package cn.com.easy.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.hibernate.validator.HibernateValidator;
import org.springframework.util.CollectionUtils;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.ValidationException;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import jakarta.validation.groups.Default;

/**
 * validator 校验工具类 <br/
 * 
 * @NotNull <br/>
 *          private String id;
 * 
 * @Email(message = "邮箱格式不正确") <br/>
 *                private String email;
 * 
 * @Size(min = 4, max = 16, message = "昵称长度为4~16个字符之间") <br/>
 *           private String nickName;
 * 
 * @NotNull
 * @Size(min = 8, max = 24, message = "密码长度为8~24个字符之间") private String pwd;
 *           <br/>
 * 
 *           Bean Validation 中内置的 constraint
 * 
 *           //<br/>
 *           //@Null 被注释的元素必须为 null //<br/>
 *           //@NotNull 被注释的元素必须不为 null //<br/>
 *           //@AssertTrue 被注释的元素必须为 true //<br/>
 *           //@AssertFalse 被注释的元素必须为 false //<br/>
 *           //@Min(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值 //<br/>
 *           //@Max(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值 //<br/>
 *           //@DecimalMin(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值 //<br/>
 *           //@DecimalMax(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值 //<br/>
 *           //@Size(max=, min=) 被注释的元素的大小必须在指定的范围内 //<br/>
 *           //@Digits (integer, fraction) 被注释的元素必须是一个数字，其值必须在可接受的范围内 //<br/>
 *           //@Past 被注释的元素必须是一个过去的日期 //<br/>
 *           //@Future 被注释的元素必须是一个将来的日期 //<br/>
 *           //@Pattern(regex=,flag=) 被注释的元素必须符合指定的正则表达式 //<br/>
 *           // //<br/>
 *           //Hibernate Validator 附加的 constraint //<br/>
 *           //@NotBlank(message =) 验证字符串非null，且长度必须大于0 //<br/>
 *           //@Email 被注释的元素必须是电子邮箱地址 //<br/>
 *           //@Length(min=,max=) 被注释的字符串的大小必须在指定的范围内 //<br/>
 *           //@NotEmpty 被注释的字符串的必须非空 //<br/>
 *           //@Range(min=,max=,message=) 被注释的元素必须在合适的范围内
 * @author nibili 2020年8月13日上午12:08:24
 *
 */
public class ValidateUtils {

//	/为true，则有一个错误就结束校验
	private static boolean failFast = true;

	private ValidateUtils() {
	}

	// 线程安全的，直接构建也可以，这里使用静态代码块一样的效果
	private static Validator validator;

	static {
		// 默认使用 hibernate validate
		validator = Validation.byProvider(HibernateValidator.class).configure().failFast(true) // 为true，则有一个错误就结束校验
				.buildValidatorFactory().getValidator();
	}

	/**
	 * 使用java validate
	 * 
	 * @auth nibili 2020年8月13日 上午12:55:10
	 */
	public static void setJavaValidateType() {
		ValidateUtils.failFast = false;
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	/**
	 * 使用hibernate validate
	 * 
	 * @param failFast 为true，则有一个错误就结束校验
	 * @auth nibili 2020年8月13日 上午12:55:13
	 */
	public static void setHibernateValidateType(boolean failFast) {
		ValidateUtils.failFast = failFast;
		validator = Validation.byProvider(HibernateValidator.class).configure().failFast(failFast) // 为true，则有一个错误就结束校验
				.buildValidatorFactory().getValidator();
	}

	public static <T> void validate(T t) throws ValidationException {
		Set<ConstraintViolation<T>> constraintViolations = validator.validate(t);
		if (constraintViolations.size() > 0) {
			StringBuilder validateError = new StringBuilder();
			for (ConstraintViolation<T> constraintViolation : constraintViolations) {
				validateError.append(
//						constraintViolation.getPropertyPath().toString() + "-" + 
						constraintViolation.getMessage()).append(";");
				if (ValidateUtils.failFast == true) {
					break;
				}
			}

			throw new ValidationException(validateError.toString());
		}
	}

	// 自定义校验结果返回
	/**
	 * 校验实体，返回实体所有属性的校验结果
	 *
	 * @param obj
	 * @param <T>
	 * @return
	 */
	public static <T> ValidationResult validateEntity(T obj) {
		// 解析校验结果
		Set<ConstraintViolation<T>> validateSet = validator.validate(obj, Default.class);
		return buildValidationResult(validateSet);
	}

	/**
	 * 校验指定实体的指定属性是否存在异常
	 *
	 * @param obj
	 * @param propertyName
	 * @param <T>
	 * @return
	 */
	public static <T> ValidationResult validateProperty(T obj, String propertyName) {
		Set<ConstraintViolation<T>> validateSet = validator.validateProperty(obj, propertyName, Default.class);
		return buildValidationResult(validateSet);
	}

	/**
	 * 将异常结果封装返回
	 *
	 * @param validateSet
	 * @param <T>
	 * @return
	 */
	private static <T> ValidationResult buildValidationResult(Set<ConstraintViolation<T>> validateSet) {
		ValidationResult validationResult = new ValidationResult();
		if (!CollectionUtils.isEmpty(validateSet)) {
			validationResult.setHasErrors(true);
			Map<String, String> errorMsgMap = new HashMap<>();
			for (ConstraintViolation<T> constraintViolation : validateSet) {
				errorMsgMap.put(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
			}
			validationResult.setErrorMsg(errorMsgMap);
		}
		return validationResult;
	}

	public static <T> ValidationResult validateEntity1(T obj) {
		ValidationResult result = new ValidationResult();
		Set<ConstraintViolation<T>> set = validator.validate(obj, Default.class);
		if (!CollectionUtils.isEmpty(set)) {
			result.setHasErrors(true);
			Map<String, String> errorMsg = new HashMap<String, String>();
			for (ConstraintViolation<T> cv : set) {
				errorMsg.put(cv.getPropertyPath().toString(), cv.getMessage());
			}
			result.setErrorMsg(errorMsg);
		}
		return result;
	}

	public static <T> ValidationResult validateProperty1(T obj, String propertyName) {
		ValidationResult result = new ValidationResult();
		Set<ConstraintViolation<T>> set = validator.validateProperty(obj, propertyName, Default.class);
		if (!CollectionUtils.isEmpty(set)) {
			result.setHasErrors(true);
			Map<String, String> errorMsg = new HashMap<String, String>();
			for (ConstraintViolation<T> cv : set) {
				errorMsg.put(propertyName, cv.getMessage());
			}
			result.setErrorMsg(errorMsg);
		}
		return result;
	}

}