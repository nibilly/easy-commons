package cn.com.easy.utils;

import java.util.Map;

import com.aliyuncs.CommonResponse;
import com.aliyuncs.exceptions.ClientException;
import com.google.common.collect.Maps;
import com.taobao.api.ApiException;

import cn.com.easy.exception.BusinessException;
import cn.com.easy.utils.AliSmsOfficeUtil.AliSmsConfigDto;
import cn.com.easy.utils.AliSmsOfficeUtil.AliSmsParamDto;

//@Service
public class AliSmsOfficeServiceDemo {

	/** appkey */
	// @Value("${ali.sms.appKey")
	private String appKey = "";
	/** appSecret */
	// @Value("${ali.sms.appSecret")
	private String appSecret = "";
	/** signName */
	// @Value("${ali.sms.signName")
	private String signName = "无限道";
	/** 短信配置 */
	private AliSmsConfigDto aliSmsConfig = null;

	public static void main(String[] args) {
		AliSmsOfficeServiceDemo aliSmsOfficeServiceDemo = new AliSmsOfficeServiceDemo();
		try {
			aliSmsOfficeServiceDemo.sendLoginCode("13205920973", "888888");
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 发送登录，验证码
	 * 
	 * @param mobile
	 * @param captcha
	 * @param product
	 * @author nibili 2016年6月29日
	 * @throws ApiException
	 * @throws BusinessException
	 * @throws ClientException
	 * @throws Exception
	 */
	public void sendLoginCode(String mobile, String captcha) throws ApiException, BusinessException, ClientException {

		AliSmsParamDto aliSmsParamDto = new AliSmsParamDto();
		aliSmsParamDto.setMobile(mobile);
		aliSmsParamDto.setTemplateCode("SMS_109945145");

		Map<String, String> map = Maps.newHashMap();
		map.put("code", captcha);
		aliSmsParamDto.setParam(map);

		CommonResponse response = AliSmsOfficeUtil.sendSms(aliSmsParamDto, this.getConfig());

		System.out.println("短信接口返回的数据----------------");
		System.out.println("Data=" + response.getData());
	}

	private AliSmsConfigDto getConfig() {
		if (aliSmsConfig == null) {
			aliSmsConfig = new AliSmsConfigDto();
			aliSmsConfig.setAppKey(appKey);
			aliSmsConfig.setAppSecret(appSecret);
			aliSmsConfig.setSignName(signName);
		}
		return aliSmsConfig;
	}
}
