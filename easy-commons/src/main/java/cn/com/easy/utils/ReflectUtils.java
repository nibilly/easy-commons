package cn.com.easy.utils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 反射工具类
 * 
 * @author nibili 2017年5月8日
 * 
 */
public class ReflectUtils {

	/**
	 * 把对像实例的字段取出转成map表现类形的数据<br/>
	 * 如果字段是对象，则会被Fastjson格式化成字符串
	 * 
	 * @param object
	 * @return
	 * @throws Exception
	 * @author nibili 2017年5月8日
	 * @throws IllegalAccessException
	 */
	public static Map<String, String> reflectObjectFieldsToMap(Object object) throws IllegalAccessException {
		Map<String, String> map = Maps.newHashMap();
		if (object != null) {
			List<Field> list = FieldUtils.getAllFieldsList(object.getClass());
			if (CollectionUtils.isNotEmpty(list) == true) {
				for (Field field : list) {
					Object valueObj = FieldUtils.readField(field, object, true);
					if (valueObj != null) {
						String value = FastJSONUtils.toJsonString(valueObj);
						if (StringUtils.isNotBlank(value) == true) {
							if (value.startsWith("\"") == true) {
								//
								value = value.substring(1);
								value = value.substring(0, value.lastIndexOf("\""));
							}
							if (StringUtils.isNotBlank(value) == true) {
								map.put(field.getName(), value);
							}
						}
					}
				}
			}
		}
		return map;
	}

	/**
	 * 获取字段集合
	 * 
	 * @param clazz
	 * @return
	 * @throws IllegalAccessException
	 * @auth nibili 2021-10-30 12:00:49
	 */
	public static List<String> reflectObjectFields(Class<?> clazz) throws IllegalAccessException {
		List<String> resList = Lists.newArrayList();
		List<Field> list = FieldUtils.getAllFieldsList(clazz);
		if (CollectionUtils.isNotEmpty(list) == true) {
			for (Field field : list) {
				resList.add(field.getName());
			}
		}
		return resList;
	}
}
