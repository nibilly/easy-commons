package cn.com.easy.utils;

import java.util.Locale;

/**
 * hex格式与String、byte[]互转工具类
 * 
 * @author nibili 2017年8月24日
 * 
 */
public class HexUtils {

	private final static char[] mChars = "0123456789ABCDEF".toCharArray();
	private final static String mHexStr = "0123456789ABCDEF";

	public static void main(String[] args) {
		String tempString = "您好";
		String hexString = str2HexStr(tempString);
		System.out.println(hexString);
		System.out.println(hexStr2Str(hexString));

		byte[] bytesTemp = hexStr2Bytes(hexString);
		System.out.println(bytesTemp);
		System.out.println(byte2HexStr(bytesTemp));

		// byte[] bytesTemp = stringToHexToBytes(tempString);
		// System.out.println(bytesToHexStringToString(bytesTemp));
	}

	/**
	 * float数据转成hex字符串
	 * 
	 * @param f
	 * @return
	 * @author nibili 2017年8月29日
	 */
	public static String floatToHexString(float f) {
		String hexString = Integer.toHexString(Float.floatToIntBits(f));
		return hexString;
	}

	/**
	 * hex字符品转float
	 * 
	 * @param hexString
	 * @return
	 * @author nibili 2017年8月29日
	 */
	public static float hexStringToFloat(String hexString) {
		// hex转float
		Long i = Long.parseLong(hexString, 16);
		Float fl = Float.intBitsToFloat(i.intValue());
		return fl;
	}

	/**
	 * String 转成hex再转成bytes
	 * 
	 * @param str
	 * @return
	 * @author nibili 2017年8月24日
	 */
	public static byte[] stringToHexToBytes(String str) {
		return hexStr2Bytes(str2HexStr(str));
	}

	/**
	 * bytes转成hex转成String
	 * 
	 * @param src
	 * @return
	 * @author nibili 2017年8月24日
	 */
	public static String bytesToHexStringToString(byte[] src) {
		return hexStr2Str(byte2HexStr(src));
	}

	/**
	 * 字符串转换成十六进制字符串
	 * 
	 * @param str
	 *            String 待转换的ASCII字符串
	 * @return String 每个Byte之间空格分隔，如: [61 6C 6B]
	 */
	public static String str2HexStr(String str) {
		StringBuilder sb = new StringBuilder();
		byte[] bs = str.getBytes();

		for (int i = 0; i < bs.length; i++) {
			sb.append(mChars[(bs[i] & 0xFF) >> 4]);
			sb.append(mChars[bs[i] & 0x0F]);
			sb.append(' ');
		}
		return sb.toString().trim();
	}

	/**
	 * 十六进制字符串转换成 ASCII字符串
	 * 
	 * @param str
	 *            String Byte字符串
	 * @return String 对应的字符串
	 */
	public static String hexStr2Str(String hexStr) {
		hexStr = hexStr.toString().trim().replace(" ", "").toUpperCase(Locale.US);
		char[] hexs = hexStr.toCharArray();
		byte[] bytes = new byte[hexStr.length() / 2];
		int iTmp = 0x00;

		for (int i = 0; i < bytes.length; i++) {
			iTmp = mHexStr.indexOf(hexs[2 * i]) << 4;
			iTmp |= mHexStr.indexOf(hexs[2 * i + 1]);
			bytes[i] = (byte) (iTmp & 0xFF);
		}
		return new String(bytes);
	}

	/**
	 * bytes转换成十六进制字符串
	 * 
	 * @param b
	 *            byte[] byte数组
	 * @param iLen
	 *            int 取前N位处理 N=iLen
	 * @return String 每个Byte值之间空格分隔
	 */
	public static String byte2HexStr(byte[] b) {
		StringBuilder sb = new StringBuilder();
		for (int n = 0, iLen = b.length; n < iLen; n++) {
			sb.append(mChars[(b[n] & 0xFF) >> 4]);
			sb.append(mChars[b[n] & 0x0F]);
			sb.append(' ');
		}
		return sb.toString().trim().toUpperCase(Locale.US);
	}

	/**
	 * bytes字符串转换为Byte值
	 * 
	 * @param src
	 *            String Byte字符串，每个Byte之间没有分隔符(字符范围:0-9 A-F)
	 * @return byte[]
	 */
	public static byte[] hexStr2Bytes(String src) {
		/* 对输入值进行规范化整理 */
		src = src.trim().replace(" ", "").toUpperCase(Locale.US);
		// 处理值初始化
		int m = 0, n = 0;
		int iLen = src.length() / 2; // 计算长度
		byte[] ret = new byte[iLen]; // 分配存储空间

		for (int i = 0; i < iLen; i++) {
			m = i * 2 + 1;
			n = m + 1;
			ret[i] = (byte) (Integer.decode("0x" + src.substring(i * 2, m) + src.substring(m, n)) & 0xFF);
		}
		return ret;
	}

}
