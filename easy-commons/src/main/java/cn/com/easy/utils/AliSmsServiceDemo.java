package cn.com.easy.utils;

import java.util.Map;

import cn.com.easy.exception.BusinessException;
import cn.com.easy.utils.AliSmsUtils.AliSmsConfigDto;
import cn.com.easy.utils.AliSmsUtils.AliSmsParamDto;

import com.google.common.collect.Maps;
import com.taobao.api.ApiException;

//@Service
public class AliSmsServiceDemo {

	/** appkey */
	// @Value("${ali.sms.appKey")
	private String appKey;
	/** appSecret */
	// @Value("${ali.sms.appSecret")
	private String appSecret;
	/** signName */
	// @Value("${ali.sms.signName")
	private String signName;
	/** 短信配置 */
	private AliSmsConfigDto aliSmsConfig = null;

	/**
	 * 发送登录，验证码
	 * 
	 * @param mobile
	 * @param captcha
	 * @param product
	 * @author nibili 2016年6月29日
	 * @throws ApiException
	 * @throws BusinessException
	 * @throws Exception
	 */
	public void sendLoginCode(String mobile, String captcha) throws ApiException, BusinessException {

		AliSmsParamDto aliSmsParamDto = new AliSmsParamDto();
		aliSmsParamDto.setMobile(mobile);
		aliSmsParamDto.setTemplateCode("SMS_7765300");

		Map<String, String> map = Maps.newHashMap();
		map.put("captcha", captcha);
		aliSmsParamDto.setParam(map);

		AliSmsUtils.sendMms(aliSmsParamDto, this.getConfig());

	}

	private AliSmsConfigDto getConfig() {
		if (aliSmsConfig == null) {
			aliSmsConfig = new AliSmsConfigDto();
			aliSmsConfig.setAppKey(appKey);
			aliSmsConfig.setAppSecret(appSecret);
			aliSmsConfig.setSignName(signName);
		}
		return aliSmsConfig;
	}
}
