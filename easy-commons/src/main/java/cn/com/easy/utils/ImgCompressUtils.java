package cn.com.easy.utils;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;

/**
 * 图片压缩
 * 
 * @author nibili 2016年3月12日下午11:02:13
 * 
 */
public class ImgCompressUtils {

	/**
	 * 图片压缩
	 *
	 * @param file
	 * @param filePath
	 * @param height
	 * @param width
	 * @param isgp     是否等比例
	 * @param force    是否强制缩放，忽略原图的大小
	 * @return
	 * @author nibili 2016年12月27日
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static void compressPic(String sourcePath, String targetPath, int height, int width, boolean isgp, boolean force) throws FileNotFoundException, IOException {
		BufferedImage bufferedImage = getCompressBufferedImage(sourcePath, height, width, isgp, force);
		if (bufferedImage == null) {
			return;
		}
		// 生成图片
		ImageIO.write(bufferedImage, "jpg", new File(targetPath));
	}

	/**
	 * 图片压缩
	 * 
	 * @param sourcePath
	 * @param outputStream
	 * @param height
	 * @param width
	 * @param isgp
	 * @param force
	 * @throws IOException
	 * @auth nibili 2020-11-29 15:08:34
	 */
	public static void compressPicToOutputStream(String sourcePath, OutputStream outputStream, int height, int width, boolean isgp, boolean force) throws IOException {
		BufferedImage bufferedImage = getCompressBufferedImage(sourcePath, height, width, isgp, force);
		if (bufferedImage == null) {
			return;
		}
		// 生成图片
		ImageIO.write(bufferedImage, "jpg", outputStream);
	}

	/**
	 * 
	 * 图片压缩
	 * 
	 * @param file
	 * @param outputStream
	 * @param height
	 * @param width
	 * @param isgp         是否等比例
	 * @param force        是否强制缩放，忽略原图的大小
	 * @return
	 * @author nibili 2018年3月29日
	 * @throws IOException
	 */
	public static void compressPicToOutputStream(File file, OutputStream outputStream, int height, int width, boolean isgp, boolean force) throws IOException {
		BufferedImage bufferedImage = getCompressBufferedImage(file.getAbsolutePath(), height, width, isgp, force);
		if (bufferedImage == null) {
			return;
		}
		// 生成图片
		ImageIO.write(bufferedImage, "jpg", outputStream);

	}

	/**
	 * 压缩逻辑
	 * 
	 * @param sourcePath
	 * @param height
	 * @param width
	 * @param isgp
	 * @param force
	 * @return
	 * @throws IOException
	 * @auth nibili 2020-11-29 15:04:19
	 */
	private static BufferedImage getCompressBufferedImage(String sourcePath, int height, int width, boolean isgp, boolean force) throws IOException {
		// 获得源文件
		Image img = ImageIO.read(new File(sourcePath));
		if (img == null) {
			return null;
		}
		int oriWidth = img.getWidth(null);
		int oriHeight = img.getHeight(null);
		// 判断图片格式是否正确
		if (oriWidth == -1) {
			return null;
		} else {
			int newWidth;
			int newHeight;
			// 判断是否是等比缩放
			if (isgp == true) {
				// 为等比缩放计算输出的图片宽度及高度
				double rate1 = ((double) img.getWidth(null)) / (double) width + 0.1;
				double rate2 = ((double) img.getHeight(null)) / (double) height + 0.1;
				// 根据缩放比率大的进行缩放控制
				double rate = rate1 > rate2 ? rate1 : rate2;
				newWidth = (int) (((double) img.getWidth(null)) / rate);
				newHeight = (int) (((double) img.getHeight(null)) / rate);
			} else {
				newWidth = width; // 输出的图片宽度
				newHeight = height; // 输出的图片高度
			}
			if (force == false) {
				// 非强制缩放，则不能超出，原图的长的宽
				if (newWidth > oriWidth || newHeight > oriHeight) {
					newWidth = oriWidth; // 输出的图片宽度
					newHeight = oriHeight; // 输出的图片高度
				}
			}
			BufferedImage buffImg = new BufferedImage((int) newWidth, (int) newHeight, BufferedImage.TYPE_INT_RGB);
			/*
			 * Image.SCALE_SMOOTH 的缩略算法 生成缩略图片的平滑度的 优先级比速度高 生成的图片质量比较好 但速度慢
			 */
			// 开启抗锯齿
			RenderingHints renderingHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			// 使用高质量压缩
			renderingHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
			Graphics2D graphics2d = buffImg.createGraphics();
			graphics2d.setRenderingHints(renderingHints);
			graphics2d.drawImage(img.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH), 0, 0, null);
			return buffImg;
		}
	}

}
