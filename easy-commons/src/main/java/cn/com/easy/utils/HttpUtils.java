package cn.com.easy.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * java sdk 原生实现的http请求工具类
 * 
 * @author nibili 2015-02-02
 * 
 */
public class HttpUtils {

	/** the connection connect time out in millionseconds */
	private static final int CONNECT_TIME_OUT = 60000;
	/** the connection read time out in millionseconds */
	private static final int READ_TIME_OUT = 60000;

	public static void main(String[] args) throws Exception {

		// Map<String, String> paramMap = Maps.newHashMap();
		// // paramMap.put("a", "我");
		// // paramMap.put("b", "b");
		// // paramMap.put("c", "b");
		// // paramMap.put("d", "b");
		//
		// System.out.println(HttpUtils.doGet("http://120.55.192.163?d=b",
		// paramMap));
		// // System.out.println(HttpUtils.doPost("http://www.163.com", ""));
		System.out.println(reflectUrlParamToMap(
				"/mp/profile_ext?=home&__biz=MzA3MDk1NTI4NQ==&scene=126&devicetype=android-25&version=2607033a&lang=zh_CN&nettype=WIFI&a8scene=3&pass_ticket=jBfIdWaxIIfHVlAzomOOP4i442ZjwfxcTdFaRJ04cirR3AthUcquCzsQ5U1XxtK2&wx_header=1 HTTP/1.1"));

	}

	/**
	 * get 请求
	 * 
	 * @param url
	 * @param paramObject 对象
	 * @return
	 * @throws Exception
	 * @author nibili 2016年3月16日
	 */
	public static String doGet(String url, Object paramObject) throws Exception {
		return doGet(url, reflectObjectFieldsToMap(paramObject), null);
	}

	/**
	 * do get
	 * 
	 * @param url
	 * @param paramMap 参数map
	 * @return
	 * @author nibili 2016年3月3日
	 * @throws Exception
	 */
	public static String doGet(String url, Map<String, String> paramMap) throws Exception {
		return doGet(url, paramMap, null);
	}

	/**
	 * do get
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public static String doGet(String url) throws Exception {
		return doGet(url, null, null);
	}

	/**
	 * get请求
	 * 
	 * @param url
	 * @param paramMap
	 * @param headerMap
	 * @return
	 * @throws Exception
	 * @auth nibili 2018年11月29日 196888813@qq.com
	 */
	public static String doGet(String url, Map<String, String> paramMap, Map<String, String> headerMap) throws Exception {

		if (MapUtils.isNotEmpty(paramMap) == true) {
			url = toRequestUrl(url, paramMap);
		}
		String result = "";
		BufferedReader in = null;
		try {
			URL realUrl = new URL(url.trim());
			// open connection
			URLConnection connection = realUrl.openConnection();
			connection.setConnectTimeout(CONNECT_TIME_OUT);
			connection.setReadTimeout(READ_TIME_OUT);
			if (MapUtils.isNotEmpty(headerMap) == true) {
				for (Map.Entry<String, String> entry : headerMap.entrySet()) {
					connection.setRequestProperty(entry.getKey(), entry.getValue());
				}
			} else {
				connection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
			}
			// connect
			connection.connect();
			// define BufferedReader to read input content
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} finally {
			if (in != null) {
				in.close();
			}
		}
		return result;
	}

	/**
	 * post 结果
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 * @author nibili 2014-11-25
	 */
	public static String doPost(String url, Object object) throws Exception {
		return doPost(url, reflectObjectFieldsToMap(object), null);
	}

	/**
	 * do post
	 * 
	 * @param url
	 * @param paramMap 参数map
	 * @return
	 * @author nibili 2016年3月3日
	 * @throws Exception
	 * @throws UnsupportedEncodingException
	 */
	public static String doPost(String url, Map<String, String> paramMap) throws UnsupportedEncodingException, Exception {
		return doPost(url, paramMap, null);
	}

	/**
	 * do post
	 * 
	 * @param url
	 * @param param name1=value1&name2=value2
	 * @return
	 * @throws Exception
	 */
	public static String doPost(String url, String param) throws Exception {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
			connection.setConnectTimeout(CONNECT_TIME_OUT);
			connection.setReadTimeout(READ_TIME_OUT);
			// do post
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			// get URLConnection output stream
			out = new PrintWriter(connection.getOutputStream());
			// send param
			out.print(param);
			out.flush();
			// define BufferedReader input stream
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 
	 * @param url
	 * @param paramMap
	 * @param headerMap
	 * @return
	 * @throws Exception
	 * @auth nibili 2018年11月29日 196888813@qq.com
	 */
	public static String doPost(String url, Map<String, String> paramMap, Map<String, String> headerMap) throws Exception {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
			connection.setConnectTimeout(CONNECT_TIME_OUT);
			connection.setReadTimeout(READ_TIME_OUT);
			// do post
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");

			if (MapUtils.isNotEmpty(headerMap) == true) {
				for (Map.Entry<String, String> entry : headerMap.entrySet()) {
					connection.setRequestProperty(entry.getKey(), entry.getValue());
				}
			} else {
				connection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
			}
			// get URLConnection output stream
			out = new PrintWriter(connection.getOutputStream());
			// send param
			out.print(toNetParam(paramMap));
			out.flush();
			// define BufferedReader input stream
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * get input stream
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 * @auth nibili 2015-3-20
	 */
	public static InputStream deGetFileInputSteam(String url) throws Exception {
		URL uploadUrl = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) uploadUrl.openConnection();
		connection.setConnectTimeout(CONNECT_TIME_OUT);
		connection.setReadTimeout(READ_TIME_OUT);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setRequestMethod("GET");
		return connection.getInputStream();
	}

	/**
	 * upload file
	 * 
	 * @param url
	 * @param fileInputStream
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public static String uploadFile(String url, InputStream fileInputStream, String fileName) throws Exception {
		String BOUNDARY = "---------7d4a6d158c9"; // 定义数据分隔线
		HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
		// 发送POST请求必须设置如下两行
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setUseCaches(false);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("connection", "Keep-Alive");
		conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
		conn.setRequestProperty("Charsert", "UTF-8");
		conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
		conn.connect();
		OutputStream out = new DataOutputStream(conn.getOutputStream());
		byte[] end_data = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();// 定义最后数据分隔线
		StringBuilder sb = new StringBuilder();
		sb.append("--");
		sb.append(BOUNDARY);
		sb.append("\r\n");
		sb.append("Content-Disposition: form-data;name=\"file\";filename=\"" + java.net.URLEncoder.encode(fileName, "UTF-8") + "\"\r\n");
		sb.append("Content-Type:application/octet-stream\r\n\r\n");
		byte[] data = sb.toString().getBytes();
		out.write(data);
		int bytes = 0;
		byte[] bufferOut = new byte[1024];
		while ((bytes = fileInputStream.read(bufferOut)) != -1) {
			out.write(bufferOut, 0, bytes);
		}
		out.write("\r\n".getBytes()); // 多个文件时，二个文件之间加入这个
		fileInputStream.close();
		out.write(end_data);
		out.flush();
		out.close();
		// 定义BufferedReader输入流来读取URL的响应
		BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		// 读取URLConnection的响应
		String result = "";
		String line;
		while ((line = reader.readLine()) != null) {
			result += line;
		}
		return result;
	}

	/**
	 * 把map转成：a=b&c=d&e=f
	 * 
	 * @param paramMap
	 * @return
	 * @author nibili 2016年3月3日
	 * @throws UnsupportedEncodingException
	 */
	public static String toNetParam(Map<String, String> paramMap) throws UnsupportedEncodingException {
		if (MapUtils.isNotEmpty(paramMap) == true) {

			StringBuffer stringBuffer = new StringBuffer();
			int i = 0;
			for (Entry<String, String> entry : paramMap.entrySet()) {
				if (i == 0) {
					stringBuffer.append(entry.getKey() + "=" + java.net.URLEncoder.encode(entry.getValue(), "UTF-8"));
				} else {
					stringBuffer.append("&" + entry.getKey() + "=" + java.net.URLEncoder.encode(entry.getValue(), "UTF-8"));
				}
				i++;
			}
			return stringBuffer.toString();
		} else {
			return "";
		}
	}

	/**
	 * url 和 参数map转成最终的请求url链接
	 * 
	 * @param url
	 * @param paramMap
	 * @return
	 * @author nibili 2016年3月3日
	 * @throws UnsupportedEncodingException
	 */
	private static String toRequestUrl(String url, Map<String, String> paramMap) throws UnsupportedEncodingException {
		if (MapUtils.isNotEmpty(paramMap) == true) {
			if (url.indexOf("?") > 0) {
				// 已有参数
				return url + "&" + toNetParam(paramMap);
			} else {
				return url + "?" + toNetParam(paramMap);
			}
		} else {
			return url;
		}
	}

	/**
	 * 将url中的参数转成map，使用操作
	 * 
	 * @param url
	 * @return
	 * @auth nibili 2018年11月28日 196888813@qq.com
	 */
	public static Map<String, String> reflectUrlParamToMap(String url) {
		if (StringUtils.isBlank(url) == true) {
			return null;
		}
		int index = url.indexOf("?");
		if (index > -1) {
			url = url.substring(index + 1);
		}
		Map<String, String> paramMap = Maps.newLinkedHashMap();
		List<String> paramList = Lists.newArrayList(url.split("&"));
		for (String paramString : paramList) {
			if (paramString.contains("=") == true) {
				index = paramString.indexOf("=");
				if (index > 0) {
					String param = paramString.substring(0, index).trim();
					if (index >= (paramString.length() + 1)) {
						// 如果“=”是在最后一个字符
						paramMap.put(param, "");
					} else {
						String value = paramString.substring(index + 1);
						if (value.trim().contains(" ") == true) {
							paramMap.put(param, value.trim().substring(0, value.trim().indexOf(" ")));
						} else {
							paramMap.put(param, value.trim());
						}
					}

				}

			}
		}
		return paramMap;
	}

	/**
	 * 把对像实例的字段取出转成map表现类形的数据
	 * 
	 * @param object
	 * @return
	 * @throws Exception
	 * @author nibili 2016年3月16日
	 */
	public static Map<String, String> reflectObjectFieldsToMap(Object object) throws Exception {

		Map<String, String> map = Maps.newHashMap();
		if (object != null) {
			List<Field> list = FieldUtils.getAllFieldsList(object.getClass());
			if (CollectionUtils.isNotEmpty(list) == true) {
				for (Field field : list) {
					Object valueObj = FieldUtils.readField(field, object, true);
					if (valueObj != null) {
						String value = FastJSONUtils.toJsonString(valueObj);
						if (StringUtils.isNotBlank(value) == true) {
							if (value.startsWith("\"") == true) {
								//
								value = value.substring(1);
								value = value.substring(0, value.lastIndexOf("\""));
							}
							if (StringUtils.isNotBlank(value) == true) {
								map.put(field.getName(), value);
							}
						}
					}
				}
			}
		}
		return map;
	}

	/**
	 * 获取url链接中的，某个参数的值
	 * 
	 * @param url
	 * @param name
	 * @return
	 * @auth nibili 2021-4-7 9:51:52
	 */
	public static String getParam(String url, String name) {
		String params = url.substring(url.indexOf("?") + 1, url.length());
		Map<String, String> split = Splitter.on("&").withKeyValueSeparator("=").split(params);
		return split.get(name);
	}
}
