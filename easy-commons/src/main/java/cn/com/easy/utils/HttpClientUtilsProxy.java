package cn.com.easy.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.RequestAcceptEncoding;
import org.apache.http.client.protocol.ResponseContentEncoding;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Data;

public class HttpClientUtilsProxy {

	private static final String[] USER_AGENTS = new String[] {
			"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.102 Safari/537.36",
			"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36",
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1309.0 Safari/537.17",
			"Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0",
			"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36 OPR/24.0.1558.53" };

	static SSLContext sslcontext;
	static {
		try {
			sslcontext = createIgnoreVerifySSL();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static HttpClient HTTP_CLIENT;

	private static RequestConfig DEFAULT_REQUEST_CONFIG;

	private static HttpClientBuilder httpClientBuilder;

	private static CookieStore httpCookieStore = new BasicCookieStore();

	static {
		rebuildHttpClient();
	}

	/** cookie对象 */
	public static CookieStore getHttpCookieStore() {
		return httpCookieStore;
	}

	public static void main(String[] args) {
		try {
//			System.out.println(getMobileFrom("13205920973", getProxyHosts().get(0)));
//			System.out.println(post("https://www.feng8.com", null, getProxyHosts().get(0)));
//			System.out.println(getMobileFrom("13205920973"));
			ProxyDto proxyDto = getProxyHosts().get(0);
			String html = HttpClientUtilsProxy.get("https://admins.bao315.com/admin/marketing/resourceList.jhtml", proxyDto);
			System.out.println(html);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 获到代理列表
	 * 
	 * @return
	 * @throws Exception
	 */
	public static synchronized List<ProxyDto> getProxyHosts() {
		List<ProxyDto> list = Lists.newArrayList();
		ProxyDto proxyDto;
		try {
//			String res = HttpClientUtils.get(
//					"http://ip.ipjldl.com/index.php/api/entry?method=proxyServer.hdtiqu_api_url&packid=7&fa=1&groupid=0&fetch_key=&time=1&qty=1&port=1&format=json&ss=5&css=&dt=&pro=%E7%A6%8F%E5%BB%BA%E7%9C%81&city=%E5%8E%A6%E9%97%A8%E5%B8%82&usertype=4");

			String res = HttpClientUtils.get(
					"http://pandavip.xiongmaodaili.com/xiongmao-web/apiPlus/vgl?secret=cc0e413f4e7514e28fdc8f7b6b874cca&orderNo=VGL20211028104356vK4J5sNF&count=1&isTxt=0&proxyType=1&validTime=0&removal=0&cityIds=350600");
//			{"code":"0","msg":"ok","obj":[{"port":"12571","ip":"59.60.132.13"}],"errno":0,"data":[null]}
			System.out.println("获取代理ip结果:" + res);

			JSONObject jsonObject = JSON.parseObject(res);
			String code = jsonObject.getString("code");
			if (StringUtils.equals(code, "0")) {
				// 有取到ip
				JSONArray jsonArray = jsonObject.getJSONArray("obj");
				if (CollectionUtils.isNotEmpty(jsonArray)) {
					for (int i = 0, len = jsonArray.size(); i < len; i++) {
						jsonObject = (JSONObject) jsonArray.get(i);
						proxyDto = new HttpClientUtilsProxy.ProxyDto();
						proxyDto.setHost(jsonObject.getString("ip"));
						proxyDto.setPort(jsonObject.getIntValue("port"));
						list.add(proxyDto);
					}
				}
			} else {

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}

	/**
	 * 重新build HttpClient对象
	 * 
	 * @auth nibili 2018年11月28日 196888813@qq.com
	 */
	@SuppressWarnings("deprecation")
	public static void rebuildHttpClient() {

		httpClientBuilder = HttpClients.custom();

		DEFAULT_REQUEST_CONFIG = RequestConfig.custom().setExpectContinueEnabled(false) // 设置不使用Expect:100-Continue握手
				.setConnectTimeout(3000) // 设置连接超时时间
				.setSocketTimeout(5000) // 设置读数据超时时间
				.setCookieSpec(CookieSpecs.BROWSER_COMPATIBILITY) // Cookie策略
				.build();
		httpClientBuilder.setDefaultRequestConfig(DEFAULT_REQUEST_CONFIG);

		httpClientBuilder.addInterceptorFirst(new RequestAcceptEncoding()); // 设置GZIP请求的支持
		httpClientBuilder.addInterceptorFirst(new ResponseContentEncoding()); // 设置GZIP响应的支持
		httpClientBuilder.setUserAgent(getRandomUserAgent()); // 设置User-Agent

		HttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(1, true);
		httpClientBuilder.setRetryHandler(retryHandler); // 设置重试

		Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create().register("http", PlainConnectionSocketFactory.INSTANCE)
				.register("https", new SSLConnectionSocketFactory(sslcontext)).build();
		PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);
		connectionManager.setMaxTotal(100);
		connectionManager.setDefaultMaxPerRoute(100);

		SocketConfig defaultSocketConfig = SocketConfig.custom().setTcpNoDelay(true).build();
		connectionManager.setDefaultSocketConfig(defaultSocketConfig);

		// Create connection configuration
		ConnectionConfig defaultConnectionConfig = ConnectionConfig.custom().setCharset(Consts.UTF_8).build();
		connectionManager.setDefaultConnectionConfig(defaultConnectionConfig);

		httpClientBuilder.setConnectionManager(connectionManager);
		httpClientBuilder.setRedirectStrategy(DefaultRedirectStrategy.INSTANCE);
		httpCookieStore = new BasicCookieStore();
		HTTP_CLIENT = httpClientBuilder.setDefaultCookieStore(httpCookieStore).build();
	}

	/**
	 * 绕过验证
	 *
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	public static SSLContext createIgnoreVerifySSL() throws NoSuchAlgorithmException, KeyManagementException {
		SSLContext sc = SSLContext.getInstance("SSLv3");

		// 实现一个X509TrustManager接口，用于绕过验证，不用修改里面的方法
		X509TrustManager trustManager = new X509TrustManager() {
			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] paramArrayOfX509Certificate, String paramString) {
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] paramArrayOfX509Certificate, String paramString) {
			}

			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		};

		sc.init(null, new TrustManager[] { trustManager }, null);
		return sc;
	}

	public static String execute(HttpUriRequest request) throws ClientProtocolException, IOException {
		return HTTP_CLIENT.execute(request, new BasicResponseHandler());
	}

	public static <T> T execute(HttpUriRequest request, ResponseHandler<T> responseHandler) throws ClientProtocolException, IOException {
		return HTTP_CLIENT.execute(request, responseHandler);
	}

	public static String getRandomUserAgent() {
		return USER_AGENTS[RandomUtils.nextInt(0, USER_AGENTS.length)];
	}

	/**
	 * 设置agent
	 * 
	 * @param useAgent
	 * @auth nibili 2018年11月7日 196888813@qq.com
	 */
	public static void setUseAgent(String useAgent) {
		if (StringUtils.isNotBlank(useAgent) == true) {
			httpClientBuilder.setUserAgent(useAgent);
			HTTP_CLIENT = httpClientBuilder.build();
		}
	}

	/**
	 * 是否使用agent，默认false
	 * 
	 * @param isUseAgent
	 * @author nibili 2017年2月7日
	 */
	public static void setUseAgent(boolean isUseAgent) {
		if (isUseAgent == true) {
			httpClientBuilder.setUserAgent(getRandomUserAgent());
		} else {
			httpClientBuilder.setUserAgent("");
		}
		HTTP_CLIENT = httpClientBuilder.build();
	}

	/**
	 * get 请求
	 * 
	 * @param url
	 * @param object 对象
	 * @return
	 * @throws Exception
	 * @author nibili 2016年3月16日
	 */
	public static String get(String url, Object object, ProxyDto proxyDto) throws Exception {
		return get(toRequestUrl(url, HttpClientUtils.reflectObjectFieldsToMap(object)), proxyDto);
	}

	/**
	 * get 请求
	 * 
	 * @param url
	 * @param paramMap
	 * @return
	 * @throws Exception
	 * @author nibili 2016年3月3日
	 */
	public static String get(String url, Map<String, String> paramMap, ProxyDto proxyDto) throws Exception {
		return get(toRequestUrl(url, paramMap), proxyDto);
	}

//	/**
//	 * get 结果
//	 * 
//	 * @param url
//	 * @return
//	 * @throws Exception
//	 * @author nibili 2014-11-25
//	 */
//	public static String get(String url) throws Exception {
//		HttpUriRequest request = new HttpGet(url);
//		return HTTP_CLIENT.execute(request, new BasicResponseHandler());
//	}

	public static String get(String url, ProxyDto proxyDto) throws Exception {

		HttpRequestBase request = new HttpGet(url);
		RequestConfig reqConfig = RequestConfig.custom().setConnectionRequestTimeout(5000).setConnectTimeout(10000) // 设置连接超时时间
				.setSocketTimeout(10000) // 设置读取超时时间
				.setExpectContinueEnabled(false).setProxy(new HttpHost(proxyDto.getHost(), proxyDto.getPort())).setCircularRedirectsAllowed(true) // 允许多次重定向
				.build();
		request.setConfig(reqConfig);
		return HTTP_CLIENT.execute(request, new BasicResponseHandler());
	}

	/**
	 * post 结果
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 * @author nibili 2014-11-25
	 */
	public static String post(String url, Map<String, String> params, ProxyDto proxyDto) throws Exception {
		HttpRequestBase request = postForm(url, null, params, null);

		RequestConfig reqConfig = RequestConfig.custom().setConnectionRequestTimeout(3000).setConnectTimeout(3000) // 设置连接超时时间
				.setSocketTimeout(10000) // 设置读取超时时间
				.setExpectContinueEnabled(false).setProxy(new HttpHost(proxyDto.getHost(), proxyDto.getPort())).setCircularRedirectsAllowed(true) // 允许多次重定向
				.build();
		request.setConfig(reqConfig);

		return HTTP_CLIENT.execute(request, new BasicResponseHandler());
	}

	/**
	 * post请求
	 * 
	 * @param url
	 * @param body
	 * @param mimeType
	 * @param charset
	 * @param params
	 * @param headers
	 * @return
	 * @throws UnsupportedEncodingException
	 * @author nibili 2016年10月18日
	 */
	public static HttpRequestBase postForm(String url, String body, Map<String, String> params, Map<String, String> headers) throws UnsupportedEncodingException {
		HttpPost httpost = new HttpPost(url);
		return form(httpost, body, params, headers);
	}

	/**
	 * 创建请求对象
	 * 
	 * @param httentity
	 * @param body
	 * @param params
	 * @param headers
	 * @param method
	 * @return
	 * @author nibili 2017年1月3日
	 */
	private static HttpRequestBase form(HttpEntityEnclosingRequestBase httentity, String body, Map<String, String> params, Map<String, String> headers) {
		if (StringUtils.isNotBlank(body)) {
			HttpEntity entity = new StringEntity(body, Consts.UTF_8);
			httentity.setEntity(entity);
		}
		if (MapUtils.isNotEmpty(params) == true) {
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			Set<String> keySet = params.keySet();
			for (String key : keySet) {
				nvps.add(new BasicNameValuePair(key, params.get(key)));
			}
			httentity.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
		}
		if (MapUtils.isNotEmpty(headers) == true) {
			for (Entry<String, String> entry : headers.entrySet()) {
				httentity.addHeader(entry.getKey(), entry.getValue());
			}
		}
		return httentity;
	}

	/**
	 * 把对像实例的字段取出转成map表现类形的数据
	 * 
	 * @param object
	 * @return
	 * @throws Exception
	 * @author nibili 2016年3月16日
	 */
	public static Map<String, String> reflectObjectFieldsToMap(Object object) throws Exception {

		Map<String, String> map = Maps.newHashMap();
		if (object != null) {
			List<Field> list = FieldUtils.getAllFieldsList(object.getClass());
			if (CollectionUtils.isNotEmpty(list) == true) {
				for (Field field : list) {
					Object valueObj = FieldUtils.readField(field, object, true);
					if (valueObj != null) {
						String value = FastJSONUtils.toJsonString(valueObj);
						if (StringUtils.isNotBlank(value) == true) {
							if (value.startsWith("\"") == true) {
								//
								value = value.substring(1);
								value = value.substring(0, value.lastIndexOf("\""));
							}
							if (StringUtils.isNotBlank(value) == true) {
								map.put(field.getName(), value);
							}
						}
					}
				}
			}
		}
		return map;
	}

	/**
	 * 把map转成：a=b&c=d&e=f
	 * 
	 * @param paramMap
	 * @return
	 * @author nibili 2016年3月3日
	 * @throws UnsupportedEncodingException
	 */
	public static String toNetParam(Map<String, String> paramMap) throws UnsupportedEncodingException {
		if (MapUtils.isNotEmpty(paramMap) == true) {

			StringBuffer stringBuffer = new StringBuffer();
			int i = 0;
			for (Entry<String, String> entry : paramMap.entrySet()) {
				String value = entry.getValue();
				if (StringUtils.isBlank(value) == true) {
					value = "";
				}
				if (i == 0) {
					stringBuffer.append(entry.getKey() + "=" + java.net.URLEncoder.encode(value, "UTF-8"));
				} else {
					stringBuffer.append("&" + entry.getKey() + "=" + java.net.URLEncoder.encode(value, "UTF-8"));
				}

				i++;
			}
			return stringBuffer.toString();
		} else {
			return "";
		}
	}

	/**
	 * url 和 参数map转成最终的请求url链接
	 * 
	 * @param url
	 * @param paramMap
	 * @return
	 * @author nibili 2016年3月3日
	 * @throws UnsupportedEncodingException
	 */
	private static String toRequestUrl(String url, Map<String, String> paramMap) throws UnsupportedEncodingException {
		if (MapUtils.isNotEmpty(paramMap) == true) {
			if (url.indexOf("?") > 0) {
				// 已有参数
				return url + "&" + toNetParam(paramMap);
			} else {
				return url + "?" + toNetParam(paramMap);
			}
		} else {
			return url;
		}
	}

	/**
	 * a=1&b=2&c=3 转成map格式
	 * 
	 * @param netParam
	 * @return
	 * @auth nibili 2020-12-10 14:49:04
	 */
	public static Map<String, String> netParamToMap(String netParam) {
		Map<String, String> map = Maps.newHashMap();
		if (StringUtils.isBlank(netParam)) {
			return map;
		}
		if (StringUtils.startsWith(netParam, "?")) {
			netParam = StringUtils.substring(netParam, 1);
		}
		String[] array = StringUtils.split(netParam, "&");
		if (ArrayUtils.isNotEmpty(array)) {
			for (String item : array) {
				String[] array_ = StringUtils.split(item, "=");
				if (ArrayUtils.getLength(array_) == 2) {
					map.put(array_[0], array_[1]);
				}
			}
		}
		return map;
	}

	@Data
	public static class ProxyDto {
		private String host;
		private Integer port;
	}

}
