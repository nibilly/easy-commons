package cn.com.easy.utils;

import java.util.List;

import org.springframework.beans.BeanUtils;

import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.google.common.collect.Lists;

/**
 * mybatis分页结果对象，spring-data分页结果对象，转成easy-commons中的Page对象工具类
 * 
 * @author nibili 2015年11月13日
 * 
 */
public class PageUtils {

	/**
	 * 生成page对象
	 * 
	 * @param springDataPage
	 * @return
	 * @author nibili 2015年12月8日
	 */
	public static <T> Page<T> getPage(org.springframework.data.domain.Page<T> springDataPage) {
		Page<T> page = new Page<T>();
		if (springDataPage != null) {
			page.setPageNo(springDataPage.getNumber() + 1);
			page.setPageSize(springDataPage.getSize());
			page.setTotalItems(springDataPage.getTotalElements());
			page.setResult(springDataPage.getContent());
		}
		return page;
	}

	/**
	 * 生成page对象<br/>
	 * 将结果转成另一个DTO,属性同名复制
	 * 
	 * @param <T>
	 * @param <V>
	 * @param springDataPage
	 * @param clazz
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @auth nibili 2021-10-3 15:08:38
	 */
	public static <T, V> Page<V> getPage(org.springframework.data.domain.Page<T> springDataPage, Class<V> clazz) throws InstantiationException, IllegalAccessException {
		if (springDataPage != null) {
			Page<V> page = new Page<V>();
			page.setPageNo(springDataPage.getNumber() + 1);
			page.setPageSize(springDataPage.getSize());
			page.setTotalItems(springDataPage.getTotalElements());
			List<V> vList = Lists.newArrayList();
			V v;
			for (T t : springDataPage.getContent()) {
				v = clazz.newInstance();
				BeanUtils.copyProperties(t, v);
				vList.add(v);
			}
			page.setResult(vList);
			return page;
		}
		return null;

	}

	/**
	 * 生成前端能显示的page对象
	 * 
	 * @param pageObject
	 * @return
	 * @author nibili 2015年12月31日
	 */
	public static <T> Page<T> getPage(PageList<T> pageList) {
		Page<T> page = new Page<T>();
		if (pageList != null) {
			page.setPageNo(pageList.getPaginator().getPage());
			page.setPageSize(pageList.getPaginator().getLimit());
			page.setTotalItems(pageList.getPaginator().getTotalCount());
			List<T> lists = Lists.newArrayList(pageList);
			page.setResult(lists);
		}
		return page;
	}

	/**
	 * * 生成page对象<br/>
	 * 将结果转成另一个DTO,属性同名复制
	 * 
	 * @param <T>
	 * @param <V>
	 * @param pageList
	 * @param clazz
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @auth nibili 2021-10-3 15:10:39
	 */
	public static <T, V> Page<V> getPage(PageList<T> pageList, Class<V> clazz) throws InstantiationException, IllegalAccessException {
		if (pageList != null) {
			Page<V> page = new Page<V>();
			page.setPageNo(pageList.getPaginator().getPage());
			page.setPageSize(pageList.getPaginator().getLimit());
			page.setTotalItems(pageList.getPaginator().getTotalCount());
			List<V> vList = Lists.newArrayList();
			V v;
			for (T t : pageList) {
				v = clazz.newInstance();
				BeanUtils.copyProperties(t, v);
				vList.add(v);
			}
			page.setResult(vList);
			return page;
		}
		return null;

	}

	/**
	 * 生成前端能显示的page对象
	 * 
	 * @param <T>
	 * @param pageList
	 * @return
	 * @auth nibili 2021-10-3 15:13:07
	 */
	public static <T> Page<T> getPage(com.baomidou.mybatisplus.extension.plugins.pagination.Page<T> pageList) {
		Page<T> page = new Page<T>();
		if (pageList != null) {
			page.setPageNo(Long.valueOf(pageList.getCurrent()).intValue());
			page.setPageSize(Long.valueOf(pageList.getPages()).intValue());
			page.setTotalItems(pageList.getTotal());
			page.setResult(pageList.getRecords());
		}
		return page;
	}

	/**
	 * * * 生成page对象<br/>
	 * 将结果转成另一个DTO,属性同名复制
	 * 
	 * @param <T>
	 * @param <V>
	 * @param pageList
	 * @param clazz
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @auth nibili 2021-10-3 15:12:46
	 */
	public static <T, V> Page<V> getPage(com.baomidou.mybatisplus.extension.plugins.pagination.Page<T> pageList, Class<V> clazz)
			throws InstantiationException, IllegalAccessException {
		if (pageList != null) {
			Page<V> page = new Page<V>();
			page.setPageNo(Long.valueOf(pageList.getCurrent()).intValue());
			page.setPageSize(Long.valueOf(pageList.getPages()).intValue());
			page.setTotalItems(pageList.getTotal());
			List<V> vList = Lists.newArrayList();
			V v;
			for (T t : pageList.getRecords()) {
				v = clazz.newInstance();
				BeanUtils.copyProperties(t, v);
				vList.add(v);
			}
			page.setResult(vList);
			return page;
		}
		return null;

	}
}
