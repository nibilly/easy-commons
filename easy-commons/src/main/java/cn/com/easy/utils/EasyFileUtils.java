package cn.com.easy.utils;

import java.io.File;
import java.util.List;

import com.google.common.collect.Lists;

/**
 * 文件工具类
 * 
 * @author nibili 2019年12月4日上午12:12:40
 * 
 */
public class EasyFileUtils {

	public static void main(String[] args) {
		List<String> list = getFiles("E:/my_soft/git/easy-commons/easy-commons/target/classes/applicationContext-base.xml");
		System.out.println(FastJSONUtils.toJsonString(list));
		list = getFiles("E:/my_soft/git/easy-commons/easy-commons/target/classes");
		System.out.println(FastJSONUtils.toJsonString(list));
	}

	/**
	 * 读取路径下的所有文件
	 * 
	 * @param path
	 * @return
	 * @auth nibili 2019年12月4日 上午12:25:57
	 */
	public static List<String> getFiles(String path) {
		List<String> list = Lists.newArrayList();
		File file = new File(path);
		if (file.isFile() == true) {
			list.add(path);
			return list;
		} else {
			EasyFileUtils.readfile(file.listFiles(), list);
		}
		return list;
	}

	/**
	 * 读子文件
	 * 
	 * @param files
	 * @param list
	 * @auth nibili 2019年12月4日 上午12:25:40
	 */
	private static void readfile(File[] files, List<String> list) {
		if (files == null) {// 如果目录为空，直接退出
			return;
		}
		for (File f : files) {
			// 如果是文件，直接输出名字
			if (f.isFile()) {
				// System.out.println(f.getAbsolutePath());
				list.add(f.getAbsolutePath());
			}
			// 如果是文件夹，递归调用
			else if (f.isDirectory()) {
				readfile(f.listFiles(), list);
			}
		}
	}
}
