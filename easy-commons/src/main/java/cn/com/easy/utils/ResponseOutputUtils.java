package cn.com.easy.utils;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.google.common.collect.Maps;

/**
 * The Class ResponseOutputUtils.
 */
public abstract class ResponseOutputUtils {
	// -- header 常量定义 --//
	/** The Constant HEADER_ENCODING. */
	private static final String HEADER_ENCODING = "encoding";

	/** The Constant HEADER_NOCACHE. */
	private static final String HEADER_NOCACHE = "no-cache";

	/** The Constant DEFAULT_ENCODING. */
	public static final String DEFAULT_ENCODING = "UTF-8";

	/** The Constant DEFAULT_NOCACHE. */
	private static final boolean DEFAULT_NOCACHE = true;

	/** The mapper. */
	private static ObjectMapper mapper = new ObjectMapper();

	/** 默认数据回调接口 */
	public static RenderBefore renderBefore = null;

	/**
	 * 是否将null字段置为空字符串
	 * 
	 * @auth nibili 2021-1-24 14:54:09
	 */
	public static void setRenderNullVlaueToEmpty(boolean isToEmpty) {
		mapper.getSerializerProvider().setNullValueSerializer(new JsonSerializer<Object>() {
			@Override
			public void serialize(Object paramT, JsonGenerator paramJsonGenerator, SerializerProvider paramSerializerProvider) throws IOException {
				// 设置返回null转为 空字符串""
				if (isToEmpty == true) {
					paramJsonGenerator.writeString("");
				} else {
					paramJsonGenerator.writeNull();
				}
			}
		});
	}

	// -- 绕过jsp/freemaker直接输出文本的函数 --//

	public static void main(String[] args) throws JsonProcessingException {
		Map<String, Object> map = Maps.newHashMap();
		map.put("name", "billy");
		map.put("value", null);
		String jsonString = mapper.writeValueAsString(map);
		System.out.println(jsonString);
	}

	/**
	 * 发送到客户端前要做的
	 * 
	 * @author nibili 2018年1月9日
	 * 
	 */
	public static interface RenderBefore {
		public String doSomething(Object data, String jsonString);
	}

	/**
	 * 回调
	 * 
	 * @param response
	 * @param data
	 * @param headers
	 * @author nibili 2018年1月9日
	 */
	public static void renderWithCallbackBefore(HttpServletResponse response, final Object data, final String... headers) {
		renderWithCallbackBefore(response, data, null, headers);
	}

	/**
	 * 回调
	 * 
	 * @param response
	 * @param data
	 * @param renderBefore
	 * @param headers
	 * @author nibili 2018年1月9日
	 */
	public static void renderWithCallbackBefore(HttpServletResponse response, final Object data, RenderBefore renderBefore, final String... headers) {
		response = initResponseHeader(ServletUtils.TEXT_TYPE, response, headers);
		try {
			String jsonString = mapper.writeValueAsString(data);
			if (renderBefore != null) {
				jsonString = renderBefore.doSomething(data, jsonString);
			} else if (ResponseOutputUtils.renderBefore != null) {
				jsonString = ResponseOutputUtils.renderBefore.doSomething(data, jsonString);
			}
			System.out.println(jsonString);
			response.getWriter().write(jsonString);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * 直接输出内容的简便函数.
	 * 
	 * eg. render("text/plain", "hello", "encoding:GBK"); render("text/plain",
	 * "hello", "no-cache:false"); render("text/plain", "hello", "encoding:GBK",
	 * "no-cache:false");
	 * 
	 * @param contentType the content type
	 * @param content     the content
	 * @param response    the response
	 * @param headers     可变的header数组，目前接受的值为"encoding:"或"no-cache:",默认值分别为UTF-8和true.
	 */
	public static void render(final String contentType, final String content, HttpServletResponse response, final String... headers) {
		response = initResponseHeader(contentType, response, headers);
		try {
			response.getWriter().write(content);
			response.getWriter().flush();
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * 直接输出文本.
	 * 
	 * @param response the response
	 * @param text     the text
	 * @param headers  the headers
	 * @see #render(String, String, String...)
	 */
	public static void renderText(final HttpServletResponse response, final String text, final String... headers) {
		render(ServletUtils.TEXT_TYPE, text, response, headers);
	}

	/**
	 * 直接输出HTML.
	 * 
	 * @param response the response
	 * @param html     the html
	 * @param headers  the headers
	 * @see #render(String, String, String...)
	 */
	public static void renderHtml(final HttpServletResponse response, final String html, final String... headers) {
		render(ServletUtils.HTML_TYPE, html, response, headers);
	}

	/**
	 * 直接输出XML.
	 * 
	 * @param response the response
	 * @param xml      the xml
	 * @param headers  the headers
	 * @see #render(String, String, String...)
	 */
	public static void renderXml(final HttpServletResponse response, final String xml, final String... headers) {
		render(ServletUtils.XML_TYPE, xml, response, headers);
	}

	/**
	 * 直接输出JSON.
	 * 
	 * @param response   the response
	 * @param jsonString json字符串.
	 * @param headers    the headers
	 * @see #render(String, String, String...)
	 */
	public static void renderJson(final HttpServletResponse response, final String jsonString, final String... headers) {
		render(ServletUtils.JSON_TYPE, jsonString, response, headers);
	}

	/**
	 * 直接输出JSON,使用Jackson转换Java对象.
	 * 
	 * @param response the response
	 * @param data     可以是List<POJO>, POJO[], POJO, 也可以Map名值对.
	 * @param headers  the headers
	 * @see #render(String, String, String...)
	 */
	public static void renderJson(HttpServletResponse response, final Object data, final String... headers) {
		response = initResponseHeader(ServletUtils.JSON_TYPE, response, headers);
		try {
			mapper.writeValue(response.getWriter(), data);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * 直接输出支持跨域Mashup的JSONP.
	 * 
	 * @param response     the response
	 * @param callbackName callback函数名.
	 * @param object       Java对象,可以是List<POJO>, POJO[], POJO ,也可以Map名值对,
	 *                     将被转化为json字符串.
	 * @param headers      the headers
	 */
	public static void renderJsonp(final HttpServletResponse response, final String callbackName, final Object object, final String... headers) {
		String jsonString = null;
		try {
			jsonString = mapper.writeValueAsString(object);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}

		String result = new StringBuilder().append(callbackName).append("(").append(jsonString).append(");").toString();

		// 渲染Content-Type为javascript的返回内容,输出结果为javascript语句,
		// 如callback197("{html:'Hello World!!!'}");
		render(ServletUtils.JS_TYPE, result, response, headers);
	}

	/**
	 * 分析并设置contentType与headers.
	 * 
	 * @param contentType the content type
	 * @param response    the response
	 * @param headers     the headers
	 * @return the http servlet response
	 */
	private static HttpServletResponse initResponseHeader(final String contentType, final HttpServletResponse response, final String... headers) {
		// 分析headers参数
		String encoding = DEFAULT_ENCODING;
		boolean noCache = DEFAULT_NOCACHE;
		for (String header : headers) {
			String headerName = StringUtils.substringBefore(header, ":");
			String headerValue = StringUtils.substringAfter(header, ":");

			if (StringUtils.equalsIgnoreCase(headerName, HEADER_ENCODING)) {
				encoding = headerValue;
			} else if (StringUtils.equalsIgnoreCase(headerName, HEADER_NOCACHE)) {
				noCache = Boolean.parseBoolean(headerValue);
			} else {
				throw new IllegalArgumentException(headerName + "不是一个合法的header类型");
			}
		}

		// 设置headers参数
		String fullContentType = contentType + ";charset=" + encoding;
		response.setContentType(fullContentType);
		if (noCache) {
			ServletUtils.setNoCacheHeader(response);
		}

		return response;
	}
}
