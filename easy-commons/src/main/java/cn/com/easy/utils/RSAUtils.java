package cn.com.easy.utils;

import java.io.ByteArrayOutputStream;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

/**
 * RSA功能有两部分<br/>
 * 一、用来加解密，公钥用于加密，私钥用于解密<br/>
 * 二、用来验签（即数据签名），私钥用于签名，公钥用于验签
 * 
 * @author nibili 2017年7月31日
 * 
 */
public class RSAUtils {

	public static final String KEY_ALGORITHM = "RSA";

	public static final String SIGNATURE_ALGORITHM = "SHA1WithRSA";
	/** map中的公钥key */
	private static final String PUBLIC_KEY = "RSAPublicKey";
	/** map中的私钥key */
	private static final String PRIVATE_KEY = "RSAPrivateKey";

	public static void main(String[] args) throws Exception {

		// 一、用来加解密，公钥用于加密，私钥用于解密
		// 明文
		String originalString = "你好，我是RSA测试";
		// 获取密钥对
		Map<String, Key> keyMap = RSAUtils.initKey();
		// 公钥加密
		String tempString = RSAUtils.encryptByPublicKeyForString(originalString, RSAUtils.getPublicKey(keyMap));
		// 私钥解密
		System.out.println("解密后的明文：" + RSAUtils.decryptByPrivateKeyForString(tempString, RSAUtils.getPrivateKey(keyMap)));

		// 二、用来验签（即数据签名），私钥用于签名，公钥用于验签
		// 用来生成签名的数据
		String toSignString = "用来验签（即数据签名），私钥用于签名，公钥用于验签aaaaaaaaaaaaaaa";
		// 私钥生成签名
		String sign = RSAUtils.sign(toSignString.getBytes("UTF-8"), RSAUtils.getPrivateKey(keyMap));
		System.out.println("签名长度：" + sign.length() + " ，签名：" + sign);
		// 公钥验证签名
		System.out.println("签名验证结果：" + RSAUtils.verify(toSignString.getBytes(), RSAUtils.getPublicKey(keyMap), sign));

		// 三、打印公钥私钥
		System.out.println("私钥" + RSAUtils.getPrivateKey(keyMap));
		System.out.println("公钥" + RSAUtils.getPublicKey(keyMap));

		// 四、分段加密、解密
		originalString = "你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试你好，我是RSA测试";
		tempString = RSAUtils.encryptByPublicKeyWithCut(originalString, RSAUtils.getPublicKey(keyMap));
		System.out.println("解密后的明文：" + RSAUtils.decryptByPrivateKeyWithCut(tempString, RSAUtils.getPrivateKey(keyMap)));

	}

	/**
	 * 二、用来验签（即数据签名），私钥用于签名，公钥用于验签<br/>
	 * 用私钥对信息生成数字签名
	 * 
	 * @param data       加密数据
	 * @param privateKey 私钥
	 * @return
	 * @throws Exception
	 */
	public static String sign(String data, String privateKey) throws Exception {
		return sign(data.getBytes("UTF-8"), privateKey);
	}

	/**
	 * 二、用来验签（即数据签名），私钥用于签名，公钥用于验签<br/>
	 * 用私钥对信息生成数字签名
	 * 
	 * @param data       加密数据
	 * @param privateKey 私钥
	 * @return
	 * @throws Exception
	 */
	public static String sign(byte[] data, String privateKey) throws Exception {
		// 解密由base64编码的私钥
		byte[] keyBytes = decryptBASE64(privateKey);
		// 构造PKCS8EncodedKeySpec对象
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		// KEY_ALGORITHM 指定的加密算法
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		// 取私钥匙对象
		PrivateKey priKey = keyFactory.generatePrivate(pkcs8KeySpec);
		// 用私钥对信息生成数字签名
		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
		signature.initSign(priKey);
		signature.update(data);
		return encryptBASE64(signature.sign());
	}

	/**
	 * 二、用来验签（即数据签名），私钥用于签名，公钥用于验签<br/>
	 * 校验数字签名
	 * 
	 * @param data      加密数据
	 * @param publicKey 公钥
	 * @param sign      数字签名
	 * @return 校验成功返回true 失败返回false
	 * @throws Exception
	 */
	public static boolean verify(String data, String publicKey, String sign) throws Exception {
		return verify(data.getBytes("UTF-8"), publicKey, sign);
	}

	/**
	 * 二、用来验签（即数据签名），私钥用于签名，公钥用于验签<br/>
	 * 校验数字签名
	 * 
	 * @param data      加密数据
	 * @param publicKey 公钥
	 * @param sign      数字签名
	 * @return 校验成功返回true 失败返回false
	 * @throws Exception
	 */
	public static boolean verify(byte[] data, String publicKey, String sign) throws Exception {
		// 解密由base64编码的公钥
		byte[] keyBytes = decryptBASE64(publicKey);
		// 构造X509EncodedKeySpec对象
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
		// KEY_ALGORITHM 指定的加密算法
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		// 取公钥匙对象
		PublicKey pubKey = keyFactory.generatePublic(keySpec);
		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
		signature.initVerify(pubKey);
		signature.update(data);
		// 验证签名是否正常
		return signature.verify(decryptBASE64(sign));
	}

	/**
	 * 一、用来加解密，公钥用于加密，私钥用于解密 解密<br>
	 * 解密<br>
	 * 用私钥解密
	 * 
	 * @param data
	 * @param privateKeyString
	 * @return
	 * @throws Exception
	 */
	public static String decryptByPrivateKeyForString(String data, String privateKeyString) throws Exception {
		byte[] bytes = decryptByPrivateKey(data, privateKeyString);
		return new String(bytes, "UTF-8");

	}

	/**
	 * 一、用来加解密，公钥用于加密，私钥用于解密 解密<br>
	 * 解密<br>
	 * 用私钥解密
	 * 
	 * @param data
	 * @param privateKeyString
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptByPrivateKey(String data, String privateKeyString) throws Exception {
		return decryptByPrivateKey(decryptBASE64(data), privateKeyString);
	}

	/**
	 * 一、用来加解密，公钥用于加密，私钥用于解密 解密<br>
	 * 解密<br>
	 * 用私钥解密
	 * 
	 * @param data
	 * @param privateKeyString
	 * @return
	 * @throws Exception
	 * @author nibili 2017年7月31日
	 */
	public static byte[] decryptByPrivateKey(byte[] data, String privateKeyString) throws Exception {
		// 对密钥解密
		byte[] keyBytes = decryptBASE64(privateKeyString);
		// 取得私钥
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
		// 对数据解密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		return cipher.doFinal(data);
	}

	/**
	 * 一、用来加解密，公钥用于加密，私钥用于解密<br>
	 * 加密<br>
	 * 用公钥加密
	 * 
	 * @param data
	 * @param publicKeyString
	 * @return
	 * @throws Exception
	 * @auth nibili 2021-4-13 10:59:10
	 */
	public static String encryptByPublicKeyForString(String data, String publicKeyString) throws Exception {
		byte[] bytes = encryptByPublicKey(data, publicKeyString);
		return RSAUtils.encryptBASE64(bytes);
	}

	/**
	 * 一、用来加解密，公钥用于加密，私钥用于解密<br>
	 * 加密<br>
	 * 用公钥加密
	 * 
	 * @param data
	 * @param publicKeyString
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptByPublicKey(String data, String publicKeyString) throws Exception {
		return encryptByPublicKey(data.getBytes("UTF-8"), publicKeyString);
	}

	/**
	 * 一、用来加解密，公钥用于加密，私钥用于解密<br>
	 * 加密<br>
	 * 用公钥加密
	 * 
	 * @param data
	 * @param publicKeyString
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptByPublicKey(byte[] data, String publicKeyString) throws Exception {
		// 对密钥解密
		byte[] keyBytes = decryptBASE64(publicKeyString);
		// 取得公钥
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key publicKey = keyFactory.generatePublic(x509KeySpec);
		// 对数据解密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		return cipher.doFinal(data);
	}

	/**
	 * 取得私钥
	 * 
	 * @param keyMap
	 * @return
	 * @throws Exception
	 */
	public static String getPrivateKey(Map<String, Key> keyMap) throws Exception {
		Key key = (Key) keyMap.get(PRIVATE_KEY);
		return encryptBASE64(key.getEncoded());
	}

	/**
	 * 取得公钥
	 * 
	 * @param keyMap
	 * @return
	 * @throws Exception
	 */
	public static String getPublicKey(Map<String, Key> keyMap) throws Exception {
		Key key = keyMap.get(PUBLIC_KEY);
		return encryptBASE64(key.getEncoded());
	}

	/**
	 * 初始化密钥对
	 * 
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Key> initKey() throws Exception {
		return initKey(1024);
	}

	/**
	 * 初始化密钥对
	 * 
	 * @param keyLength
	 * @return
	 * @throws Exception
	 * @auth nibili 2019年8月9日 下午8:52:15
	 */
	public static Map<String, Key> initKey(int keyLength) throws Exception {
		KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
		keyPairGen.initialize(keyLength);
		KeyPair keyPair = keyPairGen.generateKeyPair();
		Map<String, Key> keyMap = new HashMap<String, Key>(2);
		keyMap.put(PUBLIC_KEY, keyPair.getPublic());// 公钥
		keyMap.put(PRIVATE_KEY, keyPair.getPrivate());// 私钥
		return keyMap;
	}

	/**
	 * 字符串转byte数组
	 * 
	 * @param key
	 * @return
	 * @author nibili 2017年7月31日
	 */
	public static byte[] decryptBASE64(String key) {
		return Base64.decodeBase64(StringUtils.getBytesUtf8(key));
	}

	/**
	 * byte数组转字符串
	 * 
	 * @param bytes
	 * @return
	 * @author nibili 2017年7月31日
	 */
	public static String encryptBASE64(byte[] bytes) {
		return Base64.encodeBase64String(bytes);
	}

	/* RSA最大加密明文大小 */
	private static final int MAX_ENCRYPT_BLOCK = 117;
	/* RSA最大解密密文大小 */
	private static final int MAX_DECRYPT_BLOCK = 128;

	/**
	 * 分段加密
	 * 
	 * @param data
	 * @param publicKey
	 * @return
	 * @throws Exception
	 * @auth nibili 2021-4-13 11:08:03
	 */
	public static String encryptByPublicKeyWithCut(String data, String publicKey) throws Exception {
		return encryptBASE64(encryptByPublicKeyWithCut(data.getBytes("UTF-8"), publicKey));
	}

	/**
	 * <p>
	 * 分段 公钥加密
	 * </p>
	 *
	 * @param data      源数据
	 * @param publicKey 公钥(BASE64编码)
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptByPublicKeyWithCut(byte[] data, String publicKey) throws Exception {
		byte[] keyBytes = Base64.decodeBase64(publicKey);
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key publicK = keyFactory.generatePublic(x509KeySpec);
		// 对数据加密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, publicK);
		int inputLen = data.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段加密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
				cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(data, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_ENCRYPT_BLOCK;
		}
		byte[] encryptedData = out.toByteArray();
		out.close();
		return encryptedData;
	}

	/**
	 * 分段 私钥解密
	 * 
	 * @param encryptedData
	 * @param privateKey
	 * @return
	 * @throws Exception
	 * @auth nibili 2021-4-13 11:11:27
	 */
	public static String decryptByPrivateKeyWithCut(String encryptedData, String privateKey) throws Exception {
		return new String(decryptByPrivateKeyWithCut(Base64.decodeBase64(encryptedData), privateKey), "UTF-8");
	}

	/**
	 * <P>
	 * 私钥解密
	 * </p>
	 *
	 * @param encryptedData 已加密数据
	 * @param privateKey    私钥(BASE64编码)
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptByPrivateKeyWithCut(byte[] encryptedData, String privateKey) throws Exception {
		byte[] keyBytes = Base64.decodeBase64(privateKey);
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key privateK = keyFactory.generatePrivate(pkcs8KeySpec);
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, privateK);
		int inputLen = encryptedData.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段解密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
				cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_DECRYPT_BLOCK;
		}
		byte[] decryptedData = out.toByteArray();
		out.close();
		return decryptedData;
	}
}
