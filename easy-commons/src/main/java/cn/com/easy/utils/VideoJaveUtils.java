package cn.com.easy.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import ws.schild.jave.Encoder;
import ws.schild.jave.EncoderException;
import ws.schild.jave.InputFormatException;
import ws.schild.jave.MultimediaObject;
import ws.schild.jave.ScreenExtractor;
import ws.schild.jave.encode.AudioAttributes;
import ws.schild.jave.encode.EncodingAttributes;
import ws.schild.jave.encode.VideoAttributes;
import ws.schild.jave.info.MultimediaInfo;

public class VideoJaveUtils {

//	/** 工具包地址 */
//	public final static String utilPath = "C:\\Users\\DELL\\.m2\\repository\\ws\\schild\\jave-nativebin-win64\\3.0.1\\jave-nativebin-win64-3.0.1\\ws\\schild\\jave\\nativebin\\ffmpeg-amd64.exe";

	public static void main(String[] args) throws InputFormatException, EncoderException, FileNotFoundException, IOException {
		/** 压缩视频 */
		compressionVideo("D:\\短视频\\douyin\\test.mp4", "D:\\短视频\\douyin\\hello.mp4", false);
		/** 视频文件地址 */
		String videoPath = "D:\\短视频\\douyin\\hello.mp4";
		/** 获取视频信息 */
		Map<String, Object> map = getVideoInfo(videoPath);
		System.out.println(FastJSONUtils.toJsonString(map));
		/** 截取视频封面 */
		readCover(videoPath, "D:\\短视频\\douyin\\cover.jpg", null, null, 2000l);
	}

	/**
	 * 获取视频封面<br/>
	 * 如果 宽 高为null,那么将按照视频的高宽(除以2)来截取第一贞
	 * 
	 * @param videoPath
	 * @param outputPath
	 * @param width
	 * @param height
	 * @param millions   截取图片的时间点(单位ms),null为视频第一张图
	 * @throws InputFormatException
	 * @throws EncoderException
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @auth nibili 2021-1-7 14:59:47
	 */
	public static void readCover(String videoPath, String outputPath, Integer width, Integer height, Long millions)
			throws InputFormatException, EncoderException, FileNotFoundException, IOException {
		File source = new File(videoPath);
		MultimediaObject multimediaObject = new MultimediaObject(source);
		ScreenExtractor srcreenExtractor = new ScreenExtractor();
		if (millions == null) {
			millions = 0l;
		}
		if (width != null && height != null) {
			srcreenExtractor.renderOneImage(multimediaObject, width, height, millions, new File(outputPath), 1);
		} else {
			MultimediaInfo m = multimediaObject.getInfo();
			width = m.getVideo().getSize().getWidth();
			height = m.getVideo().getSize().getHeight();
			srcreenExtractor.renderOneImage(multimediaObject, width, height, millions, new File(outputPath), 1);
			width = width / 2;
			height = height / 2;
		}
		ImgCompressUtils.compressPic(outputPath, outputPath, height, width, false, false);
	}

	/**
	 * 获取视频信息
	 * 
	 * @param realPath
	 * @return
	 * @auth nibili 2021-1-7 15:00:22
	 */
	public static Map<String, Object> getVideoInfo(String realPath) {
		File source = new File(realPath);
		FileChannel fc = null;
		String size = "";
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			MultimediaObject multimediaObject = new MultimediaObject(source);
			MultimediaInfo m = multimediaObject.getInfo();
			long ls = m.getDuration();
			System.out.println("此视频时长为:" + ls / 60000 + "分" + (ls) / 1000 + "秒！");
			// 视频帧宽高
			System.out.println("此视频高度为:" + m.getVideo().getSize().getHeight());
			System.out.println("此视频宽度为:" + m.getVideo().getSize().getWidth());
			System.out.println("此视频格式为:" + m.getFormat());
			result.put("duration", ls);
			result.put("videoHigh", m.getVideo().getSize().getHeight());
			result.put("videoWide", m.getVideo().getSize().getWidth());
			result.put("format", m.getFormat());
			@SuppressWarnings("resource")
			FileInputStream fileInputStream = new FileInputStream(source);
			fc = fileInputStream.getChannel();
			BigDecimal fileSize = new BigDecimal(fc.size());
			size = fileSize.divide(new BigDecimal(1048576), 2, RoundingMode.HALF_UP) + "MB";
			System.out.println("此视频大小为" + size);
			result.put("size", size);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		} finally {
			if (null != fc) {
				try {
					fc.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

//	/**
//	 * @Description: 截取视频第几帧生成图片
//	 * @param videoPath 视频路径
//	 * @param frame     第几帧
//	 * @return String 图片的相对路劲
//	 */
//	public static String processImg(String videoPath, String frame) {
//		File file = new File(videoPath);
//		if (!file.exists()) {
//			System.err.println("路径[" + videoPath + "]对应的视频文件不存在!");
//			return null;
//		}
//		List<String> commands = new java.util.ArrayList<String>();
//		commands.add(utilPath);
//		commands.add("-i");
//		commands.add(videoPath);
//		commands.add("-y");
//		commands.add("-f");
//		commands.add("image2");
//		commands.add("-ss");
//		commands.add(frame); // 这个参数是设置截取视频多少秒时的画面
//		commands.add("-t");
//		commands.add("0.001");
//		commands.add("-s");
//		commands.add("360x640");// 宽X高
//		String imgPath = videoPath.substring(0, videoPath.lastIndexOf(".")).replaceFirst("vedio", "file") + ".jpg";
//		commands.add(imgPath);
//		try {
//			ProcessBuilder builder = new ProcessBuilder();
//			builder.command(commands);
//			builder.start();
//			System.out.println("截取成功");
//			return imgPath;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//
//	}

	/**
	 * 传视频File对象，返回压缩后File对象信息
	 * 
	 * @param sourcePath
	 * @param targetPath
	 * @param deleteSourceFile 是否删除原文件
	 * @auth nibili 2021-1-7 15:29:34
	 */
	public static void compressionVideo(String sourcePath, String targetPath, boolean deleteSourceFile) {
		if (StringUtils.isBlank(targetPath) || StringUtils.isBlank(targetPath)) {
			return;
		}

		File source = new File(sourcePath);
		File target = new File(targetPath);
		try {
			MultimediaObject object = new MultimediaObject(source);
//			AudioInfo audioInfo = object.getInfo().getAudio();
			// 根据视频大小来判断是否需要进行压缩,
			int maxSize = 5;
			double mb = Math.ceil(source.length() / 1048576);
			int second = (int) object.getInfo().getDuration() / 1000;
			BigDecimal bd = new BigDecimal(String.format("%.4f", mb / second));
			System.out.println("开始压缩视频了--> 视频每秒平均 " + bd + " MB ");
			// 视频 > 5MB, 或者每秒 > 0.5 MB 才做压缩， 不需要的话可以把判断去掉
			boolean temp = mb >= maxSize || bd.compareTo(new BigDecimal(0.5)) > 0;
			if (temp) {
				long time = System.currentTimeMillis();
//			// TODO 视频属性设置
//			int maxBitRate = 128000;
//			int maxSamplingRate = 44100;
//
				int bitRate = 810000;
//			int maxFrameRate = 20;
//			int maxWidth = 1280;
				AudioAttributes audio = new AudioAttributes();
//			// 设置通用编码格式10 audio.setCodec("aac");
//			// 设置最大值：比特率越高，清晰度/音质越好
//			// 设置音频比特率,单位:b (比特率越高，清晰度/音质越好，当然文件也就越大 128000 = 182kb)
//			if (audioInfo.getBitRate() > maxBitRate) {
//				audio.setBitRate(new Integer(maxBitRate));
//			}
//
//			// 设置重新编码的音频流中使用的声道数（1 =单声道，2 = 双声道（立体声））。如果未设置任何声道值，则编码器将选择默认值 0。
//			audio.setChannels(audioInfo.getChannels());
//			// 采样率越高声音的还原度越好，文件越大
//			// 设置音频采样率，单位：赫兹 hz
//			// 设置编码时候的音量值，未设置为0,如果256，则音量值不会改变
//			// audio.setVolume(256);
//			if (audioInfo.getSamplingRate() > maxSamplingRate) {
//				audio.setSamplingRate(maxSamplingRate);
//			}
				// TODO 视频编码属性配置
				ws.schild.jave.info.VideoInfo videoInfo = object.getInfo().getVideo();
				VideoAttributes video = new VideoAttributes();
//			video.setCodec("h264");
//			// 设置音频比特率,单位:b (比特率越高，清晰度/音质越好，当然文件也就越大 800000 = 800kb)
				if (videoInfo.getBitRate() > bitRate) {
					video.setBitRate(bitRate);
				}
//
//			// 视频帧率：15 f / s 帧率越低，效果越差
//			// 设置视频帧率（帧率越低，视频会出现断层，越高让人感觉越连续），视频帧率（Frame
//			// rate）是用于测量显示帧数的量度。所谓的测量单位为每秒显示帧数(Frames per Second，简：FPS）或“赫兹”（Hz）。
//			if (videoInfo.getFrameRate() > maxFrameRate) {
//				video.setFrameRate(maxFrameRate);
//			}
//
//			// 限制视频宽高
//			int width = videoInfo.getSize().getWidth();
//			int height = videoInfo.getSize().getHeight();
//			if (width > maxWidth) {
//				float rat = (float) width / maxWidth;
//				video.setSize(new VideoSize(maxWidth, (int) (height / rat)));
//			}
//
//			video.setQuality(1);
//			video.setFaststart(true);			
				EncodingAttributes attr = new EncodingAttributes();
////	                attr.setFormat("mp4");
				attr.setAudioAttributes(audio);
				attr.setVideoAttributes(video);

				// 速度最快的压缩方式， 压缩速度 从快到慢： ultrafast, superfast, veryfast, faster, fast, medium,
				// slow, slower, veryslow and placebo.
//	                attr.setPreset(PresetUtil.VERYFAST);
//	                attr.setCrf(27);
//	                // 设置线程数
//	                attr.setEncodingThreads(Runtime.getRuntime().availableProcessors()/2);

				Encoder encoder = new Encoder();
				encoder.encode(new MultimediaObject(source), target, attr);
				System.out.println("压缩总耗时：" + (System.currentTimeMillis() - time) / 1000 + "s");
				if (deleteSourceFile) {
					if (target.length() > 0) {
						source.delete();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
