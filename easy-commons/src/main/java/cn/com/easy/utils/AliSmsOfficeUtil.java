package cn.com.easy.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

/**
 * 阿里短信官方sdk封装的工具类
 * 
 * @author nibili 2019年5月5日
 * 
 */
public class AliSmsOfficeUtil {

	// 产品名称:云通信短信API产品,开发者无需替换
	static final String product = "Dysmsapi";
	// 产品域名,开发者无需替换
	static final String domain = "dysmsapi.aliyuncs.com";

	/**
	 * 发送短信
	 * 
	 * @throws ClientException
	 * @throws ServerException
	 * 
	 * @auth nibili 2020年3月30日 下午10:03:05
	 */
	public static CommonResponse sendMsg(String accessKeyId, String accessKeySecret, String phoneNumber, String signName, String templateCode, String templateParamJson)
			throws ServerException, ClientException {
		DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
		IAcsClient client = new DefaultAcsClient(profile);

		CommonRequest request = new CommonRequest();
		request.setSysMethod(MethodType.POST);
		request.setSysDomain("dysmsapi.aliyuncs.com");
		request.setSysVersion("2017-05-25");
		request.setSysAction("SendSms");
		request.putQueryParameter("RegionId", "cn-hangzhou");
		request.putQueryParameter("PhoneNumbers", phoneNumber);
		request.putQueryParameter("SignName", signName);
		request.putQueryParameter("TemplateCode", templateCode);
		if (StringUtils.isNotBlank(templateParamJson) == true) {
			request.putQueryParameter("TemplateParam", templateParamJson);
		}
		// request.putQueryParameter("SmsUpExtendCode", "{}");
		// request.putQueryParameter("OutId", "1234567890");
		CommonResponse response = client.getCommonResponse(request);
		return response;
	}

	/**
	 * 发送短信
	 * 
	 * @param aliSmsParamDto
	 * @param aliSmsConfigDto
	 * @return
	 * @throws ClientException
	 * @auth nibili 2019年5月5日 196888813@qq.com
	 */
	public static CommonResponse sendSms(AliSmsParamDto aliSmsParamDto, AliSmsConfigDto aliSmsConfigDto) throws ClientException {

		DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", aliSmsConfigDto.getAppKey(), aliSmsConfigDto.getAppSecret());
		IAcsClient client = new DefaultAcsClient(profile);

		// 组装请求对象-具体描述见控制台-文档部分内容
		CommonRequest request = new CommonRequest();

		request.setSysMethod(MethodType.POST);
		request.setSysDomain("dysmsapi.aliyuncs.com");
		request.setSysVersion("2017-05-25");
		request.setSysAction("SendSms");
		request.putQueryParameter("RegionId", "cn-hangzhou");
		request.putQueryParameter("PhoneNumbers", aliSmsParamDto.getMobile());
		request.putQueryParameter("SignName", aliSmsConfigDto.getSignName());
		request.putQueryParameter("TemplateCode", aliSmsParamDto.getTemplateCode());
		if (aliSmsParamDto.getParam() != null) {
			request.putQueryParameter("TemplateParam", FastJSONUtils.toJsonString(aliSmsParamDto.getParam()));
		}
		// hint 此处可能会抛出异常，注意catch
		CommonResponse sendSmsResponse = client.getCommonResponse(request);
		return sendSmsResponse;
	}

	/**
	 * 查询已发送的短信
	 * 
	 * @param bizId
	 * @param mobile
	 * @param pageSize
	 * @param pageNo
	 * @param aliSmsConfigDto
	 * @return
	 * @throws ClientException
	 * @auth nibili 2019年5月5日 196888813@qq.com
	 */
	public static CommonResponse querySendDetails(String bizId, String mobile, long pageSize, long pageNo, AliSmsConfigDto aliSmsConfigDto) throws ClientException {

		DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", aliSmsConfigDto.getAppKey(), aliSmsConfigDto.getAppSecret());
		IAcsClient client = new DefaultAcsClient(profile);

		CommonRequest request = new CommonRequest();
		request.setSysMethod(MethodType.POST);
		request.setSysDomain("dysmsapi.aliyuncs.com");
		request.setSysVersion("2017-05-25");
		request.setSysAction("QuerySendDetails");
		request.putQueryParameter("RegionId", "cn-hangzhou");
		request.putQueryParameter("RegionId", "cn-hangzhou");
		request.putQueryParameter("PhoneNumber", mobile);
		// 必填-发送日期 支持30天内记录查询，格式yyyyMMdd
		SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
		request.putQueryParameter("SendDate", ft.format(new Date()));
		request.putQueryParameter("PageSize", String.valueOf(pageSize));
		request.putQueryParameter("CurrentPage", String.valueOf(pageNo));
		if (StringUtils.isNotBlank(bizId) == true) {
			request.putQueryParameter("BizId", bizId);
		}
		CommonResponse response = client.getCommonResponse(request);
		return response;
	}

	public static void main(String[] args) throws ClientException, InterruptedException {

		// // 发短信
		// SendSmsResponse response = sendSms();
		// System.out.println("短信接口返回的数据----------------");
		// System.out.println("Code=" + response.getCode());
		// System.out.println("Message=" + response.getMessage());
		// System.out.println("RequestId=" + response.getRequestId());
		// System.out.println("BizId=" + response.getBizId());
		//
		// Thread.sleep(3000L);
		//
		// // 查明细
		// if (response.getCode() != null && response.getCode().equals("OK")) {
		// QuerySendDetailsResponse querySendDetailsResponse =
		// querySendDetails(response.getBizId());
		// System.out.println("短信明细查询接口返回数据----------------");
		// System.out.println("Code=" + querySendDetailsResponse.getCode());
		// System.out.println("Message=" +
		// querySendDetailsResponse.getMessage());
		// int i = 0;
		// for (QuerySendDetailsResponse.SmsSendDetailDTO smsSendDetailDTO :
		// querySendDetailsResponse.getSmsSendDetailDTOs()) {
		// System.out.println("SmsSendDetailDTO[" + i + "]:");
		// System.out.println("Content=" + smsSendDetailDTO.getContent());
		// System.out.println("ErrCode=" + smsSendDetailDTO.getErrCode());
		// System.out.println("OutId=" + smsSendDetailDTO.getOutId());
		// System.out.println("PhoneNum=" + smsSendDetailDTO.getPhoneNum());
		// System.out.println("ReceiveDate=" +
		// smsSendDetailDTO.getReceiveDate());
		// System.out.println("SendDate=" + smsSendDetailDTO.getSendDate());
		// System.out.println("SendStatus=" + smsSendDetailDTO.getSendStatus());
		// System.out.println("Template=" + smsSendDetailDTO.getTemplateCode());
		// }
		// System.out.println("TotalCount=" +
		// querySendDetailsResponse.getTotalCount());
		// System.out.println("RequestId=" +
		// querySendDetailsResponse.getRequestId());
		// }

	}

	/**
	 * 配置项
	 * 
	 * @author nibili 2019年5月5日
	 * 
	 */
	public static class AliSmsConfigDto {
		/** ali大于 短信，key */
		private String appKey;
		/** ali大于 短信， */
		private String appSecret;
		/**
		 * // 短信签名，传入的短信签名必须是在阿里大鱼“管理中心-短信签名管理”中的可用签名。如“阿里大鱼”已在短信签名管理中通过审核，则可传入”
		 * 阿里大鱼“（传参时去掉引号）作为短信签名。短信效果示例：【阿里大鱼】欢迎使用阿里大鱼服务。
		 */
		private String signName;

		/**
		 * get 短信签名，传入的短信签名必须是在阿里大鱼“管理中心-短信签名管理”中的可用签名。如“阿里大鱼”已在短信签名管理中通过审核，则可传入”
		 * 阿里大鱼“（传参时去掉引号）作为短信签名。短信效果示例：【阿里大鱼】欢迎使用阿里大鱼服务。
		 * 
		 * @return
		 * @author nibili 2019年5月5日
		 */
		public String getSignName() {
			return signName;
		}

		/**
		 * set 短信签名，传入的短信签名必须是在阿里大鱼“管理中心-短信签名管理”中的可用签名。如“阿里大鱼”已在短信签名管理中通过审核，则可传入”
		 * 阿里大鱼“（传参时去掉引号）作为短信签名。短信效果示例：【阿里大鱼】欢迎使用阿里大鱼服务。
		 * 
		 * @param signName
		 * @author nibili 2019年5月5日
		 */
		public void setSignName(String signName) {
			this.signName = signName;
		}

		/**
		 * get ali大于短信，key
		 * 
		 * @return
		 * @author nibili 2019年5月5日
		 */
		public String getAppKey() {
			return appKey;
		}

		/**
		 * set ali大于短信，key
		 * 
		 * @param appKey
		 * @author nibili 2019年5月5日
		 */
		public void setAppKey(String appKey) {
			this.appKey = appKey;
		}

		/**
		 * get ali大于短信，
		 * 
		 * @return
		 * @author nibili 2019年5月5日
		 */
		public String getAppSecret() {
			return appSecret;
		}

		/**
		 * set ali大于短信，
		 * 
		 * @param appSecret
		 * @author nibili 2019年5月5日
		 */
		public void setAppSecret(String appSecret) {
			this.appSecret = appSecret;
		}
	}

	/**
	 * 发送短信参数
	 * 
	 * @author nibili 2019年5月5日
	 * 
	 */
	public static class AliSmsParamDto {

		/** 模板id */
		private String templateCode;
		/** 目标电话号码 */
		private String mobile;
		/**
		 * 短信模板中的参数<br/>
		 * 短信模板变量，传参规则{"key":"value"}，key的名字须和申请模板中的变量名一致，多个变量之间以逗号隔开。示例：针对模板“
		 * 验证码${code}，您正在进行${product}身份验证，打死不要告诉别人哦！”，传参时需传入{"code":"1234",
		 * "product":"alidayu"}
		 */
		private Object param;
		/**
		 * 公共回传参数，在“消息返回”中会透传回该参数；举例：用户可以传入自己下级的会员ID，在消息返回时，该会员ID会包含在内，
		 * 用户可以根据该会员ID识别是哪位会员使用了你的应用
		 */
		private String extend = "123456";

		/**
		 * get 模板id
		 * 
		 * @return
		 * @author nibili 2019年5月5日
		 */
		public String getTemplateCode() {
			return templateCode;
		}

		/**
		 * set 模板id
		 * 
		 * @param templateCode
		 * @author nibili 2019年5月5日
		 */
		public void setTemplateCode(String templateCode) {
			this.templateCode = templateCode;
		}

		/**
		 * get 目标电话号码
		 * 
		 * @return
		 * @author nibili 2019年5月5日
		 */
		public String getMobile() {
			return mobile;
		}

		/**
		 * set 目标电话号码
		 * 
		 * @param mobile
		 * @author nibili 2019年5月5日
		 */
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}

		/**
		 * 短信模板中的参数<br/>
		 * 短信模板变量，传参规则{"key":"value"}，key的名字须和申请模板中的变量名一致，多个变量之间以逗号隔开。示例：针对模板“
		 * 验证码${code}，您正在进行${product}身份验证，打死不要告诉别人哦！”，传参时需传入{"code":"1234",
		 * "product":"alidayu"}
		 * 
		 * @return
		 * @author nibili 2019年5月5日
		 */
		public Object getParam() {
			return param;
		}

		/**
		 * 短信模板中的参数<br/>
		 * 短信模板变量，传参规则{"key":"value"}，key的名字须和申请模板中的变量名一致，多个变量之间以逗号隔开。示例：针对模板“
		 * 验证码${code}，您正在进行${product}身份验证，打死不要告诉别人哦！”，传参时需传入{"code":"1234",
		 * "product":"alidayu"}
		 * 
		 * @param
		 * @author nibili 2019年5月5日
		 */
		public void setParam(Object param) {
			this.param = param;
		}

		/**
		 * get 公共回传参数，在“消息返回”中会透传回该参数；举例：用户可以传入自己下级的会员ID，在消息返回时，该会员ID会包含在内，
		 * 用户可以根据该会员ID识别是哪位会员使用了你的应用
		 * 
		 * @return
		 * @author nibili 2019年5月5日
		 */
		public String getExtend() {
			return extend;
		}

		/**
		 * set 公共回传参数，在“消息返回”中会透传回该参数；举例：用户可以传入自己下级的会员ID，在消息返回时，该会员ID会包含在内，
		 * 用户可以根据该会员ID识别是哪位会员使用了你的应用
		 * 
		 * @param extend
		 * @author nibili 2019年5月5日
		 */
		public void setExtend(String extend) {
			this.extend = extend;
		}

	}
}
