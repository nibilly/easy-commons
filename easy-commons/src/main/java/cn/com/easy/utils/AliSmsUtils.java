package cn.com.easy.utils;

import org.apache.commons.lang3.StringUtils;

import cn.com.easy.exception.BusinessException;

import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;

/**
 * 短信服务
 * 
 * @author nibili 2016年6月29日
 * 
 */
public class AliSmsUtils {

	/**
	 * 发送短信
	 * 参考链连:https://api.alidayu.com/doc2/apiDetail.htm?spm=a3142.7629065.4.7
	 * .9e3kYF&apiId=25450
	 * 
	 * @param aliSmsParamDto
	 * @param aliSmsConfigDto
	 * @throws ApiException
	 * @author nibili 2017年10月12日
	 * @throws BusinessException
	 */
	public static String sendMms(AliSmsParamDto aliSmsParamDto, AliSmsConfigDto aliSmsConfigDto) throws ApiException, BusinessException {

		if (StringUtils.isBlank(aliSmsConfigDto.getAppKey()) == true) {
			throw new BusinessException("appKey不能为空");
		}
		if (StringUtils.isBlank(aliSmsConfigDto.getAppSecret()) == true) {
			throw new BusinessException("appSecret不能为空");
		}
		if (StringUtils.isBlank(aliSmsParamDto.getTemplateCode()) == true) {
			throw new BusinessException("模板id不能为空");
		}
		if (StringUtils.isBlank(aliSmsParamDto.getMobile()) == true) {
			throw new BusinessException("电话号码不能为空");
		}
		if (StringUtils.isBlank(aliSmsConfigDto.getSignName()) == true) {
			throw new BusinessException("短信签名不能为空");
		}

		TaobaoClient client = new DefaultTaobaoClient("http://gw.api.taobao.com/router/rest", aliSmsConfigDto.getAppKey(), aliSmsConfigDto.getAppSecret());
		AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
		// 公共回传参数，在“消息返回”中会透传回该参数；举例：用户可以传入自己下级的会员ID，在消息返回时，该会员ID会包含在内，用户可以根据该会员ID识别是哪位会员使用了你的应用
		req.setExtend(aliSmsParamDto.getExtend());
		// 短信类型，传入值请填写normal
		req.setSmsType("normal");
		// 短信签名，传入的短信签名必须是在阿里大鱼“管理中心-短信签名管理”中的可用签名。如“阿里大鱼”已在短信签名管理中通过审核，则可传入”阿里大鱼“（传参时去掉引号）作为短信签名。短信效果示例：【阿里大鱼】欢迎使用阿里大鱼服务。
		req.setSmsFreeSignName(aliSmsConfigDto.getSignName());
		// 短信模板变量，传参规则{"key":"value"}，key的名字须和申请模板中的变量名一致，多个变量之间以逗号隔开。示例：针对模板“验证码${code}，您正在进行${product}身份验证，打死不要告诉别人哦！”，传参时需传入{"code":"1234","product":"alidayu"}
		req.setSmsParamString(FastJSONUtils.toJsonString(aliSmsParamDto.getParam()));
		// 知信接收号码
		req.setRecNum(aliSmsParamDto.getMobile());
		// 短信模板ID，传入的模板必须是在阿里大鱼“管理中心-短信模板管理”中的可用模板。示例：SMS_585014
		req.setSmsTemplateCode(aliSmsParamDto.getTemplateCode());
		AlibabaAliqinFcSmsNumSendResponse rsp;
		rsp = client.execute(req);
		return rsp.getBody();
	}

	/**
	 * 配置项
	 * 
	 * @author nibili 2017年10月12日
	 * 
	 */
	public static class AliSmsConfigDto {
		/** ali大于 短信，key */
		private String appKey;
		/** ali大于 短信， */
		private String appSecret;
		/**
		 * // 短信签名，传入的短信签名必须是在阿里大鱼“管理中心-短信签名管理”中的可用签名。如“阿里大鱼”已在短信签名管理中通过审核，则可传入”
		 * 阿里大鱼“（传参时去掉引号）作为短信签名。短信效果示例：【阿里大鱼】欢迎使用阿里大鱼服务。
		 */
		private String signName;

		/**
		 * get
		 * 短信签名，传入的短信签名必须是在阿里大鱼“管理中心-短信签名管理”中的可用签名。如“阿里大鱼”已在短信签名管理中通过审核，则可传入”
		 * 阿里大鱼“（传参时去掉引号）作为短信签名。短信效果示例：【阿里大鱼】欢迎使用阿里大鱼服务。
		 * 
		 * @return
		 * @author nibili 2017年10月12日
		 */
		public String getSignName() {
			return signName;
		}

		/**
		 * set
		 * 短信签名，传入的短信签名必须是在阿里大鱼“管理中心-短信签名管理”中的可用签名。如“阿里大鱼”已在短信签名管理中通过审核，则可传入”
		 * 阿里大鱼“（传参时去掉引号）作为短信签名。短信效果示例：【阿里大鱼】欢迎使用阿里大鱼服务。
		 * 
		 * @param signName
		 * @author nibili 2017年10月12日
		 */
		public void setSignName(String signName) {
			this.signName = signName;
		}

		/**
		 * get ali大于短信，key
		 * 
		 * @return
		 * @author nibili 2017年10月12日
		 */
		public String getAppKey() {
			return appKey;
		}

		/**
		 * set ali大于短信，key
		 * 
		 * @param appKey
		 * @author nibili 2017年10月12日
		 */
		public void setAppKey(String appKey) {
			this.appKey = appKey;
		}

		/**
		 * get ali大于短信，
		 * 
		 * @return
		 * @author nibili 2017年10月12日
		 */
		public String getAppSecret() {
			return appSecret;
		}

		/**
		 * set ali大于短信，
		 * 
		 * @param appSecret
		 * @author nibili 2017年10月12日
		 */
		public void setAppSecret(String appSecret) {
			this.appSecret = appSecret;
		}
	}

	/**
	 * 发送短信参数
	 * 
	 * @author nibili 2017年10月12日
	 * 
	 */
	public static class AliSmsParamDto {

		/** 模板id */
		private String templateCode;
		/** 目标电话号码 */
		private String mobile;
		/**
		 * 短信模板中的参数<br/>
		 * 短信模板变量，传参规则{"key":"value"}，key的名字须和申请模板中的变量名一致，多个变量之间以逗号隔开。示例：针对模板“
		 * 验证码${code}，您正在进行${product}身份验证，打死不要告诉别人哦！”，传参时需传入{"code":"1234",
		 * "product":"alidayu"}
		 */
		private Object param;
		/**
		 * 公共回传参数，在“消息返回”中会透传回该参数；举例：用户可以传入自己下级的会员ID，在消息返回时，该会员ID会包含在内，
		 * 用户可以根据该会员ID识别是哪位会员使用了你的应用
		 */
		private String extend = "123456";

		/**
		 * get 模板id
		 * 
		 * @return
		 * @author nibili 2017年10月12日
		 */
		public String getTemplateCode() {
			return templateCode;
		}

		/**
		 * set 模板id
		 * 
		 * @param templateCode
		 * @author nibili 2017年10月12日
		 */
		public void setTemplateCode(String templateCode) {
			this.templateCode = templateCode;
		}

		/**
		 * get 目标电话号码
		 * 
		 * @return
		 * @author nibili 2017年10月12日
		 */
		public String getMobile() {
			return mobile;
		}

		/**
		 * set 目标电话号码
		 * 
		 * @param mobile
		 * @author nibili 2017年10月12日
		 */
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}

		/**
		 * 短信模板中的参数<br/>
		 * 短信模板变量，传参规则{"key":"value"}，key的名字须和申请模板中的变量名一致，多个变量之间以逗号隔开。示例：针对模板“
		 * 验证码${code}，您正在进行${product}身份验证，打死不要告诉别人哦！”，传参时需传入{"code":"1234",
		 * "product":"alidayu"}
		 * 
		 * @return
		 * @author nibili 2017年10月12日
		 */
		public Object getParam() {
			return param;
		}

		/**
		 * 短信模板中的参数<br/>
		 * 短信模板变量，传参规则{"key":"value"}，key的名字须和申请模板中的变量名一致，多个变量之间以逗号隔开。示例：针对模板“
		 * 验证码${code}，您正在进行${product}身份验证，打死不要告诉别人哦！”，传参时需传入{"code":"1234",
		 * "product":"alidayu"}
		 * 
		 * @param
		 * @author nibili 2017年10月12日
		 */
		public void setParam(Object param) {
			this.param = param;
		}

		/**
		 * get 公共回传参数，在“消息返回”中会透传回该参数；举例：用户可以传入自己下级的会员ID，在消息返回时，该会员ID会包含在内，
		 * 用户可以根据该会员ID识别是哪位会员使用了你的应用
		 * 
		 * @return
		 * @author nibili 2017年10月12日
		 */
		public String getExtend() {
			return extend;
		}

		/**
		 * set 公共回传参数，在“消息返回”中会透传回该参数；举例：用户可以传入自己下级的会员ID，在消息返回时，该会员ID会包含在内，
		 * 用户可以根据该会员ID识别是哪位会员使用了你的应用
		 * 
		 * @param extend
		 * @author nibili 2017年10月12日
		 */
		public void setExtend(String extend) {
			this.extend = extend;
		}

	}
}
