package cn.com.easy.exception;

/**
 * 自定义异常处理基类
 * 
 * @author nibili 2016年5月4日
 * 
 */
public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = -832356732171720583L;

	/** 错误码 */
	private String errorCode;

	public BusinessException() {
	}

	public BusinessException(String errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	public BusinessException(String message) {
		super(message);
	}

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public BusinessException(Throwable cause) {
		super(cause);
	}

	/**
	 * get 错误码
	 * 
	 * @return
	 * @author nibili 2017年4月24日
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * set 错误码
	 * 
	 * @param errorCode
	 * @author nibili 2017年4月24日
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}