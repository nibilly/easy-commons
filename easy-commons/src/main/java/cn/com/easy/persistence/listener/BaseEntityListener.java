package cn.com.easy.persistence.listener;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import cn.com.easy.persistence.BaseEntity;
import cn.com.easy.persistence.UuidBaseEntity;
import cn.com.easy.utils.DateUtils;

/**
 * The listener interface for receiving baseEntity events. The class that is
 * interested in processing a baseEntity event implements this interface, and
 * the object created with that class is registered with a component using the
 * component's <code>addBaseEntityListener<code> method. When the baseEntity
 * event occurs, that object's appropriate method is invoked.
 * 
 * @see BaseEntityEvent
 */
public class BaseEntityListener {

	// /** The logger. */
	// private static Logger logger =
	// LoggerFactory.getLogger(BaseEntityListener.class);

//	/** 用户id */
//	public static ThreadLocal<Long> userIdThreadLocal = new ThreadLocal<Long>();
//	/** 用户名称 */
//	public static ThreadLocal<String> userNameThreadLocal = new ThreadLocal<String>();

	/**
	 * Pre persist.
	 * 
	 * @param object the object
	 */
	@PrePersist
	public void prePersist(Object object) {

		if (object instanceof BaseEntity) {
			BaseEntity baseEntity = (BaseEntity) object;
			// 创建新对象
			Date date = DateUtils.getCurrentDateTime();
			if (baseEntity.getCreateTime() == null) {
				baseEntity.setCreateTime(date);
			}
			if (baseEntity.getLastModifyTime() == null) {
				baseEntity.setLastModifyTime(date);
			}
//			if (userIdThreadLocal.get() != null) {
//				baseEntity.setCreateById(userIdThreadLocal.get());
//			}
//			if (StringUtils.isNotBlank(userNameThreadLocal.get()) == true) {
//				baseEntity.setCreateBy(userNameThreadLocal.get());
//			}

		} else if (object instanceof UuidBaseEntity) {
			UuidBaseEntity baseEntity = (UuidBaseEntity) object;
			// 创建新对象
			Date date = DateUtils.getCurrentDateTime();
			if (baseEntity.getCreateTime() == null) {
				baseEntity.setCreateTime(date);
			}
			if (baseEntity.getLastModifyTime() == null) {
				baseEntity.setLastModifyTime(date);
			}
//			if (userIdThreadLocal.get() != null) {
//				baseEntity.setCreateById(userIdThreadLocal.get());
//			}
//			if (StringUtils.isNotBlank(userNameThreadLocal.get()) == true) {
//				baseEntity.setCreateBy(userNameThreadLocal.get());
//			}

		}
	}

	/**
	 * Pre update.
	 * 
	 * @param object the object
	 */
	@PreUpdate
	public void preUpdate(Object object) {

		if (object instanceof BaseEntity) {
			BaseEntity baseEntity = (BaseEntity) object;
			if (baseEntity.getLastModifyTime() != null && baseEntity.getLastModifyTime().getTime() > System.currentTimeMillis()) {
				// baseEntity.setLastModifyTime(DateUtils.getCurrentDateTime());
			} else {
				baseEntity.setLastModifyTime(DateUtils.getCurrentDateTime());
			}
//			// 修改旧对象
//			if (baseEntity.getLastModifyTime() == null || baseEntity.getLastModifyTime().getTime() >= baseEntity.getCreateTime().getTime()) {
//				baseEntity.setLastModifyTime(DateUtils.getCurrentDateTime());
//			}
//			if (userIdThreadLocal.get() != null) {
//				baseEntity.setLastModifyById(userIdThreadLocal.get());
//			}
//			if (StringUtils.isNotBlank(userNameThreadLocal.get()) == true) {
//				baseEntity.setLastModifyBy(userNameThreadLocal.get());
//			}
			// baseEntity.setLastModifyBy(loginName);
			// logger.info("{}对象(ID:{}) 被 {} 在 {} 修改", new Object[] {
			// object.getClass().getName(), baseEntity.getId(), null, new Date()
			// });
		} else if (object instanceof UuidBaseEntity) {
			UuidBaseEntity baseEntity = (UuidBaseEntity) object;
			// 修改旧对象
			if (baseEntity.getLastModifyTime() != null && baseEntity.getLastModifyTime().getTime() > System.currentTimeMillis()) {
				// baseEntity.setLastModifyTime(DateUtils.getCurrentDateTime());
			} else {
				baseEntity.setLastModifyTime(DateUtils.getCurrentDateTime());
			}
//			if (userIdThreadLocal.get() != null) {
//				baseEntity.setLastModifyById(userIdThreadLocal.get());
//			}
//			if (StringUtils.isNotBlank(userNameThreadLocal.get()) == true) {
//				baseEntity.setLastModifyBy(userNameThreadLocal.get());
//			}
			// baseEntity.setLastModifyBy(loginName);
			// logger.info("{}对象(ID:{}) 被 {} 在 {} 修改", new Object[] {
			// object.getClass().getName(), baseEntity.getId(), null, new Date()
			// });
		}
	}
}
