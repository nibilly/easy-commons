/**
 * 
 */
package cn.com.easy.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.Version;

import cn.com.easy.persistence.listener.BaseEntityListener;
import lombok.Data;

/**
 * @author nibili 2020年4月15日上午7:00:58
 *
 */
@Data
@MappedSuperclass
@EntityListeners(value = { BaseEntityListener.class })
@DynamicInsert
@DynamicUpdate
public abstract class BaseMyBatisPlusEntity implements Serializable {

	/** */
	private static final long serialVersionUID = 1232010218613308428L;
	/** 实体主键. */
	@TableId(value = "id", type = IdType.AUTO)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@TableField(value = "deleteStatus")
	private Boolean deleteStatus = false;
	/** 实体版本 */
	@Version
	@javax.persistence.Version
	private Long version = 0l;
	/** 创建时间. */
	@TableField(value = "createTime")
	private Date createTime;
	/** 创建人. */
	@TableField(value = "createBy")
	private String createBy;
	/** 创建人. */
	@TableField(value = "createById")
	private Long createById;
	/** 最后修改时间. */
	@TableField(value = "lastModifyTime")
	private Date lastModifyTime;
	/** 最后修改人. */
	@TableField(value = "lastModifyBy")
	private String lastModifyBy;
	/** 最后修改人. */
	@TableField(value = "lastModifyById")
	private Long lastModifyById;
}
