package cn.com.easy.persistence;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Random;

import org.apache.commons.lang3.RandomUtils;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.UUIDGenerator;

public class CustomUUIDGenerator extends UUIDGenerator {

	@Override
	public Serializable generate(SessionImplementor session, Object object) throws HibernateException {
		Object id = getFieldValueByName("id", object);
		if (id != null) {
			return (Serializable) id;
		}
		return createUUID(6);
	}

	private Object getFieldValueByName(String fieldName, Object o) {
		try {
			String firstLetter = fieldName.substring(0, 1).toUpperCase();
			String getter = "get" + firstLetter + fieldName.substring(1);
			Method method = o.getClass().getMethod(getter, new Class[] {});
			Object value = method.invoke(o, new Object[] {});
			return value;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param pwd_len
	 * @return
	 */
	private String createUUID(int pwd_len) {
		int i; // 生成的随机数
		int count = 0; // 生成的密码的长度
		char[] str = { //
				'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', //
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', //
				'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		int maxNum = str.length;
		StringBuffer pwd = new StringBuffer("");
		Random r = new Random(new Date().getTime());
		while (count < pwd_len) {
			// 生成随机数，取绝对值，防止生成负数，
			i = Math.abs(r.nextInt(RandomUtils.nextInt(0, maxNum))); //
			if (i >= 0 && i < str.length) {
				pwd.append(str[i]);
				count++;
			}
		}
		return pwd.toString();
	}

}
