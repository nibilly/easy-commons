package cn.com.easy.dto;

import cn.com.easy.utils.Page;

public class LayuiMessageDto {
	/** 状态信息 */
	private String msg;
	/** 0代表成功 */
	private int code = 0;
	/** 总记录数 */
	private long count = 0;
	/** 数据 */
	private Object data;

	/**
	 * 成功
	 * 
	 * @param page
	 * @return
	 * @auth nibili 2019年6月19日 下午4:25:59
	 */
	public static <T> LayuiMessageDto success(Page<T> page) {
		LayuiMessageDto layuiMessageDto = new LayuiMessageDto();
		// 0代表成功
		layuiMessageDto.setCode(0);
		layuiMessageDto.setCount(page.getTotalItems());
		layuiMessageDto.setData(page.getResult());
		return layuiMessageDto;
	}

	/**
	 * 失败
	 * 
	 * @param error
	 * @return
	 * @auth nibili 2019年6月19日 下午4:28:10
	 */
	public static LayuiMessageDto error(String error) {
		LayuiMessageDto layuiMessageDto = new LayuiMessageDto();
		// 0代表成功
		layuiMessageDto.setCode(-1);
		layuiMessageDto.setMsg(error);
		return layuiMessageDto;
	}

	/**
	 * 获取 状态信息
	 * 
	 * @return
	 * @auth nibili 2019年6月19日 下午4:18:32
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * 设置 状态信息
	 * 
	 * @param msg
	 * @auth nibili 2019年6月19日 下午4:18:32
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * 获取 200代表成功
	 * 
	 * @return
	 * @auth nibili 2019年6月19日 下午4:18:32
	 */
	public int getCode() {
		return code;
	}

	/**
	 * 设置 200代表成功
	 * 
	 * @param code
	 * @auth nibili 2019年6月19日 下午4:18:32
	 */
	public void setCode(int code) {
		this.code = code;
	}

	/**
	 * 获取 总记录数
	 * 
	 * @return
	 * @auth nibili 2019年6月19日 下午4:18:32
	 */
	public long getCount() {
		return count;
	}

	/**
	 * 设置 总记录数
	 * 
	 * @param count
	 * @auth nibili 2019年6月19日 下午4:18:32
	 */
	public void setCount(long count) {
		this.count = count;
	}

	/**
	 * 获取 数据
	 * 
	 * @return
	 * @auth nibili 2019年6月19日 下午4:18:32
	 */
	public Object getData() {
		return data;
	}

	/**
	 * 设置 数据
	 * 
	 * @param data
	 * @auth nibili 2019年6月19日 下午4:18:32
	 */
	public void setData(Object data) {
		this.data = data;
	}

}
