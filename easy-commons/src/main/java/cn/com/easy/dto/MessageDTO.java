package cn.com.easy.dto;

import java.io.Serializable;

/**
 * 通用消息实体
 * 
 * @author nibili 2015年1月11日下午1:38:48
 * 
 */
public class MessageDTO implements Serializable {

	/** */
	private static final long serialVersionUID = -5118324033207168492L;
	/** 消息标题 */
	private String title;
	/** 是否执行成功 */
	private boolean isSuccess;
	/** 消息内容 */
	private Object message;
	/** 如果有错误，这里可以设置错误码 */
	private String errorCode;

	/**
	 * 成功
	 * 
	 * @param object
	 * @return
	 * @auth nibili 2020年1月7日 下午9:32:49
	 */
	public static MessageDTO success(Object object) {
		return new MessageDTO("", true, object, "");
	}

	/**
	 * 错误
	 * 
	 * @param object
	 * @return
	 * @auth nibili 2020年1月7日 下午9:33:43
	 */
	public static MessageDTO fail(Object object) {
		return new MessageDTO("", false, object, "");
	}

	/**
	 * 
	 * @param isSuccess
	 *            是否执行成功
	 * @param errorCode
	 *            错误码
	 * @return
	 * @author nibili 2017年3月28日
	 */
	public static MessageDTO newInstance(String errorCode) {
		return new MessageDTO("", false, "", errorCode);
	}

	/**
	 * 
	 * @param errorCode
	 *            错误码
	 * @param message
	 *            消息
	 * @return
	 * @author nibili 2017年4月24日
	 */
	public static MessageDTO newInstance(Object message, String errorCode) {
		return new MessageDTO("", false, message, errorCode);
	}

	/**
	 * 
	 * @param isSuccess
	 *            是否执行成功
	 * @param errorCode
	 *            错误码
	 * @param message
	 *            消息
	 * @return
	 * @author nibili 2017年4月24日
	 */
	public static MessageDTO newInstance(boolean isSuccess, Object message, String errorCode) {
		return new MessageDTO("", isSuccess, message, errorCode);
	}

	/**
	 * 
	 * @param title
	 *            消息标题
	 * @param isSuccess
	 *            是否执行成功
	 * @param message
	 *            消息内容
	 * @return
	 * @auth nibili 2015年1月11日 下午1:38:56
	 */
	public static MessageDTO newInstance(String title, boolean isSuccess, Object message) {
		return new MessageDTO(title, isSuccess, message, "");
	}

	/**
	 * 
	 * @param title
	 *            消息标题
	 * @param isSuccess
	 *            是否执行成功
	 * @param message
	 *            消息内容
	 * @param errorCode
	 *            错误码
	 * @return
	 * @author nibili 2017年4月24日
	 */
	public static MessageDTO newInstance(String title, boolean isSuccess, Object message, String errorCode) {
		return new MessageDTO(title, isSuccess, message, "");
	}

	public MessageDTO() {

	}

	/**
	 * 
	 * @param title
	 * @param isSuccess
	 * @param message
	 */
	public MessageDTO(String title, boolean isSuccess, Object message, String errorCode) {

		this.title = title;
		this.isSuccess = isSuccess;
		this.message = message;
		this.errorCode = errorCode;
	}

	/**
	 * 获取 消息标题
	 * 
	 * @return
	 * @auth nibili 2015年1月11日 下午1:38:20
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 设置 消息标题
	 * 
	 * @param title
	 * @auth nibili 2015年1月11日 下午1:38:20
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 获取 是否执行成功
	 * 
	 * @return
	 * @auth nibili 2015年1月11日 下午1:38:20
	 */
	public boolean getIsSuccess() {
		return isSuccess;
	}

	/**
	 * 设置 是否执行成功
	 * 
	 * @param isSuccess
	 * @auth nibili 2015年1月11日 下午1:38:20
	 */
	public void setIsSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	/**
	 * 获取 消息内容
	 * 
	 * @return
	 * @auth nibili 2015年1月11日 下午1:38:20
	 */
	public Object getMessage() {
		return message;
	}

	/**
	 * 设置 消息内容
	 * 
	 * @param message
	 * @auth nibili 2015年1月11日 下午1:38:20
	 */
	public void setMessage(Object message) {
		this.message = message;
	}

	/**
	 * get errorCode
	 * 
	 * @return
	 * @author nibili 2017年3月28日
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * set errorCode
	 * 
	 * @param errorCode
	 * @author nibili 2017年3月28日
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
