package cn.com.easy.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.google.common.collect.Lists;

/**
 * client page request
 * 
 * @author nibili 2015-2-12
 * 
 */
public class Layui2TablePageRequestParamDTO implements Serializable {

	/** */
	private static final long serialVersionUID = 7388016065694115867L;
	/** 搜索文本 */
	private String searchText;
	/** 页码 */
	private int page = 1;
	/** 每页数量 */
	private Integer limit;
	/** 排序字段 */
	private String sortName;
	/** 排序类型:desc，asc */
	private String sortOrder;
	/** 排序集合 */
	private Map<String, String> sorts;

	/** 默认每页数量 */
	private int defaultPageSize = 10;

	public Layui2TablePageRequestParamDTO() {

	}

	//
	// public PageRequestParamDTO(DatatableRequestParamDTO
	// datatableRequestParamDTO) {
	// if (datatableRequestParamDTO != null) {
	//
	// limit = datatableRequestParamDTO.getLength();
	// page = (int) Math.floor((double)
	// (datatableRequestParamDTO.getStart()) / (double) limit);
	// }
	//
	// }

	/**
	 * get sortName
	 * 
	 * @return
	 * @auth nibili 2015-3-22
	 */
	public String getSortName() {
		return sortName;
	}

	/**
	 * 获取 页码
	 * 
	 * @return
	 * @auth nibili 2019年6月13日 上午12:04:15
	 */
	public int getPage() {
		return page;
	}

	/**
	 * 设置 页码
	 * 
	 * @param page
	 * @auth nibili 2019年6月13日 上午12:04:15
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * set sortName
	 * 
	 * @param sortName
	 * @auth nibili 2015-3-22
	 */
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	/**
	 * get sortOrder
	 * 
	 * @return
	 * @auth nibili 2015-3-22
	 */
	public String getSortOrder() {
		return sortOrder;
	}

	/**
	 * set sortOrder
	 * 
	 * @param sortOrder
	 * @auth nibili 2015-3-22
	 */
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * 转成easy-commons中的pageRequest
	 * 
	 * @return
	 * @author nibili 2015年12月18日
	 */
	public cn.com.easy.utils.PageRequest buildCommonsPageRequest() {
		cn.com.easy.utils.PageRequest pageRequest = new cn.com.easy.utils.PageRequest(this.page, this.getLimit());
		if (StringUtils.isNotBlank(this.sortName) == true && StringUtils.isNotBlank(this.sortOrder) == true) {
			pageRequest.setOrderBy(this.sortName);
			pageRequest.setOrderDir(this.sortOrder);
		}
		if (MapUtils.isNotEmpty(sorts) == true) {
			List<cn.com.easy.utils.PageRequest.Order> orders = Lists.newArrayList();
			for (Entry<String, String> entry : sorts.entrySet()) {
				if (entry.getValue().toLowerCase().equals("desc") == true) {
					orders.add(new cn.com.easy.utils.PageRequest.Order(cn.com.easy.utils.PageRequest.Direction.DESC, entry.getKey()));
				} else if (entry.getValue().toLowerCase().equals("asc") == true) {
					orders.add(new cn.com.easy.utils.PageRequest.Order(cn.com.easy.utils.PageRequest.Direction.ASC, entry.getKey()));
				}
			}
			pageRequest.setSort(new cn.com.easy.utils.PageRequest.Sort(orders));
		}
		return pageRequest;
	}

	/**
	 * 生成分页请求相关配置
	 * 
	 * @param pageParam
	 * @return
	 */
	public PageRequest buildSpringDataPageRequest() {
		int pageNo = 0;
		int pageSize = 10;
		if (getPage() > 0) {
			pageNo = getPage() - 1;
		}
		if (getLimit() > 0) {
			pageSize = getLimit();
		}
		// orders array
		List<Order> orders = Lists.newArrayList();
		// bootstrap-table控件，传递的排序参数
		if (StringUtils.isNotBlank(this.sortName) == true && StringUtils.isNotBlank(this.sortOrder) == true) {
			if (this.sortOrder.equals("asc") == true) {
				orders.add(new Order(Direction.ASC, this.sortName));
			} else if (this.sortOrder.equals("desc") == true) {
				orders.add(new Order(Direction.DESC, this.sortName));
			}
		}
		// /
		if (MapUtils.isNotEmpty(sorts) == true) {
			for (Entry<String, String> entry : sorts.entrySet()) {
				String direction = entry.getValue();
				if (direction == "asc") {
					orders.add(new Order(Direction.ASC, entry.getKey()));
				} else if (direction == "desc") {
					orders.add(new Order(Direction.DESC, entry.getKey()));
				}
			}
		}
		if (CollectionUtils.isEmpty(orders) == true) {
			orders = null;
			return new PageRequest(pageNo, pageSize);
		} else {
			return new PageRequest(pageNo, pageSize, new Sort(orders));
		}
	}

	/**
	 * 生成分页请求相关配置
	 * 
	 * @param pageParam
	 * @return
	 */
	public PageBounds buildMyBatisPageRequest() {
		int pageNo = 1;
		int pageSize = 10;
		if (getPage() > 1) {
			pageNo = getPage();
		}
		if (getLimit() > 0) {
			pageSize = getLimit();
		}
		// orders array
		List<com.github.miemiedev.mybatis.paginator.domain.Order> orders = Lists.newArrayList();
		if (MapUtils.isNotEmpty(sorts) == true) {
			for (Entry<String, String> entry : sorts.entrySet()) {
				String direction = entry.getValue();
				if (direction == "asc") {
					orders.add(new com.github.miemiedev.mybatis.paginator.domain.Order(entry.getKey(), com.github.miemiedev.mybatis.paginator.domain.Order.Direction.ASC, null));
				} else if (direction == "desc") {
					orders.add(new com.github.miemiedev.mybatis.paginator.domain.Order(entry.getKey(), com.github.miemiedev.mybatis.paginator.domain.Order.Direction.DESC, null));
				}
			}
		}
		// bootstrap-table控件，传递的排序参数
		if (StringUtils.isNotBlank(this.sortName) == true && StringUtils.isNotBlank(this.sortOrder) == true) {
			if (this.sortOrder.equals("asc") == true) {
				orders.add(new com.github.miemiedev.mybatis.paginator.domain.Order(this.sortName, com.github.miemiedev.mybatis.paginator.domain.Order.Direction.ASC, null));
			} else if (this.sortOrder.equals("desc") == true) {
				orders.add(new com.github.miemiedev.mybatis.paginator.domain.Order(this.sortName, com.github.miemiedev.mybatis.paginator.domain.Order.Direction.DESC, null));
			}
		}
		if (CollectionUtils.isEmpty(orders) == true) {
			orders = null;
			return new PageBounds(pageNo, pageSize);
		} else {
			return new PageBounds(pageNo, pageSize, orders);
		}

	}

	/**
	 * get searchText
	 * 
	 * @return
	 * @auth nibili 2015-3-25
	 */
	public String getSearchText() {
		return searchText;
	}

	/**
	 * set searchText
	 * 
	 * @param searchText
	 * @auth nibili 2015-3-25
	 */
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	/**
	 * get 排序集合
	 * 
	 * @return
	 * @author nibili 2015年12月31日
	 */
	public Map<String, String> getSorts() {
		return sorts;
	}

	/**
	 * set 排序集合
	 * 
	 * @param sorts
	 * @author nibili 2015年12月31日
	 */
	public void setSorts(Map<String, String> sorts) {
		this.sorts = sorts;
	}

	/**
	 * 获取 默认每页数量
	 * 
	 * @return
	 * @auth nibili 2019年6月6日 上午10:09:30
	 */
	public int getDefaultPageSize() {
		return defaultPageSize;
	}

	/**
	 * 设置 默认每页数量
	 * 
	 * @param defaultPageSize
	 * @auth nibili 2019年6月6日 上午10:09:30
	 */
	public void setDefaultPageSize(int defaultPageSize) {
		this.defaultPageSize = defaultPageSize;
	}

	/**
	 * 获取 每页数量
	 * 
	 * @return
	 * @auth nibili 2019年6月6日 上午10:10:02
	 */
	public Integer getLimit() {
		if (this.limit != null) {
			return limit;
		} else {
			return this.defaultPageSize;
		}
	}

	/**
	 * 设置 每页数量
	 * 
	 * @param limit
	 * @auth nibili 2019年6月6日 上午10:10:02
	 */
	public void setLimit(Integer limit) {
		this.limit = limit;
	}

}
