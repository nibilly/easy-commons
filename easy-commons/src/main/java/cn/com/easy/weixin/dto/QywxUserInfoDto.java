package cn.com.easy.weixin.dto;

import java.util.Map;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 企业微信用户dto
 * 
 * @author nibili 2017年6月23日
 * 
 */
public class QywxUserInfoDto {
	/** 如果非企业用户这边是null */
	@JSONField(name = "UserId")
	private String userid;
	@JSONField(name = "OpenId")
	private String openid;
	@JSONField(name = "DeviceId")
	private String deviceid;
	@JSONField(name = "name")
	private String name;
	@JSONField(name = "department")
	private Integer[] departments;
	@JSONField(name = "position")
	private String position;
	@JSONField(name = "mobile")
	private String mobile;
	@JSONField(name = "gender")
	private String gender;
	@JSONField(name = "email")
	private String email;
	@JSONField(name = "weixinid")
	private String weixinid;
	@JSONField(name = "avatar")
	private String avatar;
	@JSONField(name = "status")
	private Integer status;
	@JSONField(name = "extattr")
	private Map<String, Object> extattr;

	/**
	 * get 如果非企业用户这边是null
	 * 
	 * @return
	 * @author nibili 2017年6月23日
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * set 如果非企业用户这边是null
	 * 
	 * @param userid
	 * @author nibili 2017年6月23日
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}

	/**
	 * get openid
	 * 
	 * @return
	 * @author nibili 2017年6月23日
	 */
	public String getOpenid() {
		return openid;
	}

	/**
	 * set openid
	 * 
	 * @param openid
	 * @author nibili 2017年6月23日
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}

	/**
	 * get deviceid
	 * 
	 * @return
	 * @author nibili 2017年6月23日
	 */
	public String getDeviceid() {
		return deviceid;
	}

	/**
	 * set deviceid
	 * 
	 * @param deviceid
	 * @author nibili 2017年6月23日
	 */
	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	/**
	 * get name
	 * 
	 * @return
	 * @author nibili 2017年6月23日
	 */
	public String getName() {
		return name;
	}

	/**
	 * set name
	 * 
	 * @param name
	 * @author nibili 2017年6月23日
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * get departments
	 * 
	 * @return
	 * @author nibili 2017年6月23日
	 */
	public Integer[] getDepartments() {
		return departments;
	}

	/**
	 * set departments
	 * 
	 * @param departments
	 * @author nibili 2017年6月23日
	 */
	public void setDepartments(Integer[] departments) {
		this.departments = departments;
	}

	/**
	 * get position
	 * 
	 * @return
	 * @author nibili 2017年6月23日
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * set position
	 * 
	 * @param position
	 * @author nibili 2017年6月23日
	 */
	public void setPosition(String position) {
		this.position = position;
	}

	/**
	 * get mobile
	 * 
	 * @return
	 * @author nibili 2017年6月23日
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * set mobile
	 * 
	 * @param mobile
	 * @author nibili 2017年6月23日
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * get gender
	 * 
	 * @return
	 * @author nibili 2017年6月23日
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * set gender
	 * 
	 * @param gender
	 * @author nibili 2017年6月23日
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * get email
	 * 
	 * @return
	 * @author nibili 2017年6月23日
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * set email
	 * 
	 * @param email
	 * @author nibili 2017年6月23日
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * get weixinid
	 * 
	 * @return
	 * @author nibili 2017年6月23日
	 */
	public String getWeixinid() {
		return weixinid;
	}

	/**
	 * set weixinid
	 * 
	 * @param weixinid
	 * @author nibili 2017年6月23日
	 */
	public void setWeixinid(String weixinid) {
		this.weixinid = weixinid;
	}

	/**
	 * get avatar
	 * 
	 * @return
	 * @author nibili 2017年6月23日
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * set avatar
	 * 
	 * @param avatar
	 * @author nibili 2017年6月23日
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	/**
	 * get status
	 * 
	 * @return
	 * @author nibili 2017年6月23日
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * set status
	 * 
	 * @param status
	 * @author nibili 2017年6月23日
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * get extattr
	 * 
	 * @return
	 * @author nibili 2017年6月23日
	 */
	public Map<String, Object> getExtattr() {
		return extattr;
	}

	/**
	 * set extattr
	 * 
	 * @param extattr
	 * @author nibili 2017年6月23日
	 */
	public void setExtattr(Map<String, Object> extattr) {
		this.extattr = extattr;
	}

}
