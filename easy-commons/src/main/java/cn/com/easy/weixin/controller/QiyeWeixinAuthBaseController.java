package cn.com.easy.weixin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.com.easy.weixin.dto.QywxUserInfoDto;

import com.github.sd4324530.fastweixin.api.enums.OauthScope;
import com.github.sd4324530.fastweixin.company.api.QYOauthAPI;
import com.github.sd4324530.fastweixin.company.api.QYUserAPI;
import com.github.sd4324530.fastweixin.company.api.config.QYAPIConfig;
import com.github.sd4324530.fastweixin.company.api.response.GetOauthUserInfoResponse;
import com.github.sd4324530.fastweixin.company.api.response.GetQYUserInfoResponse;

/**
 * 
 * 企业微信授权基础控制器 <br/>
 * 详参：https://mp.weixin.qq.com/wiki/17/c0f37d5704f0b64713d5d2c37b468d75.html
 * 
 * @author nibili 2017年5月11日
 * 
 */
public abstract class QiyeWeixinAuthBaseController {

	/**
	 * 微信配置
	 * 
	 * @param request
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public abstract QYAPIConfig getApiConfig(HttpServletRequest request);

	/**
	 * 获取scop就一个枚举
	 * 
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public abstract OauthScope getScop();

	/**
	 * 获取授权回调地址
	 * 
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public abstract String getRedirectUrl(HttpServletRequest request);

	/**
	 * 可为空<br/>
	 * state 否 重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
	 * 
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public abstract String getState(HttpServletRequest request);

	/**
	 * 发起授权跳转
	 * 
	 * @param request
	 * @param response
	 * @author nibili 2017年5月11日
	 */
	@RequestMapping("/weixin/oauth")
	public String requestAuth(HttpServletRequest request, HttpServletResponse response) {
		QYOauthAPI oauthAPI = new QYOauthAPI(getApiConfig(request));
		String oauthUrl = oauthAPI.getOauthPageUrl(getRedirectUrl(request), getScop(), getState(request));
		return "redirect:" + oauthUrl;
	}

	/**
	 * 获取用户信息
	 * 
	 * @param getOauthUserInfoResponse
	 * @param state
	 * @param request
	 * @param response
	 * @return springMvc跳转地址
	 * @author nibili 2017年6月22日
	 */
	public abstract String requestUserInfoSuccess(QywxUserInfoDto qywxUserInfoDto, String state, HttpServletRequest request, HttpServletResponse response);

	/**
	 * 微信授权跳转回调获取用户信息
	 * 
	 * @param request
	 * @param response
	 * @param code
	 * @param state
	 * @return
	 * @author nibili 2017年5月11日
	 */
	@RequestMapping("/weixin/oauth/user_info")
	public String oauthRedirect(HttpServletRequest request, HttpServletResponse response, String code, String state) {
		QYUserAPI qYUserAPI = new QYUserAPI(getApiConfig(request));
		GetOauthUserInfoResponse getOauthUserInfoResponse = qYUserAPI.getOauthUserInfo(code);
		QywxUserInfoDto qywxUserInfoDto = new QywxUserInfoDto();
		BeanUtils.copyProperties(getOauthUserInfoResponse, qywxUserInfoDto);
		if (StringUtils.isNotBlank(getOauthUserInfoResponse.getUserid()) == true) {
			GetQYUserInfoResponse getQYUserInfoResponse = qYUserAPI.get(getOauthUserInfoResponse.getUserid());
			BeanUtils.copyProperties(getQYUserInfoResponse, qywxUserInfoDto);
		}
		return requestUserInfoSuccess(qywxUserInfoDto, state, request, response);
	}
}
