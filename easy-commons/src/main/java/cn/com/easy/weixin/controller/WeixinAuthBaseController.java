package cn.com.easy.weixin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;

import com.github.sd4324530.fastweixin.api.OauthAPI;
import com.github.sd4324530.fastweixin.api.config.ApiConfig;
import com.github.sd4324530.fastweixin.api.enums.OauthScope;

/**
 * 
 * 微信授权基础控制器 <br/>
 * 详参：https://mp.weixin.qq.com/wiki/17/c0f37d5704f0b64713d5d2c37b468d75.html
 * 
 * @author nibili 2017年5月11日
 * 
 */
public abstract class WeixinAuthBaseController {

	/**
	 * 微信配置
	 * 
	 * @param request
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public abstract ApiConfig getApiConfig(HttpServletRequest request);

	/**
	 * 获取scop就一个枚举
	 * 
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public abstract OauthScope getScop();

	/**
	 * 获取授权回调地址
	 * 
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public abstract String getRedirectUrl(HttpServletRequest request);

	/**
	 * 可为空<br/>
	 * state 否 重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
	 * 
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public abstract String getState(HttpServletRequest request);

	/**
	 * 发起授权跳转
	 * 
	 * @param request
	 * @param response
	 * @author nibili 2017年5月11日
	 */
	@RequestMapping("/weixin/oauth")
	public String requestAuth(HttpServletRequest request, HttpServletResponse response) {
		OauthAPI oauthAPI = new OauthAPI(getApiConfig(request));
		String oauthUrl = oauthAPI.getOauthPageUrl(getRedirectUrl(request), getScop(), getState(request));
		return "redirect:" + oauthUrl;
	}
}
