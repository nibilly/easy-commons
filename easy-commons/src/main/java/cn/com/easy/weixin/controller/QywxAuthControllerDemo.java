/**
 * 
 */
package cn.com.easy.weixin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.com.easy.weixin.dto.QywxUserInfoDto;
import cn.com.easy.weixin.filter.QywxAuthFilterDemo;

import com.github.sd4324530.fastweixin.api.enums.OauthScope;
import com.github.sd4324530.fastweixin.company.api.config.QYAPIConfig;

/**
 * 企业微信授权控制器
 * 
 * @author nibili 2017年6月22日下午3:32:43
 * 
 */
// @Controller
// @RequestMapping("/kangwu")
public class QywxAuthControllerDemo extends QiyeWeixinAuthBaseController {

	// /** 企业微信服务类 */
	// @Autowired
	// private QywxService qywxService;

	@Override
	public QYAPIConfig getApiConfig(HttpServletRequest request) {
		// QYAPIConfig qYAPIConfig = new QYAPIConfig(qywxService.getCorpid(),
		// qywxService.getSecret_kangwu());
		// return qYAPIConfig;
		return null;
	}

	@Override
	public OauthScope getScop() {
		// TODO Auto-generated method stub
		return OauthScope.SNSAPI_BASE;
	}

	@Override
	public String getRedirectUrl(HttpServletRequest request) {
		//
		String httpBase = request.getScheme() + "://" + request.getServerName() + request.getContextPath();
		// 授权后，去验证用户信息，保证用户登录
		return httpBase + "/kangwu/weixin/oauth/user_info";
	}

	@Override
	public String getState(HttpServletRequest request) {
		return "";
	}

	/**
	 * 获取用户信息保存到数据库后，跳转
	 */
	@Override
	public String requestUserInfoSuccess(QywxUserInfoDto qywxUserInfoDto, String state, HttpServletRequest request, HttpServletResponse response) {
		HttpSession httpSession = request.getSession();
		String backUrl = (String) httpSession.getAttribute(QywxAuthFilterDemo.QYWX_AUTH_BACK_URL_KEY);
		// 设置为已授权
		httpSession.setAttribute(QywxAuthFilterDemo.QYWX_AUTH_USER, qywxUserInfoDto);
		//
		return "redirect:" + backUrl;
	}

}
