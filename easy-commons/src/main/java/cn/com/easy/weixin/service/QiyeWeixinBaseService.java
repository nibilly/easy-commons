package cn.com.easy.weixin.service;

import com.github.sd4324530.fastweixin.company.api.QYUserAPI;
import com.github.sd4324530.fastweixin.company.api.config.QYAPIConfig;
import com.github.sd4324530.fastweixin.company.api.response.GetOauthUserInfoResponse;

/**
 * 企业微信服务类
 * 
 * @author nibili 2017年6月22日
 * 
 */
public abstract class QiyeWeixinBaseService {

	/**
	 * 微信配置
	 * 
	 * @param request
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public abstract QYAPIConfig getApiConfig();

	/**
	 * 获取用户信息
	 * 
	 * @param request
	 * @param response
	 * @param code
	 * @param state
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public GetOauthUserInfoResponse getUserInfo(String code) {
		QYUserAPI qYUserAPI = new QYUserAPI(getApiConfig());
		GetOauthUserInfoResponse getOauthUserInfoResponse = qYUserAPI.getOauthUserInfo(code);
		return getOauthUserInfoResponse;
	}
}
