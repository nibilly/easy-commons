package cn.com.easy.weixin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;

import com.github.sd4324530.fastweixin.api.OauthAPI;
import com.github.sd4324530.fastweixin.api.config.ApiConfig;
import com.github.sd4324530.fastweixin.api.response.GetUserInfoResponse;
import com.github.sd4324530.fastweixin.api.response.OauthGetTokenResponse;

/**
 * 微信授权跳转基础控制，在这里接收code
 * 
 * @author nibili 2017年5月11日
 * 
 */
public abstract class WeixinAuthRedirectBaseController {

	/**
	 * 微信配置
	 * 
	 * @param request
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public abstract ApiConfig getApiConfig(HttpServletRequest request);

	/**
	 * 是否获取用户信息<br/>
	 * 如果为true,会调用 requestUserInfoSuccess方法
	 * 
	 * @return
	 * @author nibili 2017年5月11日
	 */
	public abstract boolean isGetUserInfo();

	/**
	 * 获取用户信息
	 * 
	 * @param getUserInfoResponse
	 * @param state
	 * @param request
	 * @author nibili 2017年5月11日
	 */
	public abstract String requestUserInfoSuccess(GetUserInfoResponse getUserInfoResponse, String state, HttpServletRequest request);

	/**
	 * 成功做跳转的地址
	 * 
	 * @return
	 * @author nibili 2017年10月9日
	 */
	public abstract String successRedirectUrl(HttpServletRequest request);

	/**
	 * 微信授权跳转回调接收
	 * 
	 * @param request
	 * @param response
	 * @param code
	 * @param state
	 * @return
	 * @author nibili 2017年5月11日
	 */
	@RequestMapping("/weixin/oauth/redirect")
	public String oauthRedirect(HttpServletRequest request, HttpServletResponse response, String code, String state) {
		OauthAPI oauthAPI = new OauthAPI(getApiConfig(request));
		OauthGetTokenResponse tokenResponse = oauthAPI.getToken(code);
		// 获取token成功
		if (isGetUserInfo() == true) {
			GetUserInfoResponse getUserInfoResponse = oauthAPI.getUserInfo(tokenResponse.getAccessToken(), tokenResponse.getOpenid());
			// 获取用户信息成功
			return requestUserInfoSuccess(getUserInfoResponse, state, request);
		} else {
			return successRedirectUrl(request);
		}
	}
}
