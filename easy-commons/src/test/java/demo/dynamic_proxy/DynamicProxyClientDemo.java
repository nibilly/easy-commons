package demo.dynamic_proxy;

public class DynamicProxyClientDemo {
	public static void main(String[] args) {
		RealSubject realSubject = new RealSubject();
		Subject proxySubject = (Subject) DynamicProxyHandler.newProxyInstance(realSubject);
		proxySubject.doSomething();
	}
}
