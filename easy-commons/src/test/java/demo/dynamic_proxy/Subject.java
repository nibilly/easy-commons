package demo.dynamic_proxy;

public interface Subject {
	public void doSomething();
}
