package demo.dynamic_proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class DynamicProxyHandler implements InvocationHandler {
	private Object delegate;

	public DynamicProxyHandler(Object delegate) {
		this.delegate = delegate;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		before();
		Object result = method.invoke(delegate, args);
		after();
		return result;
	}

	private void before() {
		System.out.println("Before do something");
	}

	private void after() {
		System.out.println("After do something");
	}

	public static Object newProxyInstance(Object delegate) {
		return Proxy.newProxyInstance(delegate.getClass().getClassLoader(), delegate.getClass().getInterfaces(),
				new DynamicProxyHandler(delegate));
	}
}