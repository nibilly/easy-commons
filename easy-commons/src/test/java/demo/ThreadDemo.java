package demo;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Test;

/**
 * 线程demo
 */
public class ThreadDemo {

	/**
	 * 线程池
	 * 
	 * @throws TimeoutException
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	@Test
	public void threadPool() throws InterruptedException, ExecutionException, TimeoutException {
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		//第一
		executorService.execute(() -> {
			System.out.println("hello thread one!");
		});
		//第二
		FutureTask<String> futureTask = new FutureTask<String>(() -> {
			Thread.sleep(1000);
			return "Hello thread two!!";
		});
		executorService.execute(futureTask);
		String temp = futureTask.get(2, TimeUnit.SECONDS);
		System.out.println("第二线程：" + temp);
		//第三
		Future<String> future = executorService.submit(() -> {
			return "hello thread three!";
		});
		System.out.println("第三线程：" + future.get());

	}

	/**
	 * 可获取返回数据
	 * 
	 * @throws InterruptedException
	 * @throws ExecutionException
	 * @throws TimeoutException
	 */
	@Test
	public void callable() throws InterruptedException, ExecutionException, TimeoutException {
//		FutureTask<String> futureTask = new FutureTask<String>(new Callable<String>() {
//			@Override
//			public String call() throws Exception {
//				Thread.sleep(1000);
//				return "Hello World!";
//			}
//		});
		FutureTask<String> futureTask = new FutureTask<String>(() -> {
			Thread.sleep(1000);
			return "Hello World!";
		});
		new Thread(futureTask).start();
		String temp = futureTask.get(2, TimeUnit.SECONDS);
		System.out.println(temp);

	}
}
