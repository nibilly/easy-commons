package demo;

import demo.dynamic_proxy.RealSubject;
import demo.dynamic_proxy.Subject;

/**
 * 静态代理
 */
public class ProxySubjectDemo implements Subject {
	private RealSubject realSubject;

	public ProxySubjectDemo(RealSubject realSubject) {
		this.realSubject = realSubject;
	}

	@Override
	public void doSomething() {
		before();
		realSubject.doSomething();
		after();
	}

	private void before() {
		System.out.println("Before do something");
	}

	private void after() {
		System.out.println("After do something");
	}

	public static void main(String[] args) {
		RealSubject realSubject = new RealSubject();
		ProxySubjectDemo proxySubject = new ProxySubjectDemo(realSubject);
		proxySubject.doSomething();
	}
}
