package demo;

import org.junit.Test;

/**
 * 分为对象锁和class锁（static方法）
 */
public class SynchronizedDemo {

	@Test
	public void test1() {
		SynchronizedDemo synchronizedDemo = new SynchronizedDemo();

		new Thread(() -> {
			synchronizedDemo.sayHello("A  针对实例 线程一");

		}).start();

		new Thread(() -> {
			synchronizedDemo.sayHello("A 针对实例 线程二");

		}).start();

		SynchronizedDemo synchronizedDemo1 = new SynchronizedDemo();
		new Thread(() -> {
			synchronizedDemo1.sayHello("B 针对实例 线程一");

		}).start();

		SynchronizedDemo synchronizedDemo2 = new SynchronizedDemo();
		new Thread(() -> {
			synchronizedDemo2.sayHello("B 针对实例 线程二");

		}).start();

		new Thread(() -> {
			SynchronizedDemo.sayHa("C 针对class  线程1");

		}).start();

		new Thread(() -> {
			SynchronizedDemo.sayHa("C 针对class  线程2");

		}).start();

		try {
			Thread.sleep(60000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void test2() {

	}

	public synchronized void sayHello(String word) {
		System.out.println("say start :　" + word);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("say end :　" + word);
	}

	public synchronized static void sayHa(String word) {
		System.out.println("sayHa  start:　" + word);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("sayHa  end:　" + word);
	}
}
