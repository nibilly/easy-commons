package demo.aop.bean;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
//Spring AOP支持五种类型的通知：before、after、around、after-returning和after-throwing。
public class TransactionManageHandlerDemo {

	@Pointcut("execution(* demo.aop.*.*(..))") // 泛指，可都直接写出类方法也行 execution(public void demo.apo.a.insert())
	public void pointcut() {

	}

	@Before("pointcut()")
	public void before(JoinPoint joinPoint) {
		System.out.println("Before method: " + joinPoint.getSignature().getName());
	}

	@Around("pointcut()")
	public void around(ProceedingJoinPoint joinPoint) {
		System.out.println(" Around method: " + joinPoint.getSignature().getName());
	}

	@AfterReturning(value = "pointcut()", returning = "result")
	public void afterReturning(JoinPoint joinPoint, Object result) {
		System.out.println(
				" AfterReturning method: " + joinPoint.getSignature().getName() + ",result:" + result.toString());
	}

	@AfterThrowing(value = "pointcut()", throwing = "ex")
	public void afterThrowing(JoinPoint joinPoint, Exception ex) {
		System.out.println("AfterThrowing method: " + joinPoint.getSignature().getName() + ", Exception is:"
				+ ex.getLocalizedMessage());
	}

	@After("pointcut()")
	public void after(JoinPoint joinPoint) {
		System.out.println("After method: " + joinPoint.getSignature().getName());
	}

}
