package demo.aop.anotion;

import java.lang.reflect.Method;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 只需要在，需要的bean方法加上 @Log 注解即可 例@Log(name="根据用户id获取用户") // Log注解
 */
@Component
@Aspect
public class AnotionAspectDemo {

	// 当某个方法上,如果加了demo.aop.anotion.Log这个注解,就会进入下方around这个环绕通知方法
	@Pointcut("@annotation(demo.aop.anotion.Log)")
	private void pointcut() {

	}

	// 环绕通知方法around(),匹配"pointcut()"里的内容
	@Around("pointcut()")
	public Object around(ProceedingJoinPoint joinPoint) throws Throwable {

		// 获取用户名
		// 需要通过解析seesion或token获取

		// 获取被增强类和方法的信息
		Signature signature = joinPoint.getSignature();
		MethodSignature methodSignature = (MethodSignature) signature;

		// 获取被增强的方法对象
		Method method = methodSignature.getMethod();

		// 从方法中解析注解
		if (method != null) {
			Log logAnnotation = method.getAnnotation(Log.class);
			System.out.println(logAnnotation.name());
		}
		// 方法名字
		String name = method.getName();
		System.out.println(name);

		// 通过工具类获取Request对象
		RequestAttributes reqa = RequestContextHolder.getRequestAttributes();
		ServletRequestAttributes sra = (ServletRequestAttributes) reqa;
		HttpServletRequest request = sra.getRequest();

		// 访问的url
		String url = request.getRequestURI().toString();
		System.out.println(url);

		// 请求方式
		String methodName = request.getMethod();
		System.out.println(methodName);

		// 登录IP
		String ipAddr = getIpAddr(request);
		System.out.println(ipAddr);

		// 操作时间
		System.out.println(new Date());

		// 保存到数据库（操作日志）
		// ....
		try {
			long startTime = System.currentTimeMillis();
			// 执行目标方法
			Object proceed = joinPoint.proceed();
			// 计算并打印执行时间
			long executionTime = System.currentTimeMillis() - startTime;
			System.out.println("Execution time of " + joinPoint.getSignature() + " is " + executionTime + " ms");

			// 这边执行目标方法
			return proceed;
		} catch (IllegalArgumentException e) {
			// 处理异常
			throw new IllegalArgumentException(e);
		} catch (Throwable e) {
			// 处理其他类型的异常
			throw e;
		}
	}

	/**
	 * 获取ip地址
	 * 
	 * @param request
	 * @return
	 */
	public String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		return "0:0:0:0:0:0:0:1".equals(ip) ? "127.0.0.1" : ip;
	}

}
