package demo;

import org.junit.Test;

/**
 * threadLocal demo
 */
public class ThreadLocalDemo {

	@Test
	public void task1() {
		ThreadLocal<String> userIdThreadLocal = new ThreadLocal<String>();
		new Thread(() -> {
			userIdThreadLocal.set("billy");
			System.out.println("billy set over!");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("billy get result:" + userIdThreadLocal.get());
			// 要及时移除，不然会有内存泄露风险
			userIdThreadLocal.remove();
		}).start();
		new Thread(() -> {
			userIdThreadLocal.set("jhon");
			System.out.println("jhon set over!");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("jhon get result:" + userIdThreadLocal.get());
			// 要及时移除，不然会有内存泄露风险
			userIdThreadLocal.remove();
		}).start();

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
