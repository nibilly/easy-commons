import java.io.File;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cn.com.easy.utils.FastJSONUtils;
import cn.com.easy.utils.HttpClientUtils;

/**
 * 网页爬取整个
 * 
 * @author nibili 2019年9月17日下午2:35:45
 * 
 */
public class CatchPageHtmlAllUtils {

	public static void main(String[] args) {
		try {
			HtmlDto htmlDto = CatchPageHtmlAllUtils.catchByUrl("https://mp.weixin.qq.com/s/jh0X2hhNd7AFKFy3y5rg5w", "C:/Users/Administrator.PC201905132149I/Desktop/html/");
			System.out.println(FastJSONUtils.toJsonString(htmlDto));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static HtmlDto catchByUrl(String url, String folderPath) throws Exception {
		String htmlContent = HttpClientUtils.get(url);

		htmlContent = StringUtils.replacePattern(htmlContent, "//res.wx.qq.com/", "https://res.wx.qq.com/");
		Document document = Jsoup.parse(htmlContent);

		// 去掉微信二维码
		try {
			document.select("#js_pc_qr_code").remove();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		// 去掉脚本
		document.select("script").remove();
		// 去掉底部打赏
		document.select("#js_preview_reward_author").remove();
		document.select("#js_preview_reward_qrcode").remove();
		document.select("#js_toobar3").remove();
		document.select(".weui-dialog_haokan").remove();
		document.select("div[style='display: none;']").remove();
		document.select("div[style='display:none;']").remove();
		document.select("div[style='display: none']").remove();
		document.select("div[style='display:none']").remove();
		// 图片懒加载，下载处理
		String folderPath_ = folderPath + "/img/";
		Elements imgEmements = document.select("img");
		for (Element element : imgEmements) {
			String dataSrc = element.attr("data-src");
			if (StringUtils.isNotBlank(dataSrc) == true) {
				String imgFileName = RandomStringUtils.random(6, true, true) + ".png";
				HttpClientUtils.getFile(dataSrc, folderPath_ + imgFileName);
				element.attr("src", "img/" + imgFileName);
			}
		}
		//
		htmlContent = document.html();
		// title deccription
		Elements titleElements = document.select("meta[property='og:title']");
		Elements descriptionElements = document.select("meta[property='og:description']");
		HtmlDto htmlDto = new HtmlDto();
		if (CollectionUtils.isNotEmpty(titleElements) == true) {
			htmlDto.setTitle(titleElements.get(0).attr("content"));
		} else {
			htmlDto.setTitle(document.select("title").html());
		}
		if (CollectionUtils.isNotEmpty(descriptionElements) == true) {
			htmlDto.setDescription(descriptionElements.get(0).attr("content"));
		} else {
			htmlDto.setDescription(document.select("description").html());
		}
		htmlDto.setHtml(htmlContent);
		// 写到文件
		FileUtils.write(new File(folderPath + "/test.html"), htmlContent, "UTF-8", false);
		return htmlDto;
	}

	public static class HtmlDto {
		private String title;
		private String description;
		private String html;

		/**
		 * 获取 html
		 * 
		 * @return
		 * @auth nibili 2019年9月18日 下午2:15:43
		 */
		public String getHtml() {
			return html;
		}

		/**
		 * 设置 html
		 * 
		 * @param html
		 * @auth nibili 2019年9月18日 下午2:15:43
		 */
		public void setHtml(String html) {
			this.html = html;
		}

		/**
		 * 获取 title
		 * 
		 * @return
		 * @auth nibili 2019年9月18日 下午2:13:21
		 */
		public String getTitle() {
			return title;
		}

		/**
		 * 设置 title
		 * 
		 * @param title
		 * @auth nibili 2019年9月18日 下午2:13:21
		 */
		public void setTitle(String title) {
			this.title = title;
		}

		/**
		 * 获取 description
		 * 
		 * @return
		 * @auth nibili 2019年9月18日 下午2:13:21
		 */
		public String getDescription() {
			return description;
		}

		/**
		 * 设置 description
		 * 
		 * @param description
		 * @auth nibili 2019年9月18日 下午2:13:21
		 */
		public void setDescription(String description) {
			this.description = description;
		}

	}
}
