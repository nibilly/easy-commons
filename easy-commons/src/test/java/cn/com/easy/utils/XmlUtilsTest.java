package cn.com.easy.utils;

import cn.com.easy.pay.weixinpay.param.WeixinPayNotifyParam;

public class XmlUtilsTest {

	public static void main(String[] args) throws Exception {
		String str = "<xml><appid><![CDATA[wx75bf7b090e900622]]></appid><bank_type><![CDATA[CFT]]></bank_type><cash_fee><![CDATA[1]]></cash_fee><fee_type><![CDATA[CNY]]></fee_type><is_subscribe><![CDATA[N]]></is_subscribe><mch_id><![CDATA[1475848302]]></mch_id><nonce_str><![CDATA[1Vay2iunxr2SIjKH]]></nonce_str><openid><![CDATA[oM3qOwBYU1H43zfb5ZcqjD6BYL3I]]></openid><out_trade_no><![CDATA[255741493946380867]]></out_trade_no><result_code><![CDATA[SUCCESS]]></result_code><return_code><![CDATA[SUCCESS]]></return_code><sign><![CDATA[9240576BA4433DFDADA3180149F908A1]]></sign><time_end><![CDATA[20170524160100]]></time_end><total_fee>1</total_fee><trade_type><![CDATA[APP]]></trade_type><transaction_id><![CDATA[4001362001201705242364780295]]></transaction_id></xml>";
		// 解析xml 成 java对象
		WeixinPayNotifyParam weixinPayNotifyParam = XmlUtils.xmlStrToObject(WeixinPayNotifyParam.class, str);
		System.out.println(FastJSONUtils.toJsonString(weixinPayNotifyParam));
	}
}
