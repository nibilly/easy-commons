package cn.com.easy.utils;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
 


import com.github.rholder.retry.RetryException;
import com.github.rholder.retry.Retryer;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.StopStrategies;
import com.github.rholder.retry.WaitStrategies;
import com.google.common.base.Predicates;

public class GuavaRetryDemoTest {

	/**
	 * @desc 更新可代理报销人接口
	 * @author jianzhang11
	 * @date 2017/3/31 15:17
	 */
	public static Callable<Boolean> updateReimAgentsCall = new Callable<Boolean>() {
		@Override
		public Boolean call() throws Exception {
			System.out.println("hello");
			return false;
		}
	};

	public static void main(String[] args) {
		Retryer<Boolean> retryer = RetryerBuilder.<Boolean> newBuilder()
		// 抛出runtime异常、checked异常时都会重试，但是抛出error不会重试。
				.retryIfException()
				// 返回false也需要重试
				.retryIfResult(Predicates.equalTo(false))
				// 重调策略
				.withWaitStrategy(WaitStrategies.fixedWait(2, TimeUnit.SECONDS))
				// 尝试次数
				.withStopStrategy(StopStrategies.stopAfterAttempt(3)).build();

		try {
			retryer.call(updateReimAgentsCall);
		} catch (ExecutionException e) {
			// e.printStackTrace();
		} catch (RetryException e) {
			System.out.println("更新可代理报销人异常,需要发送提醒邮件");
		}
	}

}
