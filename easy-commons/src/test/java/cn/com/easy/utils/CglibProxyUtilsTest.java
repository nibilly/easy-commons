package cn.com.easy.utils;

import java.lang.reflect.Method;
import java.util.Map;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import org.junit.Test;

import com.google.common.collect.Maps;

public class CglibProxyUtilsTest {

	@Test
	public void start() {

		CglibProxyUtilsService cglibProxyUtilsService = CglibProxyUtils.getProxyInstance(CglibProxyUtilsService.class, new TargetInterceptor());
		cglibProxyUtilsService.say();

		//

		TargetInterceptor targetInterceptor1 = new TargetInterceptor() {
			@Override
			public Object intercept(Object obj, Method method, Object[] params, MethodProxy proxy) throws Throwable {
				System.out.println("TargetInterceptor1 调用前");
				Object result = proxy.invokeSuper(obj, params);
				System.out.println("TargetInterceptor1 调用后" + result);
				return result;
			}
		};

		TargetInterceptor targetInterceptor2 = new TargetInterceptor() {
			@Override
			public Object intercept(Object obj, Method method, Object[] params, MethodProxy proxy) throws Throwable {
				System.out.println("TargetInterceptor2 调用前");
				Object result = proxy.invokeSuper(obj, params);
				System.out.println("TargetInterceptor2 调用后" + result);
				return result;
			}
		};

		TargetInterceptor[] targetInterceptors = new TargetInterceptor[2];
		targetInterceptors[0] = targetInterceptor1;
		targetInterceptors[1] = targetInterceptor2;

		Map<String, Integer> methodName = Maps.newHashMap();
		methodName.put("say", 0);
		//methodName.put("saySomething", 1);

		CglibProxyUtilsService cglibProxyUtilsService1 = CglibProxyUtils.getProxyInstance(CglibProxyUtilsService.class, targetInterceptors, methodName);
		cglibProxyUtilsService1.say();
		cglibProxyUtilsService1.saySomething("hello");
	}

	public static class CglibProxyUtilsService {

		private String name = "";

		private int age = 1;

		public int say() {
			System.out.println("I say ......");
			return 1;
		}

		public int saySomething(String string) {
			System.out.println("I say " + string);
			return 2;
		}

		/**
		 * get name
		 * 
		 * @return
		 * @author nibili 2018年1月11日
		 */
		public String getName() {
			return name;
		}

		/**
		 * set name
		 * 
		 * @param name
		 * @author nibili 2018年1月11日
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * get age
		 * 
		 * @return
		 * @author nibili 2018年1月11日
		 */
		public int getAge() {
			return age;
		}

		/**
		 * set age
		 * 
		 * @param age
		 * @author nibili 2018年1月11日
		 */
		public void setAge(int age) {
			this.age = age;
		}

	}

	/**
	 * 目标对象拦截器，实现MethodInterceptor
	 * 
	 * @author zghw
	 * 
	 */
	public static class TargetInterceptor implements MethodInterceptor {

		/**
		 * 重写方法拦截在方法前和方法后加入业务 Object obj为目标对象 Method method为目标方法 Object[] params
		 * 为参数， MethodProxy proxy CGlib方法代理对象
		 */
		@Override
		public Object intercept(Object obj, Method method, Object[] params, MethodProxy proxy) throws Throwable {
			System.out.println("调用前");
			Object result = proxy.invokeSuper(obj, params);
			System.out.println(" 调用后" + result);
			return result;
		}

	}
}
