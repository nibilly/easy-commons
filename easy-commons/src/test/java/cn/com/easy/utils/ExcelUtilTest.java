package cn.com.easy.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.hssf.util.HSSFColor;
import org.junit.Test;

import cn.com.easy.utils.ExcelPoiUtil.CellDto;
import cn.com.easy.utils.ExcelPoiUtil.ExcelException;
import cn.com.easy.utils.ExcelPoiUtil.RowDto;

import com.google.common.collect.Lists;

public class ExcelUtilTest {

	// @Test
	public void excelToList() {
		try {
			InputStream inputStream = new FileInputStream(new File("D:/我的文档/Tencent Files/196888813/FileRecv/技术二部.xls"));
			List<List<String>> list = ExcelUtil.excelToList(inputStream, "技术二部");
			System.out.println(FastJSONUtils.toJsonString(list));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (cn.com.easy.utils.ExcelUtil.ExcelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void start() {

		// try {
		// WritableWorkbook wwb;
		//
		// wwb = Workbook.createWorkbook(new FileOutputStream(new
		// File("C:/Users/Administrator/Desktop/test.xls")));
		// WritableSheet sheet1 = wwb.createSheet("测试", 1);
		// /**
		// * 使用样式的单元格
		// */
		// WritableFont wf_title = new WritableFont(WritableFont.ARIAL, 20,
		// WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
		// jxl.format.Colour.RED); // 定义格式
		// // 字体
		// // 下划线
		// // 斜体
		// // 粗体
		// // 颜色
		// WritableCellFormat headerFormat = new WritableCellFormat(wf_title);
		// headerFormat.setAlignment(Alignment.CENTRE);
		// // 1.添加Label对象三个参数意思：【列，行，值】
		// sheet1.addCell(new Label(0, 0, "标题", headerFormat)); // 普通的带有定义格式的单元格
		// sheet1.addCell(new Label(0, 2, "表头1"));
		// sheet1.addCell(new Label(1, 2, "表头2"));
		// sheet1.addCell(new Label(2, 2, "表头3"));
		// sheet1.addCell(new Label(3, 2, "表头4"));
		// sheet1.addCell(new Label(4, 2, "表头5"));
		// sheet1.addCell(new Label(5, 2, "表头6"));
		//
		// sheet1.mergeCells(0, 0, 5, 1); // 合并单元格
		//
		// // 写入Exel工作表
		// wwb.write();
		// // 关闭Excel工作薄对象
		// wwb.close();
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// }

		List<UserDto> list = Lists.newArrayList();
		for (int i = 0; i < 10; i++) {
			UserDto userDto = new UserDto();
			userDto.setAge(1);
			userDto.setFav("hello");
			userDto.setName("倪必利");
			list.add(userDto);
		}
		List<CellDto> fieldMap = Lists.newArrayList();

		CellDto cellDtoTemp = new CellDto();
		cellDtoTemp.setEnName("name");
		cellDtoTemp.setCname("姓名");
		cellDtoTemp.setColour(HSSFColor.HSSFColorPredefined.RED.getIndex());
		fieldMap.add(cellDtoTemp);
		cellDtoTemp = new CellDto();
		cellDtoTemp.setEnName("age");
		cellDtoTemp.setCname("性别");
		fieldMap.add(cellDtoTemp);
		cellDtoTemp = new CellDto();
		cellDtoTemp.setEnName("fav");
		cellDtoTemp.setCname("喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢喜欢");
		fieldMap.add(cellDtoTemp);
		//
		cellDtoTemp = new CellDto();
		cellDtoTemp.setEnName("other");
		cellDtoTemp.setCname("其他");
		fieldMap.add(cellDtoTemp);

		try {
			RowDto additionalFieldHeaderRowDto = new RowDto();
			//
			additionalFieldHeaderRowDto.setRowHeight(500);
			List<CellDto> cells = Lists.newArrayList();
			//
			CellDto cellDto = new CellDto();
			cellDto.setCname("托寄物信息");
			cellDto.setColumns(3);
			cells.add(cellDto);
			//
			cellDto = new CellDto();
			cellDto.setCname("保价信息");
			cellDto.setColumns(2);
			cells.add(cellDto);
			//
			cellDto = new CellDto();
			cellDto.setCname("订单金额");
			cellDto.setColumns(1);
			cells.add(cellDto);
			// //
			cellDto = new CellDto();
			cellDto.setCname("服务类型");
			cellDto.setColumns(1);
			cells.add(cellDto);
			// //
			cellDto = new CellDto();
			cellDto.setCname("运单备注");
			cellDto.setColumns(1);
			cells.add(cellDto);
			// //
			cellDto = new CellDto();
			cellDto.setCname("配送业务类型");
			cellDto.setColumns(1);
			cells.add(cellDto);
			// //
			cellDto = new CellDto();
			cellDto.setCname("运单信息");
			cellDto.setColumns(1);
			cells.add(cellDto);

			//
			additionalFieldHeaderRowDto.setCells(cells);
			// listToExcel(List<T> list, List<CellDto> fieldMap, String
			// sheetName, int sheetSize, RowDto additionalFieldHeaderRowDto,
			// String templatePath,
			// int templateHeaderRowTotal, boolean isOutputFieldCnNameToExcel,
			// OutputStream out) throws ExcelException {
			ExcelPoiUtil.listToExcel(list, fieldMap, "成员表", 65535, additionalFieldHeaderRowDto, "", 2, false,
					new FileOutputStream(new File("C:/Users/Administrator/Desktop/test.xls")));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExcelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("--------------------end-----------------");
	}
}
