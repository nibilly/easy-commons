package cn.com.easy.utils;

import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.junit.Before;
import org.junit.Test;

/**
 * hdfs工具类测试
 * 
 * @author nibili 2017年7月16日
 * 
 */
public class HDFSUtilsTest {

	@Before
	public void before() {
		System.setProperty("HADOOP_USER_NAME", "hadoop");
		System.setProperty("hadoop.home.dir", "C:/git/easy-commons/easy-commons/lib/hadoop-common-2.2.0-bin");
	}

	// @Test
	public void createFile() {
		try {
			HDFSUtils.createFile("/user/hadoop/test.txt", "测试文档内容".getBytes("UTF-8"));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// @Test
	public void listFiles() {
		try {
			RemoteIterator<LocatedFileStatus> temp = HDFSUtils.listFiles("/user/hadoop");
			while (temp.hasNext() == true) {
				LocatedFileStatus locatedFileStatus = temp.next();
				Path path = locatedFileStatus.getPath();
				System.out.println("uri：" + path.toUri().getPath());
				System.out.println("toString : " + path.toString());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// @Test
	public void copyFromLocalFile() {
		try {
			HDFSUtils.copyFromLocalFile("C:/Users/Administrator/Desktop/纯音乐.mp3", "/user/hadoop/纯音乐.mp3");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Test
	public void download() {
		try {
			HDFSUtils.download("/user/hadoop/纯音乐.mp3", "C:/Users/Administrator/Desktop/新建文件夹 (4)/纯音乐.mp3");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
