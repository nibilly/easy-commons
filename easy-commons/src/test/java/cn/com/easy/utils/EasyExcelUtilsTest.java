package cn.com.easy.utils;

import java.io.IOException;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.support.ExcelTypeEnum;

import lombok.Data;

/**
 * EaseExcel 单元测试 <br/>
 * https://www.yuque.com/easyexcel/doc/easyexcel
 * 
 * @author nibili 2022-2-20 17:56:19
 *
 */
public class EasyExcelUtilsTest {

	@HeadRowHeight(value = 35) // 表头行高
	@ContentRowHeight(value = 25) // 内容行高
	@ColumnWidth(value = 50) // 列宽
	@Data
	public static class Student implements Serializable {
		/** */
		private static final long serialVersionUID = -6236871709467505449L;

		@ExcelProperty(value = { "学生信息", "学生编号" }, order = 10)
		private Integer id;

		@ExcelProperty(value = { "学生信息", "学生姓名" }, order = 2)
		private String name;

		@ExcelProperty(value = { "学生薪水" }, order = 1)
		@ColumnWidth(value = 10) // 列宽
		private Double salary;

		@ExcelProperty(value = { "学生生日" }, order = 11)
		@DateTimeFormat("yyyy-MM-dd")
		private Date birthday;

		@ExcelProperty(value = { "学生身高" }, order = 11)
		@ExcelIgnore
		private String high;
	}

	public List<Student> getData() {
		List<Student> lists = new ArrayList<>();
		for (int i = 0; i <= 10; i++) {
			Student student = new Student();
			student.setId(i + 1);
			student.setName("李四" + i);
			student.setBirthday(new Date());
			student.setSalary(1500.00D);
			student.setHigh("身高是：" + i);
			lists.add(student);
		}
		return lists;
	}

	@Test
	public void contextLoads() {
		try {
//			// 要输出的字段
//			Set<String> set = new HashSet<>();
//			set.add("id");
//			set.add("name");
//
//			EasyExcel.write("学生信息表.xlsx", Student.class).includeColumnFiledNames(set)
			EasyExcel.write("学生信息表.xlsx", Student.class)
					// 自适应宽度，但是这个不是特别精确
					// .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy())
					.sheet().doWrite(getData());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void readExcel() throws Exception {
		try {
			List<Student> list = new ArrayList<>();

			/*
			 * EasyExcel 读取 是基于SAX方式 因此在解析时需要传入监听器
			 */
			// 第一个参数 为 excel文件路径
			// 读取时的数据类型
			// 监听器
			EasyExcel.read("学生信息表" + ExcelTypeEnum.XLSX.getValue(), Student.class, new AnalysisEventListener<Student>() {

				// 每读取一行就调用该方法
				@Override
				public void invoke(Student data, AnalysisContext context) {
					list.add(data);
				}

				// 全部读取完成就调用该方法
				@Override
				public void doAfterAllAnalysed(AnalysisContext context) {
					System.out.println("读取完成");
				}
			}).sheet().doRead();

			list.forEach(System.out::println);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 文件下载（失败了会返回一个有部分数据的Excel）
	 * <p>
	 * 1. 创建excel对应的实体对象 参照{@link DownloadData}
	 * <p>
	 * 2. 设置返回的 参数
	 * <p>
	 * 3. 直接写，这里注意，finish的时候会自动关闭OutputStream,当然你外面再关闭流问题不大
	 */
	@GetMapping("download")
	public void download(HttpServletResponse response) throws IOException {
		// 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
		response.setContentType("application/vnd.ms-excel");
		response.setCharacterEncoding("utf-8");
		// 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
		String fileName = URLEncoder.encode("测试", "UTF-8");
		response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
		EasyExcel.write(response.getOutputStream(), Student.class).sheet(1).doWrite(getData());
	}

	/**
	 * 文件上传
	 * <p>
	 * 1. 创建excel对应的实体对象 参照{@link UploadData}
	 * <p>
	 * 2. 由于默认一行行的读取excel，所以需要创建excel一行一行的回调监听器，参照{@link UploadDataListener}
	 * <p>
	 * 3. 直接读即可
	 */
	@PostMapping("upload")
	@ResponseBody
	public String upload(MultipartFile file) throws IOException {
//		EasyExcel.read(file.getInputStream(), UploadData.class, new UploadDataListener(uploadDAO)).sheet().doRead();
		return "success";
	}
}
