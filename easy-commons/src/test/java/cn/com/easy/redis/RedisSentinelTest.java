package cn.com.easy.redis;

import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext-redis-sentinel-demo.xml")
public class RedisSentinelTest {

	@Resource(name = "redisTemplateWithFastjsonSearializer")
	private RedisTemplate<String, String> template;

	private String stringKey = "stringKey2";

	/**
	 * 
	 * 
	 * @auth nibili 2015年5月13日
	 */
	@Test
	public void sendString() {
		try {
			template.boundValueOps(stringKey).set("hello", 600, TimeUnit.SECONDS);
			System.out.println("发送成功");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * 
	 * @auth nibili 2015年5月13日
	 */
	@Test
	public void getString() {
		try {
			String temp = template.boundValueOps(stringKey).get();
			System.out.println("key:" + stringKey + "，value:" + temp);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
