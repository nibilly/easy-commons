package cn.com.easy;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SynchronizedTest {

	public static void main(String[] args) {

		// // float转hex
		// Float f = 220.2f;
		//
		// System.out.println(HexUtils.floatToHexString(f));
		//
		// // hex转float
		// String hexString = "4360AFB6";
		//
		// System.out.println(HexUtils.hexStringToFloat(hexString));
		// final SynchronizedTest myTest = new SynchronizedTest();
		/** 线程池对象 */
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		// executorService.execute(new Runnable() {
		//
		// @Override
		// public void run() {
		// try {
		// myTest.start("billy");
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// }
		// });
		// executorService.execute(new Runnable() {
		//
		// @Override
		// public void run() {
		// try {
		// myTest.start("billy1");
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// }
		// });
		// executorService.execute(new Runnable() {
		//
		// @Override
		// public void run() {
		// try {
		// myTest.start("billy1");
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// }
		// });

		// executorService.execute(new Runnable() {
		//
		// @Override
		// public void run() {
		// try {
		// myTest.start2("billy");
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// }
		// });
		// executorService.execute(new Runnable() {
		//
		// @Override
		// public void run() {
		// try {
		// myTest.start2("billy1");
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// }
		// });
		// executorService.execute(new Runnable() {
		//
		// @Override
		// public void run() {
		// try {
		// SynchronizedTest myTest = new SynchronizedTest();
		// myTest.start2("billy--2--");
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// }
		// });

		// executorService.execute(new Runnable() {
		//
		// @Override
		// public void run() {
		// try {
		// myTest.start3();
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// }
		// });
		// executorService.execute(new Runnable() {
		//
		// @Override
		// public void run() {
		// try {
		// myTest.start3();
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// }
		// });
		// executorService.execute(new Runnable() {
		//
		// @Override
		// public void run() {
		// try {
		// SynchronizedTest myTest = new SynchronizedTest();
		// myTest.start3();
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// }
		// });

		executorService.execute(new Runnable() {

			@Override
			public void run() {
				try {
					SynchronizedTest.startStatic("billy");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		executorService.execute(new Runnable() {

			@Override
			public void run() {
				try {
					SynchronizedTest.startStatic("billy1");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		executorService.execute(new Runnable() {

			@Override
			public void run() {
				try {
					SynchronizedTest.startStatic("billy--2--");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	// public void start(String name) throws Exception {
	// 这个是块锁，会按照name进去锁定
	// synchronized (name) {
	// System.out.println(name + "start!");
	// Thread.sleep(5000);
	// System.out.println(name + "end!");
	// }
	// }
	//
	// /**
	// * 这个是对象锁，不会按照name进行锁定
	// *
	// * @param name
	// * @throws Exception
	// * @author nibili 2017年11月27日
	// */
	// public synchronized void start2(String name) throws Exception {
	// System.out.println(name + "start-2!");
	// Thread.sleep(5000);
	// System.out.println(name + "end-2!");
	// }

	// public synchronized void start3() throws Exception {
	// System.out.println("start!");
	// Thread.sleep(5000);
	// System.out.println("end!");
	// }

	public static synchronized void startStatic(String name) throws Exception {
		System.out.println(name + "start!");
		Thread.sleep(5000);
		System.out.println(name + "end!");
	}

}
