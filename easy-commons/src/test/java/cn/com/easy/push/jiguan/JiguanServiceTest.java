package cn.com.easy.push.jiguan;

import java.util.Map;

import cn.com.easy.exception.BusinessException;
import cn.com.easy.push.jiguan.config.JiguanConfig;
import cn.com.easy.push.jiguan.dto.JiguanMessageDto;
import cn.com.easy.push.jiguan.enums.PlatformEnum;

import com.google.common.collect.Maps;

public class JiguanServiceTest extends JiguanBaseService {

	@Override
	public JiguanConfig getJiguanConfig() {
		JiguanConfig jiguanConfig = new JiguanConfig();
		jiguanConfig.setAppKey("aac6c4dbfd3f9835347098a9");
		jiguanConfig.setSecret("8f2c36e2fd4ca881be92a0ef");
		return jiguanConfig;
	}

	public static void main(String[] args) throws BusinessException {
		JiguanServiceTest jiguanServiceTest = new JiguanServiceTest();
		JiguanMessageDto jiguanMessageDto = new JiguanMessageDto();
		jiguanMessageDto.setTitle("测试标题 ");
		jiguanMessageDto.setAlert("你好，这是测试");

		// List<String> aliasList = Lists.newArrayList();
		// aliasList.add("baby1");
		// jiguanMessageDto.setAlias(aliasList);
		//
		// List<String> tagsList = Lists.newArrayList();
		// tagsList.add("tag1");
		// jiguanMessageDto.setTags(tagsList);

		jiguanMessageDto.setIsNotification(true);
		jiguanMessageDto.setPlatform(PlatformEnum.ANDROID);

		Map<String, String> extraParamMap = Maps.newHashMap();
		extraParamMap.put("type", "company");
		extraParamMap.put("data", "{companyId:1231}");
		jiguanMessageDto.setExtraParamMap(extraParamMap);

		jiguanServiceTest.sendMsg(jiguanMessageDto);
		System.out.println("----------结束---------");
	}
}
