package cn.com.easy.data;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import cn.com.easy.utils.DateUtils;
import cn.com.easy.utils.ExcelPoiUtil;
import cn.com.easy.utils.ExcelPoiUtil.CellDto;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.google.common.collect.Lists;

public class GetBaoxianDateServiceTest {

	public String url = "http://search.jqw.com/business.aspx?keyword=%B1%A3%CF%D5&page=";

	@Test
	public void start() throws Exception {
		try {
			// 数据集合
			List<BaoxianDataDto> baoxianDataDtoList = getAllPageData();
			// 声明字段
			List<CellDto> fieldMap = Lists.newArrayList();
			CellDto cellDto = new CellDto();
			cellDto.setCname("姓名");
			cellDto.setEnName("name");
			fieldMap.add(cellDto);
			//
			cellDto = new CellDto();
			cellDto.setCname("电话号码");
			cellDto.setEnName("tel");
			fieldMap.add(cellDto);
			//
			cellDto = new CellDto();
			cellDto.setCname("公司");
			cellDto.setEnName("company");
			fieldMap.add(cellDto);
			//
			cellDto = new CellDto();
			cellDto.setCname("地址");
			cellDto.setEnName("address");
			fieldMap.add(cellDto);
			//
			String outputFilePath = "C:/Users/Administrator/Desktop/保险同城" + DateUtils.formatDate(new Date(), "yyyy_MM_dd") + ".xls";
			File file = new File(outputFilePath);
			if (file.exists() == false) {
				FileUtils.write(file, "", "UTF-8");
			}
			// ExcelUtil.listToExcel(list, fieldMap, sheetName,
			// additionalFieldHeaderRowDto, out);
			ExcelPoiUtil.listToExcel(baoxianDataDtoList, fieldMap, "保障经理", 65535, null, "", 0, true, new FileOutputStream(file));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * @return
	 * @throws InterruptedException
	 * @throws ExecutionException
	 * @author nibili 2017年8月29日
	 */
	private List<BaoxianDataDto> getAllPageData() throws InterruptedException, ExecutionException {
		//
		List<BaoxianDataDto> list = Lists.newArrayList();
		// 线程池对象
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		//
		List<Future<List<BaoxianDataDto>>> futureList = Lists.newArrayList();
		//
		for (int i = 1; i <= 110; i++) {
			final int k = i;
			Future<List<BaoxianDataDto>> future = executorService.submit(new Callable<List<BaoxianDataDto>>() {
				@Override
				public List<BaoxianDataDto> call() throws Exception {
					return getPageData(url + k);
				}
			});
			futureList.add(future);
		}
		//
		for (Future<List<BaoxianDataDto>> future : futureList) {
			List<BaoxianDataDto> temp = future.get();
			if (CollectionUtils.isNotEmpty(temp) == true) {
				list.addAll(temp);
			}
		}
		return list;
	}

	/**
	 * 爬取页面
	 * 
	 * @param url
	 * @author nibili 2017年8月29日
	 * @throws Exception
	 */
	private List<BaoxianDataDto> getPageData(String url) throws Exception {
		List<BaoxianDataDto> baoxianDataDtoList = Lists.newArrayList();
		Document document = getDocument(url);
		Elements liElements = document.select(".business ul li.tuiguang");
		for (Element liElement : liElements) {
			String title = liElement.select(".bw_title_name.tui_title_name").get(0).attr("title");
			String[] titleArray = StringUtils.split(title, "-");
			if (titleArray.length == 2) {
				// 必须是名字和公司组合
				String name = titleArray[0];
				String company = titleArray[1];
				//
				String tel = liElement.select(".lianxi_mobile").get(0).attr("title");
				String address = liElement.select(".lianxi_address").get(0).attr("title");
				if (address.indexOf("-") > -1) {
					address = StringUtils.split(address, "-")[1];
				}
				System.out.println(name + "-" + tel + "-" + company + "-" + address);
				// 写入excel
				BaoxianDataDto baoxianDataDto = new BaoxianDataDto();
				baoxianDataDto.setAddress(address);
				baoxianDataDto.setCompany(company);
				baoxianDataDto.setName(name);
				baoxianDataDto.setTel(tel);
				baoxianDataDtoList.add(baoxianDataDto);
			}
		}
		return baoxianDataDtoList;
		// // 下一页
		// Elements nextBtnElements = document.select("#Next a");
		// if (nextBtnElements.size() > 0) {
		// String nextUrl = nextBtnElements.get(0).attr("href");
		// nextUrl = nextUrl.replaceAll("保险", "%B1%A3%CF%D5");
		// getPageAndSave(baoxianDataDtoList, "http://search.jqw.com" +
		// nextUrl);
		// }
	}

	/**
	 * 爬取
	 * 
	 * @param url
	 * @return
	 * @author nibili 2016年8月22日
	 */
	private Document getDocument(String url) {
		for (int i = 0; i < 10; i++) {
			try {
				Document document = Jsoup.connect(url).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.102 Safari/537.36").get();
				// Thread.sleep(1000);
				return document;
			} catch (Exception ex) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("超时!!!");
			}
		}
		System.out.println("没有爬取数据：" + url);
		return null;
	}

}
