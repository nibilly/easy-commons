package cn.com.easy.data;

public class BaoxianDataDto {

	String name;
	String company;
	String tel;
	String address;

	/**
	 * get name
	 * 
	 * @return
	 * @author nibili 2017年8月29日
	 */
	public String getName() {
		return name;
	}

	/**
	 * set name
	 * 
	 * @param name
	 * @author nibili 2017年8月29日
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * get company
	 * 
	 * @return
	 * @author nibili 2017年8月29日
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * set company
	 * 
	 * @param company
	 * @author nibili 2017年8月29日
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * get tel
	 * 
	 * @return
	 * @author nibili 2017年8月29日
	 */
	public String getTel() {
		return tel;
	}

	/**
	 * set tel
	 * 
	 * @param tel
	 * @author nibili 2017年8月29日
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}

	/**
	 * get address
	 * 
	 * @return
	 * @author nibili 2017年8月29日
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * set address
	 * 
	 * @param address
	 * @author nibili 2017年8月29日
	 */
	public void setAddress(String address) {
		this.address = address;
	}

}
